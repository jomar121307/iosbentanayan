//
//  HomeViewController.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HomeCollectionViewFlowLayout.h"

@interface HomeViewController : UIViewController <ASCollectionViewDataSource, ASCollectionViewDelegateFlowLayout, SlideNavigationControllerDelegate, ASPagerNodeDataSource, ASCollectionDelegate, UICollectionViewDelegate>

@property (strong, nonatomic) ASPagerNode *categoriesPagerNode;
@property (strong, nonatomic) ASPagerNode *subcategoriesPagerNode;

@property (strong, nonatomic) ASCollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray *categoriesResult;
@property (strong, nonatomic) NSMutableArray *subcategoriesResult;
@property (strong, nonatomic) NSMutableArray *grouplingResult;

@end
