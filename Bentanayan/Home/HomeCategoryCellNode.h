//
//  HomeCategoryCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 17/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface HomeCategoryCellNode : ASCellNode <ASNetworkImageNodeDelegate> {
    ASTextNode *_categoryLabelNode;
    ASNetworkImageNode *_categoryImageNode;
    ASDisplayNode *_categoryImageEffectNode;
}

@property (strong, nonatomic) CategoryModel *category;

- (instancetype)initWithCategory:(CategoryModel *)category;

@end
