//
//  HomeViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "HomeViewController.h"

#import "HomeCategoryCellNode.h"
#import "HomeSubcategoryCellNode.h"
#import "HomeGrouplingCellNode.h"

#import "ProductListViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    /*
     for (NSString* family in [UIFont familyNames])
     {
     NSLog(@"%@", family);
     
     for (NSString* name in [UIFont fontNamesForFamilyName: family])
     {
     NSLog(@"  %@", name);
     }
     }
     */
    
    self.view.backgroundColor = [UIColor flatWhiteColor];
    
    // Navigation
    self.navigationItem.title = @"Home";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Categories Pager Node
    self.categoriesPagerNode = [[ASPagerNode alloc] init];
    self.categoriesPagerNode.dataSource = self;
    self.categoriesPagerNode.view.asyncDelegate = self;
    self.categoriesPagerNode.backgroundColor = [UIColor flatBlackColor];
    self.categoriesPagerNode.view.alwaysBounceHorizontal = YES;
    self.categoriesPagerNode.view.translatesAutoresizingMaskIntoConstraints = NO;
    self.categoriesPagerNode.view.allowsSelection = YES;
    
    [self.view addSubnode:self.categoriesPagerNode];
    
    // Sub Categories Pager Node
    self.subcategoriesPagerNode = [[ASPagerNode alloc] init];
    self.subcategoriesPagerNode.dataSource = self;
    self.subcategoriesPagerNode.view.asyncDelegate = self;
    self.subcategoriesPagerNode.backgroundColor = [UIColor flatBlackColor];
    self.subcategoriesPagerNode.view.alwaysBounceHorizontal = YES;
    self.subcategoriesPagerNode.view.translatesAutoresizingMaskIntoConstraints = NO;
    self.subcategoriesPagerNode.view.allowsSelection = YES;
    
    [self.view addSubnode:self.subcategoriesPagerNode];
    
    // CollectionView
    HomeCollectionViewFlowLayout *layout = [[HomeCollectionViewFlowLayout alloc] init];
    
    self.collectionView = [[ASCollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:layout];
    self.collectionView.asyncDataSource = self;
    self.collectionView.asyncDelegate = self;
    self.collectionView.backgroundColor = [UIColor flatBlackColor];
    self.collectionView.alwaysBounceHorizontal = YES;
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:self.collectionView];
    
    self.categoriesResult = [NSMutableArray new];
    self.subcategoriesResult = [NSMutableArray new];
    self.grouplingResult = [NSMutableArray new];
    
    __weak HomeViewController *weakSelf = self;
    
    [[BESharedManager sharedInstance] be_categoriesWithCompletion:^(BOOL finished, NSArray *result) {
        if(finished) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.categoriesPagerNode.view performBatchUpdates:^{
                    NSMutableArray *insertIndexPaths = [NSMutableArray array];
                    NSMutableArray *deleteIndexPaths = [NSMutableArray array];
                    
                    NSInteger section = 0;
                    
                    NSArray *oldData = [weakSelf.categoriesResult copy];
                    for(CategoryModel *oldModel in oldData) {
                        NSInteger oldItem = [oldData indexOfObject:oldModel];
                        [weakSelf.categoriesResult removeObject:oldModel];
                        [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
                    }
                    
                    for(CategoryModel *newModel in result) {
                        [weakSelf.categoriesResult addObject:newModel];
                        NSInteger item = [weakSelf.categoriesResult indexOfObject:newModel];
                        [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
                    }
                    
                    [weakSelf.categoriesPagerNode.view deleteItemsAtIndexPaths:deleteIndexPaths];
                    [weakSelf.categoriesPagerNode.view insertItemsAtIndexPaths:insertIndexPaths];
                } completion:^(BOOL finished) {
                    CategoryModel *category = [self.categoriesResult objectAtIndex:0];
                    if(weakSelf.categoriesResult.count > 0) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [weakSelf.subcategoriesPagerNode.view performBatchUpdates:^{
                                NSMutableArray *insertIndexPaths = [NSMutableArray array];
                                NSMutableArray *deleteIndexPaths = [NSMutableArray array];
                                
                                NSInteger section = 0;
                                
                                NSArray *oldData = [weakSelf.subcategoriesResult copy];
                                for(SubcategoryModel *oldModel in oldData) {
                                    NSInteger oldItem = [oldData indexOfObject:oldModel];
                                    [weakSelf.subcategoriesResult removeObject:oldModel];
                                    [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
                                }
                                
                                for(SubcategoryModel *newModel in category.subcategories) {
                                    [weakSelf.subcategoriesResult addObject:newModel];
                                    NSInteger item = [weakSelf.subcategoriesResult indexOfObject:newModel];
                                    [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
                                }
                                
                                [weakSelf.subcategoriesPagerNode.view deleteItemsAtIndexPaths:deleteIndexPaths];
                                [weakSelf.subcategoriesPagerNode.view insertItemsAtIndexPaths:insertIndexPaths];
                            } completion:^(BOOL finished) {
                                if(weakSelf.subcategoriesResult.count > 0) {
                                    [weakSelf populateGrouplingsWithSubcategory:[weakSelf.subcategoriesResult objectAtIndex:0]];
                                }
                            }
                             ];
                        });
                    }
                }
                 ];
            });
        }
    }];
    
    NSDictionary *views = @{ @"categoriesPagerNode" : self.categoriesPagerNode.view,
                             @"subcategoriesPagerNode" : self.subcategoriesPagerNode.view,
                             @"collectionView" : self.collectionView };
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[categoriesPagerNode]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[subcategoriesPagerNode]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|" options:0 metrics:nil views:views]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.categoriesPagerNode.view
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:0.42
                                                           constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.subcategoriesPagerNode.view
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:0.29
                                                           constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.collectionView
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:0.29
                                                           constant:0]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[categoriesPagerNode][subcategoriesPagerNode][collectionView]|" options:0 metrics:nil views:views]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_REFRESH_MENU object:SLIDE_MENU_RELOAD_DATA];
}

#pragma mark - ASPagerNode data source.

- (NSInteger)numberOfPagesInPagerNode:(ASPagerNode *)pagerNode
{
    if([pagerNode isEqual:self.categoriesPagerNode]) {
        return self.categoriesResult.count;
    } else if([pagerNode isEqual:self.subcategoriesPagerNode]) {
        return self.subcategoriesResult.count;
    } else {
        return 0;
    }
}

- (ASCellNode *)pagerNode:(ASPagerNode *)pagerNode nodeAtIndex:(NSInteger)index
{
    if([pagerNode isEqual:self.categoriesPagerNode]) {
        HomeCategoryCellNode *categoryCellNode;
        CategoryModel *category = (CategoryModel *)[self.categoriesResult objectAtIndex:index];
        categoryCellNode = [[HomeCategoryCellNode alloc] initWithCategory:category];
        return categoryCellNode;
    } else if([pagerNode isEqual:self.subcategoriesPagerNode]) {
        SubcategoryModel *subcategory = (SubcategoryModel *)[self.subcategoriesResult objectAtIndex:index];
        HomeSubcategoryCellNode *cellNode = [[HomeSubcategoryCellNode alloc] initWithSubcategory:subcategory];
        
        return cellNode;
    } else {
        return [[ASCellNode alloc] init];
    }
}

- (ASSizeRange)pagerNode:(ASPagerNode *)pagerNode constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize itemSize = pagerNode.bounds.size;
    return ASSizeRangeMake(itemSize, itemSize);
}

#pragma mark - ASCollectionView data source.

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HomeGrouplingCellNode *cellNode;
    GrouplingModel *groupling = (GrouplingModel *)[self.grouplingResult objectAtIndex:indexPath.row];
    cellNode = [[HomeGrouplingCellNode alloc] initWithGroupling:groupling];
    return cellNode;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = ceilf(collectionView.bounds.size.height - 1);
    
    HomeCollectionViewFlowLayout *layout = (HomeCollectionViewFlowLayout *)collectionView.collectionViewLayout;
    layout.numberOfColumns = 2;
    CGSize itemSize = [layout itemSize];
    itemSize.height = height;
    
    return ASSizeRangeMake(itemSize, itemSize);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.grouplingResult.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionViewLockDataSource:(ASCollectionView *)collectionView
{
    // lock the data source
    // The data source should not be change until it is unlocked.
}

- (void)collectionViewUnlockDataSource:(ASCollectionView *)collectionView
{
    // unlock the data source to enable data source updating.
}

- (void)collectionView:(UICollectionView *)collectionView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    NSLog(@"fetch additional content");
    [context completeBatchFetching:YES];
}

#pragma mark - ASCollectionView delegate.

- (void)collectionView:(ASCollectionView *)collectionView didEndDisplayingNode:(ASCellNode *)node forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.categoriesPagerNode.view isEqual:collectionView]) {
        ASCellNode *visibleCellNode = [collectionView visibleNodes][0];
        NSIndexPath *visibleIndexPath = [collectionView indexPathForNode:visibleCellNode];
        [self populateSubcategories:visibleIndexPath];
    } else if([self.subcategoriesPagerNode.view isEqual:collectionView]) {
        NSArray *visibleNodes = [collectionView visibleNodes];
        if(visibleNodes.count > 0) {
            ASCellNode *visibleCellNode = visibleNodes[0];
            NSIndexPath *visibleIndexPath = [collectionView indexPathForNode:visibleCellNode];
            [self populateGrouplings:visibleIndexPath];
        } else {
            NSArray *categoryVisibleNodes = [self.categoriesPagerNode.view visibleNodes];
            if(categoryVisibleNodes.count > 0) {
                ASCellNode *visibleCategoryCellNode = categoryVisibleNodes[0];
                NSIndexPath *visibleCategoryIndexPath = [self.categoriesPagerNode.view indexPathForNode:visibleCategoryCellNode];
                CategoryModel *category = [self.categoriesResult objectAtIndex:visibleCategoryIndexPath.row];
                [self populateGrouplingsWithSubcategory:[category.subcategories lastObject]];
            }
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *visibleCategoryNodes = [self.categoriesPagerNode.view visibleNodes];
    NSArray *visibleSubCategoryNodes = [self.subcategoriesPagerNode.view visibleNodes];
    
    CategoryModel *catM = nil;
    SubcategoryModel *subcatM = nil;
    GrouplingModel *groupM = nil;
    
    ProductListViewController *productListController;
    
    if(visibleCategoryNodes.count > 0) {
        HomeCategoryCellNode *categoryCellNode = [visibleCategoryNodes firstObject];
        catM = categoryCellNode.category;
    }
    
    if(visibleSubCategoryNodes.count > 0) {
        HomeSubcategoryCellNode *subcategoryCellNode = [visibleSubCategoryNodes firstObject];
        subcatM = subcategoryCellNode.subcategory;
    }
    
    if([self.categoriesPagerNode.view isEqual:collectionView]) {
        productListController = [[ProductListViewController alloc] initWithCategory:catM
                                                                        subCategory:nil
                                                                          groupling:nil];
        
        [[SlideNavigationController sharedInstance] pushViewController:productListController
                                                              animated:YES];
    } else if([self.subcategoriesPagerNode.view isEqual:collectionView]) {
        productListController = [[ProductListViewController alloc] initWithCategory:catM
                                                                        subCategory:subcatM
                                                                          groupling:nil];
        
        [[SlideNavigationController sharedInstance] pushViewController:productListController
                                                              animated:YES];
    } else if([self.collectionView isEqual:collectionView]) {
        groupM = (GrouplingModel *)[self.grouplingResult objectAtIndex:indexPath.row];
        
        productListController = [[ProductListViewController alloc] initWithCategory:catM
                                                                        subCategory:subcatM
                                                                          groupling:groupM];
        
        [[SlideNavigationController sharedInstance] pushViewController:productListController
                                                              animated:YES];
    }
}

#pragma mark - Helpers

- (void)populateSubcategories:(NSIndexPath *)indexPath {
    __weak HomeViewController *weakSelf = self;
    CategoryModel *category = [self.categoriesResult objectAtIndex:indexPath.row];
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.subcategoriesPagerNode.view performBatchUpdates:^{
            NSMutableArray *insertIndexPaths = [NSMutableArray array];
            NSMutableArray *deleteIndexPaths = [NSMutableArray array];
            
            NSInteger section = 0;
            
            NSArray *oldData = [weakSelf.subcategoriesResult copy];
            for(SubcategoryModel *oldModel in oldData) {
                NSInteger oldItem = [oldData indexOfObject:oldModel];
                [weakSelf.subcategoriesResult removeObject:oldModel];
                [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
            }
            
            for(SubcategoryModel *newModel in category.subcategories) {
                [weakSelf.subcategoriesResult addObject:newModel];
                NSInteger item = [weakSelf.subcategoriesResult indexOfObject:newModel];
                [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
            }
            
            [weakSelf.subcategoriesPagerNode.view deleteItemsAtIndexPaths:deleteIndexPaths];
            [weakSelf.subcategoriesPagerNode.view insertItemsAtIndexPaths:insertIndexPaths];
        } completion:nil
         ];
    });
}

- (void)populateGrouplingsWithSubcategory:(SubcategoryModel *)subcategory {
    __weak HomeViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.collectionView performBatchUpdates:^{
            NSMutableArray *insertIndexPaths = [NSMutableArray array];
            NSMutableArray *deleteIndexPaths = [NSMutableArray array];
            
            NSInteger section = 0;
            
            NSArray *oldData = [weakSelf.grouplingResult copy];
            for(GrouplingModel *oldModel in oldData) {
                NSInteger oldItem = [oldData indexOfObject:oldModel];
                [weakSelf.grouplingResult removeObject:oldModel];
                [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
            }
            
            for(GrouplingModel *newModel in subcategory.groupling) {
                [weakSelf.grouplingResult addObject:newModel];
                NSInteger item = [weakSelf.grouplingResult indexOfObject:newModel];
                [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
            }
            
            [weakSelf.collectionView deleteItemsAtIndexPaths:deleteIndexPaths];
            [weakSelf.collectionView insertItemsAtIndexPaths:insertIndexPaths];
        } completion:nil
         ];
    });
}

- (void)populateGrouplings:(NSIndexPath *)indexPath {
    __weak HomeViewController *weakSelf = self;
    SubcategoryModel *subcategory = [self.subcategoriesResult objectAtIndex:indexPath.row];
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.collectionView performBatchUpdates:^{
            NSMutableArray *insertIndexPaths = [NSMutableArray array];
            NSMutableArray *deleteIndexPaths = [NSMutableArray array];
            
            NSInteger section = 0;
            
            NSArray *oldData = [weakSelf.grouplingResult copy];
            for(GrouplingModel *oldModel in oldData) {
                NSInteger oldItem = [oldData indexOfObject:oldModel];
                [weakSelf.grouplingResult removeObject:oldModel];
                [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
            }
            
            for(GrouplingModel *newModel in subcategory.groupling) {
                [weakSelf.grouplingResult addObject:newModel];
                NSInteger item = [weakSelf.grouplingResult indexOfObject:newModel];
                [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
            }
            
            [weakSelf.collectionView deleteItemsAtIndexPaths:deleteIndexPaths];
            [weakSelf.collectionView insertItemsAtIndexPaths:insertIndexPaths];
        } completion:nil
         ];
    });
}

#pragma mark - Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
