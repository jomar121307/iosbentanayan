//
//  HomeSpotlightCellNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 17/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "HomeGrouplingCellNode.h"

@implementation HomeGrouplingCellNode

- (instancetype)initWithGroupling:(GrouplingModel *)groupling {
    self = [super init];
    if(self) {
        self.groupling = groupling;
        
        UIColor *randColor = [UIColor colorWithRandomFlatColorOfShadeStyle:UIShadeStyleDark];
        self.backgroundColor = randColor;
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleCenter = [NSMutableParagraphStyle new];
        paragraphStyleCenter.alignment = NSTextAlignmentCenter;
        
        // Category Image
        _categoryImageNode = [[ASNetworkImageNode alloc] init];
        //_categoryImageNode.URL = [NSURL URLWithString:self.subcategory.image];
        //_categoryImageNode.URL = [NSURL URLWithString:@""];
        _categoryImageNode.delegate = self;
        _categoryImageNode.placeholderColor = randColor;
        
        [self addSubnode:_categoryImageNode];
        
        // Category Effect Display Node
        _categoryImageEffectNode = [[ASDisplayNode alloc] init];
        _categoryImageEffectNode.backgroundColor = [UIColor colorWithHexString:@"476b4c" withAlpha:0.3];
        
        [self addSubnode:_categoryImageEffectNode];
        
        // Category Label
        NSDictionary *attrsCategory = @{
                                        NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:22.0f],
                                        NSForegroundColorAttributeName: [UIColor whiteColor],
                                        NSParagraphStyleAttributeName : paragraphStyleCenter
                                        };
        
        NSAttributedString *stringCategory = [[NSAttributedString alloc] initWithString:[self.groupling.name uppercaseString]
                                                                             attributes:attrsCategory];
        
        
        _categoryLabelNode = [[ASTextNode alloc] init];
        _categoryLabelNode.attributedString = stringCategory;
        _categoryLabelNode.pointSizeScaleFactors = @[@0.9, @0.8, @0.7, @0.6];
        _categoryLabelNode.shadowColor = [UIColor flatBlackColor].CGColor;
        _categoryLabelNode.shadowOpacity = 1.0;
        _categoryLabelNode.shadowOffset = CGSizeMake(0, 2);
        _categoryLabelNode.shadowRadius = 4.0f;
        _categoryLabelNode.backgroundColor = [UIColor clearColor];
        
        [self addSubnode:_categoryLabelNode];
    }
    return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // Content
    ASInsetLayoutSpec *insetContent = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(8, 8, 8, 8)
                                                                             child:_categoryLabelNode];
    
    ASCenterLayoutSpec *centerContent = [ASCenterLayoutSpec centerLayoutSpecWithCenteringOptions:ASCenterLayoutSpecCenteringXY
                                                                                   sizingOptions:ASCenterLayoutSpecSizingOptionDefault
                                                                                           child:insetContent];
    
    // Overlay Effects
    ASBackgroundLayoutSpec *overlayEffect = [ASBackgroundLayoutSpec backgroundLayoutSpecWithChild:_categoryImageEffectNode
                                                                                       background:_categoryImageNode];
    
    // Background
    ASBackgroundLayoutSpec *backgroundContent = [ASBackgroundLayoutSpec backgroundLayoutSpecWithChild:centerContent
                                                                                           background:overlayEffect];
    
    return backgroundContent;
}

#pragma mark - ASNetworkImageNodeDelegate methods.

- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image
{
    [self setNeedsDisplay];
}

- (void)imageNode:(ASNetworkImageNode *)imageNode didFailWithError:(NSError *)error {
    [self setNeedsDisplay];
}

@end
