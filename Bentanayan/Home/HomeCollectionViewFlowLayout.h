//
//  HomeCollectionViewLayout.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 16/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCollectionViewFlowLayout : UICollectionViewFlowLayout

@property (assign, nonatomic) NSUInteger numberOfColumns;

@end
