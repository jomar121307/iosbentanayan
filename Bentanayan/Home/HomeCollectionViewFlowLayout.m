//
//  HomeCollectionViewLayout.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 16/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "HomeCollectionViewFlowLayout.h"

@implementation HomeCollectionViewFlowLayout

- (instancetype)init {
    self = [super init];
    if(self) {
        self.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        self.minimumInteritemSpacing = 0;
        self.minimumLineSpacing = 0;
        _numberOfColumns = 2;
    }
    return self;
}

- (CGSize)itemSize {
    CGFloat itemWidth = CGRectGetWidth(self.collectionView.bounds) / _numberOfColumns;
    return CGSizeMake(itemWidth, itemWidth);
}

@end
