//
//  HomeCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 16/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface HomeSubcategoryCellNode : ASCellNode <ASNetworkImageNodeDelegate> {
    ASTextNode *_categoryLabelNode;
    ASNetworkImageNode *_categoryImageNode;
    ASDisplayNode *_categoryImageEffectNode;
}

@property (strong, nonatomic) SubcategoryModel *subcategory;

- (instancetype)initWithSubcategory:(SubcategoryModel *)subcategory;

@end
