//
//  CartShopViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "CartShopViewController.h"

#import "CartShopCellNode.h"

@interface CartShopViewController ()

@end

@implementation CartShopViewController

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor flatWhiteColor];
    
    // Navigation
    self.navigationItem.title = @"Cart - Select Shop";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Home
    UIImage *homeImage = [[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    CGRect frameimg = CGRectMake(0, 0, homeImage.size.width, homeImage.size.height);
    UIButton *homeButton = [[UIButton alloc] initWithFrame:frameimg];
    [homeButton setBackgroundImage:homeImage forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(goToHome) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    // CollectionView
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 0.0f;
    layout.minimumLineSpacing = 0.0f;
    
    self.collectionView = [[ASCollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:layout];
    self.collectionView.asyncDataSource = self;
    self.collectionView.asyncDelegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.alwaysBounceVertical = YES;
    
    [self.view addSubview:self.collectionView];
    
    self.cartShopResult = [NSMutableArray new];
    
    __weak CartShopViewController *weakSelf = self;
    
    [self.collectionView addPullToRefreshWithActionHandler:^{
        [[BESharedManager sharedInstance] be_productsInCartForPayment:NO
                                                        WithCompletion:^(BOOL finished, NSArray *result)
         {
             [weakSelf.collectionView.pullToRefreshView stopAnimating];
             if(finished) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [weakSelf.collectionView performBatchUpdates:^{
                         NSMutableArray *insertIndexPaths = [NSMutableArray array];
                         NSMutableArray *deleteIndexPaths = [NSMutableArray array];
                         NSInteger section = 0;
                         
                         NSArray *oldData = [weakSelf.cartShopResult copy];
                         for(CartShopModel *oldModel in oldData) {
                             NSInteger oldItem = [oldData indexOfObject:oldModel];
                             [weakSelf.cartShopResult removeObject:oldModel];
                             [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
                         }
                         
                         // Filter Shop
                         NSMutableArray *temp = [NSMutableArray new];
                         NSMutableArray *temp2 = [NSMutableArray new];
                         
                         for(CartItemModel *cartTtemModel in result) {
                             CartShopModel *cartShopModel = [[CartShopModel alloc] init];
                             if(![temp containsObject:cartTtemModel.shopId.shopId]) {
                                 [temp addObject:cartTtemModel.shopId.shopId];
                                 cartShopModel.shopId = cartTtemModel.shopId.shopId;
                                 cartShopModel.ownerId = cartTtemModel.productId.userId;
                                 [temp2 addObject:cartShopModel];
                             }
                         }
                         
                         // Populate Shop Array
                         for(CartShopModel *newModel in temp2) {
                             float tempGrandTotal = 0;
                             float tempShippingTotalFee = 0;
                             NSMutableArray *cartItems = [NSMutableArray new];
                             for(CartItemModel *cartItemModel in result) {
                                 if([newModel.shopId isEqualToString:cartItemModel.shopId.shopId]) {
                                     [cartItems addObject:cartItemModel];
                                     tempGrandTotal += (cartItemModel.productId.price * cartItemModel.quantity);
                                     if(cartItemModel.productId.metadata.shippingData.count > 0) {
                                         ShippingDataModel *shippingData = (ShippingDataModel *)[cartItemModel.productId.metadata.shippingData firstObject];
                                         tempShippingTotalFee += shippingData.singleCost;
                                     }
                                 }
                             }
                             newModel.totalPrice = tempGrandTotal;
                             newModel.shippingCost = tempShippingTotalFee;
                             newModel.cartItems = [cartItems copy];
                             [weakSelf.cartShopResult addObject:newModel];
                             NSInteger item = [weakSelf.cartShopResult indexOfObject:newModel];
                             [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
                         }
                         
                         [weakSelf.collectionView deleteItemsAtIndexPaths:deleteIndexPaths];
                         [weakSelf.collectionView insertItemsAtIndexPaths:insertIndexPaths];
                     } completion:nil
                      ];
                 });
             }
         }];
    }];
    
    [self.collectionView triggerPullToRefresh];
}

- (void)viewWillLayoutSubviews {
    self.collectionView.frame = self.view.bounds;
}

#pragma mark - ASCollectionView data source.

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CartShopModel *cartShopModel = [self.cartShopResult objectAtIndex:indexPath.row];
    CartShopCellNode *cellNode = [[CartShopCellNode alloc] initWithShop:cartShopModel];
    return cellNode;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = self.collectionView.bounds.size.width;
    
    return ASSizeRangeMake(CGSizeMake(0.0, 0.0), CGSizeMake(width, FLT_MAX));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.cartShopResult.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionViewLockDataSource:(ASCollectionView *)collectionView
{
    // lock the data source
    // The data source should not be change until it is unlocked.
}

- (void)collectionViewUnlockDataSource:(ASCollectionView *)collectionView
{
    // unlock the data source to enable data source updating.
}

- (void)collectionView:(UICollectionView *)collectionView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    NSLog(@"fetch additional content");
    [context completeBatchFetching:YES];
}

#pragma mark - ASCollectionView delegate.

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CartShopModel *cartShop = (CartShopModel *)[self.cartShopResult objectAtIndex:indexPath.row];
    CartViewController *cartController = [[CartViewController alloc] init];
    cartController.cartShop = cartShop;
    cartController.cartItemResult = [[NSMutableArray alloc] initWithArray:cartShop.cartItems];
    cartController.delegate = self;
    [[SlideNavigationController sharedInstance] pushViewController:cartController
                                                          animated:YES];
}

#pragma mark - Cart View Delegate
- (void)emptyCart:(CartShopModel *)cartShop {
    __weak CartShopViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.collectionView performBatchUpdates:^{
            NSMutableArray *deleteIndexPaths = [NSMutableArray array];
            NSInteger section = 0;
            
            if([self.cartShopResult containsObject:cartShop]) {
                NSUInteger index = [self.cartShopResult indexOfObject:cartShop];
                [self.cartShopResult removeObject:cartShop];
                [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:index inSection:section]];
            }
            
            [weakSelf.collectionView deleteItemsAtIndexPaths:deleteIndexPaths];
        } completion:nil];
    });
}

- (void)removeCartItem:(CartShopModel *)cartShop {
    __weak CartShopViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.collectionView performBatchUpdates:^{
            NSMutableArray *updateIndexPaths = [NSMutableArray array];
            NSInteger section = 0;
            
            if([self.cartShopResult containsObject:cartShop]) {
                NSUInteger index = [self.cartShopResult indexOfObject:cartShop];
                [updateIndexPaths addObject:[NSIndexPath indexPathForItem:index inSection:section]];
            }
            
            [weakSelf.collectionView reloadItemsAtIndexPaths:updateIndexPaths];
        } completion:nil];
    });
}

#pragma mark - Methods / Selectors

- (void)goToHome {
    NSLog(@"Home");
    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
}

#pragma mark - Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
