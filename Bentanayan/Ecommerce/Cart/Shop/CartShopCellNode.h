//
//  CartShopCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface CartShopCellNode : ASCellNode <ASNetworkImageNodeDelegate> {
    ASNetworkImageNode *_userImageNode;
    
    ASTextNode *_shopNameLabelNode;
    ASTextNode *_itemCountLabelNode;
    ASTextNode *_grandTotalLabelNode;
    
    ASDisplayNode *_borderNode;
}

@property (strong, nonatomic) CartShopModel *cartShop;

- (instancetype)initWithShop:(CartShopModel *)cartShop;

@end
