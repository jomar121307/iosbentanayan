//
//  CartShopCellNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "CartShopCellNode.h"

@implementation CartShopCellNode

- (instancetype)initWithShop:(CartShopModel *)cartShop
{
    self = [super init];
    if(self) {
        
        self.cartShop = cartShop;
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        // User Avatar
        _userImageNode = [[ASNetworkImageNode alloc] init];
        if(self.cartShop.ownerId.profilePhoto) {
            ProfilePhotoModel *photo = self.cartShop.ownerId.profilePhoto;
            if(photo.cropped) {
                _userImageNode.URL = [NSURL URLWithString:photo.cropped.secure_url];
            } else if(photo.original) {
                _userImageNode.URL = [NSURL URLWithString:photo.original.secure_url];
            } else {
                _userImageNode.URL = [NSURL URLWithString:photo.secure_url];
            }
        }
        _userImageNode.delegate = self;
        _userImageNode.defaultImage = [UIImage imageNamed:@"avatar-placeholder"];
        _userImageNode.backgroundColor = [UIColor clearColor];
        
        [self addSubnode:_userImageNode];
        
        // Shop Name
        NSDictionary *attrsShopName = @{
                                        NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Bold" size:16.0f],
                                        NSForegroundColorAttributeName: [UIColor flatBlackColorDark],
                                        NSParagraphStyleAttributeName : paragraphStyleLeft
                                        };
        
        NSString *shopName;
        NSString *firstName = self.cartShop.ownerId.firstName;
        NSString *lastName = self.cartShop.ownerId.lastName;
        if(lastName != nil) {
            BOOL hasS = [lastName hasPrefix:@"s"];
            if(hasS) {
                shopName = [NSString stringWithFormat:@"%@ %@' Shop", firstName, lastName];
            } else {
                shopName = [NSString stringWithFormat:@"%@ %@'s Shop", firstName, lastName];
            }
        } else {
            BOOL hasS = [firstName hasPrefix:@"s"];
            if(hasS) {
                shopName = [NSString stringWithFormat:@"%@' Shop", firstName];
            } else {
                shopName = [NSString stringWithFormat:@"%@'s Shop", firstName];
            }
        }
        
        NSAttributedString *stringShopName = [[NSAttributedString alloc] initWithString:shopName
                                                                             attributes:attrsShopName];
        
        
        _shopNameLabelNode = [[ASTextNode alloc] init];
        _shopNameLabelNode.attributedText = stringShopName;
        _shopNameLabelNode.maximumNumberOfLines = 2;
        _shopNameLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_shopNameLabelNode];
        
        // Shop Product Count
        NSDictionary *attrsItemCount = @{
                                         NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                         NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                         NSParagraphStyleAttributeName : paragraphStyleLeft
                                         };
        
        NSString *numberOfItems = [NSString stringWithFormat:@"Items in Cart: %tu", self.cartShop.cartItems.count];
        
        NSAttributedString *stringItemCount = [[NSAttributedString alloc] initWithString:numberOfItems
                                                                              attributes:attrsItemCount];
        
        
        _itemCountLabelNode = [[ASTextNode alloc] init];
        _itemCountLabelNode.attributedText = stringItemCount;
        _itemCountLabelNode.maximumNumberOfLines = 1;
        _itemCountLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_itemCountLabelNode];
        
        /*
         // Total Label
         NSDictionary *attrsGrandTotal = @{
         NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Light" size:14.0f],
         NSForegroundColorAttributeName: [UIColor flatBlackColor],
         NSParagraphStyleAttributeName : paragraphStyleLeft
         };
         
         NSDictionary *attrsPriceGrandTotal = @{
         NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:20.0f],
         NSForegroundColorAttributeName: [UIColor bentaGreenColor],
         NSParagraphStyleAttributeName : paragraphStyleLeft
         };
         
         NSMutableAttributedString *stringGrandTotal = [[NSMutableAttributedString alloc] initWithString:@"Total: "
         attributes:attrsGrandTotal];
         
         NSAttributedString *stringPriceGrandTotal = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%0.2f", totalPrice]
         attributes:attrsPriceGrandTotal];
         
         [stringGrandTotal appendAttributedString:stringPriceGrandTotal];
         
         _grandTotalLabelNode = [[ASTextNode alloc] init];
         _grandTotalLabelNode.attributedText = stringGrandTotal;
         _grandTotalLabelNode.maximumNumberOfLines = 1;
         _grandTotalLabelNode.pointSizeScaleFactors = @[@0.9, @0.8, @0.7, @0.6];
         
         [self addSubnode:_grandTotalLabelNode];
         */
        
        // Border Node
        _borderNode = [[ASDisplayNode alloc] init];
        _borderNode.backgroundColor = [UIColor flatGrayColorDark];
        
        [self addSubnode:_borderNode];
    }
    return self;
}

- (void)layoutDidFinish {
    [super layoutDidFinish];
    
    _userImageNode.cornerRadius = _userImageNode.bounds.size.width / 2;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // Avatar
    ASRatioLayoutSpec *ratioUserImage = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:1.0
                                                                              child:_userImageNode];
    ratioUserImage.alignSelf = ASStackLayoutAlignSelfCenter;
    
    // iPhone & iPad
    UIEdgeInsets contentInset;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        contentInset = UIEdgeInsetsMake(14, 14, 14, 14);
        ratioUserImage.flexBasis = ASRelativeDimensionMakeWithPercent(0.14);
    } else {
        contentInset = UIEdgeInsetsMake(8, 8, 8, 8);
        ratioUserImage.flexBasis = ASRelativeDimensionMakeWithPercent(0.20);
    }
    
    // Content
    ASStackLayoutSpec *stackContent = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                              spacing:2.0f
                                                                       justifyContent:ASStackLayoutJustifyContentStart
                                                                           alignItems:ASStackLayoutAlignItemsStretch
                                                                             children:@[_shopNameLabelNode,
                                                                                        _itemCountLabelNode]];
    stackContent.flexShrink = YES;
    stackContent.flexGrow = YES;
    
    // Main
    ASStackLayoutSpec *stackMain = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                           spacing:12.0f
                                                                    justifyContent:ASStackLayoutJustifyContentStart
                                                                        alignItems:ASStackLayoutAlignItemsStretch
                                                                          children:@[ratioUserImage,
                                                                                     stackContent]];
    
    ASInsetLayoutSpec *insetMain = [ASInsetLayoutSpec insetLayoutSpecWithInsets:contentInset
                                                                          child:stackMain];
    
    // Main with border
    _borderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0f);
    _borderNode.alignSelf = ASStackLayoutAlignSelfEnd;
    
    ASStackLayoutSpec *stackMainBorder = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                                 spacing:0.0
                                                                          justifyContent:ASStackLayoutJustifyContentStart
                                                                              alignItems:ASStackLayoutAlignItemsStretch
                                                                                children:@[insetMain, _borderNode]];
    return stackMainBorder;
    
}

#pragma mark - ASNetworkImageNodeDelegate methods.

- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image
{
    //[self setNeedsLayout];
}

- (void)imageNode:(ASNetworkImageNode *)imageNode didFailWithError:(NSError *)error {
    //[self setNeedsLayout];
}

@end
