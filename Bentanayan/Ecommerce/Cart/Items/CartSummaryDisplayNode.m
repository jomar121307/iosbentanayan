//
//  CartSummaryDisplayNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "CartSummaryDisplayNode.h"

@interface CartSummaryDisplayNode () {
    NSDictionary *_attrsPrice;
    NSDictionary *_attrsPriceGrandTotal;
}

@end

@implementation CartSummaryDisplayNode

- (instancetype)initWithCartShop:(CartShopModel *)cartShop
{
    self = [super init];
    if(self) {
        
        self.cartShop = cartShop;
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        NSMutableParagraphStyle *paragraphStyleRight = [NSMutableParagraphStyle new];
        paragraphStyleRight.alignment = NSTextAlignmentRight;
        
        NSMutableParagraphStyle *paragraphStyleCenter = [NSMutableParagraphStyle new];
        paragraphStyleCenter.alignment = NSTextAlignmentCenter;
        
        NSDictionary *attrsLabel = @{
                                     NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Light" size:14.0f],
                                     NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                     NSParagraphStyleAttributeName : paragraphStyleLeft
                                     };
        
        _attrsPrice = @{
                        NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                        NSForegroundColorAttributeName: [UIColor flatBlackColorDark],
                        NSParagraphStyleAttributeName : paragraphStyleLeft
                        };
        
        // Subtotal Label
        NSAttributedString *stringSubtotal = [[NSAttributedString alloc] initWithString:@"Subtotal Items:"
                                                                             attributes:attrsLabel];
        
        
        _subtotalLabelNode = [[ASTextNode alloc] init];
        _subtotalLabelNode.attributedText = stringSubtotal;
        _subtotalLabelNode.maximumNumberOfLines = 1;
        _subtotalLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_subtotalLabelNode];
        
        self.priceSubtotalLabelNode = [[ASTextNode alloc] init];
        self.priceSubtotalLabelNode.maximumNumberOfLines = 1;
        self.priceSubtotalLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:self.priceSubtotalLabelNode];
        
        // Handling Fee Label
        NSAttributedString *stringHandlingFee = [[NSAttributedString alloc] initWithString:@"Shipping Fee:"
                                                                                attributes:attrsLabel];
        
        
        _shippingFeeLabelNode = [[ASTextNode alloc] init];
        _shippingFeeLabelNode.attributedText = stringHandlingFee;
        _shippingFeeLabelNode.maximumNumberOfLines = 1;
        _shippingFeeLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_shippingFeeLabelNode];
        
        self.priceShippingFeeLabelNode = [[ASTextNode alloc] init];
        self.priceShippingFeeLabelNode.maximumNumberOfLines = 1;
        self.priceShippingFeeLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:self.priceShippingFeeLabelNode];
        
        // Grand Total:
        NSAttributedString *stringTotal = [[NSAttributedString alloc] initWithString:@"TOTAL:"
                                                                          attributes:attrsLabel];
        
        
        _totalLabelNode = [[ASTextNode alloc] init];
        _totalLabelNode.attributedText = stringTotal;
        _totalLabelNode.maximumNumberOfLines = 1;
        _totalLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_totalLabelNode];
        
        _attrsPriceGrandTotal = @{
                                  NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:22.0f],
                                  NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                  NSParagraphStyleAttributeName : paragraphStyleRight
                                  };
        
        self.priceGrandTotalLabelNode = [[ASTextNode alloc] init];
        self.priceGrandTotalLabelNode.maximumNumberOfLines = 1;
        self.priceGrandTotalLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:self.priceGrandTotalLabelNode];
        
        // Empty Cart Button
        NSDictionary *attrsEmptyCart = @{
                                         NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0f],
                                         NSForegroundColorAttributeName: [UIColor whiteColor],
                                         NSParagraphStyleAttributeName : paragraphStyleCenter
                                         };
        
        NSAttributedString *stringEmptyCart = [[NSAttributedString alloc] initWithString:@"EMPTY CART"
                                                                              attributes:attrsEmptyCart];
        
        self.emptyCartButtonNode = [[ASButtonNode alloc] init];
        self.emptyCartButtonNode.titleNode.maximumNumberOfLines = 1;
        self.emptyCartButtonNode.titleNode.pointSizeScaleFactors = @[@0.9, @0.8];
        [self.emptyCartButtonNode setAttributedTitle:stringEmptyCart
                                            forState:ASControlStateNormal];
        [self.emptyCartButtonNode setContentEdgeInsets:UIEdgeInsetsMake(8, 14, 8, 14)];
        [self.emptyCartButtonNode setHitTestSlop:UIEdgeInsetsMake(-2, -4, -2, -4)];
        [self.emptyCartButtonNode setBackgroundColor:[UIColor flatRedColorDark]];
        
        [self addSubnode:self.emptyCartButtonNode];
        
        // Checkout Button
        NSDictionary *attrsCheckout = @{
                                        NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0f],
                                        NSForegroundColorAttributeName: [UIColor whiteColor],
                                        NSParagraphStyleAttributeName : paragraphStyleCenter
                                        };
        
        NSAttributedString *stringCheckout = [[NSAttributedString alloc] initWithString:@"PROCEED TO CHECKOUT"
                                                                             attributes:attrsCheckout];
        
        self.checkoutButtonNode = [[ASButtonNode alloc] init];
        self.checkoutButtonNode.titleNode.maximumNumberOfLines = 1;
        self.checkoutButtonNode.titleNode.pointSizeScaleFactors = @[@0.9, @0.8];
        [self.checkoutButtonNode setAttributedTitle:stringCheckout
                                           forState:ASControlStateNormal];
        [self.checkoutButtonNode setContentEdgeInsets:UIEdgeInsetsMake(8, 14, 8, 14)];
        [self.checkoutButtonNode setHitTestSlop:UIEdgeInsetsMake(-2, -4, -2, -4)];
        [self.checkoutButtonNode setBackgroundColor:[UIColor bentaGreenColor]];
        
        [self addSubnode:self.checkoutButtonNode];
    }
    return self;
}

- (void)layout {
    [super layout];
    [self measureWithSizeRange:ASSizeRangeMake(CGSizeMake(self.bounds.size.width, 140.0f), CGSizeMake(self.bounds.size.width, 140.0f))];
}

- (void)layoutDidFinish {
    [super layoutDidFinish];
    
    [self.view updateConstraintsIfNeeded];
}

- (void)fetchData {
    [super fetchData];
    NSLog(@"FETCH CART SUMMARY");
    
    // Sub Total
    NSString *subTotal = [NSString stringWithFormat:@"PHP %0.2f", self.cartShop.totalPrice];
    NSAttributedString *stringPriceSubtotal = [[NSAttributedString alloc] initWithString:subTotal
                                                                              attributes:_attrsPrice];
    self.priceSubtotalLabelNode.attributedText = stringPriceSubtotal;
    
    // Shipping Cost
    NSString *shippingFee = [NSString stringWithFormat:@"PHP %0.2f", self.cartShop.shippingCost];
    
    NSAttributedString *stringPriceShippingFee = [[NSAttributedString alloc] initWithString:shippingFee
                                                                                 attributes:_attrsPrice];
    self.priceShippingFeeLabelNode.attributedText = stringPriceShippingFee;
    
    // Grand Total
    CGFloat total = self.cartShop.totalPrice + self.cartShop.shippingCost;
    NSString *grandTotal = [NSString stringWithFormat:@"PHP %0.2f", total];
    
    NSAttributedString *stringPriceGrandTotal = [[NSAttributedString alloc] initWithString:grandTotal
                                                                                attributes:_attrsPriceGrandTotal];
    self.priceGrandTotalLabelNode.attributedText = stringPriceGrandTotal;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
    spacer.flexGrow = YES;
    
    // Subtotal
    self.priceSubtotalLabelNode.flexShrink = YES;
    ASStackLayoutSpec *stackSubtotal = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                               spacing:2.0f
                                                                        justifyContent:ASStackLayoutJustifyContentCenter
                                                                            alignItems:ASStackLayoutAlignItemsStretch
                                                                              children:@[_subtotalLabelNode,
                                                                                         spacer,
                                                                                         self.priceSubtotalLabelNode]];
    
    // Shipping Fee
    self.priceShippingFeeLabelNode.flexShrink = YES;
    ASStackLayoutSpec *stackShippingFee = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                  spacing:2.0f
                                                                           justifyContent:ASStackLayoutJustifyContentStart
                                                                               alignItems:ASStackLayoutAlignItemsStretch
                                                                                 children:@[_shippingFeeLabelNode,
                                                                                            spacer,
                                                                                            self.priceShippingFeeLabelNode]];
    
    // Total
    self.priceGrandTotalLabelNode.flexShrink = YES;
    ASStackLayoutSpec *stackGrandTotal = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                 spacing:2.0f
                                                                          justifyContent:ASStackLayoutJustifyContentStart
                                                                              alignItems:ASStackLayoutAlignItemsStretch
                                                                                children:@[_totalLabelNode,
                                                                                           spacer,
                                                                                           self.priceGrandTotalLabelNode]];
    
    // Right Stack
    ASStackLayoutSpec *stackRight = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                            spacing:4.0f
                                                                     justifyContent:ASStackLayoutJustifyContentEnd
                                                                         alignItems:ASStackLayoutAlignItemsStretch
                                                                           children:@[stackSubtotal,
                                                                                      stackShippingFee,
                                                                                      stackGrandTotal,
                                                                                      self.checkoutButtonNode]];
    
    // Main Stack
    stackRight.flexBasis = ASRelativeDimensionMakeWithPercent(0.6);
    _emptyCartButtonNode.flexBasis = ASRelativeDimensionMakeWithPercent(0.4);
    ASStackLayoutSpec *stackMain = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                           spacing:8.0f
                                                                    justifyContent:ASStackLayoutJustifyContentCenter
                                                                        alignItems:ASStackLayoutAlignItemsEnd
                                                                          children:@[_emptyCartButtonNode, stackRight]];
    
    ASInsetLayoutSpec *insetMain = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(10, 10, 10, 10)
                                                                          child:stackMain];
    
    return [ASCenterLayoutSpec centerLayoutSpecWithCenteringOptions:ASCenterLayoutSpecCenteringXY
                                                      sizingOptions:ASCenterLayoutSpecSizingOptionDefault
                                                              child:insetMain];
}

@end
