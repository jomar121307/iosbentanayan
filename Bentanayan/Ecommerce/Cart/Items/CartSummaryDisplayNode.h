//
//  CartSummaryDisplayNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface CartSummaryDisplayNode : ASDisplayNode {
    ASTextNode *_subtotalLabelNode;
    ASTextNode *_shippingFeeLabelNode;
    ASTextNode *_totalLabelNode;
}

@property (strong, nonatomic) ASTextNode *priceSubtotalLabelNode;
@property (strong, nonatomic) ASTextNode *priceShippingFeeLabelNode;
@property (strong, nonatomic) ASTextNode *priceGrandTotalLabelNode;

@property (strong, nonatomic) ASButtonNode *emptyCartButtonNode;
@property (strong, nonatomic) ASButtonNode *checkoutButtonNode;

@property (strong, nonatomic) CartShopModel *cartShop;

- (instancetype)initWithCartShop:(CartShopModel *)cartShop;

@end

