//
//  CartViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "CartViewController.h"

#import "CartCellNode.h"

#import "CheckoutShippingViewController.h"

@interface CartViewController ()

@end

@implementation CartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor flatWhiteColor];
    
    // Navigation
    NSString *firstName = self.cartShop.ownerId.firstName;
    NSString *checkS = [firstName substringFromIndex:[firstName length] - 1];
    if([checkS isEqualToString:@"s"]) {
        self.navigationItem.title = [NSString stringWithFormat:@"Cart - %@' Shop", firstName];
    } else {
        self.navigationItem.title = [NSString stringWithFormat:@"Cart - %@'s Shop", firstName];
        
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Back
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(goBack)];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    // Home
    UIImage *homeImage = [[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    CGRect frameimg = CGRectMake(0, 0, homeImage.size.width, homeImage.size.height);
    UIButton *homeButton = [[UIButton alloc] initWithFrame:frameimg];
    [homeButton setBackgroundImage:homeImage forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(goToHome) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    // Cart Summary
    self.cartSummaryDisplayNode = [[CartSummaryDisplayNode alloc] initWithCartShop:self.cartShop];
    self.cartSummaryDisplayNode.backgroundColor = [UIColor flatWhiteColor];
    self.cartSummaryDisplayNode.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.cartSummaryDisplayNode.checkoutButtonNode addTarget:self
                                                       action:@selector(proceedToCheckout:)
                                             forControlEvents:ASControlNodeEventTouchUpInside];
    [self.cartSummaryDisplayNode.emptyCartButtonNode addTarget:self
                                                        action:@selector(emptyCart:)
                                              forControlEvents:ASControlNodeEventTouchUpInside];
    
    [self.view addSubnode:self.cartSummaryDisplayNode];
    
    // CollectionView
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 0.0f;
    layout.minimumLineSpacing = 0.0f;
    
    self.collectionView = [[ASCollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:layout];
    self.collectionView.asyncDataSource = self;
    self.collectionView.asyncDelegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:self.collectionView];
    
    // Constraints
    NSDictionary *views = @{ @"cartSummary" : self.cartSummaryDisplayNode.view,
                             @"collectionView" : self.collectionView };
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cartSummary]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|" options:0 metrics:nil views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collectionView][cartSummary(140)]|" options:0 metrics:nil views:views]];
    
    //    self.cartItemResult = [NSMutableArray new];
    //
    //    __weak CartViewController *weakSelf = self;
    //
    //    [self.collectionView addPullToRefreshWithActionHandler:^{
    //        [[BESharedManager sharedInstance] be_productsInCartWithCompletion:^(BOOL finished, NSArray *result)
    //         {
    //             if(finished) {
    //                 dispatch_async(dispatch_get_main_queue(), ^{
    //                     [weakSelf.collectionView performBatchUpdates:^{
    //                         NSMutableArray *insertIndexPaths = [NSMutableArray array];
    //                         NSMutableArray *deleteIndexPaths = [NSMutableArray array];
    //                         NSInteger section = 0;
    //
    //                         NSArray *oldData = [weakSelf.cartItemResult copy];
    //                         for(CartItemModel *oldModel in oldData) {
    //                             NSInteger oldItem = [oldData indexOfObject:oldModel];
    //                             [weakSelf.cartItemResult removeObject:oldModel];
    //                             [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
    //                         }
    //
    //                         for(CartItemModel *newModel in result) {
    //                             [weakSelf.cartItemResult addObject:newModel];
    //                             NSInteger item = [weakSelf.cartItemResult indexOfObject:newModel];
    //                             [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
    //                         }
    //
    //                         [weakSelf.collectionView deleteItemsAtIndexPaths:deleteIndexPaths];
    //                         [weakSelf.collectionView insertItemsAtIndexPaths:insertIndexPaths];
    //                     } completion:nil
    //                      ];
    //                 });
    //             }
    //             [weakSelf.collectionView.pullToRefreshView stopAnimating];
    //        }];
    //    }];
    //
    //    [self.collectionView triggerPullToRefresh];
}

#pragma mark - ASCollectionView data source.

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CartItemModel *cartItem = [self.cartItemResult objectAtIndex:indexPath.row];
    
    CartCellNode *cellNode = [[CartCellNode alloc] initWithCartItemModel:cartItem];
    [cellNode.removeItemButtonNode addTarget:self
                                      action:@selector(removeItemFromCart:)
                            forControlEvents:ASControlNodeEventTouchUpInside];
    return cellNode;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = self.collectionView.bounds.size.width;
    
    return ASSizeRangeMake(CGSizeMake(0.0, 0.0), CGSizeMake(width, FLT_MAX));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.cartItemResult.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionViewLockDataSource:(ASCollectionView *)collectionView
{
    // lock the data source
    // The data source should not be change until it is unlocked.
}

- (void)collectionViewUnlockDataSource:(ASCollectionView *)collectionView
{
    // unlock the data source to enable data source updating.
}

- (void)collectionView:(UICollectionView *)collectionView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    NSLog(@"fetch additional content");
    [context completeBatchFetching:YES];
}

#pragma mark - Methods / Selectors

- (void)goToHome {
    NSLog(@"Home");
    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
}

- (void)goBack {
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

- (void)proceedToCheckout:(ASButtonNode *)button {
    NSDictionary *checkoutParams = @{
                                     @"checkoutShopId" : self.cartShop.shopId
                                     };
    
    NSLog(@"Setcheckoutinfo: %@", checkoutParams);
    
    __weak CartViewController *weakSelf = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[BESharedManager sharedInstance] be_checkoutInfo:checkoutParams
                                            withCompletion:^(BOOL finished)
         {
             if(finished) {
                 CheckoutShippingViewController *checkoutShippingController = [[CheckoutShippingViewController alloc] init];
                 [[SlideNavigationController sharedInstance] pushViewController:checkoutShippingController
                                                                       animated:YES];
                 checkoutShippingController.cartShop = weakSelf.cartShop;
             } else {
                 [TSMessage showNotificationWithTitle:@"Could not proceed. Please try again later."
                                                 type:TSMessageNotificationTypeError];
             }
             dispatch_async(dispatch_get_main_queue(), ^{
                 [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
             });
         }];
    });
}

- (void)emptyCart:(ASButtonNode *)button {
    __weak CartViewController *weakSelf = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[BESharedManager sharedInstance] be_cartRemoveItemsInShop:self.cartShop.shopId
                                                     withCompletion:^(BOOL finished)
         {
             if(finished) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [weakSelf.collectionView performBatchUpdates:^{
                         NSMutableArray *deleteIndexPaths = [NSMutableArray array];
                         NSInteger section = 0;
                         
                         NSArray *oldData = [weakSelf.cartItemResult copy];
                         for(CartItemModel *oldModel in oldData) {
                             NSInteger oldItem = [oldData indexOfObject:oldModel];
                             [weakSelf.cartItemResult removeObject:oldModel];
                             [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
                         }
                         
                         [weakSelf.collectionView deleteItemsAtIndexPaths:deleteIndexPaths];
                     } completion:nil];
                 });
                 [weakSelf.delegate emptyCart:self.cartShop];
                 [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
             }
             dispatch_async(dispatch_get_main_queue(), ^{
                 [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
             });
         }];
    });
}

- (void)removeItemFromCart:(ASButtonNode *)button {
    __weak CartViewController *weakSelf = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CartCellNode *cellNode = (CartCellNode *)button.supernode;
        CartItemModel *item = (CartItemModel *)cellNode.cartItem;
        if([self.cartItemResult containsObject:item]) {
            NSInteger index = [self.cartItemResult indexOfObject:item];
            [[BESharedManager sharedInstance] be_cartRemoveItem:item.cartItemId
                                                  withCompletion:^(BOOL finished)
             {
                 if(finished) {
                     [weakSelf.cartItemResult removeObject:item];
                     weakSelf.cartShop.cartItems = [weakSelf.cartItemResult copy];
                     weakSelf.cartShop.totalPrice = weakSelf.cartShop.totalPrice - (item.productId.price * item.quantity);
                     if(item.productId.metadata.shippingData.count > 0) {
                         ShippingDataModel *shippingData = (ShippingDataModel *)[item.productId.metadata.shippingData firstObject];
                         weakSelf.cartShop.shippingCost = weakSelf.cartShop.shippingCost - shippingData.singleCost;
                     }
                     if(weakSelf.cartItemResult.count > 0) {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [weakSelf.collectionView performBatchUpdates:^{
                                 NSInteger section = 0;
                                 [weakSelf.collectionView deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:index
                                                                                                        inSection:section]]];
                             } completion:nil];
                         });
                         [weakSelf.delegate removeCartItem:self.cartShop];
                     } else {
                         [weakSelf.delegate emptyCart:self.cartShop];
                         [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
                     }
                     NSLog(@"Cart Items: %lu", (unsigned long)weakSelf.cartShop.cartItems.count);
                     NSLog(@"Price: %f \n\nShipping %f", weakSelf.cartShop.totalPrice, weakSelf.cartShop.shippingCost);
                     weakSelf.cartSummaryDisplayNode.cartShop = weakSelf.cartShop;
                     [weakSelf.cartSummaryDisplayNode setNeedsDataFetch];
                 }
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
                 });
                 
             }];
        }
    });
}

#pragma mark - Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
