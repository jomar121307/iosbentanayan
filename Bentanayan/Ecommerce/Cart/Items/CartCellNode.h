//
//  CartCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface CartCellNode : ASCellNode <ASNetworkImageNodeDelegate> {
    ASNetworkImageNode *_productImageNode;
    
    ASTextNode *_productNameLabelNode;
    ASTextNode *_productOwnerLabelNode;
    ASTextNode *_productPriceLabelNode;
    
    ASTextNode *_productQuantityLabelNode;
    ASTextNode *_productTotalPriceLabelNode;
    
    ASDisplayNode *_borderNode;
}

@property (strong, nonatomic) ASButtonNode *removeItemButtonNode;

@property (strong, nonatomic) CartItemModel *cartItem;

- (instancetype)initWithCartItemModel:(CartItemModel *)cartItem;

@end
