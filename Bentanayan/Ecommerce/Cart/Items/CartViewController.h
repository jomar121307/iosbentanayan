//
//  CartViewController.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CartSummaryDisplayNode.h"

@protocol CartViewDelegate <NSObject>

- (void)emptyCart:(CartShopModel *)cartShop;
- (void)removeCartItem:(CartShopModel *)cartShop;

@end

@interface CartViewController : UIViewController <ASCollectionViewDataSource, ASCollectionViewDelegateFlowLayout, SlideNavigationControllerDelegate>

@property (strong, nonatomic) CartSummaryDisplayNode *cartSummaryDisplayNode;
@property (strong, nonatomic) ASCollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray *cartItemResult;
@property (strong, nonatomic) CartShopModel *cartShop;

@property (nonatomic,assign) id delegate;

@end
