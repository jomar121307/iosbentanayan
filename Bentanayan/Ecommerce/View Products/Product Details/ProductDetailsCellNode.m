//
//  ProductDetailsCellNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ProductDetailsCellNode.h"

#import "ProductImageCellNode.h"

@implementation ProductDetailsCellNode

- (instancetype)initWithProductModel:(ProductModel *)product {
    self = [super init];
    if(self) {
        self.product = product;
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        // Content Display
        _contentNode = [[ASDisplayNode alloc] init];
        _contentNode.backgroundColor = [UIColor whiteColor];
        
        [self addSubnode:_contentNode];
        
        // Product Image
        _productImageNode = [[ASNetworkImageNode alloc] init];
        if(self.product.metadata.photos) {
            NSArray *photos = self.product.metadata.photos;
            if(photos.count > 0) {
                ProductPhotoModel *photo = [photos firstObject];
                if(photo.cropped) {
                    _productImageNode.URL = [NSURL URLWithString:photo.cropped.secure_url];
                } else if(photo.original) {
                    _productImageNode.URL = [NSURL URLWithString:photo.original.secure_url];
                } else {
                    _productImageNode.URL = [NSURL URLWithString:photo.secure_url];
                }
            }
        }
        _productImageNode.delegate = self;
        _productImageNode.defaultImage = [UIImage imageNamed:@"product-placeholder"];
        _productImageNode.contentMode = UIViewContentModeScaleAspectFit;
        
        [self addSubnode:_productImageNode];
        
        // Product Name Label
        NSDictionary *attrsProductName = @{
                                           NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Bold" size:16.0f],
                                           NSForegroundColorAttributeName: [UIColor flatBlackColorDark],
                                           NSParagraphStyleAttributeName : paragraphStyleLeft
                                           };
        
        NSAttributedString *stringProductName = [[NSAttributedString alloc] initWithString:self.product.name
                                                                                attributes:attrsProductName];
        
        
        _productNameLabelNode = [[ASTextNode alloc] init];
        _productNameLabelNode.attributedText = stringProductName;
        _productNameLabelNode.maximumNumberOfLines = 2;
        _productNameLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_productNameLabelNode];
        
        // Product Review Label
        NSDictionary *attrsProductReview = @{
                                             NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Light" size:14.0f],
                                             NSForegroundColorAttributeName: [UIColor flatGrayColorDark],
                                             NSParagraphStyleAttributeName : paragraphStyleLeft
                                             };
        
        NSAttributedString *stringProductReview = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"Rating: %0.1f/5.0", self.product.reviewScore]
                                                                                  attributes:attrsProductReview];
        
        
        _productReviewLabelNode = [[ASTextNode alloc] init];
        _productReviewLabelNode.attributedText = stringProductReview;
        _productReviewLabelNode.maximumNumberOfLines = 1;
        _productReviewLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_productReviewLabelNode];
        
        // Product Owner Label
        NSDictionary *attrsProductOwner = @{
                                            NSLinkAttributeName: @"shopPage",
                                            NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                            NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                            NSParagraphStyleAttributeName : paragraphStyleLeft,
                                            NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                                            NSUnderlineColorAttributeName : [UIColor flatGrayColorDark]
                                            };
        
        NSDictionary *attrsBy = @{
                                  NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                  NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                  NSParagraphStyleAttributeName : paragraphStyleLeft,
                                  };
        
        NSString *productOwner;
        NSString *fullName;
        NSString *firstName = self.product.userId.firstName;
        NSString *lastName = self.product.userId.lastName;
        if(lastName != nil) {
            fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
            productOwner = [NSString stringWithFormat:@"By: %@ %@", firstName, lastName];
        } else {
            fullName = [NSString stringWithFormat:@"%@", firstName];
            productOwner = [NSString stringWithFormat:@"By: %@", firstName];
        }
        NSMutableAttributedString *stringProductOwner = [[NSMutableAttributedString alloc] initWithString:productOwner];
        
        [stringProductOwner addAttributes:attrsBy range:[productOwner rangeOfString:@"By: "]];
        [stringProductOwner addAttributes:attrsProductOwner range:[productOwner rangeOfString:fullName]];
        
        _productOwnerLabelNode = [[ASTextNode alloc] init];
        _productOwnerLabelNode.attributedText = stringProductOwner;
        _productOwnerLabelNode.maximumNumberOfLines = 1;
        _productOwnerLabelNode.delegate = self;
        _productOwnerLabelNode.userInteractionEnabled = YES;
        _productOwnerLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_productOwnerLabelNode];
        
        // Review Button
        NSAttributedString *stringReviewCount = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d", self.product.reviewCount]
                                                                                attributes:attrsBy];
        
        self.reviewButtonNode = [[ASButtonNode alloc] init];
        self.reviewButtonNode.imageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor flatGrayColor]);
        [self.reviewButtonNode setAttributedTitle:stringReviewCount
                                         forState:ASControlStateNormal];
        [self.reviewButtonNode setImage:[UIImage imageNamed:@"comment"] forState:ASControlStateNormal];
        [self.reviewButtonNode setHitTestSlop:UIEdgeInsetsMake(-2, -4, -2, -4)];
        [self.reviewButtonNode setBackgroundColor:[UIColor clearColor]];
        [self.reviewButtonNode measure:CGSizeMake(44, 44)];
        
        [self addSubnode:self.reviewButtonNode];
        
        // Product Stock
        NSDictionary *attrsLowerContent = @{
                                            NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                            NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                            NSParagraphStyleAttributeName : paragraphStyleLeft
                                            };
        
        NSAttributedString *stringStock = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"Available Stock: %0.0f", self.product.quantity]
                                                                          attributes:attrsLowerContent];
        
        
        _productStockLabelNode = [[ASTextNode alloc] init];
        _productStockLabelNode.attributedText = stringStock;
        _productStockLabelNode.maximumNumberOfLines = 1;
        _productStockLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_productStockLabelNode];
        
        // Product Description
        NSAttributedString *stringProductDescription = [[NSAttributedString alloc] initWithString:self.product.productDescription
                                                                                       attributes:attrsLowerContent];
        
        
        _productDescriptionLabelNode = [[ASTextNode alloc] init];
        _productDescriptionLabelNode.attributedText = stringProductDescription;
        _productDescriptionLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_productDescriptionLabelNode];
        
        // Product Price Label
        NSDictionary *attrsProductPrice = @{
                                            NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                            NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                            NSParagraphStyleAttributeName : paragraphStyleLeft
                                            };
        
        NSAttributedString *stringProductPrice = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%0.2f", self.product.currencyId.symbol, self.product.price]
                                                                                 attributes:attrsProductPrice];
        
        
        _productPriceLabelNode = [[ASTextNode alloc] init];
        _productPriceLabelNode.attributedText = stringProductPrice;
        _productPriceLabelNode.maximumNumberOfLines = 1;
        _productPriceLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_productPriceLabelNode];
        
        // Per Item Label
        NSDictionary *attrsPerItem = @{
                                       NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Light" size:12.0f],
                                       NSForegroundColorAttributeName: [UIColor flatGrayColorDark],
                                       NSParagraphStyleAttributeName : paragraphStyleLeft
                                       };
        
        NSAttributedString *stringPerItem = [[NSAttributedString alloc] initWithString:@"*per item"
                                                                            attributes:attrsPerItem];
        
        
        _perItemLabelNode = [[ASTextNode alloc] init];
        _perItemLabelNode.attributedText = stringPerItem;
        _perItemLabelNode.maximumNumberOfLines = 1;
        _perItemLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_perItemLabelNode];
        
        // Quantity Label
        /*
         NSDictionary *attrsQuantity = @{
         NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Bold" size:12.0f],
         NSForegroundColorAttributeName: [UIColor flatBlackColor],
         NSParagraphStyleAttributeName : paragraphStyleLeft
         };
         
         NSAttributedString *stringQuantity = [[NSAttributedString alloc] initWithString:@"Quantity: "
         attributes:attrsQuantity];
         
         
         _quantityLabelNode = [[ASTextNode alloc] init];
         _quantityLabelNode.attributedText = stringQuantity;
         _quantityLabelNode.maximumNumberOfLines = 1;
         _quantityLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
         
         [self addSubnode:_quantityLabelNode];
         */
        
        // Add To Cart Button
        if(![self.product.userId.userId isEqualToString:[BESharedManager sharedInstance].login.session.userId.userId]) {
            self.addToCartButtonNode = [[ASButtonNode alloc] init];
            [self.addToCartButtonNode setTitle:@"ADD TO CART"
                                      withFont:[UIFont fontWithName:@"RobotoCondensed-Bold" size:14.0f]
                                     withColor:[UIColor whiteColor]
                                      forState:ASControlStateNormal];
            self.addToCartButtonNode.titleNode.maximumNumberOfLines = 1;
            self.addToCartButtonNode.titleNode.pointSizeScaleFactors = @[@0.9, @0.8];
            [self.addToCartButtonNode setContentEdgeInsets:UIEdgeInsetsMake(8, 14, 8, 14)];
            [self.addToCartButtonNode setHitTestSlop:UIEdgeInsetsMake(-2, -4, -2, -4)];
            [self.addToCartButtonNode setBackgroundColor:[UIColor bentaGreenColor]];
            [self.addToCartButtonNode addTarget:self
                                         action:@selector(addToCart:)
                               forControlEvents:ASControlNodeEventTouchUpInside];
            
            [self addSubnode:self.addToCartButtonNode];
        }
        
        // Content Collection Display
        _contentCollectionNode = [[ASDisplayNode alloc] initWithViewBlock:^UIView * _Nonnull{
            // CollectionView
            UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
            layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
            
            self.collectionView = [[ASCollectionView alloc] initWithFrame:CGRectZero
                                                     collectionViewLayout:layout];
            self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            self.collectionView.asyncDataSource = self;
            self.collectionView.asyncDelegate = self;
            self.collectionView.alwaysBounceHorizontal = YES;
            self.collectionView.contentInset = UIEdgeInsetsMake(0, 8, 0, 8);
            
            return self.collectionView;
        }];
        
        _contentCollectionNode.backgroundColor = [UIColor clearColor];
        _contentCollectionNode.userInteractionEnabled = YES;
        
        [self addSubnode:_contentCollectionNode];
    }
    return self;
}

- (void)didLoad
{
    self.layer.as_allowsHighlightDrawing = YES;
    [super didLoad];
}

- (void)layoutDidFinish {
    [super layoutDidFinish];
    
    _contentNode.shadowColor = [UIColor flatBlackColor].CGColor;
    _contentNode.shadowOpacity = 0.5f;
    _contentNode.shadowRadius = 2.0f;
    _contentNode.shadowOffset = CGSizeMake(0.0f, 2.0f);
    _contentNode.layer.masksToBounds = NO;
    _contentNode.layer.shadowPath = [UIBezierPath bezierPathWithRect:_contentNode.bounds].CGPath;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // Product Image
    ASRatioLayoutSpec *ratioImage = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:1.0
                                                                          child:_productImageNode];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        ratioImage.flexBasis = ASRelativeDimensionMakeWithPercent(0.42);
    } else {
        ratioImage.flexBasis = ASRelativeDimensionMakeWithPercent(0.60);
    }
    
    ASStackLayoutSpec *stackImage = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                            spacing:0.0
                                                                     justifyContent:ASStackLayoutJustifyContentCenter
                                                                         alignItems:ASStackLayoutAlignItemsStretch
                                                                           children:@[ratioImage]];
    
    // Owner and Other Buttons
    _productOwnerLabelNode.flexShrink = YES;
    _productOwnerLabelNode.flexGrow = YES;
    self.reviewButtonNode.alignSelf = ASStackLayoutAlignSelfCenter;
    
    ASStackLayoutSpec *stackOwnerReview = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                  spacing:8.0
                                                                           justifyContent:ASStackLayoutJustifyContentCenter
                                                                               alignItems:ASStackLayoutAlignItemsStretch
                                                                                 children:@[_productOwnerLabelNode,
                                                                                            self.reviewButtonNode]];
    stackOwnerReview.alignSelf = ASStackLayoutAlignSelfStretch;
    
    // Product Details (Stock and Description)
    ASStackLayoutSpec *stackStockDesc = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                                spacing:8.0
                                                                         justifyContent:ASStackLayoutJustifyContentStart
                                                                             alignItems:ASStackLayoutAlignItemsStretch
                                                                               children:@[_productStockLabelNode,
                                                                                          _productDescriptionLabelNode]];
    
    // Product Details (Price, Quantity Input, Add To Cart Button)
    self.addToCartButtonNode.spacingBefore = 4.0f;
    NSMutableArray *arrayAddCart = [[NSMutableArray alloc] init];
    [arrayAddCart addObject:_productPriceLabelNode];
    [arrayAddCart addObject:_perItemLabelNode];
    if(![self.product.userId.userId isEqualToString:[BESharedManager sharedInstance].login.session.userId.userId]) {
        [arrayAddCart addObject:self.addToCartButtonNode];
    }
    ASStackLayoutSpec *stackPriceCart = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                                spacing:0.0f
                                                                         justifyContent:ASStackLayoutJustifyContentEnd
                                                                             alignItems:ASStackLayoutAlignItemsEnd
                                                                               children:arrayAddCart];
    
    // Product Details
    stackStockDesc.flexBasis = ASRelativeDimensionMakeWithPercent(0.6);
    stackPriceCart.flexBasis = ASRelativeDimensionMakeWithPercent(0.4);
    ASStackLayoutSpec *stackProductDetails = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                     spacing:8.0f
                                                                              justifyContent:ASStackLayoutJustifyContentCenter
                                                                                  alignItems:ASStackLayoutAlignItemsStretch
                                                                                    children:@[stackStockDesc, stackPriceCart]];
    // Main Content
    _contentCollectionNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 72);
    _contentCollectionNode.spacingAfter = 4.0f;
    _contentCollectionNode.spacingBefore = 4.0f;
    
    ASStackLayoutSpec *stackContent = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                              spacing:2.0
                                                                       justifyContent:ASStackLayoutJustifyContentStart
                                                                           alignItems:ASStackLayoutAlignItemsStretch
                                                                             children:@[stackImage,
                                                                                        _productNameLabelNode,
                                                                                        _productReviewLabelNode,
                                                                                        stackOwnerReview,
                                                                                        _contentCollectionNode,
                                                                                        stackProductDetails]];
    
    ASInsetLayoutSpec *insetContent = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(10, 10, 10, 10)
                                                                             child:stackContent];
    
    // Content Display
    _contentNode.sizeRange = ASRelativeSizeRangeMakeWithExactRelativeDimensions(ASRelativeDimensionMakeWithPercent(1), ASRelativeDimensionMakeWithPercent(1));
    
    ASStaticLayoutSpec *staticContentDisplay = [ASStaticLayoutSpec staticLayoutSpecWithChildren:@[_contentNode]];
    
    ASOverlayLayoutSpec *overlayContent = [ASOverlayLayoutSpec overlayLayoutSpecWithChild:insetContent
                                                                                  overlay:staticContentDisplay];
    
    
    return [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(10, 10, 10, 10)
                                                  child:overlayContent];
}

#pragma mark - ASCollectionView data source.

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *photos = self.product.metadata.photos;
    NSURL *photoUrl;
    if(photos.count > 0) {
        ProductPhotoModel *photo = [self.product.metadata.photos objectAtIndex:indexPath.row];
        if(photo.cropped) {
            photoUrl = [NSURL URLWithString:photo.cropped.secure_url];
        } else if(photo.original) {
            photoUrl = [NSURL URLWithString:photo.original.secure_url];
        } else {
            photoUrl = [NSURL URLWithString:photo.secure_url];
        }
        ProductImageCellNode *cellNode = [[ProductImageCellNode alloc] initWithPhotoUrl:photoUrl];
        return cellNode;
    } else {
        return [[ASCellNode alloc] init];
    }
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize itemSize = CGSizeMake(72.0f, 72.0f);
    return ASSizeRangeMake(itemSize, itemSize);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.product.metadata.photos.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionViewLockDataSource:(ASCollectionView *)collectionView
{
    // lock the data source
    // The data source should not be change until it is unlocked.
}

- (void)collectionViewUnlockDataSource:(ASCollectionView *)collectionView
{
    // unlock the data source to enable data source updating.
}

- (void)collectionView:(UICollectionView *)collectionView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    NSLog(@"fetch additional content");
    [context completeBatchFetching:YES];
}

#pragma mark - ASCollectionView delegate.

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSURL *photoUrl;
    ProductPhotoModel *photo = [self.product.metadata.photos objectAtIndex:indexPath.row];
    if(photo.cropped) {
        photoUrl = [NSURL URLWithString:photo.cropped.secure_url];
    } else if(photo.original) {
        photoUrl = [NSURL URLWithString:photo.original.secure_url];
    } else {
        photoUrl = [NSURL URLWithString:photo.secure_url];
    }
    _productImageNode.URL = photoUrl;
}

#pragma mark - Methods / Selectors

- (void)addToCart:(ASButtonNode *)button {
    
    if(![[BESharedManager sharedInstance] checkLogin]) {
        return;
    }
    
    SCLAlertView *alertView = [BentaAlertView showAlert];
    alertView.customViewColor = [UIColor bentaGreenColor];
    
    UITextField *textField = [alertView addTextField:@"Quantity"];
    [textField setKeyboardType:UIKeyboardTypeDecimalPad];
    
    [alertView addButton:@"Add" actionBlock:^(void) {
        [textField resignFirstResponder];
        if([textField.text isEqualToString:@""] || [textField.text isEqualToString:@"0"] || ([textField.text integerValue] <= 0)) {
            [TSMessage showNotificationInViewController:self.view.window.rootViewController
                                                  title:@"Validation Error"
                                               subtitle:@"Invalid Quantity"
                                                   type:TSMessageNotificationTypeWarning];
        } else {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [[BESharedManager sharedInstance] be_addProductToCartWithProductId:self.product.productId
                                                                          quantity:[textField.text integerValue]
                                                                    withCompletion:^(BOOL finished)
                 {
                     if(finished) {
                         [TSMessage showNotificationWithTitle:@"Success"
                                                     subtitle:@"Item was added to cart."
                                                         type:TSMessageNotificationTypeSuccess];
                     } else {
                         [TSMessage showNotificationWithTitle:@"Failed"
                                                     subtitle:@"Item was not added to cart."
                                                         type:TSMessageNotificationTypeError];
                         
                     }
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [MBProgressHUD hideHUDForView:self.view animated:YES];
                     });
                 }];
            });
        }
    }];
    
    [alertView showEdit:@"Enter Quantity"
               subTitle:nil
       closeButtonTitle:nil
               duration:0.0f];
}

#pragma mark ASTextNodeDelegate methods.

- (BOOL)textNode:(ASTextNode *)richTextNode shouldHighlightLinkAttribute:(NSString *)attribute value:(id)value atPoint:(CGPoint)point
{
    return YES;
}

- (void)textNode:(ASTextNode *)textNode tappedLinkAttribute:(NSString *)attribute value:(id)value atPoint:(CGPoint)point textRange:(NSRange)textRange {
    
}

#pragma mark - ASNetworkImageNodeDelegate methods.

- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image
{
    //[self setNeedsLayout];
}

- (void)imageNode:(ASNetworkImageNode *)imageNode didFailWithError:(NSError *)error {
    //[self setNeedsLayout];
}

@end
