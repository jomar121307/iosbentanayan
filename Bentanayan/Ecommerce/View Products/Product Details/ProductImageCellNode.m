//
//  ProductImageCellNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ProductImageCellNode.h"

@implementation ProductImageCellNode 

- (instancetype)initWithPhotoUrl:(NSURL *)photoUrl {
    self = [super init];
    if(self) {
        
        self.backgroundColor = [UIColor randomFlatColor];
        
        // Product Image
        _productImageNode = [[ASNetworkImageNode alloc] init];
        _productImageNode.URL = photoUrl;
        _productImageNode.delegate = self;
        _productImageNode.defaultImage = [UIImage imageNamed:@"product-placeholder"];
        
        [self addSubnode:_productImageNode];
    }
    return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // Image Ratio
    ASRatioLayoutSpec *ratioImage = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:1.0
                                                                          child:_productImageNode];
    return ratioImage;
}

#pragma mark - ASNetworkImageNodeDelegate methods.

- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image
{
    //[self setNeedsLayout];
}

- (void)imageNode:(ASNetworkImageNode *)imageNode didFailWithError:(NSError *)error {
    //[self setNeedsLayout];
}

@end
