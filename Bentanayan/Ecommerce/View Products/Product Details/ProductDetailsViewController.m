//
//  ProductDetailsViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ProductDetailsViewController.h"

#import "ProductDetailsCellNode.h"

#import "ProductReviewViewController.h"
#import "ShopBarViewController.h"

@implementation ProductDetailsViewController

- (instancetype)initWithProductModel:(ProductModel *)product
{
    self = [super init];
    if(self) {
        self.product = product;
        
        // CollectionView
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        
        self.collectionView = [[ASCollectionView alloc] initWithFrame:CGRectZero
                                                 collectionViewLayout:layout];
        self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.collectionView.asyncDataSource = self;
        self.collectionView.asyncDelegate = self;
        self.collectionView.alwaysBounceVertical = YES;
        self.collectionView.backgroundColor = [UIColor clearColor];
        
        [self.view addSubview:self.collectionView];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor flatWhiteColor];
    
    // Navigation
    self.navigationItem.title = @"Product Details";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Back
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(goBack)];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    // Home
    UIImage *homeImage = [[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    CGRect frameimg = CGRectMake(0, 0, homeImage.size.width, homeImage.size.height);
    UIButton *homeButton = [[UIButton alloc] initWithFrame:frameimg];
    [homeButton setBackgroundImage:homeImage forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(goToHome) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
}

- (void)viewWillLayoutSubviews
{
    self.collectionView.frame = self.view.bounds;
}

#pragma mark - ASCollectionView data source.

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductDetailsCellNode *cellNode = [[ProductDetailsCellNode alloc] initWithProductModel:self.product];
    [cellNode.reviewButtonNode addTarget:self
                                  action:@selector(goToProuctReview:)
                        forControlEvents:ASControlNodeEventTouchUpInside];
    
    [cellNode.productOwnerLabelNode addTarget:self
                                       action:@selector(goToShopPage:)
                             forControlEvents:ASControlNodeEventTouchUpInside];
    
    return cellNode;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = collectionView.bounds.size.width;
    return ASSizeRangeMake(CGSizeMake(width, 0.0f), CGSizeMake(width, FLT_MAX));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionViewLockDataSource:(ASCollectionView *)collectionView
{
    // lock the data source
    // The data source should not be change until it is unlocked.
}

- (void)collectionViewUnlockDataSource:(ASCollectionView *)collectionView
{
    // unlock the data source to enable data source updating.
}

- (void)collectionView:(UICollectionView *)collectionView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    NSLog(@"fetch additional content");
    [context completeBatchFetching:YES];
}


#pragma mark - Methods / Selectors

- (void)goToHome {
    NSLog(@"Home");
    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
}

- (void)goBack {
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

- (void)goToProuctReview:(ASButtonNode *)button {
    ProductReviewViewController *productReviewController = [[ProductReviewViewController alloc] initWithProductModel:self.product];
    [[SlideNavigationController sharedInstance] pushViewController:productReviewController
                                                          animated:YES];
}

- (void)goToShopPage:(ASTextNode *)textNode {
    if([[self backViewController] isKindOfClass:[ShopBarViewController class]]) {
        NSLog(@"is currently on user's shop page");
        return;
    } else {
        ShopBarViewController *shopBarController = [[ShopBarViewController alloc] initWithShopOwner:self.product.userId];
        [[SlideNavigationController sharedInstance] pushViewController:shopBarController
                                                              animated:YES];
    }
}

- (UIViewController *)backViewController
{
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers < 2)
        return nil;
    else
        return [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
