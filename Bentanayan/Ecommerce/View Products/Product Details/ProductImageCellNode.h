//
//  ProductImageCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface ProductImageCellNode : ASCellNode <ASNetworkImageNodeDelegate> {
    ASNetworkImageNode *_productImageNode;
}

- (instancetype)initWithPhotoUrl:(NSURL *)photoUrl;

@end
