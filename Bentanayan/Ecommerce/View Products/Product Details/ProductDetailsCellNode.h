//
//  ProductDetailsCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface ProductDetailsCellNode : ASCellNode <ASCollectionViewDataSource, ASCollectionViewDelegateFlowLayout, ASNetworkImageNodeDelegate, ASTextNodeDelegate> {
    ASDisplayNode *_contentNode;
    ASDisplayNode *_contentCollectionNode;
    
    ASNetworkImageNode *_productImageNode;
    
    ASTextNode *_productNameLabelNode;
    ASTextNode *_productReviewLabelNode;
    //ASTextNode *_productOwnerLabelNode;
    
    ASTextNode *_productStockLabelNode;
    ASTextNode *_productDescriptionLabelNode;
    
    ASTextNode *_productPriceLabelNode;
    ASTextNode *_perItemLabelNode;
    ASTextNode *_quantityLabelNode;
    
    ASEditableTextNode *_editQuantityLabelNode;
}

@property (strong, nonatomic) ASTextNode *productOwnerLabelNode;

@property (strong, nonatomic) ASCollectionView *collectionView;
@property (strong, nonatomic) ASButtonNode *reviewButtonNode;
@property (strong, nonatomic) ASButtonNode *addToCartButtonNode;

- (instancetype)initWithProductModel:(ProductModel *)product;

@property (strong, nonatomic) ProductModel *product;

@end
