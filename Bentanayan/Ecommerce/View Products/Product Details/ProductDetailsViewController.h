//
//  ProductDetailsViewController.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface ProductDetailsViewController : UIViewController <ASCollectionViewDataSource, ASCollectionViewDelegateFlowLayout>

@property (strong, nonatomic) ASCollectionView *collectionView;

- (instancetype)initWithProductModel:(ProductModel *)product;

@property (strong, nonatomic) ProductModel *product;

@end
