//
//  ProductReviewDetailCellNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ProductReviewDetailCellNode.h"

@implementation ProductReviewDetailCellNode

- (instancetype)initWithProductModel:(ProductModel *)product {
    self = [super init];
    if(self) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.product = product;
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        // Product Image
        _productImageNode = [[ASNetworkImageNode alloc] init];
        if(self.product.metadata.photos) {
            NSArray *photos = self.product.metadata.photos;
            if(photos.count > 0) {
                ProductPhotoModel *photo = [photos firstObject];
                if(photo.cropped) {
                    _productImageNode.URL = [NSURL URLWithString:photo.cropped.secure_url];
                } else if(photo.original) {
                    _productImageNode.URL = [NSURL URLWithString:photo.original.secure_url];
                } else {
                    _productImageNode.URL = [NSURL URLWithString:photo.secure_url];
                }
            }
        }
        _productImageNode.delegate = self;
        _productImageNode.defaultImage = [UIImage imageNamed:@"product-placeholder"];
        _productImageNode.contentMode = UIViewContentModeScaleAspectFit;
        
        [self addSubnode:_productImageNode];
        
        // Product Name Label
        NSDictionary *attrsProductName = @{
                                           NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Bold" size:16.0f],
                                           NSForegroundColorAttributeName: [UIColor flatBlackColorDark],
                                           NSParagraphStyleAttributeName : paragraphStyleLeft
                                           };
        
        NSAttributedString *stringProductName = [[NSAttributedString alloc] initWithString:self.product.name
                                                                                attributes:attrsProductName];
        
        
        _productNameLabelNode = [[ASTextNode alloc] init];
        _productNameLabelNode.attributedText = stringProductName;
        _productNameLabelNode.maximumNumberOfLines = 2;
        _productNameLabelNode.pointSizeScaleFactors = @[@0.9, @0.8, @0.7, @0.6];
        
        [self addSubnode:_productNameLabelNode];
        
        // Product Review Label
        NSDictionary *attrsProductReview = @{
                                             NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Light" size:14.0f],
                                             NSForegroundColorAttributeName: [UIColor flatGrayColorDark],
                                             NSParagraphStyleAttributeName : paragraphStyleLeft
                                             };
        
        NSAttributedString *stringProductReview = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"Rating: %0.1f/5.0", self.product.reviewScore]
                                                                                  attributes:attrsProductReview];
        
        
        _productReviewLabelNode = [[ASTextNode alloc] init];
        _productReviewLabelNode.attributedText = stringProductReview;
        _productReviewLabelNode.maximumNumberOfLines = 1;
        _productReviewLabelNode.pointSizeScaleFactors = @[@0.9, @0.8, @0.7, @0.6];
        
        [self addSubnode:_productReviewLabelNode];
        
        // Product Owner Label
        NSDictionary *attrsProductOwner = @{
                                            NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                            NSForegroundColorAttributeName: [UIColor flatGrayColorDark],
                                            NSParagraphStyleAttributeName : paragraphStyleLeft
                                            };
        
        NSString *productOwner;
        NSString *firstName = self.product.userId.firstName;
        NSString *lastName = self.product.userId.lastName;
        if(lastName != nil) {
            productOwner = [NSString stringWithFormat:@"By: %@ %@", firstName, lastName];
        } else {
            productOwner = [NSString stringWithFormat:@"By: %@", firstName];
        }
        NSAttributedString *stringProductOwner = [[NSAttributedString alloc] initWithString:productOwner
                                                                                 attributes:attrsProductOwner];
        
        
        _productOwnerLabelNode = [[ASTextNode alloc] init];
        _productOwnerLabelNode.attributedText = stringProductOwner;
        _productOwnerLabelNode.maximumNumberOfLines = 1;
        _productOwnerLabelNode.pointSizeScaleFactors = @[@0.9, @0.8, @0.7, @0.6];
        
        [self addSubnode:_productOwnerLabelNode];
        
        // Border Node
        _borderNode = [[ASDisplayNode alloc] init];
        _borderNode.backgroundColor = [UIColor flatGrayColorDark];
        
        [self addSubnode:_borderNode];
    }
    return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    
    // Product Image
    ASRatioLayoutSpec *ratioImage = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:1.0
                                                                          child:_productImageNode];
    ratioImage.flexBasis = ASRelativeDimensionMakeWithPercent(0.6);
    
    ASStackLayoutSpec *stackImage = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                            spacing:0.0
                                                                     justifyContent:ASStackLayoutJustifyContentCenter
                                                                         alignItems:ASStackLayoutAlignItemsStretch
                                                                           children:@[ratioImage]];
    
    // Main Content
    ASStackLayoutSpec *stackContent = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                              spacing:2.0
                                                                       justifyContent:ASStackLayoutJustifyContentStart
                                                                           alignItems:ASStackLayoutAlignItemsStretch
                                                                             children:@[stackImage,
                                                                                        _productNameLabelNode,
                                                                                        _productReviewLabelNode,
                                                                                        _productOwnerLabelNode]];
    
    ASInsetLayoutSpec *insetContent = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(10, 10, 10, 10)
                                                                             child:stackContent];
    
    // Main with border
    _borderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0f);
    _borderNode.alignSelf = ASStackLayoutAlignSelfEnd;
    
    ASStackLayoutSpec *stackMainBorder = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                                 spacing:0.0
                                                                          justifyContent:ASStackLayoutJustifyContentStart
                                                                              alignItems:ASStackLayoutAlignItemsStretch
                                                                                children:@[insetContent, _borderNode]];
    return stackMainBorder;
}

#pragma mark - ASNetworkImageNodeDelegate methods.

- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image
{
    //[self setNeedsLayout];
}

- (void)imageNode:(ASNetworkImageNode *)imageNode didFailWithError:(NSError *)error {
    //[self setNeedsLayout];
}

@end
