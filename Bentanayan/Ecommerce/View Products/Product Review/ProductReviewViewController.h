//
//  ProductReviewViewController.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductReviewViewController : UIViewController <ASCollectionViewDataSource, ASCollectionViewDelegateFlowLayout>

@property (strong, nonatomic) ASCollectionView *collectionView;

@property (strong, nonatomic) ProductModel *product;

@property (strong, nonatomic) NSMutableArray *productReviewsResult;

// Review Input
@property (strong, nonatomic) ASDisplayNode *inputReviewDisplayNode;

// Initialize
- (instancetype)initWithProductModel:(ProductModel *)product;

@end
