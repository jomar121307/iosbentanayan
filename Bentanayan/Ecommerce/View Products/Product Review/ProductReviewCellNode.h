//
//  ProductReviewCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface ProductReviewCellNode : ASCellNode <ASNetworkImageNodeDelegate> {
    ASNetworkImageNode *_userImageNode;
    
    ASTextNode *_userLabelNode;
    ASTextNode *_ratingLabelNode;
    ASTextNode *_reviewLabelNode;
    ASTextNode *_timestampLabelNode;
    
    ASDisplayNode *_borderNode;
}

@property (strong, nonatomic) ProductReviewModel *productReview;

- (instancetype)initWithProductReview:(ProductReviewModel *)productReview;

@end
