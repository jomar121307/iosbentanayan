//
//  ProductReviewDetailCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface ProductReviewDetailCellNode : ASCellNode <ASNetworkImageNodeDelegate> {
    ASNetworkImageNode *_productImageNode;
    
    ASTextNode *_productNameLabelNode;
    ASTextNode *_productReviewLabelNode;
    ASTextNode *_productOwnerLabelNode;
    
    ASDisplayNode *_borderNode;
}

- (instancetype)initWithProductModel:(ProductModel *)product;

@property (strong, nonatomic) ProductModel *product;

@end
