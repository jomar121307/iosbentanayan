//
//  ProductReviewCellNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ProductReviewCellNode.h"

@implementation ProductReviewCellNode

- (instancetype)initWithProductReview:(ProductReviewModel *)productReview
{
    self = [super init];
    if(self) {
        
        self.productReview = productReview;
        
        // User Avatar
        _userImageNode = [[ASNetworkImageNode alloc] init];
        _userImageNode.URL = [NSURL URLWithString:self.productReview.userId.profilePhoto.original.secure_url];
        _userImageNode.delegate = self;
        _userImageNode.cornerRadius = 24.0f;
        _userImageNode.defaultImage = [UIImage imageNamed:@"avatar-placeholder"];
        _userImageNode.backgroundColor = [UIColor clearColor];
        _userImageNode.imageModificationBlock = ^UIImage *(UIImage *image) {
            UIImage *modifiedImage;
            CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
            UIGraphicsBeginImageContextWithOptions(image.size, false, [[UIScreen mainScreen] scale]);
            [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:48.0] addClip];
            [image drawInRect:rect];
            modifiedImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return modifiedImage;
        };
        
        [self addSubnode:_userImageNode];
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        // User Name Label
        NSDictionary *attrsUser = @{
                                    NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Bold" size:14.0f],
                                    NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                    NSParagraphStyleAttributeName : paragraphStyleLeft
                                    };
        
        NSString *ownerName;
        NSString *firstName = productReview.userId.firstName;
        NSString *lastName = productReview.userId.lastName;
        if(lastName != nil) {
            ownerName = [NSString stringWithFormat:@"By: %@ %@", firstName, lastName];
        } else {
            ownerName = [NSString stringWithFormat:@"By: %@", firstName];
        }
        NSAttributedString *stringUser = [[NSAttributedString alloc] initWithString:ownerName
                                                                         attributes:attrsUser];
        
        
        _userLabelNode = [[ASTextNode alloc] init];
        _userLabelNode.attributedText = stringUser;
        _userLabelNode.maximumNumberOfLines = 2;
        _userLabelNode.pointSizeScaleFactors = @[@0.95, @0.9];
        
        [self addSubnode:_userLabelNode];
        
        // Rating Label
        NSDictionary *attrsRating = @{
                                      NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Light" size:14.0f],
                                      NSForegroundColorAttributeName: [UIColor flatBlackColorDark],
                                      NSParagraphStyleAttributeName : paragraphStyleLeft
                                      };
        
        NSString *rating = [NSString stringWithFormat:@"Rating: %0.1f/5.0", self.productReview.rating];
        NSAttributedString *stringRating = [[NSAttributedString alloc] initWithString:rating
                                                                           attributes:attrsRating];
        
        
        _ratingLabelNode = [[ASTextNode alloc] init];
        _ratingLabelNode.attributedText = stringRating;
        _ratingLabelNode.maximumNumberOfLines = 1;
        _ratingLabelNode.pointSizeScaleFactors = @[@0.95, @0.9];
        
        [self addSubnode:_ratingLabelNode];
        
        // Content Label
        NSDictionary *attrsReview = @{
                                      NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                      NSForegroundColorAttributeName: [UIColor flatBlackColor]
                                      };
        
        NSAttributedString *stringReview = [[NSAttributedString alloc] initWithString:[self.productReview.content stringByRemovingPercentEncoding]
                                                                           attributes:attrsReview];
        
        _reviewLabelNode = [[ASTextNode alloc] init];
        _reviewLabelNode.attributedText = stringReview;
        _reviewLabelNode.truncationMode = NSLineBreakByTruncatingTail;
        _reviewLabelNode.maximumNumberOfLines = 4;
        
        [self addSubnode:_reviewLabelNode];
        
        // Timestamp Label
        NSDictionary *attrsTimestamp = @{
                                         NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Light" size:12.0f],
                                         NSForegroundColorAttributeName: [UIColor flatGrayColorDark],
                                         NSParagraphStyleAttributeName : paragraphStyleLeft
                                         };
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:BENTA_DATE_FORMAT];
        NSDate *createdAtDate = [formatter dateFromString:self.productReview.createdAt];
        
        NSAttributedString *stringTimestamp = [[NSAttributedString alloc] initWithString:[createdAtDate timeAgoSinceNow]
                                                                              attributes:attrsTimestamp];
        
        
        _timestampLabelNode = [[ASTextNode alloc] init];
        _timestampLabelNode.attributedText = stringTimestamp;
        _timestampLabelNode.maximumNumberOfLines = 1;
        _timestampLabelNode.pointSizeScaleFactors = @[@0.9, @0.8, @0.7, @0.6];
        
        [self addSubnode:_timestampLabelNode];
        
        // Border Node
        _borderNode = [[ASDisplayNode alloc] init];
        _borderNode.backgroundColor = [UIColor flatGrayColorDark];
        
        [self addSubnode:_borderNode];
    }
    return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // Content
    ASStackLayoutSpec *stackContent = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                              spacing:0.0
                                                                       justifyContent:ASStackLayoutJustifyContentStart
                                                                           alignItems:ASStackLayoutAlignItemsStart
                                                                             children:@[_userLabelNode,
                                                                                        _ratingLabelNode,
                                                                                        _reviewLabelNode,
                                                                                        _timestampLabelNode]];
    
    // Avatar
    ASRatioLayoutSpec *ratioUserImage = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:1.0
                                                                              child:_userImageNode];
    ratioUserImage.flexBasis = ASRelativeDimensionMakeWithPoints(48.0f);
    ratioUserImage.alignSelf = ASStackLayoutAlignSelfCenter;
    
    // Main
    stackContent.flexShrink = YES;
    stackContent.flexGrow = YES;
    ASStackLayoutSpec *stackMain = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                           spacing:10.0f
                                                                    justifyContent:ASStackLayoutJustifyContentCenter
                                                                        alignItems:ASStackLayoutAlignItemsStretch
                                                                          children:@[ratioUserImage,
                                                                                     stackContent]];
    
    ASInsetLayoutSpec *insetMain = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(8, 8, 8, 8)
                                                                          child:stackMain];
    
    // Border Node
    _borderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0f);
    _borderNode.alignSelf = ASStackLayoutAlignSelfEnd;
    ASStackLayoutSpec *stackBorder = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                             spacing:0.0
                                                                      justifyContent:ASStackLayoutJustifyContentStart
                                                                          alignItems:ASStackLayoutAlignItemsStretch
                                                                            children:@[insetMain, _borderNode]];
    
    return stackBorder;
}

#pragma mark - ASNetworkImageNodeDelegate methods.

- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image
{
    //[self setNeedsLayout];
}

- (void)imageNode:(ASNetworkImageNode *)imageNode didFailWithError:(NSError *)error {
    //[self setNeedsLayout];
}
@end
