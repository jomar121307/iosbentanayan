//
//  ProductListViewController
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 12/05/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface ProductListViewController : ASViewController <ASCollectionViewDataSource, ASCollectionViewDelegateFlowLayout, ASTableDelegate, ASTableDataSource, UISearchBarDelegate> {
    ASDisplayNode *_filterDisplayNode;
}

@property (strong, nonatomic) ASTableNode *tableNode;

@property (strong, nonatomic) ASDisplayNode *searchDisplayNode;
@property (strong, nonatomic) ASButtonNode *filterButtonNode;
@property (strong, nonatomic) ASDisplayNode *collectionDisplayNode;

@property (strong, nonatomic) NSMutableArray *productCategoriesResult;

@property (strong, nonatomic) CategoryModel *category;
@property (strong, nonatomic) SubcategoryModel *subcategory;
@property (strong, nonatomic) GrouplingModel *groupling;

// Filter
@property (strong, nonatomic) NSString *searchString;

@property (assign, nonatomic) FilterSortOptions currentFilterOption;
@property (assign, nonatomic) FilterSortOptions selectedFilterOption;

- (instancetype)initWithCategory:(CategoryModel *)category
                     subCategory:(SubcategoryModel *)subcategory
                       groupling:(GrouplingModel *)groupling;

@end
