//
//  ProductListViewController+Beta.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 12/05/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ProductListViewController.h"

#import "ProductCategoryCellNode.h"

#import "FilterCellNode.h"
#import "SortCellNode.h"
#import "PriceRangeCellNode.h"

#import "ProductDetailsViewController.h"

@interface ProductListViewController ()

@end

@implementation ProductListViewController

- (instancetype)initWithCategory:(CategoryModel *)category
                     subCategory:(SubcategoryModel *)subcategory
                       groupling:(GrouplingModel *)groupling
{
    ASDisplayNode *node = [[ASDisplayNode alloc] init];
    node.backgroundColor = [UIColor flatWhiteColor];
    
    self = [super initWithNode:node];
    if(self) {
        
        self.category = category;
        self.subcategory = subcategory;
        self.groupling = groupling;
        
        // Search Bar Display
        self.searchDisplayNode = [[ASDisplayNode alloc] initWithViewBlock:^UIView * _Nonnull{
            UISearchBar *searchBar = [[UISearchBar alloc] init];
            searchBar.barStyle = UIBarStyleBlack;
            searchBar.searchBarStyle = UISearchBarStyleMinimal;
            searchBar.delegate = self;
            return searchBar;
        }];
        
        [self.node addSubnode:self.searchDisplayNode];
        
        // Filter Button
        self.filterButtonNode = [[ASButtonNode alloc] init];
        [self.filterButtonNode setImage:[UIImage imageNamed:@"filter"]
                               forState:ASControlStateNormal];
        [self.filterButtonNode addTarget:self
                                  action:@selector(showFilter)
                        forControlEvents:ASControlNodeEventTouchUpInside];
        [self.filterButtonNode setHitTestSlop:UIEdgeInsetsMake(-2, -4, -2, -4)];
        self.filterButtonNode.imageNode.contentMode = UIViewContentModeScaleAspectFit;
        self.filterButtonNode.imageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor flatBlackColor]);
        self.filterButtonNode.selected = NO;
        
        [self.node addSubnode:self.filterButtonNode];
        
        // Collection View
        self.collectionDisplayNode = [[ASDisplayNode alloc] initWithViewBlock:^UIView * _Nonnull{
            UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
            layout.sectionInset = UIEdgeInsetsMake(0, 12, 12, 12);
            layout.scrollDirection = UICollectionViewScrollDirectionVertical;
            layout.minimumInteritemSpacing = 12;
            layout.minimumLineSpacing = 12;
            
            ASCollectionView *collectionView = [[ASCollectionView alloc] initWithCollectionViewLayout:layout];
            collectionView.asyncDataSource = self;
            collectionView.asyncDelegate = self;
            collectionView.backgroundColor = [UIColor clearColor];
            collectionView.alwaysBounceVertical = YES;
            
            return collectionView;
        }];
        
        [self.node addSubnode:self.collectionDisplayNode];
        
        // Filter Node
        self.currentFilterOption = 0;
        self.selectedFilterOption = 0;
        self.tableNode = [[ASTableNode alloc] initWithStyle:UITableViewStylePlain];
        self.tableNode.dataSource = self;
        self.tableNode.delegate = self;
        self.tableNode.backgroundColor = [UIColor whiteColor];
        self.tableNode.view.scrollEnabled = NO;
        
        [self.node addSubnode:_tableNode];
        
        // Layout Spec Block
        __weak ProductListViewController *weakSelf = self;
        
        self.node.layoutSpecBlock = ^ASLayoutSpec *(ASDisplayNode * _Nonnull node, ASSizeRange constrainedSize) {
            ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
            spacer.flexGrow = YES;
            
            weakSelf.searchDisplayNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 44);
            weakSelf.searchDisplayNode.flexShrink = YES;
            weakSelf.searchDisplayNode.flexGrow = YES;
            weakSelf.filterButtonNode.preferredFrameSize = CGSizeMake(44, 44);
            
            ASStackLayoutSpec *stackFilter = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                     spacing:2.0f
                                                                              justifyContent:ASStackLayoutJustifyContentStart
                                                                                  alignItems:ASStackLayoutAlignItemsCenter
                                                                                    children:@[weakSelf.searchDisplayNode,
                                                                                               spacer,
                                                                                               weakSelf.filterButtonNode]];
            
            ASInsetLayoutSpec *insetFilter = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(0, 0, 0, 8)
                                                                                    child:stackFilter];
            
            weakSelf.collectionDisplayNode.flexGrow = YES;
            weakSelf.collectionDisplayNode.flexShrink = YES;
            weakSelf.collectionDisplayNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, constrainedSize.max.height);
            ASStackLayoutSpec *stackContent = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                                      spacing:0.0f
                                                                               justifyContent:ASStackLayoutJustifyContentStart
                                                                                   alignItems:ASStackLayoutAlignItemsStart
                                                                                     children:@[insetFilter,
                                                                                                weakSelf.collectionDisplayNode]];
            return stackContent;
        };
        
        // Data
        self.productCategoriesResult = [NSMutableArray new];
        
        [[BESharedManager sharedInstance] be_productsWithCategory:self.category.slug
                                                  withSubcategory:self.subcategory.groupheader.slug
                                                    withGroupling:self.groupling.slug
                                             withFilterSortOption:0
                                                   withCompletion:^(BOOL finished, NSArray *result)
         {
             if(finished) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     ASCollectionView *weakCollectionView = (ASCollectionView *)weakSelf.collectionDisplayNode.view;
                     [weakCollectionView performBatchUpdates:^{
                         NSMutableArray *insertIndexPaths = [NSMutableArray array];
                         NSMutableArray *deleteIndexPaths = [NSMutableArray array];
                         
                         NSInteger section = 0;
                         
                         NSArray *oldData = [weakSelf.productCategoriesResult copy];
                         for(ProductModel *oldModel in oldData) {
                             NSInteger oldItem = [oldData indexOfObject:oldModel];
                             [weakSelf.productCategoriesResult removeObject:oldModel];
                             [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
                         }
                         
                         for(ProductModel *newModel in result) {
                             [weakSelf.productCategoriesResult addObject:newModel];
                             NSInteger item = [weakSelf.productCategoriesResult indexOfObject:newModel];
                             [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
                         }
                         
                         [weakCollectionView deleteItemsAtIndexPaths:deleteIndexPaths];
                         [weakCollectionView insertItemsAtIndexPaths:insertIndexPaths];
                     } completion:nil
                      ];
                 });
             }
         }];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor flatWhiteColor];
    
    // Navigation
    if(self.subcategory == nil && self.groupling == nil) {
        self.navigationItem.title = self.category.name;
    } else if(self.groupling == nil) {
        self.navigationItem.title = self.subcategory.groupheader.name;
    } else {
        self.navigationItem.title = self.groupling.name;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Back
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(goBack)];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    // Home
    UIImage *homeImage = [[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    CGRect frameimg = CGRectMake(0, 0, homeImage.size.width, homeImage.size.height);
    UIButton *homeButton = [[UIButton alloc] initWithFrame:frameimg];
    [homeButton setBackgroundImage:homeImage forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(goToHome) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
}

#pragma mark - ASCollectionView data source.

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductCategoryCellNode *cellNode;
    ProductModel *product = (ProductModel *)[self.productCategoriesResult objectAtIndex:indexPath.row];
    cellNode = [[ProductCategoryCellNode alloc] initWithProduct:product];
    return cellNode;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width;
    CGFloat height;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        width = ((collectionView.bounds.size.width - 36.0f) / 3.0f) - 4.0f;
        height = 294.0f;
    } else {
        width = (collectionView.bounds.size.width - 36.0f) / 2.0f;
        height = 234.0f;
    }
    
    return ASSizeRangeMake(CGSizeMake(width, height), CGSizeMake(width, height));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.productCategoriesResult.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionViewLockDataSource:(ASCollectionView *)collectionView
{
    // lock the data source
    // The data source should not be change until it is unlocked.
}

- (void)collectionViewUnlockDataSource:(ASCollectionView *)collectionView
{
    // unlock the data source to enable data source updating.
}

- (void)collectionView:(UICollectionView *)collectionView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    NSLog(@"fetch additional content");
    [context completeBatchFetching:YES];
}

#pragma mark - ASCollectionView delegate.

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductModel *product = (ProductModel *)[self.productCategoriesResult objectAtIndex:indexPath.row];
    ProductDetailsViewController *productDetailsController = [[ProductDetailsViewController alloc] initWithProductModel:product];
    [[SlideNavigationController sharedInstance] pushViewController:productDetailsController
                                                          animated:YES];
}

- (void)scrollViewWillEndDragging:(UIScrollView*)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint*)targetContentOffset
{
    if((velocity.y < - 1.75f) || (velocity.y > 1.75f)) {
        if(self.filterButtonNode.selected) {
            [self showFilter];
        }
    }
}

#pragma mark - ASTableNode data source.

- (ASCellNode *)tableView:(ASTableView *)tableView nodeForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FilterCellNode *filterCellNode;
    SortCellNode *sortCellNode;
    PriceRangeCellNode *priceRangeCellNode;
    ASTextCellNode *textCellNode;
    
    // Attrs
    NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
    paragraphStyleLeft.alignment = NSTextAlignmentLeft;
    
    // Filter Label
    NSDictionary *attrsText = @{
                                NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Bold" size:14.0f],
                                NSForegroundColorAttributeName: [UIColor flatBlackColorDark],
                                NSParagraphStyleAttributeName : paragraphStyleLeft
                                };
    
    switch(indexPath.section) {
        case 0:
            filterCellNode = [[FilterCellNode alloc] init];
            filterCellNode.selectionStyle = UITableViewCellSelectionStyleNone;
            [filterCellNode.applyButtonNode addTarget:self
                                               action:@selector(applyFilter:)
                                     forControlEvents:ASControlNodeEventTouchUpInside];
            [filterCellNode.resetButtonNode addTarget:self
                                               action:@selector(resetFilter:)
                                     forControlEvents:ASControlNodeEventTouchUpInside];
            return filterCellNode;
            break;
        case 1:
            textCellNode = [[ASTextCellNode alloc] init];
            textCellNode.textAttributes = attrsText;
            textCellNode.text = @"SORT";
            textCellNode.backgroundColor = [UIColor flatWhiteColor];
            textCellNode.selectionStyle = UITableViewCellSelectionStyleNone;
            return textCellNode;
            break;
        case 2:
            sortCellNode = [[SortCellNode alloc] init];
            if(indexPath.row == self.currentFilterOption) {
                sortCellNode.sortImageNode.image = [UIImage imageNamed:@"selected"];
            } else {
                sortCellNode.sortImageNode.image = [UIImage new];
            }
            if(indexPath.row == 0) {
                sortCellNode.sortLabelNode.attributedText = [self sortAttributedString:@"Most Recent"];
            } else {
                if(indexPath.row == 1) {
                    sortCellNode.sortLabelNode.attributedText = [self sortAttributedString:@"Price: Low to High"];
                } else if(indexPath.row == 2) {
                    sortCellNode.sortLabelNode.attributedText = [self sortAttributedString:@"Price: Hight to Low"];
                }
            }
            sortCellNode.selectionStyle = UITableViewCellSelectionStyleNone;
            return sortCellNode;
            break;
        case 3:
            textCellNode = [[ASTextCellNode alloc] init];
            textCellNode.textAttributes = attrsText;
            textCellNode.text = @"PRICE RANGE";
            textCellNode.backgroundColor = [UIColor flatWhiteColor];
            textCellNode.selectionStyle = UITableViewCellSelectionStyleNone;
            return textCellNode;
            break;
        case 4:
            priceRangeCellNode = [[PriceRangeCellNode alloc] init];
            priceRangeCellNode.selectionStyle = UITableViewCellSelectionStyleNone;
            return priceRangeCellNode;
            break;
        default:
            return [[ASCellNode alloc] init];
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch(section) {
        case 0:
            return 1;
            break;
        case 1:
            return 1;
            break;
        case 2:
            return 3;
            break;
        case 3:
            return 1;
            break;
        case 4:
            return 1;
            break;
        default:
            return 0;
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (void)tableViewLockDataSource:(ASTableView *)tableView
{
    // lock the data source
    // The data source should not be change until it is unlocked.
}

- (void)tableViewUnlockDataSource:(ASTableView *)tableView
{
    // unlock the data source to enable data source updating.
}

- (void)tableView:(ASTableView *)tableView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    NSLog(@"fetch additional content");
    [context completeBatchFetching:YES];
}

#pragma mark - ASTableNode delegate.

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 2) {
        SortCellNode *sortCellNodeOne = (SortCellNode *)[self.tableNode.view nodeForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
        SortCellNode *sortCellNodeTwo = (SortCellNode *)[self.tableNode.view nodeForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
        SortCellNode *sortCellNodeThree = (SortCellNode *)[self.tableNode.view nodeForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:2]];
        
        self.selectedFilterOption = indexPath.row;
        
        switch(indexPath.row) {
            case 0:
                sortCellNodeOne.sortImageNode.image = [UIImage imageNamed:@"selected"];
                sortCellNodeTwo.sortImageNode.image = [UIImage new];
                sortCellNodeThree.sortImageNode.image = [UIImage new];
                break;
            case 1:
                sortCellNodeOne.sortImageNode.image = [UIImage new];
                sortCellNodeTwo.sortImageNode.image = [UIImage imageNamed:@"selected"];
                sortCellNodeThree.sortImageNode.image = [UIImage new];
                break;
            case 2:
                sortCellNodeOne.sortImageNode.image = [UIImage new];
                sortCellNodeTwo.sortImageNode.image = [UIImage new];
                sortCellNodeThree.sortImageNode.image = [UIImage imageNamed:@"selected"];
                break;
            default:
                break;
        }
        
        [sortCellNodeOne setNeedsLayout];
        [sortCellNodeTwo setNeedsLayout];
        [sortCellNodeThree setNeedsLayout];
        
        //        __weak ProductListViewController *weakSelf = self;
        
        //        dispatch_async(dispatch_get_main_queue(), ^{
        //            [weakSelf.tableNode.view beginUpdates];
        //            [weakSelf.tableNode.view reloadSections:[NSIndexSet indexSetWithIndex:2]
        //                                   withRowAnimation:UITableViewRowAnimationFade];
        //            [weakSelf.tableNode.view waitUntilAllUpdatesAreCommitted];
        //            [weakSelf.tableNode.view endUpdates];
        //        });
    }
}

#pragma mark - UISearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [self performSelector:@selector(sendSearchRequest:)
               withObject:searchText
               afterDelay:1.0f];
}

#pragma mark - Methods / Selectors

- (void)sendSearchRequest:(id)text {
    if(![text isKindOfClass:[NSString class]]) {
        return;
    }
    
    self.searchString = (NSString *)text;
    
    __weak ProductListViewController *weakSelf = self;
    
    PriceRangeCellNode *priceRangeCellNode = (PriceRangeCellNode *)[self.tableNode.view nodeForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:4]];
    
    [[BESharedManager sharedInstance] be_productsWithCategory:self.category.slug
                                              withSubcategory:self.subcategory.groupheader.slug
                                                withGroupling:self.groupling.slug
                                         withFilterSortOption:self.selectedFilterOption
                                                 withMinPrice:priceRangeCellNode.sliderView.selectedMinimum
                                                 withMaxPrice:priceRangeCellNode.sliderView.selectedMaximum
                                             withSearchString:(NSString *)text
                                               withCompletion:^(BOOL finished, NSArray *result)
     {
         if(finished) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 ASCollectionView *weakCollectionView = (ASCollectionView *)weakSelf.collectionDisplayNode.view;
                 [weakCollectionView performBatchUpdates:^{
                     NSMutableArray *insertIndexPaths = [NSMutableArray array];
                     NSMutableArray *deleteIndexPaths = [NSMutableArray array];
                     
                     NSInteger section = 0;
                     
                     NSArray *oldData = [weakSelf.productCategoriesResult copy];
                     for(ProductModel *oldModel in oldData) {
                         NSInteger oldItem = [oldData indexOfObject:oldModel];
                         [weakSelf.productCategoriesResult removeObject:oldModel];
                         [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
                     }
                     
                     for(ProductModel *newModel in result) {
                         [weakSelf.productCategoriesResult addObject:newModel];
                         NSInteger item = [weakSelf.productCategoriesResult indexOfObject:newModel];
                         [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
                     }
                     
                     [weakCollectionView deleteItemsAtIndexPaths:deleteIndexPaths];
                     [weakCollectionView insertItemsAtIndexPaths:insertIndexPaths];
                 } completion:nil
                  ];
             });
         }
     }];
}

- (void)applyFilter:(ASButtonNode *)button {
    PriceRangeCellNode *priceRangeCellNode = (PriceRangeCellNode *)[self.tableNode.view nodeForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:4]];
    [self reloadDataWithFilterSortOption:self.selectedFilterOption
                                minPrice:priceRangeCellNode.sliderView.selectedMinimum
                                maxPrice:priceRangeCellNode.sliderView.selectedMaximum];
    [self showFilter];
}

- (void)resetFilter:(ASButtonNode *)button {
    PriceRangeCellNode *priceRangeCellNode = (PriceRangeCellNode *)[self.tableNode.view nodeForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:4]];
    [self reloadDataWithFilterSortOption:0
                                minPrice:0
                                maxPrice:0];
    priceRangeCellNode.sliderView.selectedMinimum = priceRangeCellNode.sliderView.minValue;
    priceRangeCellNode.sliderView.selectedMaximum = priceRangeCellNode.sliderView.maxValue;
    
    [priceRangeCellNode setNeedsLayout];
    
    [self showFilter];
}

- (void)reloadDataWithFilterSortOption:(FilterSortOptions)sortOption
                              minPrice:(float)min
                              maxPrice:(float)max
{
    __weak ProductListViewController *weakSelf = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[BESharedManager sharedInstance] be_productsWithCategory:self.category.slug
                                                  withSubcategory:self.subcategory.groupheader.slug
                                                    withGroupling:self.groupling.slug
                                             withFilterSortOption:sortOption
                                                     withMinPrice:min
                                                     withMaxPrice:max
                                                 withSearchString:self.searchString
                                                   withCompletion:^(BOOL finished, NSArray *result)
         {
             if(finished) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     ASCollectionView *weakCollectionView = (ASCollectionView *)weakSelf.collectionDisplayNode.view;
                     [weakCollectionView performBatchUpdates:^{
                         NSMutableArray *insertIndexPaths = [NSMutableArray array];
                         NSMutableArray *deleteIndexPaths = [NSMutableArray array];
                         
                         NSInteger section = 0;
                         
                         NSArray *oldData = [weakSelf.productCategoriesResult copy];
                         for(ProductModel *oldModel in oldData) {
                             NSInteger oldItem = [oldData indexOfObject:oldModel];
                             [weakSelf.productCategoriesResult removeObject:oldModel];
                             [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
                         }
                         
                         for(ProductModel *newModel in result) {
                             [weakSelf.productCategoriesResult addObject:newModel];
                             NSInteger item = [weakSelf.productCategoriesResult indexOfObject:newModel];
                             [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
                         }
                         
                         [weakCollectionView deleteItemsAtIndexPaths:deleteIndexPaths];
                         [weakCollectionView insertItemsAtIndexPaths:insertIndexPaths];
                     } completion:nil
                      ];
                 });
                 weakSelf.currentFilterOption = sortOption;
             }
             dispatch_async(dispatch_get_main_queue(), ^{
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
             });
         }];
    });
}

- (NSAttributedString *)sortAttributedString:(NSString *)string {
    // Attrs
    NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
    paragraphStyleLeft.alignment = NSTextAlignmentLeft;
    
    NSDictionary *attrsSort = @{
                                NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                NSForegroundColorAttributeName: [UIColor flatBlackColorDark],
                                NSParagraphStyleAttributeName : paragraphStyleLeft
                                };
    
    NSAttributedString *stringSort = [[NSAttributedString alloc] initWithString:string
                                                                     attributes:attrsSort];
    
    return stringSort;
}

- (void)showFilter {
    [self.searchDisplayNode.view resignFirstResponder];
    
    CGFloat contentHeight = [self.tableNode.view contentSize].height;
    CGFloat startY = self.collectionDisplayNode.frame.origin.y;
    
    POPSpringAnimation *basicAnimation = [POPSpringAnimation animation];
    
    // 2. Decide weather you will animate a view property or layer property, Lets pick a View Property and pick kPOPViewFrame
    // View Properties - kPOPViewAlpha kPOPViewBackgroundColor kPOPViewBounds kPOPViewCenter kPOPViewFrame kPOPViewScaleXY kPOPViewSize
    // Layer Properties - kPOPLayerBackgroundColor kPOPLayerBounds kPOPLayerScaleXY kPOPLayerSize kPOPLayerOpacity kPOPLayerPosition kPOPLayerPositionX kPOPLayerPositionY kPOPLayerRotation kPOPLayerBackgroundColor
    basicAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewFrame];
    
    // 3. Figure Out which of 3 ways to set toValue
    //  anim.toValue = @(1.0);
    //  anim.toValue =  [NSValue valueWithCGRect:CGRectMake(0, 0, 400, 400)];
    //  anim.toValue =  [NSValue valueWithCGSize:CGSizeMake(40, 40)];
    if(!self.filterButtonNode.selected) {
        // Show
        [SlideNavigationController sharedInstance].enableSwipeGesture = NO;
        self.filterButtonNode.selected = YES;
        basicAnimation.fromValue = [NSValue valueWithCGRect:CGRectMake(0, startY, self.view.bounds.size.width, 0)];
        basicAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0, startY, self.view.bounds.size.width, contentHeight)];
    } else {
        // Hide
        [SlideNavigationController sharedInstance].enableSwipeGesture = YES;
        self.filterButtonNode.selected = NO;
        basicAnimation.fromValue = [NSValue valueWithCGRect:CGRectMake(0, startY, self.view.bounds.size.width, contentHeight)];
        basicAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(0, startY, self.view.bounds.size.width, 0)];
    }
    
    // 4. Create Name For Animation & Set Delegate
    basicAnimation.name = @"AnyAnimationNameYouWant";
    basicAnimation.delegate = self;
    
    // 5. Add animation to View or Layer, we picked View so self.tableView and not layer which would have been self.tableView.layer
    [_tableNode pop_addAnimation:basicAnimation forKey:@"WhatEverNameYouWant"];
}

- (void)goToHome {
    NSLog(@"Home");
    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
}

- (void)goBack {
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
