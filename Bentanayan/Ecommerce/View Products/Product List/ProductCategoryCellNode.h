//
//  ProductCategoryCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface ProductCategoryCellNode : ASCellNode <ASNetworkImageNodeDelegate> {
    ASNetworkImageNode *_productImageNode;
    ASTextNode *_productNameLabelNode;
    ASTextNode *_productDescLabelNode;
    ASTextNode *_productPriceLabelNode;
}

- (instancetype)initWithProduct:(ProductModel *)product;

@property (strong, nonatomic) ProductModel *product;

@end
