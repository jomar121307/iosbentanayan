//
//  ProductCategoryCellNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ProductCategoryCellNode.h"

@implementation ProductCategoryCellNode

- (instancetype)initWithProduct:(ProductModel *)product {
    self = [super init];
    if(self) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.product = product;
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        // Product Image
        _productImageNode = [[ASNetworkImageNode alloc] init];
        if(self.product.metadata.photos) {
            NSArray *photos = self.product.metadata.photos;
            if(photos.count > 0) {
                ProductPhotoModel *photo = [photos firstObject];
                if(photo.cropped) {
                    _productImageNode.URL = [NSURL URLWithString:photo.cropped.secure_url];
                } else if(photo.original) {
                    _productImageNode.URL = [NSURL URLWithString:photo.original.secure_url];
                } else {
                    _productImageNode.URL = [NSURL URLWithString:photo.secure_url];
                }
            }
        }
        _productImageNode.delegate = self;
        _productImageNode.defaultImage = [UIImage imageNamed:@"product-placeholder"];
        _productImageNode.contentMode = UIViewContentModeScaleAspectFit;
        
        [self addSubnode:_productImageNode];
        
        // Product Name
        NSDictionary *attrsProductName = @{
                                           NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0f],
                                           NSForegroundColorAttributeName: [UIColor flatBlackColorDark],
                                           NSParagraphStyleAttributeName : paragraphStyleLeft
                                           };
        
        NSAttributedString *stringProductName = [[NSAttributedString alloc] initWithString:self.product.name
                                                                                attributes:attrsProductName];
        
        
        _productNameLabelNode = [[ASTextNode alloc] init];
        _productNameLabelNode.attributedText = stringProductName;
        _productNameLabelNode.maximumNumberOfLines = 2;
        _productNameLabelNode.pointSizeScaleFactors = @[@0.95, @0.9];
        
        [self addSubnode:_productNameLabelNode];
        
        // Product Description
        NSDictionary *attrsProductDesc = @{
                                           NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:12.0f],
                                           NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                           NSParagraphStyleAttributeName : paragraphStyleLeft
                                           };
        
        NSAttributedString *stringProductDesc = [[NSAttributedString alloc] initWithString:self.product.productDescription
                                                                                attributes:attrsProductDesc];
        
        
        _productDescLabelNode = [[ASTextNode alloc] init];
        _productDescLabelNode.attributedText = stringProductDesc;
        _productDescLabelNode.maximumNumberOfLines = 3;
        _productDescLabelNode.pointSizeScaleFactors = @[@0.95, @0.9];
        
        [self addSubnode:_productDescLabelNode];
        
        // Product Price
        NSDictionary *attrsProductPrice = @{
                                            NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                            NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                            NSParagraphStyleAttributeName : paragraphStyleLeft
                                            };
        
        NSAttributedString *stringProductPrice = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%0.2f", self.product.currencyId.symbol, self.product.price]
                                                                                 attributes:attrsProductPrice];
        
        
        _productPriceLabelNode = [[ASTextNode alloc] init];
        _productPriceLabelNode.attributedText = stringProductPrice;
        _productPriceLabelNode.maximumNumberOfLines = 1;
        _productPriceLabelNode.pointSizeScaleFactors = @[@0.95, @0.9];
        
        [self addSubnode:_productPriceLabelNode];
    }
    return self;
}

- (void)didLoad {
    [super didLoad];
}

- (void)layoutDidFinish {
    [super layoutDidFinish];
    
    self.shadowColor = [UIColor flatBlackColor].CGColor;
    self.shadowOpacity = 0.5f;
    self.shadowRadius = 2.0f;
    self.shadowOffset = CGSizeMake(0.0f, 4.0f);
    self.layer.masksToBounds = NO;
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
    //NSLog(@"%f", self.view.bounds.size.height);
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // Product Image
    ASRatioLayoutSpec *ratioImage = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:1.0
                                                                          child:_productImageNode];
    ratioImage.spacingAfter = 4.0f;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        ratioImage.flexBasis = ASRelativeDimensionMakeWithPoints(190.0f);
    } else {
        ratioImage.flexBasis = ASRelativeDimensionMakeWithPoints(130.0f);
    }
    
    // Content
    ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
    spacer.flexGrow = YES;
    
    ASStackLayoutSpec *stackContent = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                              spacing:0.0
                                                                       justifyContent:ASStackLayoutJustifyContentCenter
                                                                           alignItems:ASStackLayoutAlignItemsStretch
                                                                             children:@[ratioImage,
                                                                                        _productNameLabelNode,
                                                                                        _productDescLabelNode,
                                                                                        _productPriceLabelNode,
                                                                                        spacer]];
    
    // Content Inset
    ASInsetLayoutSpec *insetContent = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(8, 8, 8, 8)
                                                                             child:stackContent];
    
    return insetContent;
}

#pragma mark - ASNetworkImageNodeDelegate methods.

- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image
{
    //[self setNeedsLayout];
}

- (void)imageNode:(ASNetworkImageNode *)imageNode didFailWithError:(NSError *)error {
    //[self setNeedsLayout];
}

@end
