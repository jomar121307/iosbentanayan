//
//  FilterCelllNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/05/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface FilterCellNode : ASCellNode {
    ASTextNode *_filterLabelNode;
}

@property (strong, nonatomic) ASButtonNode *resetButtonNode;
@property (strong, nonatomic) ASButtonNode *applyButtonNode;

@end
