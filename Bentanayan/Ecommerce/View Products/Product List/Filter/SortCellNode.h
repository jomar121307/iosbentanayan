//
//  SortCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/05/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface SortCellNode : ASCellNode

@property (strong, nonatomic) ASImageNode *sortImageNode;
@property (strong, nonatomic) ASTextNode *sortLabelNode;

@end
