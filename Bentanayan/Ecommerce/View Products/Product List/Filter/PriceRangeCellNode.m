//
//  PriceRangeCellNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/05/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "PriceRangeCellNode.h"

@implementation PriceRangeCellNode

- (instancetype)init {
    self = [super init];
    if(self) {
        
        self.userInteractionEnabled = YES;
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleRight = [NSMutableParagraphStyle new];
        paragraphStyleRight.alignment = NSTextAlignmentRight;
        
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        _attrsPriceMin = @{
                        NSFontAttributeName: [UIFont systemFontOfSize:15.0f],
                        NSForegroundColorAttributeName: [UIColor flatBlackColor],
                        NSParagraphStyleAttributeName : paragraphStyleLeft
                        };
        
        _attrsPriceMax = @{
                           NSFontAttributeName: [UIFont systemFontOfSize:15.0f],
                           NSForegroundColorAttributeName: [UIColor flatBlackColor],
                           NSParagraphStyleAttributeName : paragraphStyleRight
                           };
        
        // Formatter
        _numberFormatter = [[NSNumberFormatter alloc] init];
        [_numberFormatter setCurrencySymbol:@""];
        [_numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
        
        // Min Price Label
        NSDecimalNumber *decMin = [NSDecimalNumber decimalNumberWithString:@"0.00"];
        NSAttributedString *stringMinPrice = [[NSAttributedString alloc] initWithString:[_numberFormatter stringFromNumber:decMin]
                                                                             attributes:_attrsPriceMin];
        
        _minPriceLabelNode = [[ASTextNode alloc] init];
        _minPriceLabelNode.attributedString = stringMinPrice;
        _minPriceLabelNode.maximumNumberOfLines = 1.0f;
        //_minPriceLabelNode.pointSizeScaleFactors = @[@0.90, @0.80];
        
        [self addSubnode:_minPriceLabelNode];
        
        // Max Price Label
        NSDecimalNumber *decMax = [NSDecimalNumber decimalNumberWithString:@"25000.00"];
        NSMutableAttributedString *stringMaxPrice = [[NSMutableAttributedString alloc] initWithString:[_numberFormatter stringFromNumber:decMax]
                                                                                           attributes:_attrsPriceMax];
        
        _maxPriceLabelNode = [[ASTextNode alloc] init];
        _maxPriceLabelNode.attributedString = stringMaxPrice;
        _maxPriceLabelNode.maximumNumberOfLines = 1.0f;
        //_maxPriceLabelNode.pointSizeScaleFactors = @[@0.90, @0.80];
        
        [self addSubnode:_maxPriceLabelNode];
        
        // Current Price Label
        NSMutableParagraphStyle *paragraphStyleCurrency = [NSMutableParagraphStyle new];
        paragraphStyleCurrency.alignment = NSTextAlignmentCenter;
        
        NSDictionary *attrsCurrency = @{
                                        NSFontAttributeName: [UIFont systemFontOfSize:14.0f],
                                        NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                        NSParagraphStyleAttributeName : paragraphStyleCurrency
                                        };
        NSAttributedString *stringCurrency = [[NSAttributedString alloc] initWithString:@"2500"
                                                                             attributes:attrsCurrency];
        
        
        _currentPriceLabelNode = [[ASTextNode alloc] init];
        _currentPriceLabelNode.attributedString = stringCurrency;
        _currentPriceLabelNode.maximumNumberOfLines = 1.0f;
        _currentPriceLabelNode.pointSizeScaleFactors = @[@0.90, @0.80];
        
        [self addSubnode:_currentPriceLabelNode];
        
        __weak PriceRangeCellNode *weakSelf = self;
        
        self.sliderNode = [[ASDisplayNode alloc] initWithViewBlock:^UIView * _Nonnull{
            weakSelf.sliderView = [[TTRangeSlider alloc] init];
            weakSelf.sliderView.userInteractionEnabled = YES;
            weakSelf.sliderView.delegate = self;
            weakSelf.sliderView.minValue = 0;
            weakSelf.sliderView.maxValue = 25000;
            weakSelf.sliderView.selectedMinimum = 0;
            weakSelf.sliderView.selectedMaximum = 25000;
            //NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            //formatter.numberStyle = NSNumberFormatterCurrencyStyle;
            //formatter.currencyCode = @"PHP";
            weakSelf.sliderView.hideLabels = YES;
            //weakSelf.sliderView.numberFormatterOverride = formatter;
            weakSelf.sliderView.tintColor = [UIColor flatBlackColor];
            weakSelf.sliderView.tintColorBetweenHandles = [UIColor bentaGreenColor];
            weakSelf.sliderView.handleDiameter = 18.0f;
            
            return weakSelf.sliderView;
        }];
        self.sliderNode.userInteractionEnabled = YES;
        
        [self addSubnode:self.sliderNode];
        
    }
    return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // iPhone & iPad
    UIEdgeInsets contentInset;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        contentInset = UIEdgeInsetsMake(14, 14, 14, 14);
    } else {
        contentInset = UIEdgeInsetsMake(8, 8, 8, 8);
    }
    
    // Spacer
    ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
    spacer.flexGrow = YES;
    spacer.spacingAfter = 4.0f;
    spacer.spacingBefore = 4.0f;
    
    // Price
    _minPriceLabelNode.flexGrow = YES;
    _minPriceLabelNode.flexShrink = YES;
    _maxPriceLabelNode.flexGrow = YES;
    _maxPriceLabelNode.flexShrink = YES;
    ASStackLayoutSpec *stackPrice = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                            spacing:0.0f
                                                                     justifyContent:ASStackLayoutJustifyContentStart
                                                                         alignItems:ASStackLayoutAlignItemsCenter
                                                                           children:@[_minPriceLabelNode,
                                                                                      spacer,
                                                                                      _maxPriceLabelNode]];
    
    // Contetn
    self.sliderNode.preferredFrameSize = CGSizeMake(constrainedSize.min.width, 32.0f);
    ASStackLayoutSpec *stackContent = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                              spacing:4.0f
                                                                       justifyContent:ASStackLayoutJustifyContentStart
                                                                           alignItems:ASStackLayoutAlignItemsStretch
                                                                             children:@[stackPrice,
                                                                                        self.sliderNode]];
    
    ASInsetLayoutSpec *insetContent = [ASInsetLayoutSpec insetLayoutSpecWithInsets:contentInset
                                                                             child:stackContent];
    
    return insetContent;
}

#pragma mark TTRangeSliderViewDelegate
-(void)rangeSlider:(TTRangeSlider *)sender didChangeSelectedMinimumValue:(float)selectedMinimum andMaximumValue:(float)selectedMaximum{
    if(sender == self.sliderView) {
        NSMutableAttributedString *stringCurrentMinRange = [[NSMutableAttributedString alloc] initWithString:[_numberFormatter stringFromNumber:[NSNumber numberWithFloat:selectedMinimum]]
                                                                                               attributes:_attrsPriceMin];
        
        _minPriceLabelNode.attributedString = stringCurrentMinRange;
        
        NSMutableAttributedString *stringCurrentMaxRange = [[NSMutableAttributedString alloc] initWithString:[_numberFormatter stringFromNumber:[NSNumber numberWithFloat:selectedMaximum]]
                                                                                                  attributes:_attrsPriceMax];
        
        _maxPriceLabelNode.attributedString = stringCurrentMaxRange;
    }
}

@end
