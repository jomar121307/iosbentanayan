//
//  PriceRangeCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/05/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface PriceRangeCellNode : ASCellNode <TTRangeSliderDelegate> {
    NSDictionary *_attrsPriceMin;
    NSDictionary *_attrsPriceMax;
    NSNumberFormatter *_numberFormatter;
    
    ASTextNode *_minPriceLabelNode;
    ASTextNode *_maxPriceLabelNode;
    ASTextNode *_currentPriceLabelNode;
}

@property (strong, nonatomic) TTRangeSlider *sliderView;

@property (strong, nonatomic) ASDisplayNode *sliderNode;

@end
