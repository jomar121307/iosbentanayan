//
//  FilterCelllNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/05/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "FilterCellNode.h"

@implementation FilterCellNode

- (instancetype)init {
    self = [super init];
    if(self) {
        // Attrs
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        // Filter Label
        NSDictionary *attrsFilter = @{
                                    NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Bold" size:14.0f],
                                    NSForegroundColorAttributeName: [UIColor flatBlackColorDark],
                                    NSParagraphStyleAttributeName : paragraphStyleLeft
                                    };
        
        NSAttributedString *stringFilter = [[NSAttributedString alloc] initWithString:@"FILTERS"
                                                                         attributes:attrsFilter];
        
        _filterLabelNode = [[ASTextNode alloc] init];
        _filterLabelNode.attributedText = stringFilter;
        _filterLabelNode.maximumNumberOfLines = 1;
        _filterLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_filterLabelNode];
        
        // Buttons
        self.resetButtonNode = [[ASButtonNode alloc] init];
        [self.resetButtonNode setTitle:@"RESET"
                              withFont:[UIFont fontWithName:@"RobotoCondensed-Bold" size:14.0f]
                             withColor:[UIColor flatBlackColorDark]
                              forState:ASControlStateNormal];
        [self.resetButtonNode setHitTestSlop:UIEdgeInsetsMake(-2, -4, -2, -4)];
        
        [self addSubnode:self.resetButtonNode];
        
        self.applyButtonNode = [[ASButtonNode alloc] init];
        [self.applyButtonNode setTitle:@"APPLY"
                              withFont:[UIFont fontWithName:@"RobotoCondensed-Bold" size:14.0f]
                             withColor:[UIColor bentaGreenColor]
                              forState:ASControlStateNormal];
        [self.applyButtonNode setHitTestSlop:UIEdgeInsetsMake(-2, -4, -2, -4)];
        
        [self addSubnode:self.applyButtonNode];
    }
    return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // iPhone & iPad
    UIEdgeInsets contentInset;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        contentInset = UIEdgeInsetsMake(18, 14, 18, 14);
    } else {
        contentInset = UIEdgeInsetsMake(12, 8, 12, 8);
    }
    
    // Spacer
    ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
    spacer.flexGrow = YES;
    
    // Content
    ASStackLayoutSpec *stackContent = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                              spacing:12.0f
                                                                       justifyContent:ASStackLayoutJustifyContentStart
                                                                           alignItems:ASStackLayoutAlignItemsCenter
                                                                             children:@[_filterLabelNode,
                                                                                        spacer,
                                                                                        self.resetButtonNode,
                                                                                        self.applyButtonNode]];
    
    ASInsetLayoutSpec *insetContent = [ASInsetLayoutSpec insetLayoutSpecWithInsets:contentInset
                                                                             child:stackContent];
    
    return insetContent;
}

@end
