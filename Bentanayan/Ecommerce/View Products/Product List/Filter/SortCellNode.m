//
//  SortCellNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/05/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "SortCellNode.h"

@implementation SortCellNode

- (instancetype)init {
    self = [super init];
    if(self) {
        // Filter Label
        self.sortLabelNode = [[ASTextNode alloc] init];
        self.sortLabelNode.maximumNumberOfLines = 1;
        self.sortLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:self.sortLabelNode];
        
        // Sort Image
        self.sortImageNode = [[ASImageNode alloc] init];
        self.sortImageNode.contentMode = UIViewContentModeScaleAspectFit;
        self.sortImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor flatBlackColor]);
        
        [self addSubnode:self.sortImageNode];
        
    }
    return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // iPhone & iPad
    UIEdgeInsets contentInset;
    CGSize sortImageSize;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        contentInset = UIEdgeInsetsMake(14, 14, 14, 14);
        sortImageSize = CGSizeMake(26, 26);
    } else {
        contentInset = UIEdgeInsetsMake(8, 8, 8, 8);
        sortImageSize = CGSizeMake(20, 20);
    }
    
    // Spacer
    ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
    spacer.flexGrow = YES;
    
    // Content
    self.sortImageNode.preferredFrameSize = sortImageSize;
    ASStackLayoutSpec *stackContent = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                              spacing:8.0f
                                                                       justifyContent:ASStackLayoutJustifyContentStart
                                                                           alignItems:ASStackLayoutAlignItemsCenter
                                                                             children:@[self.sortLabelNode,
                                                                                        spacer,
                                                                                        self.sortImageNode]];
    
    ASInsetLayoutSpec *insetContent = [ASInsetLayoutSpec insetLayoutSpecWithInsets:contentInset
                                                                             child:stackContent];
    
    return insetContent;
}

@end
