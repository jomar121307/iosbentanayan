//
//  BuyProductsCollectionViewFlowLayout.m
//  Golfgago
//
//  Created by Juston Paul Alcantara on 12/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "BuyProductsCollectionViewFlowLayout.h"

@implementation BuyProductsCollectionViewFlowLayout

- (instancetype)init {
    self = [super init];
    if(self) {
        self.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        self.minimumInteritemSpacing = 0;
        self.minimumLineSpacing = 0;
        _numberOfColumns = 2;
    }
    return self;
}

- (CGSize)itemSize {
    CGFloat itemWidth = CGRectGetWidth(self.collectionView.bounds) / _numberOfColumns;
    return CGSizeMake(itemWidth, itemWidth);
}

@end
