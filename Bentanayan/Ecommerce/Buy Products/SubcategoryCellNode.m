//
//  SubcategoryCellNode.m
//  Golfgago
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "SubcategoryCellNode.h"

@implementation SubcategoryCellNode

- (instancetype)initWithSubcategory:(SubcategoryModel *)subcategory withBackgroundColor:(UIColor *)bColor
{
    self = [super init];
    if(self) {
        self.subcategory = subcategory;
        
        UIColor *randColor = [UIColor colorWithRandomFlatColorOfShadeStyle:UIShadeStyleDark];
        self.backgroundColor = bColor;
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleCenter = [NSMutableParagraphStyle new];
        paragraphStyleCenter.alignment = NSTextAlignmentCenter;
        
        // Category Image
        _categoryImageNode = [[ASNetworkImageNode alloc] init];
        //_categoryImageNode.URL = [NSURL URLWithString:self.subcategory.image];
        //_categoryImageNode.URL = [NSURL URLWithString:@""];
        _categoryImageNode.delegate = self;
        _categoryImageNode.placeholderColor = randColor;
        
        [self addSubnode:_categoryImageNode];
        
        // Category Effect Display Node
        _categoryImageEffectNode = [[ASDisplayNode alloc] init];
        _categoryImageEffectNode.backgroundColor = [UIColor colorWithHexString:@"476b4c" withAlpha:0.05];
        
        [self addSubnode:_categoryImageEffectNode];
        
        // Category Label
        NSDictionary *attrsCategory = @{
                                        NSFontAttributeName: [UIFont fontWithName:@"Roboto-Regular" size:20.0f],
                                        NSForegroundColorAttributeName: [UIColor colorWithContrastingBlackOrWhiteColorOn:self.backgroundColor
                                                                                                                  isFlat:YES],
                                        NSParagraphStyleAttributeName : paragraphStyleCenter
                                        };
        
        NSAttributedString *stringCategory = [[NSAttributedString alloc] initWithString:[self.subcategory.name uppercaseString]
                                                                             attributes:attrsCategory];
        
        
        _categoryLabelNode = [[ASTextNode alloc] init];
        _categoryLabelNode.attributedText = stringCategory;
        _categoryLabelNode.maximumNumberOfLines = 1;
        _categoryLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        _categoryLabelNode.shadowColor = [UIColor flatBlackColor].CGColor;
        _categoryLabelNode.shadowOpacity = 1.0;
        _categoryLabelNode.shadowOffset = CGSizeMake(0, 2);
        _categoryLabelNode.shadowRadius = 4.0f;
        _categoryLabelNode.backgroundColor = [UIColor clearColor];
        
        [self addSubnode:_categoryLabelNode];
    }
    return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // Content
    ASInsetLayoutSpec *insetContent = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(8, 8, 8, 8)
                                                                             child:_categoryLabelNode];
    
    // Overlay Effects
    ASBackgroundLayoutSpec *overlayEffect = [ASBackgroundLayoutSpec backgroundLayoutSpecWithChild:insetContent
                                                                                       background:_categoryImageEffectNode];
    
    return overlayEffect;
}

#pragma mark - ASNetworkImageNodeDelegate methods.

- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image
{
    //[self setNeedsLayout];
}

- (void)imageNode:(ASNetworkImageNode *)imageNode didFailWithError:(NSError *)error {
    //[self setNeedsLayout];
}

@end
