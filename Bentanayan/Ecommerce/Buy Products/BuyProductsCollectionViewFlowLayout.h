//
//  BuyProductsCollectionViewFlowLayout.h
//  Golfgago
//
//  Created by Juston Paul Alcantara on 12/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuyProductsCollectionViewFlowLayout : UICollectionViewFlowLayout

@property (assign, nonatomic) NSUInteger numberOfColumns;

@end
