//
//  BuyProductsViewController.h
//  Golfgago
//
//  Created by Juston Paul Alcantara on 12/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BuyProductsCollectionViewFlowLayout.h"

@interface BuyProductsViewController : UIViewController <ASCollectionViewDataSource, ASCollectionViewDelegateFlowLayout, SlideNavigationControllerDelegate, ASPagerNodeDataSource, ASCollectionDelegate, UICollectionViewDelegate, ASPagerDelegate>

@property (strong, nonatomic) ASPagerNode *categoriesPagerNode;

@property (strong, nonatomic) ASCollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray *categoriesResult;
@property (strong, nonatomic) NSMutableArray *subcategoriesResult;

@end
