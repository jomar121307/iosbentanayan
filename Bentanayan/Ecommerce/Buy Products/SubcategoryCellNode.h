//
//  SubcategoryCellNode.h
//  Golfgago
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface SubcategoryCellNode : ASCellNode <ASNetworkImageNodeDelegate> {
    ASTextNode *_categoryLabelNode;
    ASNetworkImageNode *_categoryImageNode;
    ASDisplayNode *_categoryImageEffectNode;
}

@property (strong, nonatomic) SubcategoryModel *subcategory;

- (instancetype)initWithSubcategory:(SubcategoryModel *)subcategory withBackgroundColor:(UIColor *)bColor;

@end
