//
//  BuyProductsViewController.m
//  Golfgago
//
//  Created by Juston Paul Alcantara on 12/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "BuyProductsViewController.h"

#import "CategoryCellNode.h"
#import "SubcategoryCellNode.h"

#import "ProductListViewController.h"
#import "ProductListViewController+Beta.h"

@interface BuyProductsViewController ()

@end

@implementation BuyProductsViewController

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor flatWhiteColor];
    
    // Navigation
    self.navigationItem.title = @"Products";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Home
    UIImage *homeImage = [[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    CGRect frameimg = CGRectMake(0, 0, homeImage.size.width, homeImage.size.height);
    UIButton *homeButton = [[UIButton alloc] initWithFrame:frameimg];
    [homeButton setBackgroundImage:homeImage forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(goToHome) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    // Categories Pager Node
    self.categoriesPagerNode = [[ASPagerNode alloc] init];
    self.categoriesPagerNode.dataSource = self;
    self.categoriesPagerNode.delegate = self;
    self.categoriesPagerNode.backgroundColor = [UIColor flatBlackColor];
    self.categoriesPagerNode.view.alwaysBounceHorizontal = YES;
    self.categoriesPagerNode.view.translatesAutoresizingMaskIntoConstraints = NO;
    self.categoriesPagerNode.view.allowsSelection = YES;
    
    [self.view addSubnode:self.categoriesPagerNode];
    
    // CollectionView
    //BuyProductsCollectionViewFlowLayout *layout = [[BuyProductsCollectionViewFlowLayout alloc] init];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 4;
    layout.minimumLineSpacing = 4;
    
    self.collectionView = [[ASCollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:layout];
    self.collectionView.asyncDataSource = self;
    self.collectionView.asyncDelegate = self;
    self.collectionView.backgroundColor = [UIColor flatBlackColor];
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:self.collectionView];
    
    // Constraints
    NSDictionary *views = @{ @"categoriesPagerNode" : self.categoriesPagerNode.view,
                             @"collectionView" : self.collectionView };
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[categoriesPagerNode]|" options:0 metrics:nil views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|" options:0 metrics:nil views:views]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.categoriesPagerNode.view
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:0.36
                                                           constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.collectionView
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:0.64
                                                           constant:0]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[categoriesPagerNode][collectionView]|" options:0 metrics:nil views:views]];
    
    // Data
    self.categoriesResult = [NSMutableArray new];
    self.subcategoriesResult = [NSMutableArray new];
    
    __weak BuyProductsViewController *weakSelf = self;
    
    [[VideogagoManager sharedInstance] vg_categoriesWithCompletion:^(BOOL finished, NSArray *result) {
        if(finished) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.categoriesPagerNode.view performBatchUpdates:^{
                    NSMutableArray *insertIndexPaths = [NSMutableArray array];
                    NSMutableArray *deleteIndexPaths = [NSMutableArray array];
                    
                    NSInteger section = 0;
                    
                    NSArray *oldData = [weakSelf.categoriesResult copy];
                    for(CategoryModel *oldModel in oldData) {
                        NSInteger oldItem = [oldData indexOfObject:oldModel];
                        [weakSelf.categoriesResult removeObject:oldModel];
                        [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
                    }
                    
                    for(CategoryModel *newModel in result) {
                        [weakSelf.categoriesResult addObject:newModel];
                        NSInteger item = [weakSelf.categoriesResult indexOfObject:newModel];
                        [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
                    }
                    
                    [weakSelf.categoriesPagerNode.view deleteItemsAtIndexPaths:deleteIndexPaths];
                    [weakSelf.categoriesPagerNode.view insertItemsAtIndexPaths:insertIndexPaths];
                } completion:^(BOOL finished) {
                    CategoryModel *category = [self.categoriesResult objectAtIndex:0];
                    if(weakSelf.categoriesResult.count > 0) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [weakSelf.collectionView performBatchUpdates:^{
                                NSMutableArray *insertIndexPaths = [NSMutableArray array];
                                NSMutableArray *deleteIndexPaths = [NSMutableArray array];
                                
                                NSInteger section = 0;
                                
                                NSArray *oldData = [weakSelf.subcategoriesResult copy];
                                for(SubcategoryModel *oldModel in oldData) {
                                    NSInteger oldItem = [oldData indexOfObject:oldModel];
                                    [weakSelf.subcategoriesResult removeObject:oldModel];
                                    [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
                                }
                                
                                for(SubcategoryModel *newModel in category.subcategories) {
                                    [weakSelf.subcategoriesResult addObject:newModel];
                                    NSInteger item = [weakSelf.subcategoriesResult indexOfObject:newModel];
                                    [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
                                }
                                
                                [weakSelf.collectionView deleteItemsAtIndexPaths:deleteIndexPaths];
                                [weakSelf.collectionView insertItemsAtIndexPaths:insertIndexPaths];
                            } completion:^(BOOL finished) {
                            }
                             ];
                        });
                    }
                }
                 ];
            });
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_REFRESH_MENU
                                                        object:SLIDE_MENU_RELOAD_DATA];
}

#pragma mark - ASPagerNode data source.

- (NSInteger)numberOfPagesInPagerNode:(ASPagerNode *)pagerNode
{
    if([pagerNode isEqual:self.categoriesPagerNode]) {
        return self.categoriesResult.count;
    } else {
        return 0;
    }
}

- (ASCellNode *)pagerNode:(ASPagerNode *)pagerNode nodeAtIndex:(NSInteger)index
{
    if([pagerNode isEqual:self.categoriesPagerNode]) {
        CategoryCellNode *categoryCellNode;
        CategoryModel *category = (CategoryModel *)[self.categoriesResult objectAtIndex:index];
        categoryCellNode = [[CategoryCellNode alloc] initWithCategory:category];
        return categoryCellNode;
    } else {
        return [[ASCellNode alloc] init];
    }
}

- (ASSizeRange)pagerNode:(ASPagerNode *)pagerNode constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize itemSize = pagerNode.bounds.size;
    return ASSizeRangeMake(itemSize, itemSize);
}

#pragma mark - ASCollectionView data source.

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SubcategoryCellNode *cellNode;
    SubcategoryModel *subcategory = (SubcategoryModel *)[self.subcategoriesResult objectAtIndex:indexPath.row];
    if(indexPath.row % 2) {
        cellNode = [[SubcategoryCellNode alloc] initWithSubcategory:subcategory withBackgroundColor:[UIColor flatWhiteColor]];
    } else {
        cellNode = [[SubcategoryCellNode alloc] initWithSubcategory:subcategory withBackgroundColor:[UIColor flatWhiteColorDark]];
    }
    return cellNode;
//    ASTextCellNode *cellNode = [[ASTextCellNode alloc] init];
//    cellNode.text = subcategory.name;
//    return cellNode;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
//    CGFloat height = ceilf(collectionView.bounds.size.height - 1) / 2;
//    
//    BuyProductsCollectionViewFlowLayout *layout = (BuyProductsCollectionViewFlowLayout *)collectionView.collectionViewLayout;
//    layout.numberOfColumns = 2;
//    CGSize itemSize = [layout itemSize];
//    itemSize.height = height;
    
    return ASSizeRangeMake(CGSizeMake(collectionView.bounds.size.width, 0), CGSizeMake(collectionView.bounds.size.width, FLT_MAX));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.subcategoriesResult.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionViewLockDataSource:(ASCollectionView *)collectionView
{
    // lock the data source
    // The data source should not be change until it is unlocked.
}

- (void)collectionViewUnlockDataSource:(ASCollectionView *)collectionView
{
    // unlock the data source to enable data source updating.
}

- (void)collectionView:(UICollectionView *)collectionView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    NSLog(@"fetch additional content");
    [context completeBatchFetching:YES];
}

#pragma mark - ASCollectionView delegate.

- (void)collectionView:(ASCollectionView *)collectionView didEndDisplayingNode:(ASCellNode *)node forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.categoriesPagerNode.view isEqual:collectionView]) {
        ASCellNode *visibleCellNode = [collectionView visibleNodes][0];
        NSIndexPath *visibleIndexPath = [collectionView indexPathForNode:visibleCellNode];
        [self populateSubcategories:visibleIndexPath];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *visibleCategoryNodes = [self.categoriesPagerNode.view visibleNodes];\
    
    CategoryModel *catM = nil;
    SubcategoryModel *subcatM = nil;
    
    ProductListViewController_Beta *productListController;
    
    if(visibleCategoryNodes.count > 0) {
        CategoryCellNode *categoryCellNode = [visibleCategoryNodes firstObject];
        catM = categoryCellNode.category;
    }
    
    if([self.categoriesPagerNode.view isEqual:collectionView]) {
        productListController = [[ProductListViewController_Beta alloc] initWithCategory:catM
                                                                        subCategory:nil];
        
        [[SlideNavigationController sharedInstance] pushViewController:productListController
                                                              animated:YES];
    } else if([self.collectionView isEqual:collectionView]) {
        subcatM = (SubcategoryModel *)[self.subcategoriesResult objectAtIndex:indexPath.row];
        productListController = [[ProductListViewController_Beta alloc] initWithCategory:catM
                                                                        subCategory:subcatM];
        
        [[SlideNavigationController sharedInstance] pushViewController:productListController
                                                              animated:YES];
    }
}

#pragma mark - Helpers

- (void)populateSubcategories:(NSIndexPath *)indexPath {
    __weak BuyProductsViewController *weakSelf = self;
    CategoryModel *category = [self.categoriesResult objectAtIndex:indexPath.row];
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.collectionView performBatchUpdates:^{
            NSMutableArray *insertIndexPaths = [NSMutableArray array];
            NSMutableArray *deleteIndexPaths = [NSMutableArray array];
            
            NSInteger section = 0;
            
            NSArray *oldData = [weakSelf.subcategoriesResult copy];
            for(SubcategoryModel *oldModel in oldData) {
                NSInteger oldItem = [oldData indexOfObject:oldModel];
                [weakSelf.subcategoriesResult removeObject:oldModel];
                [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
            }
            
            for(SubcategoryModel *newModel in category.subcategories) {
                [weakSelf.subcategoriesResult addObject:newModel];
                NSInteger item = [weakSelf.subcategoriesResult indexOfObject:newModel];
                [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
            }
            
            [weakSelf.collectionView deleteItemsAtIndexPaths:deleteIndexPaths];
            [weakSelf.collectionView insertItemsAtIndexPaths:insertIndexPaths];
        } completion:nil
         ];
    });
}

#pragma mark - Methods / Selectors

- (void)goToHome {
    NSLog(@"Home");
    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
}

#pragma mark - Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
