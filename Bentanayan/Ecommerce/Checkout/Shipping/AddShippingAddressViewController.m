//
//  AddShippingAddressViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 23/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "AddShippingAddressViewController.h"

@interface AddShippingAddressViewController ()

@end

NSString *const fullNameTag = @"fullName";
NSString *const countryTag = @"country";
NSString *const streetAddressTag = @"streetAddress";
NSString *const cityTag = @"city";
NSString *const provinceTag = @"province";
NSString *const zipCodeTag = @"zipcode";

@implementation AddShippingAddressViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){
        [self initializeForm];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self){
        [self initializeForm];
    }
    return self;
}

#pragma mark - XLForm Initialization

- (void)initializeForm {
    XLFormDescriptor * form;
    XLFormSectionDescriptor * section;
    XLFormRowDescriptor *row;
    
    form = [XLFormDescriptor formDescriptor];
    section = [XLFormSectionDescriptor formSection];
    [form addFormSection:section];
    
    // Set cell properties.
    UIFont *rowFontLabel = [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0];
    UIFont *rowFontText = [UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0];
    
    // XLForm Full Name.
    row = [XLFormRowDescriptor formRowDescriptorWithTag:fullNameTag
                                                rowType:XLFormRowDescriptorTypeName
                                                  title:@"Full Name"];
    [row.cellConfigAtConfigure setObject:[UIColor clearColor] forKey:@"backgroundColor"];
    [row.cellConfig setObject:[UIColor flatBlackColor] forKey:@"textField.textColor"];
    [row.cellConfig setObject:[UIColor flatGrayColorDark] forKey:@"textLabel.textColor"];
    [row.cellConfig setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [row.cellConfig setObject:rowFontText forKey:@"textField.font"];
    [row.cellConfig setObject:rowFontLabel forKey:@"textLabel.font"];
    row.required = TRUE;
    [section addFormRow:row];
    
    // XLForm Address.
    row = [XLFormRowDescriptor formRowDescriptorWithTag:streetAddressTag
                                                rowType:XLFormRowDescriptorTypeName
                                                  title:@"Address Line"];
    [row.cellConfigAtConfigure setObject:[UIColor clearColor] forKey:@"backgroundColor"];
    [row.cellConfig setObject:[UIColor flatBlackColor] forKey:@"textField.textColor"];
    [row.cellConfig setObject:[UIColor flatGrayColorDark] forKey:@"textLabel.textColor"];
    [row.cellConfig setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [row.cellConfig setObject:rowFontText forKey:@"textField.font"];
    [row.cellConfig setObject:rowFontLabel forKey:@"textLabel.font"];
    row.required = TRUE;
    [section addFormRow:row];
    
    // XLForm City.
    row = [XLFormRowDescriptor formRowDescriptorWithTag:cityTag
                                                rowType:XLFormRowDescriptorTypeName
                                                  title:@"City"];
    [row.cellConfigAtConfigure setObject:[UIColor clearColor] forKey:@"backgroundColor"];
    [row.cellConfig setObject:[UIColor flatBlackColor] forKey:@"textField.textColor"];
    [row.cellConfig setObject:[UIColor flatGrayColorDark] forKey:@"textLabel.textColor"];
    [row.cellConfig setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [row.cellConfig setObject:rowFontText forKey:@"textField.font"];
    [row.cellConfig setObject:rowFontLabel forKey:@"textLabel.font"];
    row.required = TRUE;
    [section addFormRow:row];
    
    // XLForm State/Province
    row = [XLFormRowDescriptor formRowDescriptorWithTag:provinceTag
                                                rowType:XLFormRowDescriptorTypeName
                                                  title:@"Province"];
    [row.cellConfigAtConfigure setObject:[UIColor clearColor] forKey:@"backgroundColor"];
    [row.cellConfig setObject:[UIColor flatBlackColor] forKey:@"textField.textColor"];
    [row.cellConfig setObject:[UIColor flatGrayColorDark] forKey:@"textLabel.textColor"];
    [row.cellConfig setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [row.cellConfig setObject:rowFontText forKey:@"textField.font"];
    [row.cellConfig setObject:rowFontLabel forKey:@"textLabel.font"];
    row.required = TRUE;
    [section addFormRow:row];
    
    // XLForm Zip Code.
    row = [XLFormRowDescriptor formRowDescriptorWithTag:zipCodeTag
                                                rowType:XLFormRowDescriptorTypeInteger
                                                  title:@"Zip Code"];
    [row.cellConfigAtConfigure setObject:[UIColor clearColor] forKey:@"backgroundColor"];
    [row.cellConfig setObject:[UIColor flatBlackColor] forKey:@"textField.textColor"];
    [row.cellConfig setObject:[UIColor flatGrayColorDark] forKey:@"textLabel.textColor"];
    [row.cellConfig setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [row.cellConfig setObject:rowFontText forKey:@"textField.font"];
    [row.cellConfig setObject:rowFontLabel forKey:@"textLabel.font"];
    row.required = TRUE;
    [section addFormRow:row];
    
    // Selector Push
    row = [XLFormRowDescriptor formRowDescriptorWithTag:countryTag
                                                rowType:XLFormRowDescriptorTypeSelectorPush
                                                  title:@"Country"];
    [row.cellConfigAtConfigure setObject:[UIColor clearColor] forKey:@"backgroundColor"];
    [row.cellConfig setObject:[UIColor flatBlackColor] forKey:@"detailTextLabel.textColor"];
    [row.cellConfig setObject:[UIColor flatGrayColorDark] forKey:@"textLabel.textColor"];
    [row.cellConfig setObject:@(NSTextAlignmentRight) forKey:@"detailTextLabel.textAlignment"];
    [row.cellConfig setObject:rowFontText forKey:@"detailTextLabel.font"];
    [row.cellConfig setObject:rowFontLabel forKey:@"textLabel.font"];
    //row.action.viewControllerClass = [CountriesTableViewController class];
    //row.valueTransformer = [CountryTransformer class];
    row.disabled = [NSNumber numberWithBool:1];
    row.required = TRUE;
    row.value = @"Philippines";
    [section addFormRow:row];
    
    self.form = form;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor flatWhiteColor];
    
    // Navigation
    self.navigationItem.title = @"Add Shipping Address";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Back
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(goBack)];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    // Home
    UIImage *homeImage = [[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    CGRect frameimg = CGRectMake(0, 0, homeImage.size.width, homeImage.size.height);
    UIButton *homeButton = [[UIButton alloc] initWithFrame:frameimg];
    [homeButton setBackgroundImage:homeImage forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(goToHome) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    // Set add address button properties.
    self.addThisAddressButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.addThisAddressButton.layer.cornerRadius = 3.0;
    self.addThisAddressButton.backgroundColor = [UIColor bentaGreenColor];
    self.addThisAddressButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.addThisAddressButton.titleLabel.font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0];
    [self.addThisAddressButton setTitle:@"Add Address"
                               forState:UIControlStateNormal];
    [self.addThisAddressButton setTitleColor:[UIColor whiteColor]
                                    forState:UIControlStateNormal];
    [self.addThisAddressButton setTitleColor:[UIColor flatWhiteColorDark]
                                    forState:UIControlStateHighlighted];
    [self.addThisAddressButton setContentEdgeInsets:UIEdgeInsetsMake(8, 14, 8, 14)];
    [self.addThisAddressButton addTarget:self action:@selector(doAddAddress) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.addThisAddressButton];
    
    // TableView
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // Spacer
    UIView *spacerLeft = [[UIView alloc] init];
    spacerLeft.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:spacerLeft];
    
    UIView *spacerRight = [[UIView alloc] init];
    spacerRight.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:spacerRight];
    
    // Constraints
    NSDictionary *views = @{ @"addButton" : self.addThisAddressButton,
                             @"tableView" : self.tableView,
                             @"spacerLeft" : spacerLeft,
                             @"spacerRight" : spacerRight };
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|" options:0 metrics:nil views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[spacerLeft][addButton][spacerRight(==spacerLeft)]-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[tableView]-10-[addButton]-10-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:views]];
}

#pragma mark - Methods / Selectors

- (void)doAddAddress {
    NSArray * validationErrors = [self formValidationErrors];
    if (validationErrors.count > 0){
        NSError *err = (NSError *)[validationErrors firstObject];
        [TSMessage showNotificationWithTitle:err.localizedDescription
                                        type:TSMessageNotificationTypeError];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary *addressParams = self.form.formValues;
        NSDictionary *newAddressParams = @{
                                           @"address" : addressParams
                                           };
        [[BESharedManager sharedInstance] be_addShippingAddress:newAddressParams
                                                  withCompletion:^(BOOL finished)
         {
             if(finished) {
                 [TSMessage showNotificationWithTitle:@"Added shipping address."
                                                 type:TSMessageNotificationTypeSuccess];
                 [self goBack];
             } else {
                 [TSMessage showNotificationWithTitle:@"There was an error."
                                             subtitle:@"Please try again later."
                                                 type:TSMessageNotificationTypeError];
             }
             dispatch_async(dispatch_get_main_queue(), ^{
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
             });
         }];
    });
}

- (void)goToHome {
    NSLog(@"Home");
    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
}

- (void)goBack {
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
