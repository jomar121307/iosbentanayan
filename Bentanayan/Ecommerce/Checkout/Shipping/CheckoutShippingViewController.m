//
//  CheckoutShippingViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 23/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "CheckoutShippingViewController.h"

#import "CheckoutShippingAddressCellNode.h"

#import "AddShippingAddressViewController.h"
#import "PaymentOverviewViewController.h"

@interface CheckoutShippingViewController ()

@end

@implementation CheckoutShippingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor flatWhiteColor];
    
    // Navigation
    self.navigationItem.title = @"Shipping Address";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Back
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(goBack)];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    // Home
    UIImage *homeImage = [[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    CGRect frameimg = CGRectMake(0, 0, homeImage.size.width, homeImage.size.height);
    UIButton *homeButton = [[UIButton alloc] initWithFrame:frameimg];
    [homeButton setBackgroundImage:homeImage forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(goToHome) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    // Cart Summary
    self.checkoutShippingDisplayNode = [[CheckoutShippingDisplayNode alloc] init];
    self.checkoutShippingDisplayNode.backgroundColor = [UIColor flatWhiteColor];
    self.checkoutShippingDisplayNode.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.checkoutShippingDisplayNode.addShippingAddressButtonNode addTarget:self
                                                                      action:@selector(goToAddShippingAddress:)
                                                            forControlEvents:ASControlNodeEventTouchUpInside];
    
    [self.checkoutShippingDisplayNode.selectShippingAddressButtonNode addTarget:self
                                                                         action:@selector(goToCheckout:)
                                                               forControlEvents:ASControlNodeEventTouchUpInside];
    
    [self.view addSubnode:self.checkoutShippingDisplayNode];
    
    // CollectionView
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 0.0f;
    layout.minimumLineSpacing = 0.0f;
    
    self.collectionView = [[ASCollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:layout];
    self.collectionView.asyncDataSource = self;
    self.collectionView.asyncDelegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:self.collectionView];
    
    // Constraints
    NSDictionary *views = @{ @"displayNode" : self.checkoutShippingDisplayNode.view,
                             @"collectionView" : self.collectionView };
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[displayNode]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|" options:0 metrics:nil views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collectionView][displayNode(60)]|" options:0 metrics:nil views:views]];
    
    self.shippingAddressResult = [NSMutableArray new];
    
    __weak CheckoutShippingViewController *weakSelf = self;
    
    [self.collectionView addPullToRefreshWithActionHandler:^{
        [[BESharedManager sharedInstance] be_shippingAddressWithCompletion:^(BOOL finished, NSArray *result)
         {
             [weakSelf.collectionView.pullToRefreshView stopAnimating];
             if(finished) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [weakSelf.collectionView performBatchUpdates:^{
                         NSMutableArray *insertIndexPaths = [NSMutableArray array];
                         NSMutableArray *deleteIndexPaths = [NSMutableArray array];
                         NSInteger section = 0;
                         
                         NSArray *oldData = [weakSelf.shippingAddressResult copy];
                         for(ShippingModel *oldModel in oldData) {
                             NSInteger oldItem = [oldData indexOfObject:oldModel];
                             [weakSelf.shippingAddressResult removeObject:oldModel];
                             [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
                         }
                         
                         for(ShippingModel *newModel in result) {
                             [weakSelf.shippingAddressResult addObject:newModel];
                             NSInteger item = [weakSelf.shippingAddressResult indexOfObject:newModel];
                             [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
                         }
                         
                         [weakSelf.collectionView deleteItemsAtIndexPaths:deleteIndexPaths];
                         [weakSelf.collectionView insertItemsAtIndexPaths:insertIndexPaths];
                     } completion:nil
                      ];
                 });
             }
         }];
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewWillDisappear:YES];
    
    [self.collectionView triggerPullToRefresh];
}

#pragma mark - ASCollectionView data source.

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ShippingModel *shipping = [self.shippingAddressResult objectAtIndex:indexPath.row];
    CheckoutShippingAddressCellNode *cellNode = [[CheckoutShippingAddressCellNode alloc] initWithShipping:shipping
                                                                                             withRowIndex:indexPath.row];
    return cellNode;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = self.collectionView.bounds.size.width;
    return ASSizeRangeMake(CGSizeMake(0.0, 0.0), CGSizeMake(width, FLT_MAX));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.shippingAddressResult.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionViewLockDataSource:(ASCollectionView *)collectionView
{
    // lock the data source
    // The data source should not be change until it is unlocked.
}

- (void)collectionViewUnlockDataSource:(ASCollectionView *)collectionView
{
    // unlock the data source to enable data source updating.
}

- (void)collectionView:(UICollectionView *)collectionView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    NSLog(@"fetch additional content");
    [context completeBatchFetching:YES];
}

#pragma mark - Methods / Selectors

- (void)goToAddShippingAddress:(ASButtonNode *)button {
    AddShippingAddressViewController *addShippingAddressContoller = [[AddShippingAddressViewController alloc] init];
    [[SlideNavigationController sharedInstance] pushViewController:addShippingAddressContoller
                                                          animated:YES];
}

- (void)goToCheckout:(ASButtonNode *)button {
    NSArray *selectedIndex = [self.collectionView indexPathsForSelectedItems];
    if(selectedIndex.count > 0) {
        NSIndexPath *indexPath = (NSIndexPath *)[selectedIndex firstObject];
        ShippingModel *shipping = [self.shippingAddressResult objectAtIndex:indexPath.row];
        
        NSDictionary *checkoutParams = @{
                                         @"shippingId" : shipping.shippingId
                                         };
        
        NSLog(@"Setcheckoutinfo: %@", checkoutParams);
        
        __weak CheckoutShippingViewController *weakSelf = self;
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[BESharedManager sharedInstance] be_checkoutInfo:checkoutParams
                                                withCompletion:^(BOOL finished)
             {
                 if(finished) {
                     PaymentOverviewViewController *paymentOverviewController = [[PaymentOverviewViewController alloc] init];
                     paymentOverviewController.cartShop = weakSelf.cartShop;
                     [[SlideNavigationController sharedInstance] pushViewController:paymentOverviewController
                                                                           animated:YES];
                 } else {
                     [TSMessage showNotificationWithTitle:@"Could not proceed. Please try again later."
                                                     type:TSMessageNotificationTypeError];
                 }
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
                 });
             }];
        });
    } else {
        [TSMessage showNotificationWithTitle:@"Please select shipping address."
                                        type:TSMessageNotificationTypeWarning];
    }
}

- (void)goToHome {
    NSLog(@"Home");
    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
}

- (void)goBack {
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

#pragma mark - Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
