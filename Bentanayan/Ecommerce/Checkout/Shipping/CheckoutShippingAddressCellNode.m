//
//  CheckoutShippingAddressCellNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 23/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "CheckoutShippingAddressCellNode.h"

@implementation CheckoutShippingAddressCellNode

- (instancetype)initWithShipping:(ShippingModel *)shipping
                    withRowIndex:(NSInteger)row
{
    self = [super init];
    if(self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        // Shipping Label
        NSDictionary *attrsShipping = @{
                                        NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0f],
                                        NSForegroundColorAttributeName: [UIColor flatBlackColorDark],
                                        NSParagraphStyleAttributeName : paragraphStyleLeft
                                        };
        
        NSAttributedString *stringShipping = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"Shipping Address %ld", row + 1]
                                                                             attributes:attrsShipping];
        
        
        _shippingLabelNode = [[ASTextNode alloc] init];
        _shippingLabelNode.attributedText = stringShipping;
        _shippingLabelNode.maximumNumberOfLines = 1;
        _shippingLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_shippingLabelNode];
        
        // Shipping Name Label
        NSDictionary *attrsShippingOther = @{
                                             NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                             NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                             NSParagraphStyleAttributeName : paragraphStyleLeft
                                             };
        
        NSAttributedString *stringShippingName = [[NSAttributedString alloc] initWithString:shipping.fullName                                                                                 attributes:attrsShippingOther];
        
        
        _shippingNameLabelNode = [[ASTextNode alloc] init];
        _shippingNameLabelNode.attributedText = stringShippingName;
        _shippingNameLabelNode.maximumNumberOfLines = 2;
        _shippingNameLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_shippingNameLabelNode];
        
        // Shipping Address Label
        NSString *address = [NSString stringWithFormat:@"%@, %@, %@, %@ %@", shipping.address.streetAddress, shipping.address.city, shipping.address.province, shipping.address.country, shipping.address.zipcode];
        NSAttributedString *stringShippingAddress = [[NSAttributedString alloc] initWithString:address
                                                                                    attributes:attrsShippingOther];
        
        
        _shippingAddressLabelNode = [[ASTextNode alloc] init];
        _shippingAddressLabelNode.attributedText = stringShippingAddress;
        _shippingAddressLabelNode.maximumNumberOfLines = 3;
        _shippingAddressLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_shippingAddressLabelNode];
        
        // Border Node
        _borderNode = [[ASDisplayNode alloc] init];
        _borderNode.backgroundColor = [UIColor flatGrayColorDark];
        
        [self addSubnode:_borderNode];
    }
    return self;
}

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    if(highlighted) {
        self.backgroundColor = [UIColor colorWithHexString:@"c4e0cd" withAlpha:1.0];
    } else {
        self.backgroundColor = [UIColor clearColor];
    }
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    if(selected) {
        self.backgroundColor = [UIColor colorWithHexString:@"c4e0cd" withAlpha:1.0];
    } else {
        self.backgroundColor = [UIColor clearColor];
    }
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // Content
    ASStackLayoutSpec *stackContent = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                              spacing:2.0f
                                                                       justifyContent:ASStackLayoutJustifyContentStart
                                                                           alignItems:ASStackLayoutAlignItemsStretch
                                                                             children:@[_shippingLabelNode,
                                                                                        _shippingNameLabelNode,
                                                                                        _shippingAddressLabelNode]];
    
    ASInsetLayoutSpec *insetContent = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(10, 10, 10, 10)
                                                                             child:stackContent];
    
    // Main with border
    _borderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0f);
    _borderNode.alignSelf = ASStackLayoutAlignSelfEnd;
    ASStackLayoutSpec *stackMainBorder = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                                 spacing:0.0
                                                                          justifyContent:ASStackLayoutJustifyContentStart
                                                                              alignItems:ASStackLayoutAlignItemsStretch
                                                                                children:@[insetContent, _borderNode]];
    return stackMainBorder;
}

@end
