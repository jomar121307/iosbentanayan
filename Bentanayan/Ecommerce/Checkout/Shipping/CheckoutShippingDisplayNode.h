//
//  CheckoutShippingDisplayNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 23/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface CheckoutShippingDisplayNode : ASDisplayNode

@property (strong, nonatomic) ASButtonNode *addShippingAddressButtonNode;
@property (strong, nonatomic) ASButtonNode *selectShippingAddressButtonNode;

@end
