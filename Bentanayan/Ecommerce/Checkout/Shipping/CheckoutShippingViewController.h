//
//  CheckoutShippingViewController.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 23/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CheckoutShippingDisplayNode.h"

@interface CheckoutShippingViewController : UIViewController <ASCollectionViewDataSource, ASCollectionViewDelegateFlowLayout, SlideNavigationControllerDelegate>

@property (strong, nonatomic) CheckoutShippingDisplayNode *checkoutShippingDisplayNode;
@property (strong, nonatomic) ASCollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray *shippingAddressResult;

@property (strong, nonatomic) CartShopModel *cartShop;

@end
