//
//  CheckoutShippingAddressCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 23/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface CheckoutShippingAddressCellNode : ASCellNode {
    ASTextNode *_shippingLabelNode;
    ASTextNode *_shippingNameLabelNode;
    ASTextNode *_shippingAddressLabelNode;
    
    ASDisplayNode *_borderNode;
}

- (instancetype)initWithShipping:(ShippingModel *)shipping withRowIndex:(NSInteger)row;

@end
