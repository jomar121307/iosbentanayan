//
//  CheckoutShippingDisplayNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 23/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "CheckoutShippingDisplayNode.h"

@implementation CheckoutShippingDisplayNode

- (instancetype)init {
    self = [super init];
    if(self) {
        // Attrs
        NSMutableParagraphStyle *paragraphStyleCenter = [NSMutableParagraphStyle new];
        paragraphStyleCenter.alignment = NSTextAlignmentCenter;
        
        // Empty Cart Button
        NSDictionary *attrsEmptyCart = @{
                                         NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                         NSForegroundColorAttributeName: [UIColor whiteColor],
                                         NSParagraphStyleAttributeName : paragraphStyleCenter
                                         };
        
        NSAttributedString *stringEmptyCart = [[NSAttributedString alloc] initWithString:@"ADD ADDRESS"
                                                                              attributes:attrsEmptyCart];
        
        self.addShippingAddressButtonNode = [[ASButtonNode alloc] init];
        self.addShippingAddressButtonNode.titleNode.maximumNumberOfLines = 1;
        self.addShippingAddressButtonNode.titleNode.pointSizeScaleFactors = @[@0.9, @0.8];
        [self.addShippingAddressButtonNode setAttributedTitle:stringEmptyCart
                                                     forState:ASControlStateNormal];
        [self.addShippingAddressButtonNode setContentEdgeInsets:UIEdgeInsetsMake(8, 14, 8, 14)];
        [self.addShippingAddressButtonNode setHitTestSlop:UIEdgeInsetsMake(-2, -4, -2, -4)];
        [self.addShippingAddressButtonNode setBackgroundColor:[UIColor bentaGreenColor]];
        
        [self addSubnode:self.addShippingAddressButtonNode];
        
        // Checkout Button
        NSDictionary *attrsCheckout = @{
                                        NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                        NSForegroundColorAttributeName: [UIColor whiteColor],
                                        NSParagraphStyleAttributeName : paragraphStyleCenter
                                        };
        
        NSAttributedString *stringCheckout = [[NSAttributedString alloc] initWithString:@"CHOOSE ADDRESS"
                                                                             attributes:attrsCheckout];
        
        self.selectShippingAddressButtonNode = [[ASButtonNode alloc] init];
        self.selectShippingAddressButtonNode.titleNode.maximumNumberOfLines = 1;
        self.selectShippingAddressButtonNode.titleNode.pointSizeScaleFactors = @[@0.9, @0.8];
        [self.selectShippingAddressButtonNode setAttributedTitle:stringCheckout
                                                        forState:ASControlStateNormal];
        [self.selectShippingAddressButtonNode setContentEdgeInsets:UIEdgeInsetsMake(8, 14, 8, 14)];
        [self.selectShippingAddressButtonNode setHitTestSlop:UIEdgeInsetsMake(-2, -4, -2, -4)];
        [self.selectShippingAddressButtonNode setBackgroundColor:[UIColor bentaGreenColor]];
        
        [self addSubnode:self.selectShippingAddressButtonNode];
    }
    return self;
}

- (void)layout {
    [super layout];
    [self measureWithSizeRange:ASSizeRangeMake(CGSizeMake(self.bounds.size.width, 60.0f), CGSizeMake(self.bounds.size.width, 60.0f))];
}

- (void)layoutDidFinish {
    [super layoutDidFinish];
    
    [self.view updateConstraintsIfNeeded];
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // Main Stack
    ASStackLayoutSpec *stackMain = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                           spacing:10.0f
                                                                    justifyContent:ASStackLayoutJustifyContentCenter
                                                                        alignItems:ASStackLayoutAlignItemsCenter
                                                                          children:@[self.addShippingAddressButtonNode,
                                                                                     self.selectShippingAddressButtonNode]];
    
    ASInsetLayoutSpec *insetMain = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(10, 10, 10, 10)
                                                                          child:stackMain];
    
    return insetMain;
}

@end
