//
//  AddShippingAddressViewController.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 23/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <XLForm/XLForm.h>

@interface AddShippingAddressViewController : XLFormViewController <UITextFieldDelegate>

@property (strong, nonatomic) UIButton *addThisAddressButton;

@end
