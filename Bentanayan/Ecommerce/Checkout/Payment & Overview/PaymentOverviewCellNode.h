//
//  PaymentOverviewCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 28/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface PaymentOverviewCellNode : ASCellNode <ASNetworkImageNodeDelegate> {
    ASNetworkImageNode *_productImageNode;
    
    ASTextNode *_productNameLabelNode;
    ASTextNode *_productOwnerLabelNode;
    ASTextNode *_productPriceLabelNode;
    
    ASTextNode *_productQuantityLabelNode;
    ASTextNode *_productTotalPriceLabelNode;
    
    ASDisplayNode *_borderNode;
}

@property (strong, nonatomic) CartItemModel *cartItem;

- (instancetype)initWithCartItemModel:(CartItemModel *)cartItem;

@end
