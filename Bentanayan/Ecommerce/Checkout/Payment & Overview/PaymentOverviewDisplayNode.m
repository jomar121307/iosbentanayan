//
//  PaymentOverviewDisplayNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 28/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "PaymentOverviewDisplayNode.h"

@implementation PaymentOverviewDisplayNode

- (instancetype)initWithCartShop:(CartShopModel *)cartShop
{
    self = [super init];
    if(self) {
        
        self.cartShop = cartShop;
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        NSMutableParagraphStyle *paragraphStyleRight = [NSMutableParagraphStyle new];
        paragraphStyleRight.alignment = NSTextAlignmentRight;
        
        NSMutableParagraphStyle *paragraphStyleCenter = [NSMutableParagraphStyle new];
        paragraphStyleCenter.alignment = NSTextAlignmentCenter;
        
        NSDictionary *attrsLabel = @{
                                     NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Light" size:12.0f],
                                     NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                     NSParagraphStyleAttributeName : paragraphStyleLeft
                                     };
        
        NSDictionary *attrsPrice = @{
                                     NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:12.0f],
                                     NSForegroundColorAttributeName: [UIColor flatBlackColorDark],
                                     NSParagraphStyleAttributeName : paragraphStyleLeft
                                     };
        
        // Subtotal Label
        NSAttributedString *stringSubtotal = [[NSAttributedString alloc] initWithString:@"Subtotal Items:"
                                                                             attributes:attrsLabel];
        
        
        _subtotalLabelNode = [[ASTextNode alloc] init];
        _subtotalLabelNode.attributedText = stringSubtotal;
        _subtotalLabelNode.maximumNumberOfLines = 1;
        _subtotalLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_subtotalLabelNode];
        
        NSString *subTotal = [NSString stringWithFormat:@"PHP %0.2f", self.cartShop.totalPrice];
        
        NSAttributedString *stringPriceSubtotal = [[NSAttributedString alloc] initWithString:subTotal
                                                                                  attributes:attrsPrice];
        
        
        self.priceSubtotalLabelNode = [[ASTextNode alloc] init];
        self.priceSubtotalLabelNode.attributedText = stringPriceSubtotal;
        self.priceSubtotalLabelNode.maximumNumberOfLines = 1;
        self.priceSubtotalLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:self.priceSubtotalLabelNode];
        
        // Shipping Fee Label
        NSAttributedString *stringShippingFee = [[NSAttributedString alloc] initWithString:@"Shipping Fee:"
                                                                                attributes:attrsLabel];
        
        
        _shippingFeeLabelNode = [[ASTextNode alloc] init];
        _shippingFeeLabelNode.attributedText = stringShippingFee;
        _shippingFeeLabelNode.maximumNumberOfLines = 1;
        _shippingFeeLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_shippingFeeLabelNode];
        
        NSString *shippingFee = [NSString stringWithFormat:@"PHP %0.2f", self.cartShop.shippingCost];
        
        NSAttributedString *stringPriceShippingFee = [[NSAttributedString alloc] initWithString:shippingFee
                                                                                     attributes:attrsPrice];
        
        
        self.priceShippingFeeLabelNode = [[ASTextNode alloc] init];
        self.priceShippingFeeLabelNode.attributedText = stringPriceShippingFee;
        self.priceShippingFeeLabelNode.maximumNumberOfLines = 1;
        self.priceShippingFeeLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:self.priceShippingFeeLabelNode];
        
        // Handling Fee Label
        NSAttributedString *stringHandlingFee = [[NSAttributedString alloc] initWithString:@"Handling Fee:"
                                                                                attributes:attrsLabel];
        
        
        _handlingFeeLabelNode = [[ASTextNode alloc] init];
        _handlingFeeLabelNode.attributedText = stringHandlingFee;
        _handlingFeeLabelNode.maximumNumberOfLines = 1;
        _handlingFeeLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_handlingFeeLabelNode];
        
        CGFloat total = self.cartShop.totalPrice + self.cartShop.shippingCost;
        CGFloat handling = (total * 0.01) + 0.3;
        
        NSString *handlingFee = [NSString stringWithFormat:@"PHP %0.2f", handling];
        
        NSAttributedString *stringPriceHandlingFee = [[NSAttributedString alloc] initWithString:handlingFee
                                                                                     attributes:attrsPrice];
        
        
        self.priceHandlingFeeLabelNode = [[ASTextNode alloc] init];
        self.priceHandlingFeeLabelNode.attributedText = stringPriceHandlingFee;
        self.priceHandlingFeeLabelNode.maximumNumberOfLines = 1;
        self.priceHandlingFeeLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:self.priceHandlingFeeLabelNode];
        
        // Grand Total:
        NSAttributedString *stringGrandTotal = [[NSAttributedString alloc] initWithString:@"GRAND TOTAL:"
                                                                               attributes:attrsLabel];
        
        
        _grandTotalLabelNode = [[ASTextNode alloc] init];
        _grandTotalLabelNode.attributedText = stringGrandTotal;
        _grandTotalLabelNode.maximumNumberOfLines = 1;
        _grandTotalLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_grandTotalLabelNode];
        
        NSDictionary *attrsPriceGrandTotal = @{
                                               NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:20.0f],
                                               NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                               NSParagraphStyleAttributeName : paragraphStyleRight
                                               };
        
        NSString *grandTotal = [NSString stringWithFormat:@"PHP %0.2f", total + handling];
        
        NSAttributedString *stringPriceGrandTotal = [[NSAttributedString alloc] initWithString:grandTotal
                                                                                    attributes:attrsPriceGrandTotal];
        
        
        self.priceGrandTotalLabelNode = [[ASTextNode alloc] init];
        self.priceGrandTotalLabelNode.attributedText = stringPriceGrandTotal;
        self.priceGrandTotalLabelNode.maximumNumberOfLines = 1;
        self.priceGrandTotalLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:self.priceGrandTotalLabelNode];
        
        // Pay Button
        NSDictionary *attrsSubmitOrder = @{
                                           NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                           NSForegroundColorAttributeName: [UIColor whiteColor],
                                           NSParagraphStyleAttributeName : paragraphStyleCenter
                                           };
        
        NSAttributedString *stringSubmitOrder = [[NSAttributedString alloc] initWithString:@"SUBMIT ORDER"
                                                                                attributes:attrsSubmitOrder];
        
        self.payButtonNode = [[ASButtonNode alloc] init];
        self.payButtonNode.titleNode.maximumNumberOfLines = 1;
        self.payButtonNode.titleNode.pointSizeScaleFactors = @[@0.9, @0.8];
        [self.payButtonNode setAttributedTitle:stringSubmitOrder
                                      forState:ASControlStateNormal];
        [self.payButtonNode setContentEdgeInsets:UIEdgeInsetsMake(8, 14, 8, 14)];
        [self.payButtonNode setHitTestSlop:UIEdgeInsetsMake(-2, -4, -2, -4)];
        [self.payButtonNode setBackgroundColor:[UIColor bentaGreenColor]];
        
        [self addSubnode:self.payButtonNode];
    }
    return self;
}

- (void)layout {
    [super layout];
    [self measureWithSizeRange:ASSizeRangeMake(CGSizeMake(self.bounds.size.width, 140.0f), CGSizeMake(self.bounds.size.width, 140.0f))];
}

- (void)layoutDidFinish {
    [super layoutDidFinish];
    
    [self.view updateConstraintsIfNeeded];
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
    spacer.flexGrow = YES;
    
    // Subtotal
    self.priceSubtotalLabelNode.flexShrink = YES;
    ASStackLayoutSpec *stackSubtotal = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                               spacing:2.0f
                                                                        justifyContent:ASStackLayoutJustifyContentCenter
                                                                            alignItems:ASStackLayoutAlignItemsStretch
                                                                              children:@[_subtotalLabelNode,
                                                                                         spacer,
                                                                                         self.priceSubtotalLabelNode]];
    
    // Shipping Fee
    self.priceShippingFeeLabelNode.flexShrink = YES;
    ASStackLayoutSpec *stackShippingFee = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                  spacing:2.0f
                                                                           justifyContent:ASStackLayoutJustifyContentStart
                                                                               alignItems:ASStackLayoutAlignItemsStretch
                                                                                 children:@[_shippingFeeLabelNode,
                                                                                            spacer,
                                                                                            self.priceShippingFeeLabelNode]];
    
    // Handling Fee
    self.priceHandlingFeeLabelNode.flexShrink = YES;
    ASStackLayoutSpec *stackHandlingFee = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                  spacing:2.0f
                                                                           justifyContent:ASStackLayoutJustifyContentStart
                                                                               alignItems:ASStackLayoutAlignItemsStretch
                                                                                 children:@[_handlingFeeLabelNode,
                                                                                            spacer,
                                                                                            self.priceHandlingFeeLabelNode]];
    
    
    // Grand Total
    self.priceGrandTotalLabelNode.flexShrink = YES;
    ASStackLayoutSpec *stackGrandTotal = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                 spacing:2.0f
                                                                          justifyContent:ASStackLayoutJustifyContentStart
                                                                              alignItems:ASStackLayoutAlignItemsStretch
                                                                                children:@[_grandTotalLabelNode,
                                                                                           spacer,
                                                                                           self.priceGrandTotalLabelNode]];
    
    // Right Stack
    ASStackLayoutSpec *stackRight = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                            spacing:4.0f
                                                                     justifyContent:ASStackLayoutJustifyContentEnd
                                                                         alignItems:ASStackLayoutAlignItemsStretch
                                                                           children:@[stackSubtotal,
                                                                                      stackShippingFee,
                                                                                      stackHandlingFee,
                                                                                      stackGrandTotal]];
    
    // Content Stack
    ASStackLayoutSpec *spacerContent = [[ASStackLayoutSpec alloc] init];
    stackRight.flexBasis = ASRelativeDimensionMakeWithPercent(0.6);
    spacerContent.flexBasis = ASRelativeDimensionMakeWithPercent(0.4);
    ASStackLayoutSpec *stackContent = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                              spacing:0.0f
                                                                       justifyContent:ASStackLayoutJustifyContentCenter
                                                                           alignItems:ASStackLayoutAlignItemsEnd
                                                                             children:@[spacerContent, stackRight]];
    
    // Main Stack
    self.payButtonNode.alignSelf = ASStackLayoutAlignSelfCenter;
    ASStackLayoutSpec *stackMain = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                           spacing:4.0f
                                                                    justifyContent:ASStackLayoutJustifyContentCenter
                                                                        alignItems:ASStackLayoutAlignItemsStretch
                                                                          children:@[stackContent, self.payButtonNode]];
    
    ASInsetLayoutSpec *insetMain = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(10, 10, 10, 10)
                                                                          child:stackMain];
    
    return [ASCenterLayoutSpec centerLayoutSpecWithCenteringOptions:ASCenterLayoutSpecCenteringXY
                                                      sizingOptions:ASCenterLayoutSpecSizingOptionDefault
                                                              child:insetMain];
}


@end
