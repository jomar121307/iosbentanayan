//
//  PaymentOverviewViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 28/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "PaymentOverviewViewController.h"

#import "PaymentOverviewCellNode.h"

@interface PaymentOverviewViewController ()

@end

@implementation PaymentOverviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor flatWhiteColor];
    
    // Navigation
    self.navigationItem.title = @"Overview & Payment";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Back
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(goBack)];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    // Home
    UIImage *homeImage = [[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    CGRect frameimg = CGRectMake(0, 0, homeImage.size.width, homeImage.size.height);
    UIButton *homeButton = [[UIButton alloc] initWithFrame:frameimg];
    [homeButton setBackgroundImage:homeImage forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(goToHome) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    // Cart Summary
    self.paymentOverviewDisplayNode = [[PaymentOverviewDisplayNode alloc] initWithCartShop:self.cartShop];
    self.paymentOverviewDisplayNode.backgroundColor = [UIColor flatWhiteColor];
    self.paymentOverviewDisplayNode.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.paymentOverviewDisplayNode.payButtonNode addTarget:self
                                                      action:@selector(doSubmitOrder:)
                                            forControlEvents:ASControlNodeEventTouchUpInside];
    
    [self.view addSubnode:self.paymentOverviewDisplayNode];
    
    // CollectionView
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 0.0f;
    layout.minimumLineSpacing = 0.0f;
    
    self.collectionView = [[ASCollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:layout];
    self.collectionView.asyncDataSource = self;
    self.collectionView.asyncDelegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:self.collectionView];
    
    // Constraints
    NSDictionary *views = @{ @"cartSummary" : self.paymentOverviewDisplayNode.view,
                             @"collectionView" : self.collectionView };
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cartSummary]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|" options:0 metrics:nil views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collectionView][cartSummary(140)]|" options:0 metrics:nil views:views]];
    
    self.cartItemResult = [NSMutableArray new];
    
    __weak PaymentOverviewViewController *weakSelf = self;
    
    [self.collectionView addPullToRefreshWithActionHandler:^{
        [[BESharedManager sharedInstance] be_productsInCartForPayment:YES
                                                       WithCompletion:^(BOOL finished, NSArray *result)
         {
             [weakSelf.collectionView.pullToRefreshView stopAnimating];
             if(finished) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [weakSelf.collectionView performBatchUpdates:^{
                         NSMutableArray *insertIndexPaths = [NSMutableArray array];
                         NSMutableArray *deleteIndexPaths = [NSMutableArray array];
                         NSInteger section = 0;
                         
                         NSArray *oldData = [weakSelf.cartItemResult copy];
                         for(CartItemModel *oldModel in oldData) {
                             NSInteger oldItem = [oldData indexOfObject:oldModel];
                             [weakSelf.cartItemResult removeObject:oldModel];
                             [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
                         }
                         
                         for(CartItemModel *newModel in result) {
                             [weakSelf.cartItemResult addObject:newModel];
                             NSInteger item = [weakSelf.cartItemResult indexOfObject:newModel];
                             [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
                         }
                         
                         [weakSelf.collectionView deleteItemsAtIndexPaths:deleteIndexPaths];
                         [weakSelf.collectionView insertItemsAtIndexPaths:insertIndexPaths];
                     } completion:nil];
                 });
             }
         }];
    }];
    
    [self.collectionView triggerPullToRefresh];
}

#pragma mark - ASCollectionView data source.

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CartItemModel *cartItem = [self.cartItemResult objectAtIndex:indexPath.row];
    
    PaymentOverviewCellNode *cellNode = [[PaymentOverviewCellNode alloc] initWithCartItemModel:cartItem];
    return cellNode;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = self.collectionView.bounds.size.width;
    
    return ASSizeRangeMake(CGSizeMake(0.0, 0.0), CGSizeMake(width, FLT_MAX));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.cartItemResult.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionViewLockDataSource:(ASCollectionView *)collectionView
{
    // lock the data source
    // The data source should not be change until it is unlocked.
}

- (void)collectionViewUnlockDataSource:(ASCollectionView *)collectionView
{
    // unlock the data source to enable data source updating.
}

- (void)collectionView:(UICollectionView *)collectionView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    NSLog(@"fetch additional content");
    [context completeBatchFetching:YES];
}

#pragma mark - Methods / Selectors

- (void)goToHome {
    NSLog(@"Home");
    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
}

- (void)goBack {
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

- (void)doSubmitOrder:(ASButtonNode *)button {
    SCLAlertView *alertView = [BentaAlertView showAlert];
    
    for(NSString *option in self.cartShop.ownerId.paymentPlatformsAvailable) {
        if([option isEqualToString:@"paypal"]) {
            [alertView addButton:@"Pay With Paypal"
                     actionBlock:^
             {
                 [self payWithPaypal];
             }];
        } else if([option isEqualToString:@"ecash"]) {
            [alertView addButton:@"Pay With E-Cash"
                     actionBlock:^
             {
                 NSLog(@"E-Cash");
                 [self payWithECash];
             }];
        }
    }
    
    [alertView showQuestion:self
                      title:@"Choose Payment Method"
                   subTitle:nil
           closeButtonTitle:@"Cancel"
                   duration:0.0];
}

- (void)payWithPaypal {
    SCLAlertView *alertView = [BentaAlertView showAlert];
    
    [alertView addButton:@"Proceed"
             actionBlock:^
     {
         [MBProgressHUD showHUDAddedTo:self.view animated:YES];
         dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
             NSDictionary *checkoutParams = @{
                                              @"paymentPlatform" : @"paypal"
                                              };
             [[BESharedManager sharedInstance] be_checkoutInfo:checkoutParams
                                                withCompletion:^(BOOL finished)
              {
                  if(finished) {
                      [[BESharedManager sharedInstance] be_payOrderWithCompletion:^(BOOL finished)
                       {
                           if(!finished) {
                               [TSMessage showNotificationWithTitle:@"Paypal Payment Failed"
                                                               type:TSMessageNotificationTypeError];
                           }
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [MBProgressHUD hideHUDForView:self.view animated:YES];
                           });
                       }];
                  } else {
                      [TSMessage showNotificationWithTitle:@"Paypal Payment Failed"
                                                      type:TSMessageNotificationTypeError];
                      dispatch_async(dispatch_get_main_queue(), ^{
                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                      });
                  }
              }];
         });
     }];
    
    [alertView showQuestion:self
                      title:@"Proceed"
                   subTitle:@"You will be redirected to paypal."
           closeButtonTitle:@"Cancel"
                   duration:0.0f];
}

- (void)payWithECash {
    SCLAlertView *alertView = [BentaAlertView showAlert];
    alertView.customViewColor = [UIColor bentaGreenColor];
    
    UITextField *textFieldUsername = [alertView addTextField:@"Username"];
    [textFieldUsername setKeyboardType:UIKeyboardTypeDefault];
    [textFieldUsername setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [textFieldUsername setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    UITextField *textFieldPassword = [alertView addTextField:@"Password"];
    [textFieldPassword setKeyboardType:UIKeyboardTypeDefault];
    [textFieldPassword setAutocorrectionType:UITextAutocorrectionTypeNo];
    [textFieldPassword setSecureTextEntry:YES];
    
    [alertView addButton:@"Pay now"
             actionBlock:^
     {
         [textFieldUsername resignFirstResponder];
         [textFieldPassword resignFirstResponder];
         
         if(!((textFieldUsername.text && textFieldUsername.text.length > 0) || (textFieldPassword.text && textFieldPassword.text.length > 0))){
             [TSMessage showNotificationWithTitle:@"Validation Error"
                                         subtitle:@"Username / Password field is empty"
                                             type:TSMessageNotificationTypeError];
             return;
         }
         
         [MBProgressHUD showHUDAddedTo:self.view animated:YES];
         dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
             NSDictionary *checkoutParams = @{
                                              @"paymentPlatform" : @"ecash"
                                              };
             NSDictionary *ecashParams = @{
                                           @"username" : textFieldUsername.text,
                                           @"password" : textFieldPassword.text
                                           };
             [[BESharedManager sharedInstance] be_checkoutInfo:checkoutParams
                                                withCompletion:^(BOOL finished)
              {
                  if(finished) {
                      [[BESharedManager sharedInstance] be_payOrderWithECash:ecashParams
                                                              withCompletion:^(BOOL finished, NSString *error)
                       {
                           if(finished) {
                               [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
                               [TSMessage showNotificationWithTitle:@"E-Cash Payment Successful"
                                                           subtitle:@"Thank you for purchasing."
                                                               type:TSMessageNotificationTypeSuccess];
                           } else {
                               [TSMessage showNotificationWithTitle:@"E-Cash Payment Failed"
                                                           subtitle:error
                                                               type:TSMessageNotificationTypeError];
                           }
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [MBProgressHUD hideHUDForView:self.view animated:YES];
                           });
                       }];
                  } else {
                      [TSMessage showNotificationWithTitle:@"E-Cash Payment Failed"
                                                      type:TSMessageNotificationTypeError];
                      dispatch_async(dispatch_get_main_queue(), ^{
                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                      });
                  }
              }];
         });
     }];
    
    [alertView showInfo:self
                  title:@"E-Cash Login"
               subTitle:nil
       closeButtonTitle:@"Cancel"
               duration:0.0f];
}

#pragma mark - Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
