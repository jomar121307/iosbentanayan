//
//  PaymentOverviewViewController.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 28/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PaymentOverviewDisplayNode.h"

@interface PaymentOverviewViewController : UIViewController <ASCollectionViewDataSource, ASCollectionViewDelegateFlowLayout, SlideNavigationControllerDelegate>

@property (strong, nonatomic) PaymentOverviewDisplayNode *paymentOverviewDisplayNode;
@property (strong, nonatomic) ASCollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray *cartItemResult;

@property (strong, nonatomic) CartShopModel *cartShop;

@end
