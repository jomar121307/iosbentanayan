//
//  PaymentOverviewDisplayNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 28/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface PaymentOverviewDisplayNode : ASDisplayNode {
    ASTextNode *_subtotalLabelNode;
    ASTextNode *_handlingFeeLabelNode;
    ASTextNode *_shippingFeeLabelNode;
    ASTextNode *_grandTotalLabelNode;
}

@property (strong, nonatomic) ASTextNode *priceSubtotalLabelNode;
@property (strong, nonatomic) ASTextNode *priceHandlingFeeLabelNode;
@property (strong, nonatomic) ASTextNode *priceShippingFeeLabelNode;
@property (strong, nonatomic) ASTextNode *priceGrandTotalLabelNode;

@property (strong, nonatomic) ASButtonNode *payButtonNode;

@property (strong, nonatomic) CartShopModel *cartShop;

- (instancetype)initWithCartShop:(CartShopModel *)cartShop;

@end
