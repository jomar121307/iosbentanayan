//
//  PurchaseHistoryItemDisplayNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 29/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "PurchaseHistoryItemDisplayNode.h"

@implementation PurchaseHistoryItemDisplayNode

- (instancetype)initWithAddress:(ShippingAddressModel *)address
                       subTotal:(float)subTotal
                   shippingCost:(float)shippingCost
{
    self = [super init];
    if(self) {
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        NSMutableParagraphStyle *paragraphStyleRight = [NSMutableParagraphStyle new];
        paragraphStyleRight.alignment = NSTextAlignmentRight;
        
        NSMutableParagraphStyle *paragraphStyleCenter = [NSMutableParagraphStyle new];
        paragraphStyleCenter.alignment = NSTextAlignmentCenter;
        
        NSDictionary *attrsLabelLight = @{
                                          NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Light" size:14.0f],
                                          NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                          NSParagraphStyleAttributeName : paragraphStyleLeft
                                          };
        
        NSDictionary *attrsLabelRegular = @{
                                            NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                            NSForegroundColorAttributeName: [UIColor flatBlackColorDark],
                                            NSParagraphStyleAttributeName : paragraphStyleLeft
                                            };
        
        
        // Address Label
        NSMutableAttributedString *stringAddressLabel = [[NSMutableAttributedString alloc] initWithString:@"Billing Address: "
                                                                                               attributes:attrsLabelLight];
        NSString *add = [NSString stringWithFormat:@"%@, %@, %@, %@ %@", address.streetAddress, address.city, address.province, address.country, address.zipcode];
        NSMutableAttributedString *stringAddress = [[NSMutableAttributedString alloc] initWithString:add
                                                                                          attributes:attrsLabelRegular];
        
        [stringAddressLabel appendAttributedString:stringAddress];
        
        _addressLabelNode = [[ASTextNode alloc] init];
        _addressLabelNode.attributedText = stringAddressLabel;
        _addressLabelNode.maximumNumberOfLines = 2;
        _addressLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_addressLabelNode];
        
        // Subtotal Label
        NSAttributedString *stringSubtotal = [[NSAttributedString alloc] initWithString:@"Subtotal Items:"
                                                                             attributes:attrsLabelLight];
        
        
        _subtotalLabelNode = [[ASTextNode alloc] init];
        _subtotalLabelNode.attributedText = stringSubtotal;
        _subtotalLabelNode.maximumNumberOfLines = 1;
        _subtotalLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_subtotalLabelNode];
        
        NSAttributedString *stringPriceSubtotal = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"PHP %0.2f", subTotal]
                                                                                  attributes:attrsLabelRegular];
        
        
        _priceSubtotalLabelNode = [[ASTextNode alloc] init];
        _priceSubtotalLabelNode.attributedText = stringPriceSubtotal;
        _priceSubtotalLabelNode.maximumNumberOfLines = 1;
        _priceSubtotalLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_priceSubtotalLabelNode];
        
        // Shipping Fee Label
        NSAttributedString *stringShippingFee = [[NSAttributedString alloc] initWithString:@"Shipping Fee:"
                                                                                attributes:attrsLabelLight];
        
        
        _shippingFeeLabelNode = [[ASTextNode alloc] init];
        _shippingFeeLabelNode.attributedText = stringShippingFee;
        _shippingFeeLabelNode.maximumNumberOfLines = 1;
        _shippingFeeLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_shippingFeeLabelNode];
        
        NSAttributedString *stringPriceShippingFee = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"PHP %0.2f", shippingCost]
                                                                                     attributes:attrsLabelRegular];
        
        _priceShippingFeeLabelNode = [[ASTextNode alloc] init];
        _priceShippingFeeLabelNode.attributedText = stringPriceShippingFee;
        _priceShippingFeeLabelNode.maximumNumberOfLines = 1;
        _priceShippingFeeLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_priceShippingFeeLabelNode];
        
        // Handling Fee Label
        NSAttributedString *stringHandlingFee = [[NSAttributedString alloc] initWithString:@"Handling Fee:"
                                                                                attributes:attrsLabelLight];
        
        
        _handlingFeeLabelNode = [[ASTextNode alloc] init];
        _handlingFeeLabelNode.attributedText = stringHandlingFee;
        _handlingFeeLabelNode.maximumNumberOfLines = 1;
        _handlingFeeLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_handlingFeeLabelNode];
        
        CGFloat total = subTotal + shippingCost;
        CGFloat handling = (total * 0.01) + 0.3;
        
        NSString *handlingFee = [NSString stringWithFormat:@"PHP %0.2f", handling];
        
        NSAttributedString *stringPriceHandlingFee = [[NSAttributedString alloc] initWithString:handlingFee
                                                                                     attributes:attrsLabelRegular];
        
        
        _priceHandlingFeeLabelNode = [[ASTextNode alloc] init];
        _priceHandlingFeeLabelNode.attributedText = stringPriceHandlingFee;
        _priceHandlingFeeLabelNode.maximumNumberOfLines = 1;
        _priceHandlingFeeLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_priceHandlingFeeLabelNode];
        
        // Grand Total:
        NSAttributedString *stringGrandTotal = [[NSAttributedString alloc] initWithString:@"GRAND TOTAL:"
                                                                               attributes:attrsLabelLight];
        
        
        _grandTotalLabelNode = [[ASTextNode alloc] init];
        _grandTotalLabelNode.attributedText = stringGrandTotal;
        _grandTotalLabelNode.maximumNumberOfLines = 1;
        _grandTotalLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_grandTotalLabelNode];
        
        NSDictionary *attrsLabelRegularGrandTotal = @{
                                                      NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:20.0f],
                                                      NSForegroundColorAttributeName: [UIColor bentaGreenColor],
                                                      NSParagraphStyleAttributeName : paragraphStyleRight
                                                      };
        
        NSString *grandTotal = [NSString stringWithFormat:@"PHP %0.2f", total + handling];
        
        NSAttributedString *stringPriceGrandTotal = [[NSAttributedString alloc] initWithString:grandTotal
                                                                                    attributes:attrsLabelRegularGrandTotal];
        
        
        _priceGrandTotalLabelNode = [[ASTextNode alloc] init];
        _priceGrandTotalLabelNode.attributedText = stringPriceGrandTotal;
        _priceGrandTotalLabelNode.maximumNumberOfLines = 1;
        _priceGrandTotalLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_priceGrandTotalLabelNode];
    }
    return self;
}

- (void)layout {
    [super layout];
    [self measureWithSizeRange:ASSizeRangeMake(CGSizeMake(self.bounds.size.width, 140.0f), CGSizeMake(self.bounds.size.width, 140.0f))];
}

- (void)layoutDidFinish {
    [super layoutDidFinish];
    
    [self.view updateConstraintsIfNeeded];
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
    spacer.flexGrow = YES;
    
    // Subtotal
    _priceSubtotalLabelNode.flexShrink = YES;
    ASStackLayoutSpec *stackSubtotal = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                               spacing:4.0f
                                                                        justifyContent:ASStackLayoutJustifyContentStart
                                                                            alignItems:ASStackLayoutAlignItemsStretch
                                                                              children:@[_subtotalLabelNode,
                                                                                         _priceSubtotalLabelNode]];
    
    // Shipping Fee
    _priceShippingFeeLabelNode.flexShrink = YES;
    ASStackLayoutSpec *stackShippingFee = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                  spacing:4.0f
                                                                           justifyContent:ASStackLayoutJustifyContentStart
                                                                               alignItems:ASStackLayoutAlignItemsStretch
                                                                                 children:@[_shippingFeeLabelNode,
                                                                                            _priceShippingFeeLabelNode]];
    
    // Handling Fee
    _priceHandlingFeeLabelNode.flexShrink = YES;
    ASStackLayoutSpec *stackHandlingFee = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                  spacing:4.0f
                                                                           justifyContent:ASStackLayoutJustifyContentStart
                                                                               alignItems:ASStackLayoutAlignItemsStretch
                                                                                 children:@[_handlingFeeLabelNode,
                                                                                            _priceHandlingFeeLabelNode]];
    
    
    // Grand Total
    _priceGrandTotalLabelNode.flexShrink = YES;
    ASStackLayoutSpec *stackGrandTotal = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                 spacing:4.0f
                                                                          justifyContent:ASStackLayoutJustifyContentStart
                                                                              alignItems:ASStackLayoutAlignItemsStretch
                                                                                children:@[_grandTotalLabelNode,
                                                                                           _priceGrandTotalLabelNode]];
    
    // Main Stack
    _addressLabelNode.spacingAfter = 6.0f;
    ASStackLayoutSpec *stackMain = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                           spacing:2.0f
                                                                    justifyContent:ASStackLayoutJustifyContentStart
                                                                        alignItems:ASStackLayoutAlignItemsStretch
                                                                          children:@[_addressLabelNode,
                                                                                     stackSubtotal,
                                                                                     stackShippingFee,
                                                                                     stackHandlingFee,
                                                                                     stackGrandTotal]];
    
    // Main Stack
    ASInsetLayoutSpec *insetMain = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(10, 10, 10, 10)
                                                                          child:stackMain];
    
    return insetMain;
}

@end
