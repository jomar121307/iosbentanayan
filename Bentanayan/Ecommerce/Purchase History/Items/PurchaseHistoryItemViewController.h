//
//  PurchaseHistoryItemViewController.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 29/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PurchaseHistoryItemDisplayNode.h"

@interface PurchaseHistoryItemViewController : UIViewController <ASCollectionViewDataSource, ASCollectionViewDelegateFlowLayout, SlideNavigationControllerDelegate>

@property (strong, nonatomic) PurchaseHistoryItemDisplayNode *purchaseHistoryItemDisplayNode;

@property (strong, nonatomic) ASCollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray *itemResult;

@property (strong, nonatomic) OrderModel *order;

- (instancetype)initWithOrder:(OrderModel *)order;

@end
