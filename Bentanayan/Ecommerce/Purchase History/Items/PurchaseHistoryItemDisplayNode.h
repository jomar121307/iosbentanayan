//
//  PurchaseHistoryItemDisplayNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 29/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface PurchaseHistoryItemDisplayNode : ASDisplayNode {
    ASTextNode *_addressLabelNode;
    
    ASTextNode *_subtotalLabelNode;
    ASTextNode *_handlingFeeLabelNode;
    ASTextNode *_shippingFeeLabelNode;
    ASTextNode *_grandTotalLabelNode;
    
    ASTextNode *_priceSubtotalLabelNode;
    ASTextNode *_priceHandlingFeeLabelNode;
    ASTextNode *_priceShippingFeeLabelNode;
    ASTextNode *_priceGrandTotalLabelNode;
}

- (instancetype)initWithAddress:(ShippingAddressModel *)address
                       subTotal:(float)subTotal
                   shippingCost:(float)shippingCost;

@end
