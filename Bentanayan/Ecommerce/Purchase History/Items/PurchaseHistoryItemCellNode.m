//
//  PurchasedHistoryItemCellNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 29/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "PurchaseHistoryItemCellNode.h"

@implementation PurchaseHistoryItemCellNode

- (instancetype)initWithItem:(CartItemModel *)item {
    self = [super init];
    if(self) {
        
        self.item = item;
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        NSDictionary *attrsLabel = @{
                                     NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                     NSForegroundColorAttributeName: [UIColor flatGrayColorDark],
                                     NSParagraphStyleAttributeName : paragraphStyleLeft
                                     };
        
        // Product Image
        _productImageNode = [[ASNetworkImageNode alloc] init];
        if(self.item.productId.metadata.photos) {
            NSArray *photos = self.item.productId.metadata.photos;
            if(photos.count > 0) {
                ProductPhotoModel *photo = [photos firstObject];
                if(photo.cropped) {
                    _productImageNode.URL = [NSURL URLWithString:photo.cropped.secure_url];
                } else if(photo.original) {
                    _productImageNode.URL = [NSURL URLWithString:photo.original.secure_url];
                } else {
                    _productImageNode.URL = [NSURL URLWithString:photo.secure_url];
                }
            }
        }
        _productImageNode.delegate = self;
        _productImageNode.defaultImage = [UIImage imageNamed:@"product-placeholder"];
        _productImageNode.contentMode = UIViewContentModeScaleAspectFit;
        
        [self addSubnode:_productImageNode];
        
        // Product Name Label
        NSDictionary *attrsProductName = @{
                                           NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0f],
                                           NSForegroundColorAttributeName: [UIColor flatBlackColorDark],
                                           NSParagraphStyleAttributeName : paragraphStyleLeft
                                           };
        
        NSAttributedString *stringProductName = [[NSAttributedString alloc] initWithString:self.item.productId.name
                                                                                attributes:attrsProductName];
        
        
        _productNameLabelNode = [[ASTextNode alloc] init];
        _productNameLabelNode.attributedText = stringProductName;
        _productNameLabelNode.maximumNumberOfLines = 2;
        _productNameLabelNode.pointSizeScaleFactors = @[@0.9, @0.8, @0.7];
        
        [self addSubnode:_productNameLabelNode];
        
        // Product Owner Label
        NSString *fullName;
        NSString *firstName = self.item.productId.userId.firstName;
        NSString *lastName = self.item.productId.userId.lastName;
        if(lastName != nil) {
            fullName = [NSString stringWithFormat:@"By: %@ %@", firstName, lastName];
        } else {
            fullName = [NSString stringWithFormat:@"By: %@", firstName];
        }
        NSAttributedString *stringProductOwner = [[NSAttributedString alloc] initWithString:fullName
                                                                                 attributes:attrsLabel];
        
        
        _productOwnerLabelNode = [[ASTextNode alloc] init];
        _productOwnerLabelNode.attributedText = stringProductOwner;
        _productOwnerLabelNode.maximumNumberOfLines = 1;
        _productOwnerLabelNode.pointSizeScaleFactors = @[@0.9, @0.8, @0.7];
        
        [self addSubnode:_productOwnerLabelNode];
        
        // Item Price Label
        CGFloat price = self.item.productId.price;
        NSString *itemPrice = [NSString stringWithFormat:@"Item Price: PHP %0.2f", price];
        NSAttributedString *stringPrice = [[NSAttributedString alloc] initWithString:itemPrice
                                                                          attributes:attrsLabel];
        
        
        _productPriceLabelNode = [[ASTextNode alloc] init];
        _productPriceLabelNode.attributedText = stringPrice;
        _productPriceLabelNode.maximumNumberOfLines = 1;
        _productPriceLabelNode.pointSizeScaleFactors = @[@0.9, @0.8, @0.7];
        
        [self addSubnode:_productPriceLabelNode];
        
        // Item Quantity Label
        CGFloat quantity = self.item.quantity;
        NSString *itemQuantity = [NSString stringWithFormat:@"Quantity: %0.0f", quantity];
        NSAttributedString *stringQuantity = [[NSAttributedString alloc] initWithString:itemQuantity
                                                                             attributes:attrsLabel];
        
        
        _productQuantityLabelNode = [[ASTextNode alloc] init];
        _productQuantityLabelNode.attributedText = stringQuantity;
        _productQuantityLabelNode.maximumNumberOfLines = 1;
        _productQuantityLabelNode.pointSizeScaleFactors = @[@0.9, @0.8, @0.7];
        
        [self addSubnode:_productQuantityLabelNode];
        
        
        // Total Price Label
        NSDictionary *attrsTotalPrice = @{
                                          NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Bold" size:16.0f],
                                          NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                          NSParagraphStyleAttributeName : paragraphStyleLeft
                                          };
        
        NSMutableAttributedString *stringtTotalLabel = [[NSMutableAttributedString alloc] initWithString:@"Subtotal: "
                                                                                              attributes:attrsLabel];
        
        CGFloat subTotal = quantity * price;
        NSString *subtotalPrice = [NSString stringWithFormat:@"PHP %0.2f", subTotal];
        
        NSAttributedString *stringTotalPrice = [[NSAttributedString alloc] initWithString:subtotalPrice
                                                                               attributes:attrsTotalPrice];
        
        [stringtTotalLabel  appendAttributedString:stringTotalPrice];
        
        _productTotalPriceLabelNode = [[ASTextNode alloc] init];
        _productTotalPriceLabelNode.attributedText = stringtTotalLabel;
        _productTotalPriceLabelNode.maximumNumberOfLines = 1;
        _productTotalPriceLabelNode.pointSizeScaleFactors = @[@0.9, @0.8, @0.7];
        
        [self addSubnode:_productTotalPriceLabelNode];
        
        // Border Node
        _borderNode = [[ASDisplayNode alloc] init];
        _borderNode.backgroundColor = [UIColor flatGrayColorDark];
        
        [self addSubnode:_borderNode];
    }
    return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // Spacer
    ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
    spacer.flexGrow = YES;
    
    // Product Image
    ASRatioLayoutSpec *ratioProductImage = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:1.0
                                                                                 child:_productImageNode];
    
    UIEdgeInsets contentInset;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        contentInset = UIEdgeInsetsMake(10, 14, 10, 14);
        ratioProductImage.flexBasis = ASRelativeDimensionMakeWithPercent(0.24);
    } else {
        contentInset = UIEdgeInsetsMake(8, 10, 8, 10);
        ratioProductImage.flexBasis = ASRelativeDimensionMakeWithPercent(0.36);
    }
    
    // Content Stack
    _productQuantityLabelNode.alignSelf = ASStackLayoutAlignSelfEnd;
    _productTotalPriceLabelNode.alignSelf = ASStackLayoutAlignSelfEnd;
    ASStackLayoutSpec *stackContent = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                              spacing:0.0
                                                                       justifyContent:ASStackLayoutJustifyContentStart
                                                                           alignItems:ASStackLayoutAlignItemsStretch
                                                                             children:@[_productNameLabelNode,
                                                                                        _productOwnerLabelNode,
                                                                                        _productPriceLabelNode,
                                                                                        spacer,
                                                                                        _productQuantityLabelNode,
                                                                                        _productTotalPriceLabelNode]];
    
    // Main Stack
    stackContent.flexGrow = YES;
    stackContent.flexShrink = YES;
    ASStackLayoutSpec *stackMain = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                           spacing:8.0f
                                                                    justifyContent:ASStackLayoutJustifyContentStart
                                                                        alignItems:ASStackLayoutAlignItemsStretch
                                                                          children:@[ratioProductImage,
                                                                                     stackContent]];
    
    ASInsetLayoutSpec *insetMain = [ASInsetLayoutSpec insetLayoutSpecWithInsets:contentInset
                                                                          child:stackMain];
    
    // Main with border
    _borderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0f);
    _borderNode.alignSelf = ASStackLayoutAlignSelfEnd;
    
    ASStackLayoutSpec *stackMainBorder = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                                 spacing:0.0
                                                                          justifyContent:ASStackLayoutJustifyContentStart
                                                                              alignItems:ASStackLayoutAlignItemsStretch
                                                                                children:@[insetMain, _borderNode]];
    return stackMainBorder;
}

#pragma mark - ASNetworkImageNodeDelegate methods.

- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image
{
    //[self setNeedsLayout];
}

- (void)imageNode:(ASNetworkImageNode *)imageNode didFailWithError:(NSError *)error {
    //[self setNeedsLayout];
}

@end
