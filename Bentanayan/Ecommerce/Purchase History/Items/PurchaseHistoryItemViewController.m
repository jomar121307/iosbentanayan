//
//  PurchaseHistoryItemViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 29/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "PurchaseHistoryItemViewController.h"

#import "PurchaseHistoryItemCellNode.h"

@interface PurchaseHistoryItemViewController ()

@end

@implementation PurchaseHistoryItemViewController

- (instancetype)initWithOrder:(OrderModel *)order
{
    self = [super init];
    if(self) {
        self.order = order;
        self.itemResult = [[NSMutableArray alloc] initWithArray:self.order.items];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor flatWhiteColor];
    
    // Navigation
    self.navigationItem.title = @"Order Details";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Back
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(goBack)];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    // Home
    UIImage *homeImage = [[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    CGRect frameimg = CGRectMake(0, 0, homeImage.size.width, homeImage.size.height);
    UIButton *homeButton = [[UIButton alloc] initWithFrame:frameimg];
    [homeButton setBackgroundImage:homeImage forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(goToHome) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    // Order Model
    float subTotal = 0;
    float shippingCost = 0;
    for(CartItemModel *cartItemModel in self.itemResult) {
        subTotal += (cartItemModel.productId.price * cartItemModel.quantity);
        shippingCost += cartItemModel.shippingCost;
    }
    NSLog(@"Item-Total: %f, Shipping: %f", subTotal, shippingCost);
    
    // Cart Summary
    self.purchaseHistoryItemDisplayNode = [[PurchaseHistoryItemDisplayNode alloc] initWithAddress:self.order.shippingId.address
                                                                                         subTotal:subTotal
                                                                                     shippingCost:shippingCost];
    self.purchaseHistoryItemDisplayNode.backgroundColor = [UIColor flatWhiteColor];
    self.purchaseHistoryItemDisplayNode.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubnode:self.purchaseHistoryItemDisplayNode];
    
    // CollectionView
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 0.0f;
    layout.minimumLineSpacing = 0.0f;
    
    self.collectionView = [[ASCollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:layout];
    self.collectionView.asyncDataSource = self;
    self.collectionView.asyncDelegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:self.collectionView];
    
    // Constraints
    NSDictionary *views = @{ @"orderSummary" : self.purchaseHistoryItemDisplayNode.view,
                             @"collectionView" : self.collectionView };
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[orderSummary]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|" options:0 metrics:nil views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collectionView][orderSummary(140)]|" options:0 metrics:nil views:views]];
}

#pragma mark - ASCollectionView data source.

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CartItemModel *item = [self.itemResult objectAtIndex:indexPath.row];
    item.productId.userId = self.order.shopId.userId;
    PurchaseHistoryItemCellNode *cellNode = [[PurchaseHistoryItemCellNode alloc] initWithItem:item];
    return cellNode;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = self.collectionView.bounds.size.width;
    
    return ASSizeRangeMake(CGSizeMake(0.0, 0.0), CGSizeMake(width, FLT_MAX));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.itemResult.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionViewLockDataSource:(ASCollectionView *)collectionView
{
    // lock the data source
    // The data source should not be change until it is unlocked.
}

- (void)collectionViewUnlockDataSource:(ASCollectionView *)collectionView
{
    // unlock the data source to enable data source updating.
}

- (void)collectionView:(UICollectionView *)collectionView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    NSLog(@"fetch additional content");
    [context completeBatchFetching:YES];
}

#pragma mark - Methods / Selectors

- (void)goToHome {
    NSLog(@"Home");
    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
}

- (void)goBack {
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

#pragma mark - Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
