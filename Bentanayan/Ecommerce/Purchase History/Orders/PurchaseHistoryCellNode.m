//
//  PurchaseHistoryCellNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 28/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "PurchaseHistoryCellNode.h"

@implementation PurchaseHistoryCellNode

- (instancetype)initWithOrder:(OrderModel *)order
{
    self = [super init];
    if(self) {
        
        self.order = order;
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        NSDictionary *attrsLabel = @{
                                     NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                     NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                     NSParagraphStyleAttributeName : paragraphStyleLeft
                                     };
        
        NSDictionary *attrsLabelLight = @{
                                          NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Light" size:14.0f],
                                          NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                          NSParagraphStyleAttributeName : paragraphStyleLeft
                                          };
        
        // Order No. Label
        NSString *orderNo = [NSString stringWithFormat:@"Order #%@", [[self.order.orderId substringToIndex:10] uppercaseString]];
        NSAttributedString *stringOrderNo = [[NSAttributedString alloc] initWithString:orderNo
                                                                            attributes:attrsLabelLight];
        
        _orderNoLabelNode = [[ASTextNode alloc] init];
        _orderNoLabelNode.attributedText = stringOrderNo;
        _orderNoLabelNode.maximumNumberOfLines = 1;
        _orderNoLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_orderNoLabelNode];
        
        // Seller Label
        NSString *sellerName;
        NSString *firstName = self.order.shopId.userId.firstName;
        NSString *lastName = self.order.shopId.userId.lastName;
        if(lastName != nil) {
            sellerName = [NSString stringWithFormat:@"Seller: %@ %@", firstName, lastName];
        } else {
            sellerName = [NSString stringWithFormat:@"Seller: %@", firstName];
        }
        NSAttributedString *stringSeller = [[NSAttributedString alloc] initWithString:sellerName
                                                                           attributes:attrsLabel];
        
        _sellerNameLabelNode = [[ASTextNode alloc] init];
        _sellerNameLabelNode.attributedText = stringSeller;
        _sellerNameLabelNode.maximumNumberOfLines = 2;
        _sellerNameLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_sellerNameLabelNode];
        
        // Purchase Date Label
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:BENTA_DATE_FORMAT];
        NSDate *date = [dateFormatter dateFromString:self.order.createdAt];
        [dateFormatter setDateFormat:BENTA_DATE_STRING_LONG_FORMAT];
        NSString *purchasedDate = [NSString stringWithFormat:@"Purchase Date: %@", [dateFormatter stringFromDate:date]];
        
        NSAttributedString *stringDate = [[NSAttributedString alloc] initWithString:purchasedDate
                                                                         attributes:attrsLabel];
        
        _purchaseDateLabelNode = [[ASTextNode alloc] init];
        _purchaseDateLabelNode.attributedText = stringDate;
        _purchaseDateLabelNode.maximumNumberOfLines = 1;
        _purchaseDateLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_purchaseDateLabelNode];
        
        // Status Label
        NSString *status = [NSString stringWithFormat:@"STATUS: %@", [self.order.status uppercaseString]];
        NSAttributedString *stringStatus = [[NSAttributedString alloc] initWithString:status
                                                                           attributes:attrsLabelLight];
        
        _statusLabelNode = [[ASTextNode alloc] init];
        _statusLabelNode.attributedText = stringStatus;
        _statusLabelNode.maximumNumberOfLines = 1;
        _statusLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_statusLabelNode];
        
        // Border Node
        _borderNode = [[ASDisplayNode alloc] init];
        _borderNode.backgroundColor = [UIColor flatGrayColorDark];
        
        [self addSubnode:_borderNode];
    }
    return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
    spacer.flexGrow = YES;
    
    // Order and Status
    _orderNoLabelNode.alignSelf = ASStackLayoutAlignSelfStart;
    _statusLabelNode.alignSelf = ASStackLayoutAlignSelfEnd;
    ASStackLayoutSpec *stackOrderStatus = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                  spacing:4.0f
                                                                           justifyContent:ASStackLayoutJustifyContentStart
                                                                               alignItems:ASStackLayoutAlignItemsStretch
                                                                                 children:@[_orderNoLabelNode,
                                                                                            spacer,
                                                                                            _statusLabelNode]];
    
    // Content
    ASStackLayoutSpec *stackContent = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                              spacing:2.0f
                                                                       justifyContent:ASStackLayoutJustifyContentStart
                                                                           alignItems:ASStackLayoutAlignItemsStretch
                                                                             children:@[stackOrderStatus,
                                                                                        _sellerNameLabelNode,
                                                                                        _purchaseDateLabelNode]];
    stackContent.flexShrink = YES;
    stackContent.flexGrow = YES;
    
    ASInsetLayoutSpec *insetMain = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(10, 10, 10, 10)
                                                                          child:stackContent];
    
    // Main with border
    _borderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0f);
    _borderNode.alignSelf = ASStackLayoutAlignSelfEnd;
    
    ASStackLayoutSpec *stackMainBorder = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                                 spacing:0.0
                                                                          justifyContent:ASStackLayoutJustifyContentStart
                                                                              alignItems:ASStackLayoutAlignItemsStretch
                                                                                children:@[insetMain, _borderNode]];
    return stackMainBorder;
}

#pragma mark - ASNetworkImageNodeDelegate methods.

- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image
{
    //[self setNeedsLayout];
}

- (void)imageNode:(ASNetworkImageNode *)imageNode didFailWithError:(NSError *)error {
    //[self setNeedsLayout];
}

@end
