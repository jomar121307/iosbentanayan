//
//  PurchaseHistoryViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 28/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "PurchaseHistoryViewController.h"

#import "PurchaseHistoryCellNode.h"

#import "PurchaseHistoryItemViewController.h"

@interface PurchaseHistoryViewController ()

@end

@implementation PurchaseHistoryViewController

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor flatWhiteColor];
    
    // Navigation
    self.navigationItem.title = @"Purchased Items";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Home
    UIImage *homeImage = [[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    CGRect frameimg = CGRectMake(0, 0, homeImage.size.width, homeImage.size.height);
    UIButton *homeButton = [[UIButton alloc] initWithFrame:frameimg];
    [homeButton setBackgroundImage:homeImage forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(goToHome) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    // CollectionView
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 0.0f;
    layout.minimumLineSpacing = 0.0f;
    
    self.collectionView = [[ASCollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:layout];
    self.collectionView.asyncDataSource = self;
    self.collectionView.asyncDelegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.alwaysBounceVertical = YES;
    
    [self.view addSubview:self.collectionView];
    
    self.orderResult = [NSMutableArray new];
    
    __weak PurchaseHistoryViewController *weakSelf = self;
    
    [self.collectionView addPullToRefreshWithActionHandler:^{
        [[BESharedManager sharedInstance] be_purchaseHistoryWithCompletion:^(BOOL finished, NSArray *result)
         {
             [weakSelf.collectionView.pullToRefreshView stopAnimating];
             if(finished) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [weakSelf.collectionView performBatchUpdates:^{
                         NSMutableArray *insertIndexPaths = [NSMutableArray array];
                         NSMutableArray *deleteIndexPaths = [NSMutableArray array];
                         NSInteger section = 0;
                         
                         NSArray *oldData = [weakSelf.orderResult copy];
                         for(OrderModel *oldModel in oldData) {
                             NSInteger oldItem = [oldData indexOfObject:oldModel];
                             [weakSelf.orderResult removeObject:oldModel];
                             [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
                         }
                         
                         for(OrderModel *newModel in result) {
                             [weakSelf.orderResult addObject:newModel];
                             NSInteger item = [weakSelf.orderResult indexOfObject:newModel];
                             [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
                         }
                         
                         [weakSelf.collectionView deleteItemsAtIndexPaths:deleteIndexPaths];
                         [weakSelf.collectionView insertItemsAtIndexPaths:insertIndexPaths];
                     } completion:nil];
                 });
             }
         }];
    }];
    
    [self.collectionView triggerPullToRefresh];
}

- (void)viewWillLayoutSubviews {
    self.collectionView.frame = self.view.bounds;
}

#pragma mark - ASCollectionView data source.

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OrderModel *order = (OrderModel *)[self.orderResult objectAtIndex:indexPath.row];
    PurchaseHistoryCellNode *cellNode = [[PurchaseHistoryCellNode alloc] initWithOrder:order];
    return cellNode;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = self.collectionView.bounds.size.width;
    
    return ASSizeRangeMake(CGSizeMake(0.0, 0.0), CGSizeMake(width, FLT_MAX));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.orderResult.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionViewLockDataSource:(ASCollectionView *)collectionView
{
    // lock the data source
    // The data source should not be change until it is unlocked.
}

- (void)collectionViewUnlockDataSource:(ASCollectionView *)collectionView
{
    // unlock the data source to enable data source updating.
}

- (void)collectionView:(UICollectionView *)collectionView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    NSLog(@"fetch additional content");
    [context completeBatchFetching:YES];
}

#pragma mark - ASCollectionView delegate.

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    OrderModel *order = (OrderModel *)[self.orderResult objectAtIndex:indexPath.row];
    PurchaseHistoryItemViewController *purchaseHistoryItemViewController = [[PurchaseHistoryItemViewController alloc] initWithOrder:order];
    //purchaseHistoryItemViewController.order = order;
    [[SlideNavigationController sharedInstance] pushViewController:purchaseHistoryItemViewController
                                                          animated:YES];
}

#pragma mark - Methods / Selectors

- (void)goToHome {
    NSLog(@"Home");
    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
}

#pragma mark - Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
