//
//  PurchaseHistoryCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 28/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface PurchaseHistoryCellNode : ASCellNode {
    ASTextNode *_orderNoLabelNode;
    ASTextNode *_sellerNameLabelNode;
    ASTextNode *_purchaseDateLabelNode;
    ASTextNode *_statusLabelNode;
    
    ASDisplayNode *_borderNode;
}

@property (strong, nonatomic) ASButtonNode *_viewItemButtonNode;
@property (strong, nonatomic) ASButtonNode *_viewDetailsButtonNode;

@property (strong, nonatomic) OrderModel *order;

- (instancetype)initWithOrder:(OrderModel *)order;

@end
