//
//  ShopBarViewController.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopBarViewController : XLButtonBarPagerTabStripViewController

@property (strong, nonatomic) UserModel *shopOwner;

- (instancetype)initWithShopOwner:(UserModel *)owner;

@end

