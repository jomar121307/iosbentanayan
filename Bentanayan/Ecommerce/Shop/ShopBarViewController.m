//
//  ShopBarViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ShopBarViewController.h"

#import "ShopViewController.h"
#import "ShopProfileViewController.h"

@interface ShopBarViewController ()

@end

@implementation ShopBarViewController

- (instancetype)initWithShopOwner:(UserModel *)owner
{
    self = [super init];
    if(self) {
        self.shopOwner = owner;
    }
    return self;
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    if([[SlideNavigationController sharedInstance] viewControllers].count > 2) {
        return NO;
    } else {
        return YES;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor flatWhiteColor];
    
    if([self.shopOwner.userId isEqualToString:[[BESharedManager sharedInstance] login].session.userId.userId]) {
        self.navigationItem.title = @"My Shop";
    } else {
        NSString *shopName;
        if([self.shopOwner.firstName hasSuffix:@"s"]) {
            shopName = [NSString stringWithFormat:@"%@' Shop", self.shopOwner.firstName];
        } else {
            shopName = [NSString stringWithFormat:@"%@'s Shop", self.shopOwner.firstName];
        }
        self.navigationItem.title = shopName;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Back
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(goBack)];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    // Home
    UIImage *homeImage = [[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    CGRect frameimg = CGRectMake(0, 0, homeImage.size.width, homeImage.size.height);
    UIButton *homeButton = [[UIButton alloc] initWithFrame:frameimg];
    [homeButton setBackgroundImage:homeImage forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(goToHome) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    // Button Bar Pager
    self.isProgressiveIndicator = NO;
    self.buttonBarView.shouldCellsFillAvailableWidth = YES;
    self.buttonBarView.labelFont = [UIFont fontWithName:@"RobotoCondensed-Bold" size:12.0f];
    self.buttonBarView.selectedBar.backgroundColor = [UIColor flatWhiteColor];
    self.buttonBarView.selectedBarHeight = 2.0f;
    self.buttonBarView.backgroundColor = [UIColor flatBlackColor];
    
    self.changeCurrentIndexBlock = (^void(XLButtonBarViewCell* oldCell, XLButtonBarViewCell *newCell, BOOL animated){
        oldCell.label.textColor = [UIColor lightTextColor];
        newCell.label.textColor = [UIColor whiteColor];
    });
}

- (void)viewDidAppear:(BOOL)animated {
    if([self currentIndex] == 1) {
        return;
    } else {
        [self moveToViewControllerAtIndex:0 animated:YES];
    }
}

#pragma mark - XLPagerTabStripViewControllerDataSource

-(NSArray *)childViewControllersForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController
{
    ShopViewController *shopController = [[ShopViewController alloc] initWithUser:self.shopOwner];
    
    ShopProfileViewController *shopProfileController = [[ShopProfileViewController alloc] initWithShopOwner:self.shopOwner];
    
    return @[shopController, shopProfileController];
}

#pragma mark - Methods / Selectors

- (void)goToHome {
    NSLog(@"Home");
    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
}

- (void)goBack {
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
