//
//  ShopProfileScrollNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ShopProfileScrollNode.h"

@implementation ShopProfileScrollNode

- (instancetype)initWithShopOwner:(UserModel *)owner {
    self = [super init];
    if(self) {
        
        self.owner = owner;
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        NSDictionary *attrsLabel = @{
                                     NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0f],
                                     NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                     NSParagraphStyleAttributeName : paragraphStyleLeft
                                     };
        
        // Cover Photo Node
        UIColor *randColor = [UIColor flatBlackColor];
        
        _coverPhotoNode = [[ASDisplayNode alloc] init];
        _coverPhotoNode.backgroundColor = randColor;
        
        [self addSubnode:_coverPhotoNode];
        
        // Owner Avatar
        _profilePhotoImageNode = [[ASNetworkImageNode alloc] init];
        if(self.owner.profilePhoto) {
            ProfilePhotoModel *photo = self.owner.profilePhoto;
            if(photo.cropped) {
                _profilePhotoImageNode.URL = [NSURL URLWithString:photo.cropped.secure_url];
            } else if(photo.original) {
                _profilePhotoImageNode.URL = [NSURL URLWithString:photo.original.secure_url];
            } else {
                _profilePhotoImageNode.URL = [NSURL URLWithString:photo.secure_url];
            }
        }
        _profilePhotoImageNode.delegate = self;
        _profilePhotoImageNode.defaultImage = [UIImage imageNamed:@"avatar-placeholder"];
        _profilePhotoImageNode.backgroundColor = [UIColor clearColor];
        
        [self addSubnode:_profilePhotoImageNode];
        
        // Shop Name Label
        NSDictionary *attrsShopName = @{
                                        NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Bold" size:16.0f],
                                        NSForegroundColorAttributeName: [UIColor colorWithContrastingBlackOrWhiteColorOn:randColor isFlat:YES],
                                        NSParagraphStyleAttributeName : paragraphStyleLeft
                                        };
        
        NSString *shopName;
        NSString *firstName = self.owner.firstName;
        NSString *checkS = [firstName substringFromIndex:[firstName length] - 1];
        if([checkS isEqualToString:@"s"]) {
            shopName = [NSString stringWithFormat:@"%@' Shop", firstName];
        } else {
            shopName = [NSString stringWithFormat:@"%@'s Shop", firstName];
        }
        
        NSAttributedString *stringShopName = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", shopName]
                                                                             attributes:attrsShopName];
        
        
        _nameLabelNode = [[ASTextNode alloc] init];
        _nameLabelNode.attributedText = stringShopName;
        _nameLabelNode.maximumNumberOfLines = 1;
        _nameLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_nameLabelNode];
        
        // Message Me Button
        if(![self.owner.userId isEqualToString:[BESharedManager sharedInstance].login.session.userId.userId]) {
            self.messageMeButtonNode = [[ASButtonNode alloc] init];
            self.messageMeButtonNode.cornerRadius = 4.0f;
            [self.messageMeButtonNode setTitle:@"MESSAGE ME"
                                      withFont:[UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0f]
                                     withColor:[UIColor whiteColor]
                                      forState:ASControlStateNormal];
            [self.messageMeButtonNode setContentEdgeInsets:UIEdgeInsetsMake(8, 14, 8, 14)];
            [self.messageMeButtonNode setHitTestSlop:UIEdgeInsetsMake(-2, -4, -2, -4)];
            [self.messageMeButtonNode setBackgroundColor:[UIColor bentaGreenColor]];
            
            [self addSubnode:self.messageMeButtonNode];
        }
        // Name Image
        _nameImageNode = [[ASImageNode alloc] init];
        _nameImageNode.contentMode = UIViewContentModeScaleAspectFit;
        _nameImageNode.image = [UIImage imageNamed:@"user"];
        
        [self addSubnode:_nameImageNode];
        
        // Name Label
        NSAttributedString *stringFirstName = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", self.owner.firstName]
                                                                              attributes:attrsLabel];
        
        _firstNameLabelNode = [[ASTextNode alloc] init];
        _firstNameLabelNode.attributedText = stringFirstName;
        _firstNameLabelNode.maximumNumberOfLines = 1;
        _firstNameLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_firstNameLabelNode];
        
        NSString *lastName;
        if(self.owner.lastName) {
            lastName = self.owner.lastName;
        } else {
            lastName = @"";
        }
        NSAttributedString *stringLastName = [[NSAttributedString alloc] initWithString:lastName
                                                                             attributes:attrsLabel];
        
        _lastNameLabelNode = [[ASTextNode alloc] init];
        _lastNameLabelNode.attributedText = stringLastName;
        _lastNameLabelNode.maximumNumberOfLines = 1;
        _lastNameLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_lastNameLabelNode];
        
        // Email Image
        _emailImageNode = [[ASImageNode alloc] init];
        _emailImageNode.contentMode = UIViewContentModeScaleAspectFit;
        _emailImageNode.image = [UIImage imageNamed:@"email"];
        
        [self addSubnode:_emailImageNode];
        
        // Email Label
        NSString *email;
        if(self.owner.email) {
            email = [NSString stringWithFormat:@"%@", self.owner.email];
        } else {
            email = @"n/a";
        }
        NSAttributedString *stringEmail = [[NSAttributedString alloc] initWithString:email
                                                                          attributes:attrsLabel];
        
        _emailLabelNode = [[ASTextNode alloc] init];
        _emailLabelNode.attributedText = stringEmail;
        _emailLabelNode.maximumNumberOfLines = 1;
        _emailLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_emailLabelNode];
        
        // Joined Date Image
        _joinedDateImageNode = [[ASImageNode alloc] init];
        _joinedDateImageNode.contentMode = UIViewContentModeScaleAspectFit;
        _joinedDateImageNode.image = [UIImage imageNamed:@"calendar"];
        
        [self addSubnode:_joinedDateImageNode];
        
        // Joined Date Label
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:BENTA_DATE_FORMAT];
        NSDate *dateJoinedDate = [dateFormatter dateFromString:self.owner.createdAt];
        [dateFormatter setDateFormat:BENTA_DATE_STRING_FORMAT];
        NSString *joinedDate = [dateFormatter stringFromDate:dateJoinedDate];
        
        NSAttributedString *stringJoinedDate = [[NSAttributedString alloc] initWithString:joinedDate
                                                                               attributes:attrsLabel];
        
        _joinedDateLabelNode = [[ASTextNode alloc] init];
        _joinedDateLabelNode.attributedText = stringJoinedDate;
        _joinedDateLabelNode.maximumNumberOfLines = 1;
        _joinedDateLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_joinedDateLabelNode];
    }
    return self;
}

- (void)layoutDidFinish {
    [super layoutDidFinish];
    
    _profilePhotoImageNode.cornerRadius = _profilePhotoImageNode.bounds.size.width / 2;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
    
    // Name and Avatar
    ASRatioLayoutSpec *ratioAvatar = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:1.0
                                                                           child:_profilePhotoImageNode];
    
    ASRatioLayoutSpec *ratioNameImage = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:0.75
                                                                              child:_nameImageNode];
    
    ASRatioLayoutSpec *ratioEmailImage = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:0.75
                                                                               child:_emailImageNode];
    
    ASRatioLayoutSpec *ratioJoinedDateImage = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:0.75
                                                                                    child:_joinedDateImageNode];
    
    // iPhone & iPad
    UIEdgeInsets contentInset;
    ASRelativeDimension iconImage;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        contentInset = UIEdgeInsetsMake(14, 18, 14, 18);
        ratioAvatar.flexBasis = ASRelativeDimensionMakeWithPercent(0.30);
        
        iconImage = ASRelativeDimensionMakeWithPercent(0.07);
    } else {
        contentInset = UIEdgeInsetsMake(8, 12, 8, 12);
        ratioAvatar.flexBasis = ASRelativeDimensionMakeWithPercent(0.48);
        
        iconImage = ASRelativeDimensionMakeWithPercent(0.10);
    }
    
    ratioNameImage.flexBasis = iconImage;
    ratioEmailImage.flexBasis = iconImage;
    ratioJoinedDateImage.flexBasis = iconImage;
    spacer.flexBasis = iconImage;
    
    
    ASStackLayoutSpec *stackNameAvatar = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                                 spacing:8.0f
                                                                          justifyContent:ASStackLayoutJustifyContentCenter
                                                                              alignItems:ASStackLayoutAlignItemsCenter
                                                                                children:@[ratioAvatar,
                                                                                           _nameLabelNode]];
    
    ASCenterLayoutSpec *centerNameAvatar = [ASCenterLayoutSpec centerLayoutSpecWithCenteringOptions:ASCenterLayoutSpecCenteringXY
                                                                                      sizingOptions:ASCenterLayoutSpecSizingOptionDefault
                                                                                              child:stackNameAvatar];
    
    ASBackgroundLayoutSpec *backgroundHeaderContent = [ASBackgroundLayoutSpec backgroundLayoutSpecWithChild:centerNameAvatar
                                                                                                 background:_coverPhotoNode];
    
    ASRatioLayoutSpec *ratioCover = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:0.56
                                                                          child:backgroundHeaderContent];
    
    // Icon Name
    ASStackLayoutSpec *stackIconName = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                               spacing:12.0f
                                                                        justifyContent:ASStackLayoutJustifyContentStart
                                                                            alignItems:ASStackLayoutAlignItemsCenter
                                                                              children:@[ratioNameImage,
                                                                                         _firstNameLabelNode]];
    
    // Icon Email
    ASStackLayoutSpec *stackIconEmail = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                spacing:12.0f
                                                                         justifyContent:ASStackLayoutJustifyContentStart
                                                                             alignItems:ASStackLayoutAlignItemsCenter
                                                                               children:@[ratioEmailImage,
                                                                                          _emailLabelNode]];
    
    // Icon Joined Date
    ASStackLayoutSpec *stackIconJoinedDate = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                     spacing:12.0f
                                                                              justifyContent:ASStackLayoutJustifyContentStart
                                                                                  alignItems:ASStackLayoutAlignItemsCenter
                                                                                    children:@[ratioJoinedDateImage,
                                                                                               _joinedDateLabelNode]];
    
    // Header Content and Lower Content
    ASStackLayoutSpec *stackLastName = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                               spacing:12.0f
                                                                        justifyContent:ASStackLayoutJustifyContentStart
                                                                            alignItems:ASStackLayoutAlignItemsStart
                                                                              children:@[spacer,
                                                                                         _lastNameLabelNode]];
    
    ASStackLayoutSpec *stackLower = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                            spacing:12.0f
                                                                     justifyContent:ASStackLayoutJustifyContentCenter
                                                                         alignItems:ASStackLayoutAlignItemsStart
                                                                           children:@[stackIconName,
                                                                                      stackLastName,
                                                                                      stackIconEmail,
                                                                                      stackIconJoinedDate]];
    
    ASInsetLayoutSpec *insetLower = [ASInsetLayoutSpec insetLayoutSpecWithInsets:contentInset
                                                                           child:stackLower];
    
    NSMutableArray *arrayHeaderLower = [[NSMutableArray alloc] init];
    [arrayHeaderLower addObject:ratioCover];
    if(![self.owner.userId isEqualToString:[BESharedManager sharedInstance].login.session.userId.userId]) {
        self.messageMeButtonNode.spacingAfter = 8.0f;
        self.messageMeButtonNode.spacingBefore = 8.0f;
        [arrayHeaderLower addObject:self.messageMeButtonNode];
    }
    insetLower.alignSelf = ASStackLayoutAlignSelfStretch;
    [arrayHeaderLower addObject:insetLower];
    
    ASStackLayoutSpec *stackHeaderLower = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                                  spacing:4.0f
                                                                           justifyContent:ASStackLayoutJustifyContentCenter
                                                                               alignItems:ASStackLayoutAlignItemsCenter
                                                                                 children:arrayHeaderLower];
    
    return [ASStaticLayoutSpec staticLayoutSpecWithChildren:@[stackHeaderLower]];
}

#pragma mark - ASNetworkImageNodeDelegate methods.

- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image
{
    //[self setNeedsLayout];
}

- (void)imageNode:(ASNetworkImageNode *)imageNode didFailWithError:(NSError *)error {
    //[self setNeedsLayout];
}

@end
