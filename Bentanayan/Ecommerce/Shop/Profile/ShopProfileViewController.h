//
//  ShopProfileViewController.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

#import "ShopProfileScrollNode.h"

@interface ShopProfileViewController : ASViewController <XLPagerTabStripChildItem, UIScrollViewDelegate> {
    STPopupController *_messagePopupController;
}

@property (strong, nonatomic) ShopProfileScrollNode *scrollNode;

- (instancetype)initWithShopOwner:(UserModel *)owner;

@end

