//
//  ShopProfileViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ShopProfileViewController.h"

#import "ShopMessageViewController.h"

@interface ShopProfileViewController ()

@end

@implementation ShopProfileViewController

- (instancetype)initWithShopOwner:(UserModel *)owner
{
    ShopProfileScrollNode *node = [[ShopProfileScrollNode alloc] initWithShopOwner:owner];
    if (!(self = [super initWithNode:node]))
        return nil;
    
    self.scrollNode = node;
    self.scrollNode.view.alwaysBounceVertical = YES;
    self.scrollNode.backgroundColor = [UIColor clearColor];
    
    [self.scrollNode.messageMeButtonNode addTarget:self
                                            action:@selector(messageMe:)
                                  forControlEvents:ASControlNodeEventTouchUpInside];
    
    _messagePopupController = [[STPopupController alloc] initWithRootViewController:[[ShopMessageViewController alloc] initWithUser:owner]];
    [_messagePopupController.backgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundViewDidTap)]];
    _messagePopupController.containerView.layer.cornerRadius = 4;
    _messagePopupController.transitionStyle = STPopupTransitionStyleFade;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Methods / Selectors

- (void)messageMe:(ASButtonNode *)button {
    [_messagePopupController presentInViewController:self];
}

- (void)backgroundViewDidTap {
    [_messagePopupController dismiss];
}

#pragma mark - XLPagerTabStripViewControllerDelegate

-(NSString *)titleForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController
{
    return @"PROFILE";
}

-(UIColor *)colorForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController
{
    return [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
