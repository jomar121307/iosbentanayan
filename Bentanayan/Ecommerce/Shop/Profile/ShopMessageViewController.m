//
//  ShopMessageViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ShopMessageViewController.h"

@interface ShopMessageViewController ()

@end

@implementation ShopMessageViewController

- (instancetype)initWithUser:(UserModel *)user {
    self = [super init];
    if(self) {
        self.userId = user.userId;
        
        NSString *messageTitle = [NSString stringWithFormat:@"Message to %@ %@", user.firstName, user.lastName];
        self.title = messageTitle;
        
        CGFloat popupWidth = [UIScreen mainScreen].bounds.size.width * 0.74;
        
        self.contentSizeInPopup = CGSizeMake(popupWidth, 220);
        self.landscapeContentSizeInPopup = CGSizeMake(popupWidth, 180);
        self.view.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.messageView = [[UITextView alloc] init];
    self.messageView.font = [UIFont fontWithName:@"RobotoCondensed-Regular"
                                            size:16.0f];
    self.messageView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:self.messageView];
    
    self.messageButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.messageButton setTitle:@"Send Message"
                        forState:UIControlStateNormal];
    [self.messageButton setTitleColor:[UIColor whiteColor]
                             forState:UIControlStateNormal];
    [self.messageButton setBackgroundColor:[UIColor bentaGreenColor]];
    [self.messageButton setContentEdgeInsets:UIEdgeInsetsMake(8, 8, 8, 8)];
    [self.messageButton addTarget:self
                           action:@selector(sendMessage)
                 forControlEvents:UIControlEventTouchUpInside];
    
    self.messageButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:self.messageButton];
    
    // Constraints
    NSDictionary *views = @{ @"messageView" : self.messageView,
                             @"messageButton" : self.messageButton };
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[messageView]|" options:0 metrics:nil views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[messageButton]|" options:0 metrics:nil views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[messageView]-[messageButton(40)]|" options:0 metrics:nil views:views]];
}

- (void)sendMessage {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if([self.messageView.text isEqualToString:@""]) {
        return;
    }
    
    NSDictionary *payloadMessage = @{
                                     @"payload" : @{
                                             @"message" : self.messageView.text
                                             }
                                     };
    
    [[BESharedManager sharedInstance] be_initMessageWithOtherUserId:self.userId
                                                         withMessage:payloadMessage
                                                      withCompletion:^(BOOL finished)
     {
         if(finished) {
             self.messageView.text = @"";
             [TSMessage showNotificationWithTitle:@"Message sent."
                                             type:TSMessageNotificationTypeSuccess];
         } else {
             [TSMessage showNotificationWithTitle:@"Message sent failed."
                                             type:TSMessageNotificationTypeError];
         }
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
