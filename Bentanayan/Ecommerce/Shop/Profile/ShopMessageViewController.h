//
//  ShopMessageViewController.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopMessageViewController : UIViewController

@property (strong, nonatomic) UITextView *messageView;
@property (strong, nonatomic) UIButton *messageButton;

@property (strong, nonatomic) NSString *userId;

- (instancetype)initWithUser:(UserModel *)user;

@end
