//
//  ShopProfileScrollNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface ShopProfileScrollNode : ASScrollNode <ASNetworkImageNodeDelegate> {
    ASDisplayNode *_coverPhotoNode;
    ASNetworkImageNode *_profilePhotoImageNode;
    ASTextNode *_nameLabelNode;
    
    ASImageNode *_nameImageNode;
    ASTextNode *_firstNameLabelNode;
    ASTextNode *_lastNameLabelNode;
    
    ASImageNode *_emailImageNode;
    ASTextNode *_emailLabelNode;
    
    ASImageNode *_joinedDateImageNode;
    ASTextNode *_joinedDateLabelNode;
}

@property (strong, nonatomic) ASButtonNode *messageMeButtonNode;

@property (strong, nonatomic) UserModel *owner;

- (instancetype)initWithShopOwner:(UserModel *)owner;

@end
