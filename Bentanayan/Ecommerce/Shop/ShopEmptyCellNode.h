//
//  ShopEmptyCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface ShopEmptyCellNode : ASCellNode {
    ASDisplayNode *_contentNode;
    
    ASTextNode *_titleLabelNode;
    ASTextNode *_subtitlelabelNode;
}

@end
