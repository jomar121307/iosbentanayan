//
//  ShopEmptyCellNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ShopEmptyCellNode.h"

@implementation ShopEmptyCellNode

- (instancetype)init {
    self = [super init];
    if(self) {
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleCenter = [NSMutableParagraphStyle new];
        paragraphStyleCenter.alignment = NSTextAlignmentCenter;
        
        // Content Display
        _contentNode = [[ASDisplayNode alloc] init];
        _contentNode.backgroundColor = [UIColor whiteColor];
        
        [self addSubnode:_contentNode];
        
        // Title Label
        NSDictionary *attrsTitle = @{
                                     NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Bold" size:16.0f],
                                     NSForegroundColorAttributeName: [UIColor flatBlackColorDark],
                                     NSParagraphStyleAttributeName : paragraphStyleCenter
                                     };
        
        NSAttributedString *stringTitle = [[NSAttributedString alloc] initWithString:@"This shop is empty."
                                                                          attributes:attrsTitle];
        
        _titleLabelNode = [[ASTextNode alloc] init];
        _titleLabelNode.attributedText = stringTitle;
        _titleLabelNode.maximumNumberOfLines = 1;
        _titleLabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_titleLabelNode];
        
        // Subtitle Label
        NSDictionary *attrsSubtitle = @{
                                        NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                        NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                        NSParagraphStyleAttributeName : paragraphStyleCenter
                                        };
        
        NSAttributedString *stringSubtitle = [[NSAttributedString alloc] initWithString:@"Owner does not have any items for you to purchase."
                                                                             attributes:attrsSubtitle];
        
        _subtitlelabelNode = [[ASTextNode alloc] init];
        _subtitlelabelNode.attributedText = stringSubtitle;
        _subtitlelabelNode.maximumNumberOfLines = 2;
        _subtitlelabelNode.pointSizeScaleFactors = @[@0.9, @0.8];
        
        [self addSubnode:_subtitlelabelNode];
    }
    return self;
}

- (void)layoutDidFinish {
    [super layoutDidFinish];
    
    _contentNode.shadowColor = [UIColor flatBlackColor].CGColor;
    _contentNode.shadowOpacity = 0.5f;
    _contentNode.shadowRadius = 2.0f;
    _contentNode.shadowOffset = CGSizeMake(0.0f, 2.0f);
    _contentNode.layer.masksToBounds = NO;
    _contentNode.layer.shadowPath = [UIBezierPath bezierPathWithRect:_contentNode.bounds].CGPath;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // iPhone & iPad
    UIEdgeInsets contentInset;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        contentInset = UIEdgeInsetsMake(12, 14, 12, 14);
    } else {
        contentInset = UIEdgeInsetsMake(8, 12, 8, 12);
    }
    
    // Main
    ASStackLayoutSpec *stackMain = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                           spacing:8.0f
                                                                    justifyContent:ASStackLayoutJustifyContentCenter
                                                                        alignItems:ASStackLayoutAlignItemsStretch
                                                                          children:@[_titleLabelNode,
                                                                                     _subtitlelabelNode]];
    
    ASInsetLayoutSpec *insetMain = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(6, 10, 6, 10)
                                                                          child:stackMain];
    
    // Content Display
    _contentNode.sizeRange = ASRelativeSizeRangeMakeWithExactRelativeDimensions(ASRelativeDimensionMakeWithPercent(1),
                                                                                ASRelativeDimensionMakeWithPercent(1));
    
    ASOverlayLayoutSpec *overlayContent = [ASOverlayLayoutSpec overlayLayoutSpecWithChild:insetMain
                                                                                  overlay:_contentNode];
    
    
    return [ASInsetLayoutSpec insetLayoutSpecWithInsets:contentInset
                                                  child:overlayContent];
}

@end
