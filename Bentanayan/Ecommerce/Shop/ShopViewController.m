//
//  ShopViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ShopViewController.h"

#import "ProductDetailsViewController.h"
#import "ProductCategoryCellNode.h"

#import "ShopEmptyCellNode.h"

@interface ShopViewController ()

@end

@implementation ShopViewController

- (instancetype)initWithUser:(UserModel *)user {
    self = [super init];
    if(self) {
        
        self.owner = user;
        
        // CollectionView
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.minimumInteritemSpacing = 12;
        layout.minimumLineSpacing = 12;
        
        self.collectionView = [[ASCollectionView alloc] initWithFrame:CGRectZero
                                                 collectionViewLayout:layout];
        self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.collectionView.asyncDataSource = self;
        self.collectionView.asyncDelegate = self;
        self.collectionView.backgroundColor = [UIColor clearColor];
        self.collectionView.alwaysBounceVertical = YES;
        self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.view addSubview:self.collectionView];
        
        // Constraints
        NSDictionary *views = @{ @"collectionView" : self.collectionView };
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|" options:0 metrics:nil views:views]];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collectionView]|" options:0 metrics:nil views:views]];
        
        self.productResult = [NSMutableArray new];
        
        __weak ShopViewController *weakSelf = self;
        
        [[BESharedManager sharedInstance] be_productsWithUserId:self.owner.userId
                                                  withCompletion:^(BOOL finished, NSArray *result)
         {
             if(finished) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [weakSelf.collectionView performBatchUpdates:^{
                         NSMutableArray *insertIndexPaths = [NSMutableArray array];
                         NSMutableArray *deleteIndexPaths = [NSMutableArray array];
                         
                         NSInteger section = 0;
                         
                         NSArray *oldData = [weakSelf.productResult copy];
                         for(ProductModel *oldModel in oldData) {
                             NSInteger oldItem = [oldData indexOfObject:oldModel];
                             [weakSelf.productResult removeObject:oldModel];
                             [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
                         }
                         
                         for(ProductModel *newModel in result) {
                             [weakSelf.productResult addObject:newModel];
                             NSInteger item = [weakSelf.productResult indexOfObject:newModel];
                             [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
                         }
                         
                         [weakSelf.collectionView deleteItemsAtIndexPaths:deleteIndexPaths];
                         [weakSelf.collectionView insertItemsAtIndexPaths:insertIndexPaths];
                     } completion:nil
                      ];
                 });
             }
         }];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - ASCollectionView data source.

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.minimumInteritemSpacing = 12;
    layout.minimumLineSpacing = 12;
    
    if(self.productResult.count > 0) {
        layout.sectionInset = UIEdgeInsetsMake(12, 12, 12, 12);
        collectionView.collectionViewLayout = layout;
        ProductCategoryCellNode *cellNode;
        ProductModel *product = (ProductModel *)[self.productResult objectAtIndex:indexPath.row];
        cellNode = [[ProductCategoryCellNode alloc] initWithProduct:product];
        return cellNode;
    } else {
        layout.sectionInset = UIEdgeInsetsZero;
        collectionView.collectionViewLayout = layout;
        return [[ShopEmptyCellNode alloc] init];
    }
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width;
    CGFloat height;
    
    if(self.productResult.count > 0) {
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            width = ((collectionView.bounds.size.width - 36.0f) / 3.0f) - 4.0f;
            height = 294.0f;
        } else {
            width = (collectionView.bounds.size.width - 36.0f) / 2.0f;
            height = 234.0f;
        }
        return ASSizeRangeMake(CGSizeMake(width, height), CGSizeMake(width, height));
    } else {
        return ASSizeRangeMake(CGSizeMake(0, 0), CGSizeMake(collectionView.bounds.size.width, FLT_MAX));
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(self.productResult.count > 0) {
        return self.productResult.count;
    } else {
        return 0;
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionViewLockDataSource:(ASCollectionView *)collectionView
{
    // lock the data source
    // The data source should not be change until it is unlocked.
}

- (void)collectionViewUnlockDataSource:(ASCollectionView *)collectionView
{
    // unlock the data source to enable data source updating.
}

- (void)collectionView:(UICollectionView *)collectionView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    NSLog(@"fetch additional content");
    [context completeBatchFetching:YES];
}

#pragma mark - ASCollectionView delegate.

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductModel *product = (ProductModel *)[self.productResult objectAtIndex:indexPath.row];
    ProductDetailsViewController *productDetailsController = [[ProductDetailsViewController alloc] initWithProductModel:product];
    [[SlideNavigationController sharedInstance] pushViewController:productDetailsController
                                                          animated:YES];
}


#pragma mark - XLPagerTabStripViewControllerDelegate

-(NSString *)titleForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController
{
    return @"SHOP";
}

-(UIColor *)colorForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController
{
    return [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
