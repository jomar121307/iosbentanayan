//
//  ShopViewController.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopViewController : UIViewController <XLPagerTabStripChildItem, ASCollectionViewDataSource, ASCollectionViewDelegateFlowLayout>

@property (strong, nonatomic) ASCollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray *productResult;

@property (strong, nonatomic) UserModel *owner;

- (instancetype)initWithUser:(UserModel *)user;

@end
