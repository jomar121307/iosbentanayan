//
//  LoginViewController.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

#import <UIKit/UIKit.h>

#import "LoginScrollNode.h"

@interface LoginViewController : ASViewController <XLPagerTabStripChildItem, UIScrollViewDelegate>

@property (strong, nonatomic) LoginScrollNode *scrollNode;

@end
