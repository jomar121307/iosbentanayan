//
//  LoginBarViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "LoginBarViewController.h"

// Login
#import "LoginViewController.h"
#import "LoginScrollNode.h"

// Register
#import "RegisterViewController.h"
#import "RegisterScrollNode.h"

// Home
#import "HomeViewController.h"

@interface LoginBarViewController ()

@end

@implementation LoginBarViewController

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor flatWhiteColor];
    
    // Navigation
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor bentaGreenColor];
    self.navigationController.navigationBar.translucent = NO;
    
    self.navigationItem.title = @"Bentanayan Login";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Exit
    UIImage *homeImage = [[UIImage imageNamed:@"x"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    CGRect frameimg = CGRectMake(0, 0, homeImage.size.width, homeImage.size.height);
    UIButton *homeButton = [[UIButton alloc] initWithFrame:frameimg];
    [homeButton setBackgroundImage:homeImage forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(exitLogin) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    // Background Image
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login-bg"]];
    backgroundImage.frame = self.view.bounds;
    [self.view addSubview:backgroundImage];
    [self.view sendSubviewToBack:backgroundImage];
    
    // Button Bar Pager
    self.isProgressiveIndicator = NO;
    self.buttonBarView.shouldCellsFillAvailableWidth = YES;
    self.buttonBarView.labelFont = [UIFont boldSystemFontOfSize:12.0f];
    self.buttonBarView.selectedBar.backgroundColor = [UIColor flatWhiteColor];
    self.buttonBarView.selectedBarHeight = 2.0f;
    self.buttonBarView.backgroundColor = [UIColor bentaGreenColor];
    
    self.changeCurrentIndexBlock = (^void(XLButtonBarViewCell* oldCell, XLButtonBarViewCell *newCell, BOOL animated){
        oldCell.label.textColor = [UIColor lightTextColor];
        newCell.label.textColor = [UIColor whiteColor];
    });
}

- (void)viewDidAppear:(BOOL)animated {
    [self moveToViewControllerAtIndex:0 animated:YES];
}

#pragma mark - XLPagerTabStripViewControllerDataSource

-(NSArray *)childViewControllersForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController
{
    LoginViewController *loginController = [[LoginViewController alloc] init];
    
    [loginController.scrollNode.loginButtonNode addTarget:self
                                                   action:@selector(doNativeLogin:)
                                         forControlEvents:ASControlNodeEventTouchUpInside];
    
    [loginController.scrollNode.upsButtonNode addTarget:self
                                                 action:@selector(doUpsLogin:)
                                       forControlEvents:ASControlNodeEventTouchUpInside];
    
    [loginController.scrollNode.facebookButtonNode addTarget:self
                                                      action:@selector(doFbLogin)
                                            forControlEvents:ASControlNodeEventTouchUpInside];
    
    [loginController.scrollNode.forgotPasswordButtonNode addTarget:self
                                                            action:@selector(doForgotPassword:)
                                                  forControlEvents:ASControlNodeEventTouchUpInside];
    
    RegisterViewController *registerController = [[RegisterViewController alloc] init];
    
    [registerController.scrollNode.registerButtonNode addTarget:self
                                                         action:@selector(doRegister:)
                                               forControlEvents:ASControlNodeEventTouchUpInside];
    
    return @[loginController, registerController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Method / Selectors

- (void)exitLogin {
    [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES
                                                                   completion:nil];
}

- (void)doRegister:(ASButtonNode *)sender {
    RegisterScrollNode *scrollNode = (RegisterScrollNode *)sender.supernode;
    UITextField *usernameField = (UITextField *)scrollNode.usernameFieldNode.view;
    UITextField *passwordField = (UITextField *)scrollNode.passwordFieldNode.view;
    UITextField *confirmPasswordField = (UITextField *)scrollNode.confirmPasswordFieldNode.view;
    UITextField *emailField = (UITextField *)scrollNode.emailFieldNode.view;
    UITextField *firstNameField = (UITextField *)scrollNode.firstNameFieldNode.view;
    UITextField *lastNameField = (UITextField *)scrollNode.lastNameFieldNode.view;
    
    NSDictionary *registerParams = @{
                                     @"username" : usernameField.text,
                                     @"email" : emailField.text,
                                     @"password" : passwordField.text,
                                     @"confirmPassword" : confirmPasswordField.text,
                                     @"firstName" : firstNameField.text,
                                     @"lastName" : lastNameField.text,
                                     @"gender" : @"male"
                                     };
    
    for(NSString *param in [registerParams allValues]) {
        if([param isEqual:[NSNull null]] || [param isEqualToString:@""]) {
            [TSMessage showNotificationInViewController:self
                                                  title:@"Validation Error"
                                               subtitle:@"All fields are required."
                                                   type:TSMessageNotificationTypeError];
            return;
        }
    }
    
    if(usernameField.text.length < 7) {
        [TSMessage showNotificationInViewController:self
                                              title:@"Validation Error"
                                           subtitle:@"Username must have 7 or more characters"
                                               type:TSMessageNotificationTypeError];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[BESharedManager sharedInstance] be_registerWithRegisterParams:registerParams
                                                         withCompletion:^(BOOL finished)
         {
             if(finished) {
                 _alertController = [UIAlertController alertControllerWithTitle:@"Registration Successful"
                                                                        message:@"You may now login"
                                                                 preferredStyle:UIAlertControllerStyleAlert];
                 [_alertController addAction:
                  [UIAlertAction actionWithTitle:@"OK"
                                           style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * _Nonnull action) {
                                             [_alertController dismissViewControllerAnimated:YES completion:nil];
                                         }]];
                 
                 [self presentViewController:_alertController
                                    animated:YES
                                  completion:nil];
                 
                 [self moveToViewControllerAtIndex:0 animated:YES];
             }
             dispatch_async(dispatch_get_main_queue(), ^{
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
             });
         }];
    });
}

- (void)doNativeLogin:(ASButtonNode *)sender {
    LoginScrollNode *scrollNode = (LoginScrollNode *)sender.supernode;
    UITextField *usernameField = (UITextField *)scrollNode.usernameFieldNode.view;
    UITextField *passwordField = (UITextField *)scrollNode.passwordFieldNode.view;
    
    if(([usernameField.text isEqual:[NSNull null]] || [usernameField.text isEqualToString:@""]) && ([passwordField.text isEqual:[NSNull null]] || [passwordField.text isEqualToString:@""])) {
        [TSMessage showNotificationInViewController:self
                                              title:@"Login Validation Error"
                                           subtitle:@"Empty username/password field."
                                               type:TSMessageNotificationTypeError];
        return;
    } else if([usernameField.text isEqual:[NSNull null]] || [usernameField.text isEqualToString:@""]) {
        [TSMessage showNotificationInViewController:self
                                              title:@"Login Validation Error"
                                           subtitle:@"Empty usename field."
                                               type:TSMessageNotificationTypeError];
        return;
    } else if([passwordField.text isEqual:[NSNull null]] || [passwordField.text isEqualToString:@""]) {
        [TSMessage showNotificationInViewController:self
                                              title:@"Login Validation Error"
                                           subtitle:@"Empty password field."
                                               type:TSMessageNotificationTypeError];
        return;
    }
    
    NSDictionary *loginParams = @{
                                  @"userEmail" : usernameField.text,
                                  @"password" : passwordField.text,
                                  @"type" : @"native"
                                  };
    
    [scrollNode resignFirstResponder];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[BESharedManager sharedInstance] be_loginWithLoginParams:loginParams
                                                   withCompletion:^(BOOL finished, NSString *error)
         {
             if(finished) {
                 [TSMessage showNotificationWithTitle:@"Login Successful"
                                                 type:TSMessageNotificationTypeSuccess];
                 
                 [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES completion:^{
                     [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_REFRESH_MENU
                                                                         object:SLIDE_MENU_RELOAD_INDEX];
                 }];
             } else {
                 [TSMessage showNotificationInViewController:self
                                                       title:@"Login Error"
                                                    subtitle:error
                                                        type:TSMessageNotificationTypeError];
             }
             dispatch_async(dispatch_get_main_queue(), ^{
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
             });
         }];
    });
}

- (void)doUpsLogin:(ASButtonNode *)sender {
    LoginScrollNode *scrollNode = (LoginScrollNode *)sender.supernode;
    UITextField *usernameField = (UITextField *)scrollNode.usernameFieldNode.view;
    UITextField *passwordField = (UITextField *)scrollNode.passwordFieldNode.view;
    
    if(([usernameField.text isEqual:[NSNull null]] || [usernameField.text isEqualToString:@""]) && ([passwordField.text isEqual:[NSNull null]] || [passwordField.text isEqualToString:@""])) {
        [TSMessage showNotificationInViewController:self
                                              title:@"UPS Login Validation Error"
                                           subtitle:@"Empty username/password field."
                                               type:TSMessageNotificationTypeError];
        return;
    } else if([usernameField.text isEqual:[NSNull null]] || [usernameField.text isEqualToString:@""]) {
        [TSMessage showNotificationInViewController:self
                                              title:@"UPS Login Validation Error"
                                           subtitle:@"Empty usename field."
                                               type:TSMessageNotificationTypeError];
        return;
    } else if([passwordField.text isEqual:[NSNull null]] || [passwordField.text isEqualToString:@""]) {
        [TSMessage showNotificationInViewController:self
                                              title:@"UPS Login Validation Error"
                                           subtitle:@"Empty password field."
                                               type:TSMessageNotificationTypeError];
        return;
    }
    
    NSDictionary *loginParams = @{
                                  @"userEmail" : usernameField.text,
                                  @"password" : passwordField.text,
                                  @"type" : @"ups"
                                  };
    
    [scrollNode resignFirstResponder];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[BESharedManager sharedInstance] be_loginWithLoginParams:loginParams
                                                   withCompletion:^(BOOL finished, NSString *error)
         {
             if(finished) {
                 [TSMessage showNotificationWithTitle:@"UPS Login Successful"
                                                 type:TSMessageNotificationTypeSuccess];
                 
                 [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES completion:^{
                     [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_REFRESH_MENU
                                                                         object:SLIDE_MENU_RELOAD_INDEX];
                 }];
             } else {
                 [TSMessage showNotificationInViewController:self
                                                       title:@"UPS Login Error"
                                                    subtitle:@"Invalid username/email or password."
                                                        type:TSMessageNotificationTypeError];
             }
             dispatch_async(dispatch_get_main_queue(), ^{
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
             });
         }];
    });
}

- (void)doFbLogin {
    if([FBSDKAccessToken currentAccessToken]) {
        [self loginBentaWithFacebook];
        return;
    }
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile", @"email"]
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
     {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in");
             if([FBSDKAccessToken currentAccessToken]) {
                 [self loginBentaWithFacebook];
             }
         }
     }];
}

- (void)loginBentaWithFacebook {
    NSLog(@"fb access token: %@", [FBSDKAccessToken currentAccessToken].tokenString);
    NSDictionary *loginFBParams = @{
                                    FBLOGIN_ACCESSTOKEN : [FBSDKAccessToken currentAccessToken].tokenString,
                                    FBLOGIN_SOCIALNETWORK : FBLOGIN_SNTYPE_FB
                                    };
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[BESharedManager sharedInstance] be_loginWithLoginParams:loginFBParams
                                                   withCompletion:^(BOOL finished, NSString *error)
         {
             if(finished) {
                 [TSMessage showNotificationWithTitle:@"Facebook Login Successful"
                                                 type:TSMessageNotificationTypeSuccess];
                 
                 [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES completion:^{
                     [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_REFRESH_MENU
                                                                         object:SLIDE_MENU_RELOAD_INDEX];
                 }];
             } else {
                 [TSMessage showNotificationInViewController:self
                                                       title:@"Facebook Login Error"
                                                    subtitle:error
                                                        type:TSMessageNotificationTypeError];
             }
             dispatch_async(dispatch_get_main_queue(), ^{
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
             });
         }];
    });
}

- (void)doForgotPassword:(ASButtonNode *)button {
    SCLAlertView *alertView = [BentaAlertView showAlert];
    alertView.customViewColor = [UIColor bentaGreenColor];
    
    UITextField *textFieldEmail = [alertView addTextField:@"Email"];
    [textFieldEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    [textFieldEmail setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [textFieldEmail setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    [alertView addButton:@"Request New Password"
             actionBlock:^
     {
         [textFieldEmail resignFirstResponder];
         
         [MBProgressHUD showHUDAddedTo:self.view animated:YES];
         dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
             NSDictionary *resetParams = @{
                                           @"email" : textFieldEmail.text
                                           };
             [[BESharedManager sharedInstance] be_resetPassword:resetParams
                                                 withCompletion:^(BOOL finished, NSString *error)
              {
                  if(finished) {
                      [TSMessage showNotificationInViewController:self
                                                            title:@"Request Sent"
                                                         subtitle:@"You will receive an email containing instructions to change your password."
                                                             type:TSMessageNotificationTypeMessage];
                  } else {
                      [TSMessage showNotificationInViewController:self
                                                            title:@"Reset Password Failed"
                                                         subtitle:error
                                                             type:TSMessageNotificationTypeError];
                  }
                  dispatch_async(dispatch_get_main_queue(), ^{
                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                  });
              }];
         });
     }];
    
    [alertView showInfo:self
                  title:@"Reset Password"
               subTitle:@"Enter email address"
       closeButtonTitle:@"Cancel"
               duration:0.0f];
}

@end
