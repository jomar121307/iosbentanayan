//
//  RegisterScrollNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface RegisterScrollNode : ASScrollNode <UITextFieldDelegate> {
    ASTextNode *_usernameLabelNode;
    ASDisplayNode *_usernameFieldBorderNode;
    
    ASTextNode *_emailLabelNode;
    ASDisplayNode *_emailFieldBorderNode;
    
    ASTextNode *_passwordLabelNode;
    ASDisplayNode *_passwordFieldBorderNode;
    
    ASTextNode *_confirmPasswordLabelNode;
    ASDisplayNode *_confirmPasswordFieldBorderNode;
    
    ASTextNode *_firstNameLabelNode;
    ASDisplayNode *_firstNameFieldBorderNode;
    
    ASTextNode *_lastNameLabelNode;
    ASDisplayNode *_lastNameFieldBorderNode;
    
    UITextField *activeField;
}

@property (strong, nonatomic) ASDisplayNode *usernameFieldNode;
@property (strong, nonatomic) ASDisplayNode *emailFieldNode;
@property (strong, nonatomic) ASDisplayNode *passwordFieldNode;
@property (strong, nonatomic) ASDisplayNode *confirmPasswordFieldNode;
@property (strong, nonatomic) ASDisplayNode *firstNameFieldNode;
@property (strong, nonatomic) ASDisplayNode *lastNameFieldNode;

@property (strong, nonatomic) ASButtonNode *registerButtonNode;

@end
