//
//  RegisterViewController.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

#import <UIKit/UIKit.h>

#import "RegisterScrollNode.h"

@interface RegisterViewController : ASViewController <XLPagerTabStripChildItem, UIScrollViewDelegate>

@property (strong, nonatomic) RegisterScrollNode *scrollNode;

@end
