//
//  LoginScrollNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "LoginScrollNode.h"

@implementation LoginScrollNode

- (instancetype)init
{
    self = [super init];
    if(self) {
        // weakSelf
        __weak LoginScrollNode *weakSelf = self;
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        NSDictionary *attrsCredential = @{
                                          NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                          NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                          NSParagraphStyleAttributeName : paragraphStyleLeft
                                          };
        
        // Username
        NSAttributedString *stringUsername = [[NSAttributedString alloc] initWithString:@"Username / Email"
                                                                             attributes:attrsCredential];
        
        _usernameLabelNode = [[ASTextNode alloc] init];
        _usernameLabelNode.attributedString = stringUsername;
        [self addSubnode:_usernameLabelNode];
        
        _usernameFieldNode = [[ASDisplayNode alloc] initWithViewBlock:^UIView * _Nonnull{
            UITextField *textField = [[UITextField alloc] init];
            textField.delegate = weakSelf;
            textField.tag = 0;
            textField.returnKeyType = UIReturnKeyNext;
            textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            textField.font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0f];
            return textField;
        }];
        [self addSubnode:_usernameFieldNode];
        
        _usernameFieldBorderNode = [[ASDisplayNode alloc] init];
        _usernameFieldBorderNode.backgroundColor = [UIColor flatGrayColorDark];
        [self addSubnode:_usernameFieldBorderNode];
        
        // Password
        NSAttributedString *stringPassword = [[NSAttributedString alloc] initWithString:@"Password"
                                                                             attributes:attrsCredential];
        
        _passwordLabelNode = [[ASTextNode alloc] init];
        _passwordLabelNode.attributedString = stringPassword;
        [self addSubnode:_passwordLabelNode];
        
        _passwordFieldNode = [[ASDisplayNode alloc] initWithViewBlock:^UIView * _Nonnull{
            UITextField *textField = [[UITextField alloc] init];
            textField.delegate = weakSelf;
            textField.tag = 1;
            textField.returnKeyType = UIReturnKeyDone;
            textField.secureTextEntry = YES;
            textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            textField.font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0f];
            return textField;
        }];
        [self addSubnode:_passwordFieldNode];
        
        _passwordFieldBorderNode = [[ASDisplayNode alloc] init];
        _passwordFieldBorderNode.backgroundColor = [UIColor flatGrayColorDark];
        [self addSubnode:_passwordFieldBorderNode];
        
        // Login Button
        self.loginButtonNode = [[ASButtonNode alloc] init];
        self.loginButtonNode.shadowColor = [UIColor blackColor].CGColor;
        self.loginButtonNode.shadowOpacity = 0.40f;
        self.loginButtonNode.shadowRadius = 2.0f;
        self.loginButtonNode.shadowOffset = CGSizeMake(0.0f, 3.0f);
        self.loginButtonNode.cornerRadius = 2.0f;
        self.loginButtonNode.borderWidth = 1.0f;
        self.loginButtonNode.borderColor = [UIColor clearColor].CGColor;
        self.loginButtonNode.clipsToBounds = NO;
        [self.loginButtonNode setTitle:@"LOGIN"
                              withFont:[UIFont fontWithName:@"RobotoCondensed-Bold" size:14.0f]
                             withColor:[UIColor whiteColor]
                              forState:ASControlStateNormal];
        self.loginButtonNode.backgroundColor = [UIColor bentaGreenColor];
        [self addSubnode:self.loginButtonNode];
        
        // UPS
        self.upsButtonNode = [[ASButtonNode alloc] init];
        self.upsButtonNode.shadowColor = [UIColor blackColor].CGColor;
        self.upsButtonNode.shadowOpacity = 0.40f;
        self.upsButtonNode.shadowRadius = 2.0f;
        self.upsButtonNode.shadowOffset = CGSizeMake(0.0f, 3.0f);
        self.upsButtonNode.cornerRadius = 4.0f;
        self.upsButtonNode.borderWidth = 1.0f;
        self.upsButtonNode.borderColor = [UIColor clearColor].CGColor;
        self.upsButtonNode.clipsToBounds = NO;
        [self.upsButtonNode setTitle:@"LOGIN WITH UPS"
                            withFont:[UIFont fontWithName:@"RobotoCondensed-Bold" size:14.0f]
                           withColor:[UIColor whiteColor]
                            forState:ASControlStateNormal];
        [self.upsButtonNode setContentEdgeInsets:UIEdgeInsetsMake(6, 12, 6, 12)];
        [self.upsButtonNode setHitTestSlop:UIEdgeInsetsMake(-2, -4, -2, -4)];
        self.upsButtonNode.imageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor whiteColor]);
        self.upsButtonNode.backgroundColor = [UIColor colorWithHexString:@"FCA600"];
        [self addSubnode:self.upsButtonNode];
        
        // Forgot Password Button
        self.forgotPasswordButtonNode = [[ASButtonNode alloc] init];
        [self.forgotPasswordButtonNode setTitle:@"Forgot Password?"
                                       withFont:[UIFont fontWithName:@"RobotoCondensed-Regular" size:12.0f]
                                      withColor:[UIColor flatBlackColor]
                                       forState:ASControlStateNormal];
        [self addSubnode:self.forgotPasswordButtonNode];
        
        // Left Border
        _leftBorderNode = [[ASDisplayNode alloc] init];
        _leftBorderNode.backgroundColor = [UIColor flatGrayColorDark];
        [self addSubnode:_leftBorderNode];
        
        // Right Border
        _rightBorderNode = [[ASDisplayNode alloc] init];
        _rightBorderNode.backgroundColor = [UIColor flatGrayColorDark];
        [self addSubnode:_rightBorderNode];
        
        
        // Border Label
        NSMutableParagraphStyle *paragraphStyleBorder = [NSMutableParagraphStyle new];
        paragraphStyleBorder.alignment = NSTextAlignmentCenter;
        
        NSDictionary *attrsBorder = @{
                                      NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:12.0f],
                                      NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                      NSParagraphStyleAttributeName : paragraphStyleBorder
                                      };
        
        NSAttributedString *stringBorder = [[NSAttributedString alloc] initWithString:@"Or Login Using"
                                                                           attributes:attrsBorder];
        
        _borderLabelNode = [[ASTextNode alloc] init];
        _borderLabelNode.attributedString = stringBorder;
        [self addSubnode:_borderLabelNode];
        
        // Facebook
        self.facebookButtonNode = [[ASButtonNode alloc] init];
        self.facebookButtonNode.shadowColor = [UIColor blackColor].CGColor;
        self.facebookButtonNode.shadowOpacity = 0.40f;
        self.facebookButtonNode.shadowRadius = 2.0f;
        self.facebookButtonNode.shadowOffset = CGSizeMake(0.0f, 3.0f);
        self.facebookButtonNode.cornerRadius = 4.0f;
        self.facebookButtonNode.borderWidth = 1.0f;
        self.facebookButtonNode.borderColor = [UIColor clearColor].CGColor;
        self.facebookButtonNode.clipsToBounds = NO;
        [self.facebookButtonNode setTitle:@"LOGIN WITH FACEBOOK"
                                 withFont:[UIFont fontWithName:@"RobotoCondensed-Bold" size:14.0f]
                                withColor:[UIColor whiteColor]
                                 forState:ASControlStateNormal];
        [self.facebookButtonNode setImage:[UIImage imageNamed:@"facebook"]
                                 forState:ASControlStateNormal];
        [self.facebookButtonNode setContentEdgeInsets:UIEdgeInsetsMake(6, 12, 6, 12)];
        [self.facebookButtonNode setHitTestSlop:UIEdgeInsetsMake(-2, -4, -2, -4)];
        self.facebookButtonNode.imageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor whiteColor]);
        self.facebookButtonNode.backgroundColor = [UIColor colorWithRed:59.0f/255.0f green:89.0f/255.0f blue:152.0f/255.0f alpha:1.0];
        [self addSubnode:self.facebookButtonNode];
    }
    return self;
}

- (void)didLoad {
    [super didLoad];
    [self registerForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)layout {
    [super layout];
    
    float sizeOfContent = 0;
    ASDisplayNode *lastNode = [self.subnodes lastObject];
    NSInteger wd = lastNode.view.frame.origin.y;
    NSInteger ht = lastNode.view.frame.size.height;
    sizeOfContent = wd+ht;
    
    [self.view setContentSize:CGSizeMake(self.view.bounds.size.width, sizeOfContent)];
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // Username
    _usernameLabelNode.spacingAfter = 1.0f;
    
    _usernameFieldNode.sizeRange = ASRelativeSizeRangeMakeWithExactRelativeDimensions(ASRelativeDimensionMakeWithPercent(1.0), ASRelativeDimensionMakeWithPoints(24.0));
    ASStaticLayoutSpec *usernameFieldStatic = [ASStaticLayoutSpec staticLayoutSpecWithChildren:@[_usernameFieldNode]];
    usernameFieldStatic.spacingAfter = 1.0f;
    
    _usernameFieldBorderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0);
    _usernameFieldBorderNode.spacingAfter = 22.0f;
    
    // Password
    _passwordLabelNode.spacingAfter = 1.0f;
    
    _passwordFieldNode.sizeRange = ASRelativeSizeRangeMakeWithExactRelativeDimensions(ASRelativeDimensionMakeWithPercent(1.0), ASRelativeDimensionMakeWithPoints(24.0));
    ASStaticLayoutSpec *passwordFieldStatic = [ASStaticLayoutSpec staticLayoutSpecWithChildren:@[_passwordFieldNode]];
    passwordFieldStatic.spacingAfter = 1.0f;
    
    _passwordFieldBorderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0);
    _passwordFieldBorderNode.spacingAfter = 22.0f;
    
    // Button Ratio
    CGFloat buttonRatio;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        buttonRatio = 0.11;
    } else {
        buttonRatio = 0.16;
    }
    
    // Register Button
    ASRatioLayoutSpec *ratioLogin = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:buttonRatio
                                                                                   child:self.loginButtonNode];
    ratioLogin.spacingAfter = 22.0f;
    
    // UPS Login Button
    ASRatioLayoutSpec *ratioUps = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:buttonRatio
                                                                        child:self.upsButtonNode];
    
    ratioUps.spacingAfter = 11.0f;
    
    // Forgot Password Button
    self.forgotPasswordButtonNode.flexGrow = YES;
    self.forgotPasswordButtonNode.flexShrink = NO;
    ASStaticLayoutSpec *forgotPasswordStatic = [ASStaticLayoutSpec staticLayoutSpecWithChildren:@[self.forgotPasswordButtonNode]];
    forgotPasswordStatic.alignSelf = ASStackLayoutAlignSelfCenter;
    forgotPasswordStatic.spacingAfter = 22.0f;
    
    // Border
    _leftBorderNode.flexGrow = YES;
    _rightBorderNode.flexGrow = YES;
    
    _borderLabelNode.flexGrow = YES;
    _borderLabelNode.flexShrink = NO;
    
    _leftBorderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0);
    _rightBorderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0);
    
    ASStackLayoutSpec *stackBorder = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                             spacing:10.0f
                                                                      justifyContent:ASStackLayoutJustifyContentCenter
                                                                          alignItems:ASStackLayoutAlignItemsCenter
                                                                            children:@[_leftBorderNode,
                                                                                       _borderLabelNode,
                                                                                       _rightBorderNode]];
    
    ASInsetLayoutSpec *insetBorder = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(10.0f, 0.0f, 10.0f, 0.0f)
                                                                            child:stackBorder];
    insetBorder.spacingAfter = 22.0f;
    
    // Facebook Login Button
    ASRatioLayoutSpec *ratioFacebook = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:buttonRatio
                                                                             child:self.facebookButtonNode];
    
    // Content
    NSArray *contentChildren = @[_usernameLabelNode,
                                 usernameFieldStatic,
                                 _usernameFieldBorderNode,
                                 _passwordLabelNode,
                                 passwordFieldStatic,
                                 _passwordFieldBorderNode,
                                 ratioLogin,
                                 ratioUps,
                                 forgotPasswordStatic,
                                 insetBorder,
                                 ratioFacebook];
    
    ASStackLayoutSpec *contentStack = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                              spacing:0.0f
                                                                       justifyContent:ASStackLayoutJustifyContentStart
                                                                           alignItems:ASStackLayoutAlignItemsStart
                                                                             children:contentChildren];
    
    return [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(20, 40, 20, 40)
                                                  child:contentStack];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    // Attrs
    NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
    paragraphStyleLeft.alignment = NSTextAlignmentLeft;
    
    NSDictionary *attrsCredential = @{
                                      NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                      NSForegroundColorAttributeName: [UIColor bentaGreenColor],
                                      NSParagraphStyleAttributeName : paragraphStyleLeft
                                      };
    NSAttributedString *attrString;
    
    NSInteger tag = textField.tag;
    
    switch (tag) {
        case 0:
            attrString = [[NSAttributedString alloc] initWithString:_usernameLabelNode.attributedString.string
                                                         attributes:attrsCredential];
            _usernameLabelNode.attributedString = attrString;
            _usernameFieldBorderNode.backgroundColor = [UIColor bentaGreenColor];
            break;
        case 1:
            attrString = [[NSAttributedString alloc] initWithString:_passwordLabelNode.attributedString.string
                                                         attributes:attrsCredential];
            _passwordLabelNode.attributedString = attrString;
            _passwordFieldBorderNode.backgroundColor = [UIColor bentaGreenColor];
            break;
        default:
            break;
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    // Attrs
    NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
    paragraphStyleLeft.alignment = NSTextAlignmentLeft;
    
    NSDictionary *attrsCredential = @{
                                      NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                      NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                      NSParagraphStyleAttributeName : paragraphStyleLeft
                                      };
    NSAttributedString *attrString;
    
    NSInteger tag = textField.tag;
    
    switch (tag) {
        case 0:
            attrString = [[NSAttributedString alloc] initWithString:_usernameLabelNode.attributedString.string
                                                         attributes:attrsCredential];
            _usernameLabelNode.attributedString = attrString;
            _usernameFieldBorderNode.backgroundColor = [UIColor flatGrayColorDark];
            break;
        case 1:
            attrString = [[NSAttributedString alloc] initWithString:_passwordLabelNode.attributedString.string
                                                         attributes:attrsCredential];
            _passwordLabelNode.attributedString = attrString;
            _passwordFieldBorderNode.backgroundColor = [UIColor flatGrayColorDark];
            break;
        default:
            break;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if(nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}

#pragma mark - Keyboard Notifications

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.view.contentInset = contentInsets;
    self.view.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.view scrollRectToVisible:activeField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.view.contentInset = contentInsets;
    self.view.scrollIndicatorInsets = contentInsets;
}

@end