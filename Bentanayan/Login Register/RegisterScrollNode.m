//
//  RegisterScrollNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "RegisterScrollNode.h"

@implementation RegisterScrollNode

- (instancetype)init
{
    self = [super init];
    if(self) {
        // weakSelf
        __weak RegisterScrollNode *weakSelf = self;
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        NSDictionary *attrsCredential = @{
                                          NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                          NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                          NSParagraphStyleAttributeName : paragraphStyleLeft
                                          };
        
        // Username
        NSAttributedString *stringUsername = [[NSAttributedString alloc] initWithString:@"Username"
                                                                             attributes:attrsCredential];
        
        _usernameLabelNode = [[ASTextNode alloc] init];
        _usernameLabelNode.attributedString = stringUsername;
        [self addSubnode:_usernameLabelNode];
        
        _usernameFieldNode = [[ASDisplayNode alloc] initWithViewBlock:^UIView * _Nonnull{
            UITextField *textField = [[UITextField alloc] init];
            textField.delegate = weakSelf;
            textField.tag = 0;
            textField.returnKeyType = UIReturnKeyNext;
            textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            textField.font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0f];
            return textField;
        }];
        [self addSubnode:_usernameFieldNode];
        
        _usernameFieldBorderNode = [[ASDisplayNode alloc] init];
        _usernameFieldBorderNode.backgroundColor = [UIColor flatGrayColorDark];
        [self addSubnode:_usernameFieldBorderNode];
        
        // Email
        NSAttributedString *stringEmail = [[NSAttributedString alloc] initWithString:@"Email Address"
                                                                          attributes:attrsCredential];
        
        _emailLabelNode = [[ASTextNode alloc] init];
        _emailLabelNode.attributedString = stringEmail;
        [self addSubnode:_emailLabelNode];
        
        _emailFieldNode = [[ASDisplayNode alloc] initWithViewBlock:^UIView * _Nonnull{
            UITextField *textField = [[UITextField alloc] init];
            textField.keyboardType = UIKeyboardTypeEmailAddress;
            textField.delegate = weakSelf;
            textField.tag = 1;
            textField.returnKeyType = UIReturnKeyNext;
            textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            textField.font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0f];
            return textField;
        }];
        [self addSubnode:_emailFieldNode];
        
        _emailFieldBorderNode = [[ASDisplayNode alloc] init];
        _emailFieldBorderNode.backgroundColor = [UIColor flatGrayColorDark];
        [self addSubnode:_emailFieldBorderNode];
        
        // Password
        NSAttributedString *stringPassword = [[NSAttributedString alloc] initWithString:@"Password"
                                                                             attributes:attrsCredential];
        
        _passwordLabelNode = [[ASTextNode alloc] init];
        _passwordLabelNode.attributedString = stringPassword;
        [self addSubnode:_passwordLabelNode];
        
        _passwordFieldNode = [[ASDisplayNode alloc] initWithViewBlock:^UIView * _Nonnull{
            UITextField *textField = [[UITextField alloc] init];
            textField.delegate = weakSelf;
            textField.tag = 2;
            textField.returnKeyType = UIReturnKeyNext;
            textField.secureTextEntry = YES;
            textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            textField.font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0f];
            return textField;
        }];
        [self addSubnode:_passwordFieldNode];
        
        _passwordFieldBorderNode = [[ASDisplayNode alloc] init];
        _passwordFieldBorderNode.backgroundColor = [UIColor flatGrayColorDark];
        [self addSubnode:_passwordFieldBorderNode];
        
        // Confirm Password
        NSAttributedString *stringConfirmPassword = [[NSAttributedString alloc] initWithString:@"Confirm Password"
                                                                                    attributes:attrsCredential];
        
        _confirmPasswordLabelNode = [[ASTextNode alloc] init];
        _confirmPasswordLabelNode.attributedString = stringConfirmPassword;
        [self addSubnode:_confirmPasswordLabelNode];
        
        _confirmPasswordFieldNode = [[ASDisplayNode alloc] initWithViewBlock:^UIView * _Nonnull{
            UITextField *textField = [[UITextField alloc] init];
            textField.delegate = weakSelf;
            textField.tag = 3;
            textField.returnKeyType = UIReturnKeyNext;
            textField.secureTextEntry = YES;
            textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            textField.font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0f];
            return textField;
        }];
        [self addSubnode:_confirmPasswordFieldNode];
        
        _confirmPasswordFieldBorderNode = [[ASDisplayNode alloc] init];
        _confirmPasswordFieldBorderNode.backgroundColor = [UIColor flatGrayColorDark];
        [self addSubnode:_confirmPasswordFieldBorderNode];
        
        // First Name
        NSAttributedString *stringFirstName = [[NSAttributedString alloc] initWithString:@"First Name"
                                                                              attributes:attrsCredential];
        
        _firstNameLabelNode = [[ASTextNode alloc] init];
        _firstNameLabelNode.attributedString = stringFirstName;
        [self addSubnode:_firstNameLabelNode];
        
        _firstNameFieldNode = [[ASDisplayNode alloc] initWithViewBlock:^UIView * _Nonnull{
            UITextField *textField = [[UITextField alloc] init];
            textField.delegate = weakSelf;
            textField.tag = 4;
            textField.returnKeyType = UIReturnKeyNext;
            textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            textField.font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0f];
            return textField;
        }];
        [self addSubnode:_firstNameFieldNode];
        
        _firstNameFieldBorderNode = [[ASDisplayNode alloc] init];
        _firstNameFieldBorderNode.backgroundColor = [UIColor flatGrayColorDark];
        [self addSubnode:_firstNameFieldBorderNode];
        
        // Last Name
        NSAttributedString *stringLastName = [[NSAttributedString alloc] initWithString:@"Last Name"
                                                                             attributes:attrsCredential];
        
        _lastNameLabelNode = [[ASTextNode alloc] init];
        _lastNameLabelNode.attributedString = stringLastName;
        [self addSubnode:_lastNameLabelNode];
        
        _lastNameFieldNode = [[ASDisplayNode alloc] initWithViewBlock:^UIView * _Nonnull{
            UITextField *textField = [[UITextField alloc] init];
            textField.delegate = weakSelf;
            textField.tag = 5;
            textField.returnKeyType = UIReturnKeyDone;
            textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            textField.font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0f];
            return textField;
        }];
        [self addSubnode:_lastNameFieldNode];
        
        _lastNameFieldBorderNode = [[ASDisplayNode alloc] init];
        _lastNameFieldBorderNode.backgroundColor = [UIColor flatGrayColorDark];
        [self addSubnode:_lastNameFieldBorderNode];
        
        // Login Button
        self.registerButtonNode = [[ASButtonNode alloc] init];
        self.registerButtonNode.shadowColor = [UIColor blackColor].CGColor;
        self.registerButtonNode.shadowOpacity = 0.40f;
        self.registerButtonNode.shadowRadius = 2.0f;
        self.registerButtonNode.shadowOffset = CGSizeMake(0.0f, 3.0f);
        self.registerButtonNode.cornerRadius = 4.0f;
        self.registerButtonNode.borderColor = [UIColor clearColor].CGColor;
        self.registerButtonNode.clipsToBounds = NO;
        [self.registerButtonNode setTitle:@"Register"
                                 withFont:[UIFont fontWithName:@"RobotoCondensed-Bold" size:14.0f]
                                withColor:[UIColor flatBlackColor]
                                 forState:ASControlStateNormal];
        self.registerButtonNode.backgroundColor = [UIColor whiteColor];
        [self addSubnode:self.registerButtonNode];
    }
    return self;
}

- (void)didLoad {
    [super didLoad];
    [self registerForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)layout {
    [super layout];
    
    float sizeOfContent = 0;
    ASDisplayNode *lastNode = [self.subnodes lastObject];
    NSInteger wd = lastNode.view.frame.origin.y;
    NSInteger ht = lastNode.view.frame.size.height;
    sizeOfContent = wd+ht;
    
    [self.view setContentSize:CGSizeMake(self.view.bounds.size.width, sizeOfContent)];
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // Username
    _usernameLabelNode.spacingAfter = 1.0f;
    
    _usernameFieldNode.sizeRange = ASRelativeSizeRangeMakeWithExactRelativeDimensions(ASRelativeDimensionMakeWithPercent(1.0), ASRelativeDimensionMakeWithPoints(24.0));
    ASStaticLayoutSpec *usernameFieldStatic = [ASStaticLayoutSpec staticLayoutSpecWithChildren:@[_usernameFieldNode]];
    usernameFieldStatic.spacingAfter = 1.0f;
    
    _usernameFieldBorderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0);
    _usernameFieldBorderNode.spacingAfter = 20.0f;
    
    // Email
    _emailLabelNode.spacingAfter = 1.0f;
    
    _emailFieldNode.sizeRange = ASRelativeSizeRangeMakeWithExactRelativeDimensions(ASRelativeDimensionMakeWithPercent(1.0), ASRelativeDimensionMakeWithPoints(24.0));
    ASStaticLayoutSpec *emailFieldStatic = [ASStaticLayoutSpec staticLayoutSpecWithChildren:@[_emailFieldNode]];
    emailFieldStatic.spacingAfter = 1.0f;
    
    _emailFieldBorderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0);
    _emailFieldBorderNode.spacingAfter = 20.0f;
    
    // Password
    _passwordLabelNode.spacingAfter = 1.0f;
    
    _passwordFieldNode.sizeRange = ASRelativeSizeRangeMakeWithExactRelativeDimensions(ASRelativeDimensionMakeWithPercent(1.0), ASRelativeDimensionMakeWithPoints(24.0));
    ASStaticLayoutSpec *passwordFieldStatic = [ASStaticLayoutSpec staticLayoutSpecWithChildren:@[_passwordFieldNode]];
    passwordFieldStatic.spacingAfter = 1.0f;
    
    _passwordFieldBorderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0);
    _passwordFieldBorderNode.spacingAfter = 20.0f;
    
    // Password
    _confirmPasswordLabelNode.spacingAfter = 1.0f;
    
    _confirmPasswordFieldNode.sizeRange = ASRelativeSizeRangeMakeWithExactRelativeDimensions(ASRelativeDimensionMakeWithPercent(1.0), ASRelativeDimensionMakeWithPoints(24.0));
    ASStaticLayoutSpec *confirmPasswordFieldStatic = [ASStaticLayoutSpec staticLayoutSpecWithChildren:@[_confirmPasswordFieldNode]];
    confirmPasswordFieldStatic.spacingAfter = 1.0f;
    
    _confirmPasswordFieldBorderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0);
    _confirmPasswordFieldBorderNode.spacingAfter = 20.0f;
    
    // First Name
    _firstNameLabelNode.spacingAfter = 1.0f;
    
    _firstNameFieldNode.sizeRange = ASRelativeSizeRangeMakeWithExactRelativeDimensions(ASRelativeDimensionMakeWithPercent(1.0), ASRelativeDimensionMakeWithPoints(24.0));
    ASStaticLayoutSpec *firstNameFieldStatic = [ASStaticLayoutSpec staticLayoutSpecWithChildren:@[_firstNameFieldNode]];
    firstNameFieldStatic.spacingAfter = 1.0f;
    
    _firstNameFieldBorderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0);
    _firstNameFieldBorderNode.spacingAfter = 20.0f;
    
    // First Name
    _lastNameLabelNode.spacingAfter = 1.0f;
    
    _lastNameFieldNode.sizeRange = ASRelativeSizeRangeMakeWithExactRelativeDimensions(ASRelativeDimensionMakeWithPercent(1.0), ASRelativeDimensionMakeWithPoints(24.0));
    ASStaticLayoutSpec *lastNameFieldStatic = [ASStaticLayoutSpec staticLayoutSpecWithChildren:@[_lastNameFieldNode]];
    lastNameFieldStatic.spacingAfter = 1.0f;
    
    _lastNameFieldBorderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0);
    _lastNameFieldBorderNode.spacingAfter = 20.0f;
    
    // Register Button
    ASRatioLayoutSpec *registerButtonRatio = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:0.16
                                                                                   child:self.registerButtonNode];
    
    NSArray *contentChildren = @[_usernameLabelNode,
                                 usernameFieldStatic,
                                 _usernameFieldBorderNode,
                                 _emailLabelNode,
                                 emailFieldStatic,
                                 _emailFieldBorderNode,
                                 _passwordLabelNode,
                                 passwordFieldStatic,
                                 _passwordFieldBorderNode,
                                 _confirmPasswordLabelNode,
                                 confirmPasswordFieldStatic,
                                 _confirmPasswordFieldBorderNode,
                                 _firstNameLabelNode,
                                 firstNameFieldStatic,
                                 _firstNameFieldBorderNode,
                                 _lastNameLabelNode,
                                 lastNameFieldStatic,
                                 _lastNameFieldBorderNode,
                                 registerButtonRatio];
    
    ASStackLayoutSpec *contentStack = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                              spacing:0.0f
                                                                       justifyContent:ASStackLayoutJustifyContentStart
                                                                           alignItems:ASStackLayoutAlignItemsStart
                                                                             children:contentChildren];
    
    return [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(20, 40, 20, 40)
                                                  child:contentStack];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    // Attrs
    NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
    paragraphStyleLeft.alignment = NSTextAlignmentLeft;
    
    NSDictionary *attrsCredential = @{
                                      NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                      NSForegroundColorAttributeName: [UIColor bentaGreenColor],
                                      NSParagraphStyleAttributeName : paragraphStyleLeft
                                      };
    NSAttributedString *attrString;
    
    NSInteger tag = textField.tag;
    
    switch (tag) {
        case 0:
            attrString = [[NSAttributedString alloc] initWithString:_usernameLabelNode.attributedString.string
                                                         attributes:attrsCredential];
            _usernameLabelNode.attributedString = attrString;
            _usernameFieldBorderNode.backgroundColor = [UIColor bentaGreenColor];
            break;
        case 1:
            attrString = [[NSAttributedString alloc] initWithString:_emailLabelNode.attributedString.string
                                                         attributes:attrsCredential];
            _emailLabelNode.attributedString = attrString;
            _emailFieldBorderNode.backgroundColor = [UIColor bentaGreenColor];
            break;
        case 2:
            attrString = [[NSAttributedString alloc] initWithString:_passwordLabelNode.attributedString.string
                                                         attributes:attrsCredential];
            _passwordLabelNode.attributedString = attrString;
            _passwordFieldBorderNode.backgroundColor = [UIColor bentaGreenColor];
            break;
        case 3:
            attrString = [[NSAttributedString alloc] initWithString:_confirmPasswordLabelNode.attributedString.string
                                                         attributes:attrsCredential];
            _confirmPasswordLabelNode.attributedString = attrString;
            _confirmPasswordFieldBorderNode.backgroundColor = [UIColor bentaGreenColor];
            break;
        case 4:
            attrString = [[NSAttributedString alloc] initWithString:_firstNameLabelNode.attributedString.string
                                                         attributes:attrsCredential];
            _firstNameLabelNode.attributedString = attrString;
            _firstNameFieldBorderNode.backgroundColor = [UIColor bentaGreenColor];
            break;
        case 5:
            attrString = [[NSAttributedString alloc] initWithString:_lastNameLabelNode.attributedString.string
                                                         attributes:attrsCredential];
            _lastNameLabelNode.attributedString = attrString;
            _lastNameFieldBorderNode.backgroundColor = [UIColor bentaGreenColor];
            break;
        default:
            break;
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    // Attrs
    NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
    paragraphStyleLeft.alignment = NSTextAlignmentLeft;
    
    NSDictionary *attrsCredential = @{
                                      NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                      NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                      NSParagraphStyleAttributeName : paragraphStyleLeft
                                      };
    NSAttributedString *attrString;
    
    NSInteger tag = textField.tag;
    
    switch (tag) {
        case 0:
            attrString = [[NSAttributedString alloc] initWithString:_usernameLabelNode.attributedString.string
                                                         attributes:attrsCredential];
            _usernameLabelNode.attributedString = attrString;
            _usernameFieldBorderNode.backgroundColor = [UIColor flatGrayColorDark];
            break;
        case 1:
            attrString = [[NSAttributedString alloc] initWithString:_emailLabelNode.attributedString.string
                                                         attributes:attrsCredential];
            _emailLabelNode.attributedString = attrString;
            _emailFieldBorderNode.backgroundColor = [UIColor flatGrayColorDark];
            break;
        case 2:
            attrString = [[NSAttributedString alloc] initWithString:_passwordLabelNode.attributedString.string
                                                         attributes:attrsCredential];
            _passwordLabelNode.attributedString = attrString;
            _passwordFieldBorderNode.backgroundColor = [UIColor flatGrayColorDark];
            break;
        case 3:
            attrString = [[NSAttributedString alloc] initWithString:_confirmPasswordLabelNode.attributedString.string
                                                         attributes:attrsCredential];
            _confirmPasswordLabelNode.attributedString = attrString;
            _confirmPasswordFieldBorderNode.backgroundColor = [UIColor flatGrayColorDark];
            break;
        case 4:
            attrString = [[NSAttributedString alloc] initWithString:_firstNameLabelNode.attributedString.string
                                                         attributes:attrsCredential];
            _firstNameLabelNode.attributedString = attrString;
            _firstNameFieldBorderNode.backgroundColor = [UIColor flatGrayColorDark];
            break;
        case 5:
            attrString = [[NSAttributedString alloc] initWithString:_lastNameLabelNode.attributedString.string
                                                         attributes:attrsCredential];
            _lastNameLabelNode.attributedString = attrString;
            _lastNameFieldBorderNode.backgroundColor = [UIColor flatGrayColorDark];
            break;
            
        default:
            break;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if(nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}

#pragma mark - Keyboard Notifications

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification *)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.view.contentInset = contentInsets;
    self.view.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.view scrollRectToVisible:activeField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.view.contentInset = contentInsets;
    self.view.scrollIndicatorInsets = contentInsets;
}

@end
