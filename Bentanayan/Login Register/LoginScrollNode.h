//
//  LoginScrollNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface LoginScrollNode : ASScrollNode <UITextFieldDelegate> {
    ASTextNode *_usernameLabelNode;
    ASDisplayNode *_usernameFieldBorderNode;
    
    ASTextNode *_passwordLabelNode;
    ASDisplayNode *_passwordFieldBorderNode;
    
    ASDisplayNode *_leftBorderNode;
    ASTextNode *_borderLabelNode;
    ASDisplayNode *_rightBorderNode;
    
    UITextField *activeField;
}

@property (strong, nonatomic) ASDisplayNode *usernameFieldNode;
@property (strong, nonatomic) ASDisplayNode *passwordFieldNode;

@property (strong, nonatomic) ASButtonNode *loginButtonNode;
@property (strong, nonatomic) ASButtonNode *forgotPasswordButtonNode;
@property (strong, nonatomic) ASButtonNode *facebookButtonNode;
@property (strong, nonatomic) ASButtonNode *upsButtonNode;

@end
