//
//  BESharedManager.h
//
//
//  Created by Juston Paul Alcantara on 16/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BentaAPIClient.h"
#import "LoginBarViewController.h"

@interface BESharedManager : NSObject {
    UICKeyChainStore *_gbKeychain;
    SCLAlertView *_alertView;
    __weak BESharedManager *_weakSelf;
}

#pragma mark - Shared Instance

+ (BESharedManager *)sharedInstance;

#pragma mark - Properties

@property (strong, nonatomic) LoginModel *login;
@property (strong, nonatomic) NSString *accessToken;

#pragma mark - Login / Register / Edit Profile / Logout

- (void)be_loginWithLoginParams:(NSDictionary *)loginParams
                 withCompletion:(void (^)(BOOL finished, NSString *error))completion;

- (void)be_registerWithRegisterParams:(NSDictionary *)registerParams
                       withCompletion:(void (^)(BOOL finished))completion;

- (void)be_editProfileWithParams:(NSDictionary *)editParams
                  withCompletion:(void (^)(BOOL finished, NSString *error))completion;

- (void)be_resetPassword:(NSDictionary *)resetParams
          withCompletion:(void (^)(BOOL finished, NSString *error))completion;

- (void)be_logout;

#pragma mark - Categories

- (void)be_categoriesWithCompletion:(void (^)(BOOL finished, NSArray *result))completion;

#pragma mark - Products
/**
 * Find Product with Filter Sort.
 * @return
 */
- (void)be_productsWithCategory:(NSString *)category
                withSubcategory:(NSString *)subcategory
                  withGroupling:(NSString *)groupling
           withFilterSortOption:(FilterSortOptions)sortOption
                 withCompletion:(void (^)(BOOL finished, NSArray *result))completion;

/**
 * Find Product with Sort, Min-Max Price.
 * @return
 */
- (void)be_productsWithCategory:(NSString *)category
                withSubcategory:(NSString *)subcategory
                  withGroupling:(NSString *)groupling
           withFilterSortOption:(FilterSortOptions)sortOption
                   withMinPrice:(CGFloat)min
                   withMaxPrice:(CGFloat)max
                 withCompletion:(void (^)(BOOL finished, NSArray *result))completion;

/**
 * Find Product with Sort, Min-Max Price, Search.
 * @return
 */
- (void)be_productsWithCategory:(NSString *)category
                withSubcategory:(NSString *)subcategory
                  withGroupling:(NSString *)groupling
           withFilterSortOption:(FilterSortOptions)sortOption
                   withMinPrice:(CGFloat)min
                   withMaxPrice:(CGFloat)max
               withSearchString:(NSString *)search
                 withCompletion:(void (^)(BOOL finished, NSArray *result))completion;

- (void)be_productsInCartForPayment:(BOOL)forPayment
                     WithCompletion:(void (^)(BOOL finished, NSArray *result))completion;

- (void)be_numberOfItemInCartWithCompletion:(void (^)(BOOL finished, NSString *numOfItems, NSString *error))completion;

- (void)be_addProductToCartWithProductId:(NSString *)productId
                                quantity:(NSInteger)quantity
                          withCompletion:(void (^)(BOOL finished))completion;

- (void)be_productsWithUserId:(NSString *)userId
               withCompletion:(void (^)(BOOL finished, NSArray *result))completion;

- (void)be_cartRemoveItemsInShop:(NSString *)shopId
                  withCompletion:(void (^)(BOOL finished))completion;

- (void)be_cartRemoveItem:(NSString *)itemId
           withCompletion:(void (^)(BOOL finished))completion;

#pragma mark - Product Review

- (void)be_productWithReview:(NSString *)productId
              withCompletion:(void (^)(BOOL finished, ProductModel *product))completion;

#pragma mark - Shipping & Payment

- (void)be_shippingAddressWithCompletion:(void (^)(BOOL finished, NSArray *result))completion;

- (void)be_addShippingAddress:(NSDictionary *)addressParams
               withCompletion:(void (^)(BOOL finished))completion;

- (void)be_checkoutInfo:(NSDictionary *)checkoutParams
         withCompletion:(void (^)(BOOL finished))completion;

- (void)be_payOrderWithCompletion:(void (^)(BOOL finished))completion;

- (void)be_payOrderWithECash:(NSDictionary *)ecashParams
              withCompletion:(void (^)(BOOL finished, NSString *error))completion;

#pragma mark - Purchase History

- (void)be_purchaseHistoryWithCompletion:(void (^)(BOOL finished, NSArray *result))completion;

#pragma mark - Messages

- (void)be_roomsSkip:(NSUInteger)skip
      withCompletion:(void (^)(BOOL finished, NSArray *result))completion;

- (void)be_messagesWithRoomId:(NSString *)roomId
                         skip:(NSUInteger)skip
               withCompletion:(void (^)(BOOL finished, RoomModel *room))completion;

- (void)be_sendMessageWithOtherUserId:(NSString *)otherUserId
                          withMessage:(NSDictionary *)message
                       withCompletion:(void (^)(BOOL finished, MessageModel *newMessage))completion;

- (void)be_initMessageWithOtherUserId:(NSString *)otherUserId
                          withMessage:(NSDictionary *)message
                       withCompletion:(void (^)(BOOL finished))completion;

#pragma mark - SCLAlerts

- (SCLAlertView *)showAlert;

#pragma mark - Others

- (BOOL)checkLogin;

- (BOOL)isLoggedIn;

@end
