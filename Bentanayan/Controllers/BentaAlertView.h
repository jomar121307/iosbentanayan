//
//  BentaAlertView.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 26/05/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "SCLAlertView.h"

@interface BentaAlertView : SCLAlertView

+ (SCLAlertView *)showAlert;

@end
