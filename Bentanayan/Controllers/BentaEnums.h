//
//  BentaEnums.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 26/05/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, FilterSortOptions)
{
    FilterSortOptionMostRecent = 0,
    FilterSortOptionLowHigh,
    FilterSortOptionHighLow
};

@interface BentaEnums : NSObject

@end
