//
//  BentaAPIClient.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 25/05/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface BentaAPIClient : AFHTTPSessionManager

+ (instancetype)sharedClient;

@end
