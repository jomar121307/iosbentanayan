//
//  BESharedManager.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 16/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "BESharedManager.h"

#define AUTH_TOKEN_KEY @"Authorization"
#define USERDATA_KEY @"USERDATA"

static BESharedManager *sharedInstance = nil;

@implementation BESharedManager

#pragma mark - SHARED INSTANCE

+ (BESharedManager *)sharedInstance
{
    // Thread blocking to be sure for singleton instance
    @synchronized(self) {
        if (!sharedInstance) {
            sharedInstance = [BESharedManager new];
        }
    }
    return sharedInstance;
}

#pragma mark - Initialize Bentanayan Manager

- (id)init {
    self = [super init];
    if (self) {
        // Initialize Keychain
        _gbKeychain = [UICKeyChainStore keyChainStoreWithService:[[NSBundle mainBundle] bundleIdentifier]];
        
        // Weak Self
        _weakSelf = self;
    }
    return self;
}

#pragma mark - Login / Register / Edit Profile / Logout

- (void)be_loginWithLoginParams:(NSDictionary *)loginParams
                 withCompletion:(void (^)(BOOL finished, NSString *error))completion
{
    [[BentaAPIClient sharedClient]
     POST:@"/sessions"
     parameters:loginParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSDictionary *response = (NSDictionary *)[self getJSONObject:responseObject];
         NSLog(@"POST - Login Success: %@", response);
         NSError *err;
         self.login = [[LoginModel alloc] initWithDictionary:response
                                                       error:&err];
         if(!err) {
             NSLog(@"Login Successful");
             [self saveCredentials];
             completion(1, nil);
         } else {
             NSLog(@"Error Login Model");
             completion(0, nil);
         }
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"POST - Login Failed: %@", [self showRequestError:error]);
         if([[AFNetworkReachabilityManager sharedManager] isReachable]) {
             NSString *errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
             completion(0, errorResponse);
         } else {
             completion(0, @"No Internet Connection");
         }
     }];
}

- (void)be_registerWithRegisterParams:(NSDictionary *)registerParams
                       withCompletion:(void (^)(BOOL finished))completion
{
    [[BentaAPIClient sharedClient]
     POST:@"/users"
     parameters:registerParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"POST - Register Success: %@", [self getJSONObject:responseObject]);
         completion(1);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"POST - Register Failed: %@", [self showRequestError:error]);
         completion(0);
     }];
}

- (void)be_myProfileWithCompletion:(void (^)(BOOL finished, UserModel *user))completion {
    [[BentaAPIClient sharedClient]
     GET:@"/users/me"
     parameters:nil
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"GET - My Profile Success");
         NSDictionary *response = (NSDictionary *)[self getJSONObject:responseObject];
         NSError *err;
         UserModel *user = [[UserModel alloc] initWithDictionary:response
                                                           error:&err];
         if(!err) {
             [self saveCredentials];
             completion(1, user);
         } else {
             NSLog(@"Error User Model");
             completion(0, nil);
         }
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"GET - My Profile Failed: %@", [self showRequestError:error]);
         completion(0, nil);
     }];
}

- (void)be_editProfileWithParams:(NSDictionary *)editParams
                  withCompletion:(void (^)(BOOL finished, NSString *error))completion
{
    [[BentaAPIClient sharedClient]
     PUT:[NSString stringWithFormat:@"/users/%@", self.login.session.userId.userId]
     parameters:editParams
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSDictionary *response = (NSDictionary *)[self getJSONObject:responseObject];
         NSLog(@"POST - Login Success: %@", response);
         NSError *err;
         self.login.session.userId = [[UserModel alloc] initWithDictionary:response
                                                                     error:&err];
         if(!err) {
             NSLog(@"Edit Profile Successful");
             [self saveCredentials];
             completion(1, nil);
         } else {
             NSLog(@"Error Edit Profile User Model");
             completion(0, nil);
         }
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"Edit Profile Failed");
         if([[AFNetworkReachabilityManager sharedManager] isReachable]) {
             NSString *errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
             completion(0, errorResponse);
         } else {
             completion(0, @"No Internet Connection");
         }
     }];
}

- (void)be_resetPassword:(NSDictionary *)resetParams
          withCompletion:(void (^)(BOOL finished, NSString *error))completion
{
    [[BentaAPIClient sharedClient]
     POST:@"/users/resetpassword"
     parameters:resetParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"POST- Reset Password Success");
         completion(1, nil);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"POST - Reset Password Failed");
         completion(0, [self showRequestError:error]);
     }];
}

- (void)be_logout
{
    [self clearSavedCredentials];
}

#pragma mark - Shop

- (void)be_shopWithShopId:(NSString *)shopId
           withCompletion:(void (^)(BOOL finished, ShopModel *result))completion
{
    [[BentaAPIClient sharedClient]
     GET:[NSString stringWithFormat:@"/shops/%@", shopId]
     parameters:nil
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"GET - Shop Success");
         NSDictionary *response = (NSDictionary *)[self getJSONObject:responseObject];
         NSError *err;
         ShopModel *shop = [[ShopModel alloc] initWithDictionary:response
                                                           error:&err];
         if(!err) {
             completion(1, shop);
         } else {
             NSLog(@"Error Shop Model: %@", err.localizedDescription);
             completion(0, nil);
         }
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"GET - Shop Failed: %@", [self showRequestError:error]);
         completion(0, nil);
     }];
}

#pragma mark - Categories

- (void)be_categoriesWithCompletion:(void (^)(BOOL finished, NSArray *result))completion
{
    [[BentaAPIClient sharedClient]
     GET:@"/misc"
     parameters:nil
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"GET - Categories Success");
         NSArray *response = (NSArray *)[self getJSONObject:responseObject];
         NSDictionary *responseDict = [response firstObject];
         NSMutableArray *categories = [NSMutableArray new];
         for(NSDictionary *dataDict in [responseDict objectForKey:@"metadata"]) {
             NSError *err;
             CategoryModel *category = [[CategoryModel alloc] initWithDictionary:dataDict
                                                                           error:&err];
             if(!err) {
                 [categories addObject:category];
             } else {
                 NSLog(@"Error Category Model: %@", err.localizedDescription);
             }
         }
         completion(1, [categories copy]);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"GET - Categories Failed: %@", [self showRequestError:error]);
         completion(0, nil);
     }];
}

#pragma mark - Products

- (void)be_productsWithCategory:(NSString *)category
                withSubcategory:(NSString *)subcategory
                  withGroupling:(NSString *)groupling
           withFilterSortOption:(FilterSortOptions)sortOption
                 withCompletion:(void (^)(BOOL finished, NSArray *result))completion
{
    [self be_productsWithCategory:category
                  withSubcategory:subcategory
                    withGroupling:groupling
             withFilterSortOption:sortOption
                     withMinPrice:0
                     withMaxPrice:0
                 withSearchString:nil
                   withCompletion:^(BOOL finished, NSArray *result) {
                       completion(finished, result);
                   }];
}

- (void)be_productsWithCategory:(NSString *)category
                withSubcategory:(NSString *)subcategory
                  withGroupling:(NSString *)groupling
           withFilterSortOption:(FilterSortOptions)sortOption
                   withMinPrice:(CGFloat)min
                   withMaxPrice:(CGFloat)max
                 withCompletion:(void (^)(BOOL finished, NSArray *result))completion
{
    [self be_productsWithCategory:category
                  withSubcategory:subcategory
                    withGroupling:groupling
             withFilterSortOption:sortOption
                     withMinPrice:min
                     withMaxPrice:max
                 withSearchString:nil
                   withCompletion:^(BOOL finished, NSArray *result) {
                       completion(finished, result);
                   }];
}

- (void)be_productsWithCategory:(NSString *)category
                withSubcategory:(NSString *)subcategory
                  withGroupling:(NSString *)groupling
           withFilterSortOption:(FilterSortOptions)sortOption
                   withMinPrice:(CGFloat)min
                   withMaxPrice:(CGFloat)max
               withSearchString:(NSString *)search
                 withCompletion:(void (^)(BOOL finished, NSArray *result))completion
{
    NSString *urlString;
    NSString *filterOptionString;
    NSString *rangeString;
    NSString *searchString;
    
    if(search != nil) {
        searchString = [NSString stringWithFormat:@", \"name\" : {\"contains\" : \"%@\"}", search];
    } else {
        searchString = @"";
    }
    
    if(min > 0 || max > 0) {
        rangeString = [NSString stringWithFormat:@", \"price\" : {\">=\" : \"%f\", \"<=\" : \"%f\"}", min, max];
    } else {
        rangeString = @"";
    }
    
    switch(sortOption) {
        case 0:
            filterOptionString = @"&sort=createdAt+DESC";
            break;
        case 1:
            filterOptionString = @"&sort=price+ASC";
            break;
        case 2:
            filterOptionString = @"&sort=price+DESC";
            break;
        default:
            filterOptionString = @"&sort=createdAt+DESC";
            break;
    }
    
    if(subcategory == nil && groupling == nil) {
        // Search by category
        urlString = [self encodeURL:[NSString stringWithFormat:@"where={\"category\" : {\"startsWith\" : \"%@\"} %@%@ }%@", category, searchString, rangeString, filterOptionString]];
    } else if(groupling == nil) {
        // Search by subcategory
        urlString = [self encodeURL:[NSString stringWithFormat:@"where={\"category\" : {\"startsWith\" : \"%@.%@\"} %@%@ }%@", category, subcategory, searchString, rangeString, filterOptionString]];
    } else {
        // Search by groupling
        urlString = [self encodeURL:[NSString stringWithFormat:@"where={\"category\" : \"%@.%@.%@\" %@%@ }%@", category, subcategory, groupling,searchString, rangeString, filterOptionString]];
    }
    
    [[BentaAPIClient sharedClient]
     GET:[NSString stringWithFormat:@"/products?%@", urlString]
     parameters:nil
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSDictionary *response = (NSDictionary *)[self getJSONObject:responseObject];
         NSLog(@"GET - Product(Category) Success");
         NSMutableArray *products = [NSMutableArray new];
         for(NSDictionary *dataDict in [response valueForKey:@"products"]) {
             NSError *err;
             ProductModel *category = [[ProductModel alloc] initWithDictionary:dataDict
                                                                         error:&err];
             if(!err) {
                 [products addObject:category];
             } else {
                 NSLog(@"Error Product Model: %@", err.localizedDescription);
             }
         }
         completion(1, [products copy]);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"GET - Product(Category) Failed: %@", [self showRequestError:error]);
         completion(0, nil);
     }];
}

- (void)be_productsInCartForPayment:(BOOL)forPayment
                     WithCompletion:(void (^)(BOOL finished, NSArray *result))completion
{
    NSString *urlString;
    if(forPayment) {
        urlString = @"/cart?payment=true";
    } else {
        urlString = @"/cart";
    }
    
    [[BentaAPIClient sharedClient]
     GET:urlString
     parameters:nil
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"GET - Cart Items Success");
         NSDictionary *response = (NSDictionary *)[self getJSONObject:responseObject];
         NSMutableArray *cartItems = [NSMutableArray new];
         for(NSDictionary *dataDict in [response valueForKey:@"items"]) {
             NSError *err;
             CartItemModel *item = [[CartItemModel alloc] initWithDictionary:dataDict
                                                                       error:&err];
             if(!err) {
                 [cartItems addObject:item];
             } else {
                 NSLog(@"Error Cart Item Model: %@", err.localizedDescription);
             }
         }
         completion(1, [cartItems copy]);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"GET - Cart Items Failed: %@", [self showRequestError:error]);
         completion(0, nil);
     }];
}

- (void)be_numberOfItemInCartWithCompletion:(void (^)(BOOL finished, NSString *numOfItems, NSString *error))completion
{
    [[BentaAPIClient sharedClient]
     GET:@"/cart"
     parameters:nil
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"GET - No. Of Cart Items Success");
         NSDictionary *response = (NSDictionary *)[self getJSONObject:responseObject];
         NSNumber *count = [response objectForKey:@"count"];
         NSString *numOfItems = [NSString stringWithFormat:@"%d", [count intValue]];
         completion(1, numOfItems, nil);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"GET - Cart Items Failed");
         completion(0, nil, [self showRequestError:error]);
     }];
}

- (void)be_addProductToCartWithProductId:(NSString *)productId
                                quantity:(NSInteger)quantity
                          withCompletion:(void (^)(BOOL finished))completion
{
    NSDictionary *postParams = @{
                                 @"productId" : productId,
                                 @"quantity" : [NSNumber numberWithInteger:quantity]
                                 };
    
    [[BentaAPIClient sharedClient]
     POST:@"/cart"
     parameters:postParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"POST - Add Product To Cart Success");
         completion(1);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"POST - Add Product To Cart Failed: %@", [self showRequestError:error]);
         completion(0);
     }];
}

- (void)be_productsWithUserId:(NSString *)userId
               withCompletion:(void (^)(BOOL finished, NSArray *result))completion
{
    NSString *urlString = [self encodeURL:[NSString stringWithFormat:@"where={\"userId\" : \"%@\"}", userId]];
    
    [[BentaAPIClient sharedClient]
     GET:[NSString stringWithFormat:@"/products?%@", urlString]
     parameters:nil
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"GET - User Products Success");
         NSDictionary *response = (NSDictionary *)[self getJSONObject:responseObject];
         NSMutableArray *products = [NSMutableArray new];
         for(NSDictionary *dataDict in [response valueForKey:@"products"]) {
             NSError *err;
             ProductModel *category = [[ProductModel alloc] initWithDictionary:dataDict
                                                                         error:&err];
             if(!err) {
                 [products addObject:category];
             } else {
                 NSLog(@"Error Product Model: %@", err.localizedDescription);
             }
         }
         completion(1, [products copy]);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"GET - User Products Failed: %@", [self showRequestError:error]);
     }];
}

- (void)be_cartRemoveItemsInShop:(NSString *)shopId
                  withCompletion:(void (^)(BOOL finished))completion
{
    [[BentaAPIClient sharedClient]
     DELETE:[NSString stringWithFormat:@"/cart/empty/%@", shopId]
     parameters:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"DELETE - Empty Cart Success");
         completion(1);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"DELETE - Empty Cart Failed: %@", [self showRequestError:error]);
         completion(0);
     }];
}

- (void)be_cartRemoveItem:(NSString *)itemId
           withCompletion:(void (^)(BOOL finished))completion
{
    [[BentaAPIClient sharedClient]
     DELETE:[NSString stringWithFormat:@"/cart/%@", itemId]
     parameters:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"DELETE - Remove Item From Cart Success: %@", [self getJSONObject:responseObject]);
         completion(1);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"DELETE - Remove Item From Cart Failed: %@", [self showRequestError:error]);
         completion(0);
     }];
}

#pragma mark - Product Review

- (void)be_productWithReview:(NSString *)productId
              withCompletion:(void (^)(BOOL finished, ProductModel *product))completion
{
    NSString *urlString = [NSString stringWithFormat:@"/products/%@?reviews=true", productId];
    
    [[BentaAPIClient sharedClient]
     GET:urlString
     parameters:nil
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"GET - Product(Review) Success");
         NSDictionary *response = (NSDictionary *)[self getJSONObject:responseObject];
         NSError *err;
         ProductModel *product = [[ProductModel alloc] initWithDictionary:response
                                                                    error:&err];
         if(!err) {
             completion(1, product);
         } else {
             NSLog(@"Error Product(Review) Model: %@", err.localizedDescription);
             completion(0, nil);
         }
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"GET - Product(Review) Failed: %@", [self showRequestError:error]);
         completion(0, nil);
     }];
}

#pragma mark - Shipping / Payment

- (void)be_shippingAddressWithCompletion:(void (^)(BOOL finished, NSArray *result))completion
{
    [[BentaAPIClient sharedClient]
     GET:@"/shipping"
     parameters:nil
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"GET - Shipping Address Success");
         NSArray *response = (NSArray *)[self getJSONObject:responseObject];
         NSMutableArray *shippingAddresses = [NSMutableArray new];
         for(NSDictionary *dataDict in response) {
             NSError *err;
             ShippingModel *shipping = [[ShippingModel alloc] initWithDictionary:dataDict
                                                                           error:&err];
             if(!err) {
                 [shippingAddresses addObject:shipping];
             } else {
                 NSLog(@"Error Shipping Model: %@", err.localizedDescription);
             }
         }
         completion(1, shippingAddresses);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"GET - Shipping Address Failed: %@", [self showRequestError:error]);
         completion(0, nil);
     }];
}

- (void)be_addShippingAddress:(NSDictionary *)addressParams
               withCompletion:(void (^)(BOOL finished))completion
{
    [[BentaAPIClient sharedClient]
     POST:@"/shipping"
     parameters:addressParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"POST - Add Shipping Address Success: %@", [self getJSONObject:responseObject]);
         completion(1);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"POST - Add Shipping Address Failed: %@", [self showRequestError:error]);
         completion(0);
     }];
}

- (void)be_checkoutInfo:(NSDictionary *)checkoutParams
         withCompletion:(void (^)(BOOL finished))completion
{
    [[BentaAPIClient sharedClient]
     POST:@"/setcheckoutinfo"
     parameters:checkoutParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"POST - Set Checkout Info Success: %@", [self getJSONObject:responseObject]);
         completion(1);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"POST - Set Checkout Info Failed: %@", [self showRequestError:error]);
         completion(0);
     }];
}

- (void)be_payOrderWithCompletion:(void (^)(BOOL finished))completion
{
    [[BentaAPIClient sharedClient]
     GET:@"/gonnapaynow"
     parameters:nil
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"GET - Submit Order Success");
         NSDictionary *response = (NSDictionary *)[self getJSONObject:responseObject];
         NSLog(@"%@ %@", responseObject, response);
         WebViewController *vc = [[WebViewController alloc] initWithURL:response[@"paymentApprovalUrl"]];
         UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
         [[SlideNavigationController sharedInstance] presentViewController:navController animated:YES completion:^{
             [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
         }];
         completion(1);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"GET - Submit Order Failed: %@", [self showRequestError:error]);
         completion(0);
     }];
}

- (void)be_payOrderWithECash:(NSDictionary *)ecashParams
              withCompletion:(void (^)(BOOL finished, NSString *error))completion
{
    [[BentaAPIClient sharedClient]
     POST:@"/gonnapaynow"
     parameters:ecashParams
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"GET - Pay Ecash Order Success: %@", [self getJSONObject:responseObject]);
         completion(1, nil);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"GET - Pay Ecash Order Failed: %@", [self showRequestError:error]);
         completion(0, [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding]);
     }];
}

#pragma mark - Purchase History

- (void)be_purchaseHistoryWithCompletion:(void (^)(BOOL finished, NSArray *result))completion
{
    [[BentaAPIClient sharedClient]
     GET:@"/orders?purchases=true&sort=createdAt+DESC"
     parameters:nil
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"GET - Purchased Items Success");
         NSDictionary *response = (NSDictionary *)[self getJSONObject:responseObject];
         NSMutableArray *orders = [NSMutableArray new];
         for(NSDictionary *dataDict in [response objectForKey:@"orders"]) {
             NSError *err;
             OrderModel *order = [[OrderModel alloc] initWithDictionary:dataDict
                                                                  error:&err];
             if(!err) {
                 [orders addObject:order];
             } else {
                 NSLog(@"Error Order Model: %@", err.localizedDescription);
             }
         }
         completion(1, orders);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"GET - Purchased Items Failed: %@", [self showRequestError:error]);
         completion(0, nil);
     }];
}

#pragma mark - Messages

- (void)be_roomsSkip:(NSUInteger)skip
      withCompletion:(void (^)(BOOL finished, NSArray *result))completion
{
    NSString *urlString = [NSString stringWithFormat:@"/rooms?skip=%lud&limit=10&sort=updatedAt+DESC", (unsigned long)skip];
    
    [[BentaAPIClient sharedClient]
     GET:urlString
     parameters:nil
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"GET - Rooms Success");
         NSArray *response = (NSArray *)[self getJSONObject:responseObject];
         NSMutableArray *rooms = [NSMutableArray new];
         for(NSDictionary *dataDict in response) {
             NSError *err;
             RoomModel *room = [[RoomModel alloc] initWithDictionary:dataDict
                                                               error:&err];
             if(!err) {
                 [rooms addObject:room];
             } else {
                 NSLog(@"Error Room Model: %@", err.localizedDescription);
             }
         }
         completion(1, [rooms copy]);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"GET - Rooms Failed: %@", [self showRequestError:error]);
         completion(0, nil);
     }];
}

- (void)be_messagesWithRoomId:(NSString *)roomId
                         skip:(NSUInteger)skip
               withCompletion:(void (^)(BOOL finished, RoomModel *room))completion
{
    NSString *urlString = [NSString stringWithFormat:@"/rooms/%@?skip=%lud&limit=5&sort=updatedAt+DESC", roomId, (unsigned long)skip];
    
    [[BentaAPIClient sharedClient]
     GET:urlString
     parameters:nil
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"GET - Messages Success");
         NSDictionary *response = (NSDictionary *)[self getJSONObject:responseObject];
         NSError *err;
         RoomModel *room = [[RoomModel alloc] initWithDictionary:response
                                                           error:&err];
         if(!err) {
             completion(1, room);
         } else {
             NSLog(@"Error Room Model: %@", err.localizedDescription);
             completion(0, nil);
         }
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"GET - Messages Failed: %@", [self showRequestError:error]);
         completion(0, nil);
     }];
}

- (void)be_sendMessageWithOtherUserId:(NSString *)otherUserId
                          withMessage:(NSDictionary *)message
                       withCompletion:(void (^)(BOOL finished, MessageModel *newMessage))completion
{
    NSString *urlString = [NSString stringWithFormat:@"/message/%@", otherUserId];
    
    [[BentaAPIClient sharedClient]
     POST:urlString
     parameters:message
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"POST - Send Messages Success: %@", [self getJSONObject:responseObject]);
         NSDictionary *response = (NSDictionary *)[self getJSONObject:responseObject];
         NSError *err;
         MessageModel *message = [[MessageModel alloc] initWithDictionary:response
                                                                    error:&err];
         if(!err) {
             completion(1, message);
         } else {
             NSLog(@"Error Message Model: %@", err.localizedDescription);
             completion(0, nil);
         }
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"POST - Send Messages Failed: %@", [self showRequestError:error]);
         completion(0, nil);
     }];
}

- (void)be_initMessageWithOtherUserId:(NSString *)otherUserId
                          withMessage:(NSDictionary *)message
                       withCompletion:(void (^)(BOOL finished))completion
{
    NSString *urlString = [NSString stringWithFormat:@"/messages/initiate/%@", otherUserId];
    
    [[BentaAPIClient sharedClient]
     POST:urlString
     parameters:message
     progress:nil
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSLog(@"POST - Send Init Messages Success");
         completion(1);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"POST - Send Init Messages Failed: %@", [self showRequestError:error]);
         completion(0);
     }];
}

#pragma mark - Keychain and Auth Token Functions

- (BOOL)checkLogin
{
    NSString *accessToken = [self accessToken];
    NSData *userData = [self userData];
    BOOL hasAccessToken = accessToken != nil;
    BOOL hasUserData = userData != nil;
    if(!(hasAccessToken && hasUserData)) {
        SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
        alert.customViewColor = [UIColor bentaGreenColor];
        alert.hideAnimationType = SlideOutToCenter;
        alert.showAnimationType = SlideInToCenter;
        alert.shouldDismissOnTapOutside = YES;
        
        [alert addButton:@"Login / Register"
             actionBlock:^{
                 LoginBarViewController *loginController = [[LoginBarViewController alloc] init];
                 UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:loginController];
                 [[SlideNavigationController sharedInstance] presentViewController:navController animated:YES completion:^{
                     [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
                 }];
             }];
        
        [alert showNotice:@"Login Required"
                 subTitle:@"Do you want to login/register?"
         closeButtonTitle:@"Not now"
                 duration:12.0f];
        
    }
    return hasAccessToken && hasUserData;
}

- (BOOL)isLoggedIn
{
    NSString *accessToken = [self accessToken];
    NSData *userData = [self userData];
    BOOL hasAccessToken = accessToken != nil;
    BOOL hasUserData = userData != nil;
    if(hasAccessToken && hasUserData) {
        [[BentaAPIClient sharedClient].requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", accessToken]
                                               forHTTPHeaderField:AUTH_TOKEN_KEY];
        self.login = [[LoginModel alloc] initWithData:userData error:nil];
        self.accessToken = accessToken;
        [self be_myProfileWithCompletion:^(BOOL finished, UserModel *user) {
            if(finished) {
                self.login.session.userId = user;
                [self saveCredentials];
            }
        }];
        NSLog(@"Access Token: %@", self.accessToken);
        NSLog(@"User ID: %@", self.login.session.userId.userId);
        return 1;
    } else {
        return 0;
    }
}

- (void)clearSavedCredentials
{
    self.login = nil;
    self.accessToken = nil;
    _gbKeychain[AUTH_TOKEN_KEY] = nil;
    _gbKeychain[USERDATA_KEY] = nil;
    [[BentaAPIClient sharedClient].requestSerializer setValue:nil
                                           forHTTPHeaderField:AUTH_TOKEN_KEY];
}

- (NSString *)accessToken
{
    return [_gbKeychain stringForKey:AUTH_TOKEN_KEY];
}

- (NSData *)userData
{
    return [_gbKeychain dataForKey:USERDATA_KEY];
}

- (void)saveCredentials
{
    NSLog(@"Save User Data");
    [[BentaAPIClient sharedClient].requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", self.login.session.accessToken]
                                           forHTTPHeaderField:AUTH_TOKEN_KEY];
    [_gbKeychain setString:self.login.session.accessToken forKey:AUTH_TOKEN_KEY];
    [_gbKeychain setData:[self.login toJSONData] forKey:USERDATA_KEY];
}

#pragma mark - Helpers

- (id<NSObject>)getJSONObject:(id)responseObject {
    if([responseObject isKindOfClass:[NSData class]]) {
        id serializedObject = [NSJSONSerialization JSONObjectWithData:(NSData *)responseObject
                                                              options:kNilOptions error:nil];
        if([serializedObject isKindOfClass:[NSDictionary class]] ||
           [serializedObject isKindOfClass:[NSMutableDictionary class]])
        {
            return [[NSMutableDictionary alloc] initWithDictionary:serializedObject];
        }
        else if([serializedObject isKindOfClass:[NSArray class]] ||
                [serializedObject isKindOfClass:[NSMutableArray class]])
        {
            return [[NSMutableArray alloc] initWithArray:serializedObject];
        }
        else
        {
            NSLog(@"Response is not a valid object");
            return nil;
        }
    } else {
        NSLog(@"Reponse is not a NSData class");
        return nil;
    }
}

- (NSString *)encodeURL:(NSString *)httpString {
    return [httpString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
}

#pragma mark - Alerts

- (SCLAlertView *)showAlert {
    _alertView = [[SCLAlertView alloc] initWithNewWindow];
    _alertView.shouldDismissOnTapOutside = YES;
    _alertView.hideAnimationType = SlideOutToCenter;
    _alertView.showAnimationType = SlideInToCenter;
    _alertView.shouldDismissOnTapOutside = YES;
    
    _alertView.buttonFormatBlock = ^NSDictionary* {
        NSDictionary *format = @{
                                 @"textColor" : [UIColor flatWhiteColor]
                                 };
        return format;
    };
    
    _alertView.completeButtonFormatBlock = ^NSDictionary* {
        NSDictionary *format = @{
                                 @"textColor" : [UIColor flatWhiteColor]
                                 };
        return format;
    };
    
    return _alertView;
}

#pragma mark - Error Handler

- (NSString *)showRequestError:(NSError *)error {
    NSString *errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
    NSLog(@"%@", [NSString stringWithFormat:@"%@ - %@", error.localizedDescription, errorResponse]);
    return errorResponse;
}

@end
