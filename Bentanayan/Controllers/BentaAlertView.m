//
//  BentaAlertView.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 26/05/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "BentaAlertView.h"

@interface BentaAlertView ()

@end

@implementation BentaAlertView

+ (SCLAlertView *)showAlert {
    BentaAlertView *_alertView = [[BentaAlertView alloc] initWithNewWindow];
    _alertView.shouldDismissOnTapOutside = YES;
    _alertView.hideAnimationType = SlideOutToCenter;
    _alertView.showAnimationType = SlideInToCenter;
    _alertView.shouldDismissOnTapOutside = YES;
    _alertView.labelTitle.adjustsFontSizeToFitWidth = YES;
    [_alertView.labelTitle sizeToFit];
    
    _alertView.buttonFormatBlock = ^NSDictionary* {
        NSDictionary *format = @{
                                 @"textColor" : [UIColor flatWhiteColor]
                                 };
        return format;
    };
    
    _alertView.completeButtonFormatBlock = ^NSDictionary* {
        NSDictionary *format = @{
                                 @"textColor" : [UIColor flatWhiteColor]
                                 };
        return format;
    };
    return _alertView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
