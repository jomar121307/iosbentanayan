//
//  BentaKeywords.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BentaKeywords : NSObject

// Notifications
extern NSString * const NOTIF_REFRESH_MENU;

// Slide Menu
extern NSString * const SLIDE_MENU_RELOAD_DATA;
extern NSString * const SLIDE_MENU_RELOAD_INDEX;

// Facebook Login
extern NSString * const FBLOGIN_ACCESSTOKEN;
extern NSString * const FBLOGIN_SOCIALNETWORK;
extern NSString * const FBLOGIN_SNTYPE_FB;

// Date Formatter
extern NSString * const BENTA_DATE_FORMAT;
extern NSString * const BENTA_DATE_BIRTHDAY_FORMAT;
extern NSString * const BENTA_DATE_STRING_FORMAT;
extern NSString * const BENTA_DATE_STRING_LONG_FORMAT;
extern NSString * const BENTA_DATE_STRING_TIME_FORMAT;

// Cloudinary
extern NSString * const BENTA_CLOUDINARY_URL;
extern NSString * const BENTA_CLOUDINARY_PROFILE_TYPE;

@end
