//
//  BentaKeywords.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "BentaKeywords.h"

@implementation BentaKeywords

// Notifications
NSString * const NOTIF_REFRESH_MENU = @"refreshMenu";

// Slide Menu
NSString * const SLIDE_MENU_RELOAD_DATA = @"reloadData";
NSString * const SLIDE_MENU_RELOAD_INDEX = @"reloadIndex";

// Facebook Login.
NSString * const FBLOGIN_ACCESSTOKEN = @"accessToken";
NSString * const FBLOGIN_SOCIALNETWORK = @"socialNetwork";
NSString * const FBLOGIN_SNTYPE_FB = @"fb";

// Date Formatter
NSString * const BENTA_DATE_FORMAT = @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";
NSString * const BENTA_DATE_BIRTHDAY_FORMAT = @"yyyy-MM-dd HH:mm:ss VVVV";
NSString * const BENTA_DATE_STRING_FORMAT = @"MMM dd, YYYY";
NSString * const BENTA_DATE_STRING_LONG_FORMAT = @"MMM dd, YYYY hh:mm a";
NSString * const BENTA_DATE_STRING_TIME_FORMAT = @"hh:mm a";

// Cloudinary
NSString * const BENTA_CLOUDINARY_URL = @"cloudinary://549416146747742:HUuX-_WL6D_PUbLdKpnDNyeQ_sM@www-gobookd-com"; //key-secret-cloudname
NSString * const BENTA_CLOUDINARY_PROFILE_TYPE = @"profilephoto";

@end
