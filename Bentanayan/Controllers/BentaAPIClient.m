//
//  BentaAPIClient.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 25/05/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "BentaAPIClient.h"

@implementation BentaAPIClient

+ (instancetype)sharedClient {
    static BentaAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[BentaAPIClient alloc] initWithBaseURL:[NSURL URLWithString:API_BENTANAYAN]];
        _sharedClient.responseSerializer = [AFHTTPResponseSerializer serializer];
        _sharedClient.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/plain", @"application/json", nil];
        [_sharedClient.requestSerializer setValue:@"zcommerce"
                               forHTTPHeaderField:@"x-app"];
    });
    return _sharedClient;
}

@end
