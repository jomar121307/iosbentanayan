//
//  AppDelegate.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "AppDelegate.h"

#import "LoginBarViewController.h"
#import "HomeViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // Reachability
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
    }];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    // Check user is logged in
    [[BESharedManager sharedInstance] isLoggedIn];
    
    // Init Facebook
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    [STPopupNavigationBar appearance].barTintColor = [UIColor bentaGreenColor];
    [STPopupNavigationBar appearance].tintColor = [UIColor flatWhiteColor];
    [STPopupNavigationBar appearance].barStyle = UIBarStyleDefault;
    [STPopupNavigationBar appearance].titleTextAttributes = @{ NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                                               NSForegroundColorAttributeName: [UIColor whiteColor] };
    
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[STPopupNavigationBar class]]] setTitleTextAttributes:@{ NSFontAttributeName:[UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f] } forState:UIControlStateNormal];
     
    //////////////////////////////////////////////////////////
    // Init Slide Navigation Controller
    /////////////////////////////////////////////////////////

    HomeViewController *homeController = [[HomeViewController alloc] init];
    SlideNavigationController *slideNavController = [[SlideNavigationController alloc] initWithRootViewController:homeController];
    
    [SlideNavigationController sharedInstance].navigationBar.tintColor = [UIColor flatWhiteColor];
    [SlideNavigationController sharedInstance].navigationBarHidden = NO;
    [SlideNavigationController sharedInstance].navigationBar.barTintColor = [UIColor bentaGreenColor];
    [SlideNavigationController sharedInstance].navigationBar.translucent = NO;
    SlideMenuViewController *slideMenu = [[SlideMenuViewController alloc] init];
    [SlideNavigationController sharedInstance].leftMenu = slideMenu;
    [SlideNavigationController sharedInstance].menuRevealAnimationDuration = 0.25;
    [SlideNavigationController sharedInstance].enableShadow = 1;
    [SlideNavigationController sharedInstance].menuRevealAnimator = [[SlideNavigationContorllerAnimatorSlideAndFade alloc] initWithMaximumFadeAlpha:0.75 fadeColor:[UIColor blackColor] andSlideMovement:100];
    [SlideNavigationController sharedInstance].enableSwipeGesture = NO;
    
    self.window.rootViewController = slideNavController;

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[NgKeyboardTracker sharedTracker] stop];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
    [[NgKeyboardTracker sharedTracker] start];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

@end
