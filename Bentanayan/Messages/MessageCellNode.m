//
//  MessageCellNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 01/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "MessageCellNode.h"

@implementation MessageCellNode

- (instancetype)initWithMessage:(MessageModel *)message
                       fromUser:(UserModel *)user
{
    self = [super init];
    if(self) {
        
        self.message = message;
        self.user = user;
        
        // Attrs
        NSMutableParagraphStyle *paragraphStyleLeft = [NSMutableParagraphStyle new];
        paragraphStyleLeft.alignment = NSTextAlignmentLeft;
        
        // User Avatar
        _avatarImageNode = [[ASNetworkImageNode alloc] init];
        if(self.user.profilePhoto) {
            ProfilePhotoModel *photo = self.user.profilePhoto;
            if(photo.cropped) {
                _avatarImageNode.URL = [NSURL URLWithString:photo.cropped.secure_url];
            } else if(photo.original) {
                _avatarImageNode.URL = [NSURL URLWithString:photo.original.secure_url];
            } else {
                _avatarImageNode.URL = [NSURL URLWithString:photo.secure_url];
            }
        }
        _avatarImageNode.delegate = self;
        _avatarImageNode.clipsToBounds = YES;
        _avatarImageNode.shouldRasterizeDescendants = YES;
        _avatarImageNode.defaultImage = [UIImage imageNamed:@"avatar-placeholder"];
        _avatarImageNode.backgroundColor = [UIColor clearColor];
        
        [self addSubnode:_avatarImageNode];
        
        // Name Label
        NSDictionary *attrsFromName = @{
                                        NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Bold" size:14.0f],
                                        NSForegroundColorAttributeName: [UIColor flatBlackColorDark],
                                        NSParagraphStyleAttributeName : paragraphStyleLeft
                                        };
        
        NSString *fromName;
        NSString *firstName = self.user.firstName;
        NSString *lastName = self.user.lastName;
        if(lastName != nil) {
            fromName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
        } else {
            fromName = [NSString stringWithFormat:@"%@", firstName];
        }
        NSAttributedString *stringFromName = [[NSAttributedString alloc] initWithString:fromName
                                                                             attributes:attrsFromName];
        
        
        _nameLabelNode = [[ASTextNode alloc] init];
        _nameLabelNode.attributedString = stringFromName;
        _nameLabelNode.maximumNumberOfLines = 1;
        
        [self addSubnode:_nameLabelNode];
        
        // Timestamp Label
        NSDictionary *attrsTimestamp = @{
                                         NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:12.0f],
                                         NSForegroundColorAttributeName: [UIColor flatGrayColorDark],
                                         NSParagraphStyleAttributeName : paragraphStyleLeft
                                         };
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:BENTA_DATE_FORMAT];
        NSDate *date = [formatter dateFromString:self.message.updatedAt];
        
        NSAttributedString *stringTimestamp = [[NSAttributedString alloc] initWithString:[date timeAgoSinceNow]
                                                                              attributes:attrsTimestamp];
        
        
        _timestampLabelNode = [[ASTextNode alloc] init];
        _timestampLabelNode.attributedString = stringTimestamp;
        _timestampLabelNode.maximumNumberOfLines = 1;
        
        [self addSubnode:_timestampLabelNode];
        
        // Message Label
        NSDictionary *attrsMessage = @{
                                       NSFontAttributeName: [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f],
                                       NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                       NSParagraphStyleAttributeName : paragraphStyleLeft
                                       };
        //self.message.payload.message
        NSAttributedString *stringMessage = [[NSAttributedString alloc] initWithString:self.message.payload.message
                                                                            attributes:attrsMessage];
        
        
        _messageLabelNode = [[ASTextNode alloc] init];
        _messageLabelNode.attributedString = stringMessage;
        _messageLabelNode.maximumNumberOfLines = 0;
        
        [self addSubnode:_messageLabelNode];
        
        // Border Node
        _borderNode = [[ASDisplayNode alloc] init];
        _borderNode.backgroundColor = [UIColor flatGrayColor];
        
        [self addSubnode:_borderNode];
    }
    return self;
}

- (void)layoutDidFinish {
    [super layoutDidFinish];
    
    _avatarImageNode.cornerRadius = _avatarImageNode.bounds.size.width / 2;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // Spacer
    ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
    spacer.flexGrow = YES;
    
    // Avatar
    ASRatioLayoutSpec *ratioAvatarImage = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:1.0
                                                                                child:_avatarImageNode];
    ratioAvatarImage.alignSelf = ASStackLayoutAlignSelfCenter;
    
    // iPhone & iPad
    UIEdgeInsets contentInset;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        contentInset = UIEdgeInsetsMake(14, 14, 14, 14);
        ratioAvatarImage.flexBasis = ASRelativeDimensionMakeWithPercent(0.08);
    } else {
        contentInset = UIEdgeInsetsMake(8, 8, 8, 8);
        ratioAvatarImage.flexBasis = ASRelativeDimensionMakeWithPercent(0.14);
    }
    
    // Name Timestamp Stack
    _nameLabelNode.flexGrow = YES;
    _nameLabelNode.flexShrink = YES;
    ASStackLayoutSpec *stackNameTimestamp = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                                    spacing:4.0f
                                                                             justifyContent:ASStackLayoutJustifyContentStart
                                                                                 alignItems:ASStackLayoutAlignItemsCenter
                                                                                   children:@[_nameLabelNode,
                                                                                              _timestampLabelNode]];
    
    // Content
    ASStackLayoutSpec *stackContent = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                              spacing:2.0f
                                                                       justifyContent:ASStackLayoutJustifyContentStart
                                                                           alignItems:ASStackLayoutAlignItemsStretch
                                                                             children:@[stackNameTimestamp,
                                                                                        _messageLabelNode]];
    stackContent.flexShrink = YES;
    stackContent.flexGrow = YES;
    
    // Main
    ratioAvatarImage.alignSelf = ASStackLayoutAlignSelfStart;
    ASStackLayoutSpec *stackMain = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                           spacing:12.0f
                                                                    justifyContent:ASStackLayoutJustifyContentStart
                                                                        alignItems:ASStackLayoutAlignItemsCenter
                                                                          children:@[ratioAvatarImage,
                                                                                     stackContent]];
    
    ASInsetLayoutSpec *insetMain = [ASInsetLayoutSpec insetLayoutSpecWithInsets:contentInset
                                                                          child:stackMain];
    
    // Main with border
    _borderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1.0f);
    _borderNode.alignSelf = ASStackLayoutAlignSelfEnd;
    
    ASStackLayoutSpec *stackMainBorder = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                                 spacing:0.0
                                                                          justifyContent:ASStackLayoutJustifyContentStart
                                                                              alignItems:ASStackLayoutAlignItemsStretch
                                                                                children:@[insetMain, _borderNode]];
    return stackMainBorder;
    
}

#pragma mark - ASNetworkImageNodeDelegate methods.

- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image
{
    //[self setNeedsDisplay];
}

- (void)imageNode:(ASNetworkImageNode *)imageNode didFailWithError:(NSError *)error {
    //[self setNeedsDisplay];
}

@end
