//
//  MessageViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 01/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "MessageViewController.h"

#import "MessageCellNode.h"

@interface MessageViewController ()

@end

@implementation MessageViewController

- (instancetype)initWithRoom:(RoomModel *)room {
    self = [super init];
    if(self) {
        self.room = room;
        NSLog(@"room id: %@", self.room.roomId);
    }
    return self;
}

- (void)loadView {
    [super loadView];
    
    _coordinator = [[NgKeyboardTracker sharedTracker] createPseudoInputAccessoryViewCoordinator];
    [_coordinator setPseudoInputAccessoryViewHeight:44.f];
    
    [[NgKeyboardTracker sharedTracker] addDelegate:self];
}

- (void)dealloc {
    [[NgKeyboardTracker sharedTracker] removeDelegate:self];
}

- (UIView *)inputAccessoryView {
    return _coordinator.pseudoInputAccessoryView;
}

- (void)setupInputView {
    _inputView = [[UIView alloc] init];
    _inputView.backgroundColor = [UIColor colorWithWhite:0.86 alpha:1.0];
    
    [self.view addSubview:_inputView];
    
    _textView = [[UITextView alloc] init];
    _textView.backgroundColor = [UIColor whiteColor];
    _textView.textContainerInset = UIEdgeInsetsMake(4, 4, 4, 4);
    _textView.font = [UIFont systemFontOfSize:14];
    _textView.clipsToBounds = YES;
    _textView.layer.cornerRadius = 4.0f;
    _textView.translatesAutoresizingMaskIntoConstraints = NO;
    [_textView sizeToFit];
    [_textView layoutIfNeeded];
    [_inputView addSubview:_textView];
    
    _sendButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _sendButton.translatesAutoresizingMaskIntoConstraints = NO;
    _sendButton.titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    [_sendButton setTitle:@"Send" forState:UIControlStateNormal];
    [_sendButton setTitleColor:[UIColor bentaGreenColor] forState:UIControlStateNormal];
    [_sendButton addTarget:self
                    action:@selector(sendMessage:)
          forControlEvents:UIControlEventTouchUpInside];
    [_inputView addSubview:_sendButton];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_textView, _sendButton);
    
    [_inputView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_textView]-[_sendButton]-|" options:0 metrics:nil views:views]];
    
    [_inputView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_sendButton]-|" options:0 metrics:nil views:views]];
    [_inputView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_textView]-|" options:0 metrics:nil views:views]];
    
}

- (void)layoutTextView {
    CGRect kbframe = [[NgKeyboardTracker sharedTracker] keyboardCurrentFrameForView:self.view];
    CGSize s = self.view.frame.size;
    
    CGFloat textViewH = _coordinator.pseudoInputAccessoryViewHeight;
    
    CGFloat bottomPadding = -textViewH;
    
    if (!CGRectEqualToRect(CGRectZero, kbframe)) {
        bottomPadding += ( s.height - kbframe.origin.y );
    }
    
    bottomPadding = MAX(0, bottomPadding);
    
    _inputView.frame = (CGRect) {
        0,
        s.height - textViewH - bottomPadding,
        s.width,
        textViewH
    };
    
    UIView * border = [_inputView viewWithTag:1000];
    border.frame = (CGRect) { 0, 0, _inputView.frame.size.width, .6 };
    
    [_coordinator setPseudoInputAccessoryViewHeight:textViewH];
}

- (void)keyboardTrackerDidUpdate:(NgKeyboardTracker *)tracker {
    [self layoutTextView];
}

- (void)keyboardTrackerDidChangeAppearanceState:(NgKeyboardTracker *)tracker {
    if(tracker.appearanceState == NgKeyboardTrackerKeyboardAppearanceStateShown) {
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, tracker.currentFrame.size.height, 0.0);
        self.collectionView.contentInset = contentInsets;
        self.collectionView.scrollIndicatorInsets = contentInsets;
        
        NSInteger section = [self.collectionView numberOfSections] - 1;
        NSInteger item = [self.collectionView numberOfItemsInSection:section] - 1;
        if(item > 0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:section];
            [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:(UICollectionViewScrollPositionBottom) animated:YES];
        }
        return;
    }
    if(tracker.appearanceState == NgKeyboardTrackerKeyboardAppearanceStateWillHide) {
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, 44.0f, 0.0);
        self.collectionView.contentInset = contentInsets;
        self.collectionView.scrollIndicatorInsets = contentInsets;
        
        return;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor flatWhiteColor];
    
    
    NSArray *users = self.room.users;
    NSString *navTitle = @"Messages";
    for(UserModel *userModel in users) {
        if(!userModel.isSelf) {
            self.otherUser = userModel;
            NSString *firstName = userModel.firstName;
            NSString *lastName = userModel.lastName;
            if(lastName != nil) {
                navTitle = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
            } else {
                navTitle = [NSString stringWithFormat:@"%@", firstName];
            }
        } else {
            self.meUser = userModel;
        }
    }
    
    // Navigation
    self.navigationItem.title = navTitle;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Back
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(goBack)];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    // Home
    UIImage *homeImage = [[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    CGRect frameimg = CGRectMake(0, 0, homeImage.size.width, homeImage.size.height);
    UIButton *homeButton = [[UIButton alloc] initWithFrame:frameimg];
    [homeButton setBackgroundImage:homeImage forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(goToHome) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    // CollectionView
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 0.0f;
    layout.minimumLineSpacing = 0.0f;
    
    self.collectionView = [[ASCollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:layout];
    self.collectionView.asyncDataSource = self;
    self.collectionView.asyncDelegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.alwaysBounceVertical = YES;
    
    [self.view addSubview:self.collectionView];
    
    // Setup textfield
    [self setupInputView];
    
    // Initialize Refresh Control
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    
    // Configure Refresh Control
    [refreshControl addTarget:self action:@selector(showOldMessages:) forControlEvents:UIControlEventValueChanged];
    
    // Configure View Controller
    [self.collectionView addSubview:refreshControl];
    
    self.messageResult = [NSMutableArray new];
    
    __weak MessageViewController *weakSelf = self;
    
    [[BESharedManager sharedInstance] be_messagesWithRoomId:weakSelf.room.roomId
                                                       skip:0
                                             withCompletion:^(BOOL finished, RoomModel *room)
     {
         if(finished) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [weakSelf.collectionView performBatchAnimated:NO updates:^{
                     NSMutableArray *insertIndexPaths = [NSMutableArray array];
                     NSMutableArray *deleteIndexPaths = [NSMutableArray array];
                     NSInteger section = 0;
                     
                     NSArray *oldData = [weakSelf.messageResult copy];
                     for(MessageModel *oldModel in oldData) {
                         NSInteger oldItem = [oldData indexOfObject:oldModel];
                         [weakSelf.messageResult removeObject:oldModel];
                         [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
                     }
                     
                     NSUInteger insertIndex = 0;
                     for(MessageModel *newModel in room.messages) {
                         [weakSelf.messageResult insertObject:newModel
                                                      atIndex:0];
                         [insertIndexPaths addObject:[NSIndexPath indexPathForItem:insertIndex inSection:section]];
                         insertIndex++;
                     }
                     
                     [weakSelf.collectionView deleteItemsAtIndexPaths:deleteIndexPaths];
                     [weakSelf.collectionView insertItemsAtIndexPaths:insertIndexPaths];
                 } completion:^(BOOL finished) {
                     if(weakSelf.messageResult.count > 0) {
                         NSIndexPath *lastIndex = [NSIndexPath indexPathForItem:weakSelf.messageResult.count -1
                                                                      inSection:0];
                         [weakSelf.collectionView scrollToItemAtIndexPath:lastIndex
                                                         atScrollPosition:UICollectionViewScrollPositionNone
                                                                 animated:NO];
                     }
                     _messagingTimer = [NSTimer scheduledTimerWithTimeInterval:2.0f
                                                                        target:self
                                                                      selector:@selector(refresh:)
                                                                      userInfo:nil
                                                                       repeats:YES];
                 }];
             });
         }
     }];
    
    //    [self.collectionView addPullToRefreshWithActionHandler:^{
    //        [[BESharedManager sharedInstance] be_messagesWithRoomId:weakSelf.room.roomId
    //                                                           skip:0
    //                                                 withCompletion:^(BOOL finished, RoomModel *room)
    //         {
    //             [weakSelf.collectionView.pullToRefreshView stopAnimating];
    //             if(finished) {
    //                 dispatch_async(dispatch_get_main_queue(), ^{
    //                     [weakSelf.collectionView performBatchUpdates:^{
    //                         NSMutableArray *insertIndexPaths = [NSMutableArray array];
    //                         NSMutableArray *deleteIndexPaths = [NSMutableArray array];
    //                         NSInteger section = 0;
    //
    //                         NSArray *oldData = [weakSelf.messageResult copy];
    //                         for(MessageModel *oldModel in oldData) {
    //                             NSInteger oldItem = [oldData indexOfObject:oldModel];
    //                             [weakSelf.messageResult removeObject:oldModel];
    //                             [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
    //                         }
    //
    //                         for(MessageModel *newModel in room.messages) {
    //                             [weakSelf.messageResult addObject:newModel];
    //                             NSInteger item = [weakSelf.messageResult indexOfObject:newModel];
    //                             [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
    //                         }
    //
    //                         [weakSelf.collectionView deleteItemsAtIndexPaths:[[deleteIndexPaths reverseObjectEnumerator] allObjects]];
    //                         [weakSelf.collectionView insertItemsAtIndexPaths:[[insertIndexPaths reverseObjectEnumerator] allObjects]];
    //                     } completion:nil];
    //                 });
    //             }
    //         }];
    //    }];
    //
    //    [self.collectionView triggerPullToRefresh];
    
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [_messagingTimer invalidate];
    _messagingTimer = nil;
}

- (void)viewWillLayoutSubviews {
    self.collectionView.frame = self.view.bounds;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, 44.0, 0.0);
    self.collectionView.contentInset = contentInsets;
    self.collectionView.scrollIndicatorInsets = contentInsets;
    
    [self layoutTextView];
}

#pragma mark - ASCollectionView data source.

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MessageModel *message = [self.messageResult objectAtIndex:indexPath.row];
    MessageCellNode *cellNode;
    if([message.from isEqualToString:self.meUser.userId]) {
        cellNode = [[MessageCellNode alloc] initWithMessage:message
                                                   fromUser:self.meUser];
    } else {
        cellNode = [[MessageCellNode alloc] initWithMessage:message
                                                   fromUser:self.otherUser];
    }
    
    return cellNode;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = self.collectionView.bounds.size.width;
    
    return ASSizeRangeMake(CGSizeMake(0.0, 0.0), CGSizeMake(width, FLT_MAX));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.messageResult.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionViewLockDataSource:(ASCollectionView *)collectionView
{
    // lock the data source
    // The data source should not be change until it is unlocked.
}

- (void)collectionViewUnlockDataSource:(ASCollectionView *)collectionView
{
    // unlock the data source to enable data source updating.
}

- (void)collectionView:(UICollectionView *)collectionView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    NSLog(@"fetch additional content");
    [context completeBatchFetching:YES];
}

#pragma mark - ASCollectionView delegate.

- (void)scrollViewWillEndDragging:(UIScrollView*)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint*)targetContentOffset
{
    if(velocity.y < - 1.8f) {
        [_textView resignFirstResponder];
    }
}

#pragma mark - Methods / Selectors

- (void)sendMessage:(id)sender {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:BENTA_DATE_FORMAT];
    
    if([_textView.text isEqualToString:@""]) {
        return;
    }
    
    __weak MessageViewController *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.collectionView performBatchUpdates:^{
            
            PayloadModel *payload = [[PayloadModel alloc] init];
            payload.message = weakSelf.textView.text;
            
            MessageModel *message = [[MessageModel alloc] init];
            message.payload = payload;
            message.to = self.otherUser.userId;
            message.from = self.meUser.userId;
            message.createdAt = [dateFormatter stringFromDate:[NSDate new]];
            message.updatedAt = [dateFormatter stringFromDate:[NSDate new]];
            message.roomId = self.room.roomId;
            
            [self.messageResult addObject:message];
            NSInteger index = [self.messageResult indexOfObject:message];
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
            
            [weakSelf.collectionView insertItemsAtIndexPaths:@[indexPath]];
            
            [[BESharedManager sharedInstance] be_sendMessageWithOtherUserId:self.otherUser.userId
                                                                withMessage:[message toDictionaryWithKeys:@[@"roomId", @"payload"]]
                                                             withCompletion:^(BOOL finished, MessageModel *newMessage)
             {
                 if(finished) {
                     [weakSelf.messageResult replaceObjectAtIndex:index withObject:newMessage];
                     [weakSelf.collectionView reloadItemsAtIndexPaths:@[indexPath]];
                 }
             }];
        } completion:^(BOOL finished) {
            if(finished) {
                weakSelf.textView.text = @"";
                NSInteger item = [self.collectionView numberOfItemsInSection:0];
                NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForItem:item - 1 inSection:0];
                [weakSelf.collectionView scrollToItemAtIndexPath:scrollIndexPath
                                                atScrollPosition:UICollectionViewScrollPositionBottom
                                                        animated:YES];
            }
        }];
    });
}

- (void)showOldMessages:(id)sender
{
    NSLog(@"Show old messages");
    
    __weak MessageViewController *weakSelf = self;
    
    [[BESharedManager sharedInstance] be_messagesWithRoomId:weakSelf.room.roomId
                                                       skip:self.messageResult.count
                                             withCompletion:^(BOOL finished, RoomModel *room)
     {
         if(finished) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [weakSelf.collectionView performBatchAnimated:NO updates:^{
                     NSMutableArray *insertIndexPaths = [NSMutableArray array];
                     NSInteger section = 0;
                     NSInteger insertIndex = 0;
                     
                     for(MessageModel *newModel in room.messages) {
                         [weakSelf.messageResult insertObject:newModel
                                                      atIndex:0];
                         [insertIndexPaths addObject:[NSIndexPath indexPathForItem:insertIndex inSection:section]];
                         insertIndex++;
                     }
                     
                     [weakSelf.collectionView insertItemsAtIndexPaths:insertIndexPaths];
                 } completion:^(BOOL finished) {
                     NSLog(@"Count: %ld", weakSelf.messageResult.count);
                     if(weakSelf.messageResult.count > 0) {
                         NSIndexPath *lastIndex = [NSIndexPath indexPathForItem:0
                                                                      inSection:0];
                         [weakSelf.collectionView scrollToItemAtIndexPath:lastIndex
                                                         atScrollPosition:UICollectionViewScrollPositionTop
                                                                 animated:YES];
                     }
                 }];
             });
         }
         [(UIRefreshControl *)sender endRefreshing];
     }];
}

- (void)refresh:(id)sender {
    NSLog(@"Refresh Messages");
    
    __weak MessageViewController *weakSelf = self;
    
    [[BESharedManager sharedInstance] be_messagesWithRoomId:weakSelf.room.roomId
                                                       skip:0
                                             withCompletion:^(BOOL finished, RoomModel *room)
     {
         if(finished) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 __block BOOL hasNewMessage = 0;
                 [weakSelf.collectionView performBatchAnimated:NO updates:^{
                     NSMutableArray *insertIndexPaths = [NSMutableArray array];
                     //NSMutableArray *updateIndexPaths = [NSMutableArray array];
                     NSInteger section = 0;
                     
                     NSMutableArray *newData = [[NSMutableArray alloc] initWithArray:room.messages];
                     NSArray *newDataTemp = [newData copy];
                     NSArray *oldData = [weakSelf.messageResult copy];
                     
                     for(MessageModel *oldModel in oldData) {
                         for(MessageModel *newModel in newDataTemp) {
                             if([oldModel.messageId isEqualToString:newModel.messageId]) {
                                 //NSInteger oldItem = [oldData indexOfObject:oldModel];
                                 //[weakSelf.messageResult replaceObjectAtIndex:oldItem withObject:newModel];
                                 [newData removeObject:newModel];
                                 //[updateIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
                             }
                         }
                     }
                     
                     if(newData.count > 0) {
                         hasNewMessage = 1;
                     }
                     
                     for(MessageModel *newModel in newData) {
                         [weakSelf.messageResult addObject:newModel];
                         NSInteger insertIndex = [weakSelf.messageResult indexOfObject:newModel];
                         [insertIndexPaths addObject:[NSIndexPath indexPathForItem:insertIndex inSection:section]];
                     }
                     
                     //[weakSelf.collectionView reloadItemsAtIndexPaths:updateIndexPaths];
                     [weakSelf.collectionView insertItemsAtIndexPaths:insertIndexPaths];
                 } completion:^(BOOL finished) {
                     if(finished && hasNewMessage) {
                         NSInteger item = [self.collectionView numberOfItemsInSection:0];
                         NSArray *visibleNodes = [weakSelf.collectionView visibleNodes];
                         if(weakSelf.messageResult.count > 1) {
                             ASCellNode *lastCell = [weakSelf.collectionView nodeForItemAtIndexPath:[NSIndexPath indexPathForItem:item - 2
                                                                                                                        inSection:0]];
                             if([visibleNodes containsObject:lastCell]) {
                                 NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForItem:item - 1
                                                                                    inSection:0];
                                 [weakSelf.collectionView scrollToItemAtIndexPath:scrollIndexPath
                                                                 atScrollPosition:UICollectionViewScrollPositionBottom
                                                                         animated:YES];
                                 
                             }
                         }
                     }
                 }];
             });
         }
     }];
}

- (void)goToHome {
    NSLog(@"Home");
    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
}

- (void)goBack {
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:NO];
}

#pragma mark - Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
