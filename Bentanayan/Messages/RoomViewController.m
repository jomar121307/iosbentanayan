//
//  MessagesViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 31/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "RoomViewController.h"

#import "RoomCellNode.h"

#import "MessageViewController.h"

@interface RoomViewController ()

@end

@implementation RoomViewController

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor flatWhiteColor];
    
    // Navigation
    self.navigationItem.title = @"Messages";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Home
    UIImage *homeImage = [[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    CGRect frameimg = CGRectMake(0, 0, homeImage.size.width, homeImage.size.height);
    UIButton *homeButton = [[UIButton alloc] initWithFrame:frameimg];
    [homeButton setBackgroundImage:homeImage forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(goToHome) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    // CollectionView
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 0.0f;
    layout.minimumLineSpacing = 0.0f;
    
    self.collectionView = [[ASCollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:layout];
    self.collectionView.asyncDataSource = self;
    self.collectionView.asyncDelegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.alwaysBounceVertical = YES;
    
    [self.view addSubview:self.collectionView];
    
    self.roomsResult = [NSMutableArray new];
    
    __weak RoomViewController *weakSelf = self;
    
    [self.collectionView addPullToRefreshWithActionHandler:^{
        [[BESharedManager sharedInstance] be_roomsSkip:0
                                        withCompletion:^(BOOL finished, NSArray *result)
         {
             [weakSelf.collectionView.pullToRefreshView stopAnimating];
             if(finished) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [weakSelf.collectionView performBatchUpdates:^{
                         NSMutableArray *insertIndexPaths = [NSMutableArray array];
                         NSMutableArray *deleteIndexPaths = [NSMutableArray array];
                         NSInteger section = 0;
                         
                         NSArray *oldData = [weakSelf.roomsResult copy];
                         for(RoomModel *oldModel in oldData) {
                             NSInteger oldItem = [oldData indexOfObject:oldModel];
                             [weakSelf.roomsResult removeObject:oldModel];
                             [deleteIndexPaths addObject:[NSIndexPath indexPathForItem:oldItem inSection:section]];
                         }
                         
                         for(RoomModel *newModel in result) {
                             [weakSelf.roomsResult addObject:newModel];
                             NSInteger item = [weakSelf.roomsResult indexOfObject:newModel];
                             [insertIndexPaths addObject:[NSIndexPath indexPathForItem:item inSection:section]];
                         }
                         
                         [weakSelf.collectionView deleteItemsAtIndexPaths:deleteIndexPaths];
                         [weakSelf.collectionView insertItemsAtIndexPaths:insertIndexPaths];
                     } completion:nil];
                 });
             }
         }];
    }];
    
    [self.collectionView triggerPullToRefresh];

    
}

- (void)viewWillLayoutSubviews {
    self.collectionView.frame = self.view.bounds;
}

#pragma mark - ASCollectionView data source.

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RoomModel *room = [self.roomsResult objectAtIndex:indexPath.row];
    RoomCellNode *cellNode = [[RoomCellNode alloc] initWithRoom:room];
    return cellNode;
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = self.collectionView.bounds.size.width;
    
    return ASSizeRangeMake(CGSizeMake(0.0, 0.0), CGSizeMake(width, FLT_MAX));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.roomsResult.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionViewLockDataSource:(ASCollectionView *)collectionView
{
    // lock the data source
    // The data source should not be change until it is unlocked.
}

- (void)collectionViewUnlockDataSource:(ASCollectionView *)collectionView
{
    // unlock the data source to enable data source updating.
}

- (void)collectionView:(UICollectionView *)collectionView willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    NSLog(@"fetch additional content");
    [context completeBatchFetching:YES];
}

#pragma mark - ASCollectionView delegate.

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    RoomModel *room = [self.roomsResult objectAtIndex:indexPath.row];
    MessageViewController *messageController = [[MessageViewController alloc] initWithRoom:room];
    [[SlideNavigationController sharedInstance] pushViewController:messageController
                                                          animated:YES];
}

#pragma mark - Methods / Selectors

- (void)goToHome {
    NSLog(@"Home");
    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
}

#pragma mark - Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
