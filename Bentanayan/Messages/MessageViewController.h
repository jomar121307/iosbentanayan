//
//  MessageViewController.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 01/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface MessageViewController : UIViewController <ASCollectionViewDataSource, ASCollectionViewDelegateFlowLayout, SlideNavigationControllerDelegate, NgKeyboardTrackerDelegate> {
    NSTimer *_messagingTimer;
}

@property (strong, nonatomic) ASCollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray *messageResult;

@property (strong, nonatomic) RoomModel *room;

@property (strong, nonatomic) UserModel *meUser;
@property (strong, nonatomic) UserModel *otherUser;

// Input View
@property (nonatomic, strong, readonly) UIView *inputView;
@property (nonatomic, strong, readonly) UITextView *textView;
@property (nonatomic, strong, readonly) UIButton *sendButton;
@property (nonatomic, strong, readonly) NgPseudoInputAccessoryViewCoordinator * coordinator;

- (instancetype)initWithRoom:(RoomModel *)room;

@end
