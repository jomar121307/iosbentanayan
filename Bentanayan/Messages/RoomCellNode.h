//
//  RoomCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 31/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface RoomCellNode : ASCellNode <ASNetworkImageNodeDelegate> {
    ASNetworkImageNode *_avatarImageNode;
    
    ASTextNode *_nameLabelNode;
    ASTextNode *_messageLabelNode;
    ASTextNode *_timestampLabelNode;
    
    ASDisplayNode *_borderNode;
}

@property (strong, nonatomic) RoomModel *room;

- (instancetype)initWithRoom:(RoomModel *)room;

@end
