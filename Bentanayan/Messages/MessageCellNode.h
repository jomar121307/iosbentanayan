//
//  MessageCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 01/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface MessageCellNode : ASCellNode <ASNetworkImageNodeDelegate> {
    ASNetworkImageNode *_avatarImageNode;
    
    ASTextNode *_nameLabelNode;
    ASTextNode *_messageLabelNode;
    ASTextNode *_timestampLabelNode;
    
    ASDisplayNode *_borderNode;
}

@property (strong, nonatomic) MessageModel *message;
@property (strong, nonatomic) UserModel *user;

- (instancetype)initWithMessage:(MessageModel *)message
                       fromUser:(UserModel *)user;

@end
