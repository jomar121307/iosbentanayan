//
//  MenuCellNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 17/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "MenuCellNode.h"

@implementation MenuCellNode

- (instancetype)initWithTitle:(NSString *)title
                    withImage:(UIImage *)image
{
    self = [super init];
    if(self) {
        _title = title;
        _image = image;
        _numOfItems = nil;
        [self setup];
    }
    return self;
}

- (instancetype)initWithTitle:(NSString *)title
                    withImage:(UIImage *)image
           withNumOfCartItems:(NSString *)numOfItems
{
    self = [super init];
    if(self) {
        _title = title;
        _image = image;
        _numOfItems = numOfItems;
        [self setup];
    }
    return self;
}

- (void)setup {
    // Menu Image
    self.menuImageNode = [[ASImageNode alloc] init];
    self.menuImageNode.image = _image;
    self.menuImageNode.preferredFrameSize = CGSizeMake(30, 24);
    self.menuImageNode.contentMode = UIViewContentModeScaleAspectFit;
    self.menuImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor flatBlackColor]);
    
    [self addSubnode:self.menuImageNode];
    
    // Menu Label
    NSMutableParagraphStyle *paragraphStyleMenu = [NSMutableParagraphStyle new];
    paragraphStyleMenu.alignment = NSTextAlignmentLeft;
    
    NSDictionary *attrsMenu = @{
                                NSFontAttributeName: [UIFont boldSystemFontOfSize:14.0f],
                                NSForegroundColorAttributeName: [UIColor flatBlackColor],
                                NSParagraphStyleAttributeName : paragraphStyleMenu
                                };
    
    // Username
    NSAttributedString *stringMenu = [[NSAttributedString alloc] initWithString:_title
                                                                     attributes:attrsMenu];
    
    self.menuLabelNode = [[ASTextNode alloc] init];
    self.menuLabelNode.attributedString = stringMenu;
    
    [self addSubnode:self.menuLabelNode];
    
    // Cart Items
    if(_numOfItems) {
        self.menuItemsLabelNode = [[ASButtonNode alloc] init];
        [self.menuItemsLabelNode setTitle:self.numOfItems
                                 withFont:[UIFont boldSystemFontOfSize:14.0f]
                                withColor:[UIColor whiteColor]
                                 forState:ASControlStateNormal];
        self.menuItemsLabelNode.titleNode.maximumNumberOfLines = 1;
        self.menuItemsLabelNode.contentEdgeInsets = UIEdgeInsetsMake(2, 4, 2, 4);
        self.menuItemsLabelNode.backgroundColor = [UIColor flatRedColor];
        self.menuItemsLabelNode.cornerRadius = 4.0f;
        [self addSubnode:self.menuItemsLabelNode];
    }
}

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    if(highlighted) {
        self.backgroundColor = [UIColor colorWithHexString:@"c4e0cd" withAlpha:1.0];
    } else {
        self.backgroundColor = [UIColor clearColor];
    }
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    NSMutableParagraphStyle *paragraphStyleMenu = [NSMutableParagraphStyle new];
    paragraphStyleMenu.alignment = NSTextAlignmentLeft;
    NSDictionary *attrsMenuLabel;
    NSAttributedString *stringMenu;
    
    if(selected) {
        attrsMenuLabel = @{
                           NSFontAttributeName: [UIFont boldSystemFontOfSize:14.0f],
                           NSForegroundColorAttributeName: [UIColor bentaGreenColor],
                           NSParagraphStyleAttributeName : paragraphStyleMenu
                           };
        
        stringMenu = [[NSAttributedString alloc] initWithString:self.menuLabelNode.attributedString.string
                                                     attributes:attrsMenuLabel];
        
        self.menuImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor bentaGreenColor]);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.menuImageNode setNeedsDisplayWithCompletion:nil];
        });
        self.backgroundColor = [UIColor colorWithHexString:@"c4e0cd" withAlpha:1.0];
        self.menuLabelNode.attributedString = stringMenu;
    } else {
        attrsMenuLabel = @{
                           NSFontAttributeName: [UIFont boldSystemFontOfSize:14.0f],
                           NSForegroundColorAttributeName: [UIColor flatBlackColor],
                           NSParagraphStyleAttributeName : paragraphStyleMenu
                           };
        
        stringMenu = [[NSAttributedString alloc] initWithString:self.menuLabelNode.attributedString.string
                                                     attributes:attrsMenuLabel];
        
        self.menuImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock([UIColor flatBlackColor]);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.menuImageNode setNeedsDisplayWithCompletion:nil];
        });
        self.backgroundColor = [UIColor clearColor];
        self.menuLabelNode.attributedString = stringMenu;
    }
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    NSMutableArray *arrayContent = [NSMutableArray new];
    [arrayContent addObject:self.menuImageNode];
    [arrayContent addObject:self.menuLabelNode];
    
    if(_numOfItems) {
        ASLayoutSpec *spacer = [[ASLayoutSpec alloc] init];
        spacer.flexGrow = YES;
        
        self.menuItemsLabelNode.flexShrink = YES;
        [arrayContent addObject:spacer];
        [arrayContent addObject:self.menuItemsLabelNode];
    }
    
    ASStackLayoutSpec *stackMain = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                           spacing:20.0f
                                                                    justifyContent:ASStackLayoutJustifyContentStart
                                                                        alignItems:ASStackLayoutAlignItemsCenter
                                                                          children:arrayContent];
    
    return [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(8.0f, 16.0f, 8.0f, 16.0f)
                                                  child:stackMain];
}

@end
