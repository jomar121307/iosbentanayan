//
//  MenuHeaderCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 17/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface MenuHeaderCellNode : ASCellNode <ASNetworkImageNodeDelegate> {
    ASDisplayNode *_borderNode;
    ASImageNode *_backgroundNode;
}

@property (strong, nonatomic) UserModel *user;

@property (strong, nonatomic) ASNetworkImageNode *profilePhotoImageNode;
@property (strong, nonatomic) ASTextNode *nameLabelNode;
@property (strong, nonatomic) ASTextNode *emailLabelNode;

@end
