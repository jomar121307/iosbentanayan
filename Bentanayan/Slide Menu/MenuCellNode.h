//
//  MenuCellNode.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 17/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface MenuCellNode : ASCellNode {
    NSString *_title;
    UIImage *_image;
}

@property (strong, nonatomic) NSString *numOfItems;

@property (strong, nonatomic) ASImageNode *menuImageNode;
@property (strong, nonatomic) ASTextNode *menuLabelNode;
@property (strong, nonatomic) ASButtonNode *menuItemsLabelNode;

- (instancetype)initWithTitle:(NSString *)title
                    withImage:(UIImage *)image;

- (instancetype)initWithTitle:(NSString *)title
                    withImage:(UIImage *)image
           withNumOfCartItems:(NSString *)numOfItems;

@end
