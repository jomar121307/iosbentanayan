//
//  MenuHeaderCellNode.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 17/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "MenuHeaderCellNode.h"

@implementation MenuHeaderCellNode

- (id)init {
    self = [super init];
    if(self) {
        self.user = [[BESharedManager sharedInstance] login].session.userId;
        NSLog(@"User Profile: %@", [self.user toDictionary]);
        
        self.backgroundColor = [UIColor bentaGreenColor];
        
        _backgroundNode = [[ASImageNode alloc] init];
        _backgroundNode.image = [UIImage imageNamed:@"header-bg"];
        _backgroundNode.contentMode = UIViewContentModeScaleToFill;
        _backgroundNode.backgroundColor = [UIColor bentaGreenColor];
        
        [self addSubnode:_backgroundNode];
        
        // Avatar Image
        self.profilePhotoImageNode = [[ASNetworkImageNode alloc] init];
        if(self.user.profilePhoto) {
            ProfilePhotoModel *photo = self.user.profilePhoto;
            if(photo.cropped) {
                self.profilePhotoImageNode.URL = [NSURL URLWithString:photo.cropped.secure_url];
            } else if(photo.original) {
                self.profilePhotoImageNode.URL = [NSURL URLWithString:photo.original.secure_url];
            } else {
                self.profilePhotoImageNode.URL = [NSURL URLWithString:photo.secure_url];
            }
        }
        self.profilePhotoImageNode.delegate = self;
        self.profilePhotoImageNode.cornerRadius = 40.0f;
        self.profilePhotoImageNode.shouldRasterizeDescendants = YES;
        self.profilePhotoImageNode.defaultImage = [UIImage imageNamed:@"avatar-placeholder"];
        self.profilePhotoImageNode.backgroundColor = [UIColor clearColor];
        self.profilePhotoImageNode.imageModificationBlock = ^UIImage *(UIImage *image) {
            UIImage *modifiedImage;
            CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
            UIGraphicsBeginImageContext(rect.size);
            [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:80.0f] addClip];
            [image drawInRect:rect];
            modifiedImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return modifiedImage;
        };
        
        [self addSubnode:self.profilePhotoImageNode];
        
        // Menu Label
        NSMutableParagraphStyle *paragraphStyleLabel = [NSMutableParagraphStyle new];
        paragraphStyleLabel.alignment = NSTextAlignmentLeft;
        
        NSDictionary *attrsNameLabel = @{
                                         NSFontAttributeName: [UIFont boldSystemFontOfSize:14.0f],
                                         NSForegroundColorAttributeName: [UIColor whiteColor],
                                         NSParagraphStyleAttributeName : paragraphStyleLabel
                                         };
        // Username
        NSString *welcomeString;
        
        if(self.user) {
            NSString *firstName = self.user.firstName;
            NSString *lastName = self.user.lastName;
            if(lastName != nil) {
                welcomeString = [NSString stringWithFormat:@"Welcome, %@ %@", firstName, lastName];
            } else {
                welcomeString = [NSString stringWithFormat:@"Welcome, %@", firstName];
            }
            
            NSString *email;
            if(self.user.email) {
                email = self.user.email;
                NSDictionary *attrsEmailLabel = @{
                                                  NSFontAttributeName: [UIFont systemFontOfSize:14.0f],
                                                  NSForegroundColorAttributeName: [UIColor flatWhiteColor],
                                                  NSParagraphStyleAttributeName : paragraphStyleLabel
                                                  };
                NSAttributedString *emailLabel = [[NSAttributedString alloc] initWithString:email
                                                                                 attributes:attrsEmailLabel];
                
                self.emailLabelNode = [[ASTextNode alloc] init];
                self.emailLabelNode.attributedText = emailLabel;
                self.emailLabelNode.maximumNumberOfLines = 2;
                self.emailLabelNode.pointSizeScaleFactors = @[@1.0, @0.9, @0.8];
                
                [self addSubnode:self.emailLabelNode];
            }
        } else {
            welcomeString = @"Welcome, Guest";
        }
        
        NSAttributedString *nameLabel = [[NSAttributedString alloc] initWithString:welcomeString
                                                                        attributes:attrsNameLabel];
        self.nameLabelNode = [[ASTextNode alloc] init];
        self.nameLabelNode.attributedText = nameLabel;
        self.nameLabelNode.maximumNumberOfLines = 2;
        self.nameLabelNode.pointSizeScaleFactors = @[@1.0, @0.9, @0.8];
        [self addSubnode:self.nameLabelNode];
        
        // Border Node
        _borderNode = [[ASDisplayNode alloc] init];
        _borderNode.backgroundColor = [UIColor flatBlackColor];
        [self addSubnode:_borderNode];
    }
    return self;
}

- (ASLayoutSpec *)layoutSpecThatFits:(ASSizeRange)constrainedSize {
    // Avatar
    ASRatioLayoutSpec *ratioAvatar = [ASRatioLayoutSpec ratioLayoutSpecWithRatio:1.0
                                                                           child:self.profilePhotoImageNode];
    ratioAvatar.flexBasis = ASRelativeDimensionMakeWithPoints(80.0f);
    ratioAvatar.spacingAfter = 8.0f;
    ratioAvatar.alignSelf = ASStackLayoutAlignSelfStart;
    
    // Name and Email
    NSMutableArray *arrayContent = [[NSMutableArray alloc] init];
    [arrayContent addObject:ratioAvatar];
    self.nameLabelNode.flexShrink = YES;
    self.nameLabelNode.flexGrow = YES;
    [arrayContent addObject:self.nameLabelNode];
    if(self.user.email) {
        self.emailLabelNode.flexShrink = YES;
        self.emailLabelNode.flexGrow = YES;
        [arrayContent addObject:self.emailLabelNode];
    }
    
    ASStackLayoutSpec *stackContent = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                              spacing:0.0f
                                                                       justifyContent:ASStackLayoutJustifyContentStart
                                                                           alignItems:ASStackLayoutAlignItemsStretch
                                                                             children:arrayContent];
    
    ASInsetLayoutSpec *insetContent = [ASInsetLayoutSpec insetLayoutSpecWithInsets:UIEdgeInsetsMake(26.0f, 12.0f, 12.0f, 12.0f)
                                                                             child:stackContent];
    insetContent.flexBasis = ASRelativeDimensionMakeWithPercent(1);
    
    // Main Stack
    ASStackLayoutSpec *stackMain = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionHorizontal
                                                                           spacing:0.0f
                                                                    justifyContent:ASStackLayoutJustifyContentStart
                                                                        alignItems:ASStackLayoutAlignItemsStretch
                                                                          children:@[insetContent]];
    
    // Main Stack with Border
    _borderNode.preferredFrameSize = CGSizeMake(constrainedSize.max.width, 1);
    _borderNode.alignSelf = ASStackLayoutAlignSelfEnd;
    ASStackLayoutSpec *stackMainBorder = [ASStackLayoutSpec stackLayoutSpecWithDirection:ASStackLayoutDirectionVertical
                                                                                 spacing:0.0f
                                                                          justifyContent:ASStackLayoutJustifyContentStart
                                                                              alignItems:ASStackLayoutAlignItemsStretch
                                                                                children:@[stackMain,
                                                                                           _borderNode]];
    
    ASBackgroundLayoutSpec *backgroundMain = [ASBackgroundLayoutSpec backgroundLayoutSpecWithChild:stackMainBorder
                                                                                        background:_backgroundNode];
    
    return backgroundMain;
}

#pragma mark - ASNetworkImageNodeDelegate methods.

- (void)imageNode:(ASNetworkImageNode *)imageNode didLoadImage:(UIImage *)image
{
    //[self setNeedsLayout];
}

- (void)imageNode:(ASNetworkImageNode *)imageNode didFailWithError:(NSError *)error {
    //[self setNeedsLayout];
}

@end
