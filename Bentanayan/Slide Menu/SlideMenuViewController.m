//
//  SlideMenuViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "SlideMenuViewController.h"

#import "MenuCellNode.h"

#import "ShopBarViewController.h"
#import "CartShopViewController.h"
#import "PurchaseHistoryViewController.h"
#import "RoomViewController.h"
#import "AccountSettingsViewController.h"
#import "LoginBarViewController.h"

@interface SlideMenuViewController ()

@end

@implementation SlideMenuViewController

- (instancetype)init {
    self = [super init];
    if(self) {
        [self checkUserLogin];
        _requestBusy = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor flatWhiteColor];
    
    // Refresh Menu Notification
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NOTIF_REFRESH_MENU
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMenu:)
                                                 name:NOTIF_REFRESH_MENU
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:SlideNavigationControllerDidOpen
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(menuDidOpen)
                                                 name:SlideNavigationControllerDidOpen
                                               object:nil];
    
    // CollectionView
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 2.0f;
    
    self.collectionView = [[ASCollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 60.0f, self.view.frame.size.height)
                                             collectionViewLayout:layout];
    self.collectionView.asyncDataSource = self;
    self.collectionView.asyncDelegate = self;
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.backgroundColor = [UIColor flatWhiteColor];
    
    [self.collectionView registerSupplementaryNodeOfKind:UICollectionElementKindSectionHeader];
    
    [self.view addSubview:self.collectionView];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillLayoutSubviews
{
    self.collectionView.frame = CGRectMake(0, 0, self.view.frame.size.width - 60.0f, self.view.frame.size.height);
}

#pragma mark - ASTableView data source.

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _menuArray.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCellNode *cellNode;
    if(indexPath.row == 1) {
        cellNode = [[MenuCellNode alloc] initWithTitle:[_menuArray objectAtIndex:indexPath.row]
                                             withImage:[UIImage imageNamed:[_imageArray objectAtIndex:indexPath.row]]
                                    withNumOfCartItems:@"0"];
    } else {
        cellNode = [[MenuCellNode alloc] initWithTitle:[_menuArray objectAtIndex:indexPath.row]
                                             withImage:[UIImage imageNamed:[_imageArray objectAtIndex:indexPath.row]]];
    }
    return cellNode;
}

- (ASCellNode *)collectionView:(ASCollectionView *)collectionView nodeForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    return _headerCellNode;
}

- (CGSize)collectionView:(ASCollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if(section == 0) {
        _headerCellNode = [[MenuHeaderCellNode alloc] init];
        _headerCellHeight = [_headerCellNode measure:CGSizeMake(collectionView.bounds.size.width, CGFLOAT_MAX)].height;
        return CGSizeMake(collectionView.bounds.size.width, _headerCellHeight);
    } else {
        return CGSizeZero;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row != 0) {
        _lastSelectedIndex = indexPath;
    }
    if([BESharedManager sharedInstance].login) {
        switch (indexPath.row) {
            case 0:
                // My Shop
                [self gotoMyShop];
                break;
            case 1:
                // Cart
                [self gotoCart];
                break;
            case 2:
                // Purchase History
                [self gotoHistory];
                break;
            case 3:
                // Direct Feedback
                [self gotoFeedback];
                break;
            case 4:
                // Account Settings
                [self gotoAccountSettings];
                break;
            case 5:
                // Logout
                [self logout];
                break;
            default:
                break;
        }
    } else {
        switch (indexPath.row) {
            case 0:
                // Login
                [self gotoLogin];
                [self.collectionView deselectItemAtIndexPath:indexPath animated:NO];
                break;
            default:
                break;
        }
    }
}

- (ASSizeRange)collectionView:(ASCollectionView *)collectionView constrainedSizeForNodeAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = collectionView.bounds.size.width;
    return ASSizeRangeMake(CGSizeMake(width, 0), CGSizeMake(width, 100.0f));
}

#pragma mark - Method / Selectors

- (void)menuDidOpen {
    if(!_requestBusy && [BESharedManager sharedInstance].login) {
        _requestBusy = 1;
        [[BESharedManager sharedInstance] be_numberOfItemInCartWithCompletion:^(BOOL finished, NSString *numOfItems, NSString *error)
         {
             if(finished) {
                 MenuCellNode *cellNode = (MenuCellNode *)[self.collectionView nodeForItemAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0]];
                 cellNode.numOfItems = numOfItems;
                 [cellNode.menuItemsLabelNode setTitle:numOfItems
                                              withFont:[UIFont boldSystemFontOfSize:14.0f]
                                             withColor:[UIColor whiteColor]
                                              forState:ASControlStateNormal];
                 [cellNode setNeedsLayout];
             }
             _requestBusy = 0;
         }];
    }
}

- (void)refreshMenu:(NSNotification *)notification {
    [self checkUserLogin];
    [self.collectionView reloadData];
}

- (void)checkUserLogin {
    if([BESharedManager sharedInstance].login) {
        _menuArray = @[@"My Shop",
                       @"Cart",
                       @"Purchased Item",
                       @"Messages",
                       @"Account Settings",
                       @"Logout"];
        
        _imageArray = @[@"shop",
                        @"cart",
                        @"history",
                        @"feedback",
                        @"settings",
                        @"logout"];
    } else {
        _menuArray = @[@"Sign in or Register"];
        
        _imageArray = @[@"login"];
    }
}

- (void)gotoMyShop {
    ShopBarViewController *shopController = [[ShopBarViewController alloc] initWithShopOwner:[BESharedManager sharedInstance].login.session.userId];
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:shopController
                                                             withSlideOutAnimation:NO
                                                                     andCompletion:nil];
}

- (void)gotoCart {
    CartShopViewController *cartShopController = [[CartShopViewController alloc] init];
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:cartShopController
                                                             withSlideOutAnimation:NO
                                                                     andCompletion:nil];
}

- (void)gotoHistory {
    PurchaseHistoryViewController *purchaseHistoryController = [[PurchaseHistoryViewController alloc] init];
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:purchaseHistoryController
                                                             withSlideOutAnimation:NO
                                                                     andCompletion:nil];
}

- (void)gotoFeedback {
    RoomViewController *messagesController = [[RoomViewController alloc] init];
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:messagesController
                                                             withSlideOutAnimation:NO
                                                                     andCompletion:nil];
}

- (void)gotoAccountSettings {
    AccountSettingsViewController *accountSettingsController = [[AccountSettingsViewController alloc] init];
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:accountSettingsController
                                                             withSlideOutAnimation:NO
                                                                     andCompletion:nil];
}

- (void)logout {
    NSLog(@"Logout");
    [[BESharedManager sharedInstance] be_logout];
    [self checkUserLogin];
    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
    [self.collectionView reloadData];
}

- (void)gotoLogin {
    LoginBarViewController *loginController = [[LoginBarViewController alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:loginController];
    [[SlideNavigationController sharedInstance] presentViewController:navController animated:YES completion:^{
        [[SlideNavigationController sharedInstance] closeMenuWithCompletion:nil];
    }];
}

@end
