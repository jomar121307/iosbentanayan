//
//  SlideMenuViewController.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MenuHeaderCellNode.h"

@interface SlideMenuViewController : UIViewController <ASCollectionViewDataSource, ASCollectionViewDelegateFlowLayout> {
    NSArray *_menuArray;
    NSArray *_imageArray;
    NSIndexPath *_lastSelectedIndex;
    BOOL _requestBusy;
    CGFloat _headerCellHeight;
    MenuHeaderCellNode *_headerCellNode;
}

@property (strong, nonatomic) ASCollectionView *collectionView;

@end
