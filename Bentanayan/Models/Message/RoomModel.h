//
//  RoomModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class UserModel;
@class MessageModel;

@protocol UserModel
@end

@protocol MessageModel
@end

@interface RoomModel : JSONModel

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSArray<UserModel> *users;
@property (assign, nonatomic) int messageCount;
@property (assign, nonatomic) int unreadCount;
@property (strong, nonatomic) NSArray<MessageModel> *messages;
@property (strong, nonatomic) NSString *roomId;
@property (strong, nonatomic) NSString *updatedAt;
@property (strong, nonatomic) NSString *createdAt;

@end
