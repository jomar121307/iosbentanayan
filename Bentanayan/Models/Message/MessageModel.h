//
//  MessageModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface PayloadModel : JSONModel

@property (strong, nonatomic) NSString *message;

@end

@interface MessageModel : JSONModel

@property (strong, nonatomic) NSString *roomId;
@property (strong, nonatomic) NSString *to;
@property (strong, nonatomic) NSString *from;
@property (strong, nonatomic) PayloadModel *payload;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *createdAt;
@property (strong, nonatomic) NSString *updatedAt;
@property (strong, nonatomic) NSString *messageId;

@end
