//
//  PhotoModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 10/05/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface PhotoTypeModel : JSONModel

@property (strong, nonatomic) NSString *secure_url;
@property (assign, nonatomic) float width;
@property (assign, nonatomic) float height;
@property (strong, nonatomic) NSString *resource_type;

@end
