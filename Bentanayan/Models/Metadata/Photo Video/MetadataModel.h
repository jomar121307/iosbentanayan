//
//  MetadataModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 20/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class MediaTypeModel;

@class PhotoModel;
@class ProductPhotoModel;
@class ShippingDataModel;

@protocol ProductPhotoModel
@end

@protocol ShippingDataModel
@end

@interface MetadataModel : JSONModel

// Media
@property (strong, nonatomic) MediaTypeModel<Optional> *media;

// Photos
@property (strong, nonatomic) PhotoModel<Optional> *pagePhoto;
@property (strong, nonatomic) PhotoModel<Optional> *coverPhoto;

// Ecommerce
@property (strong, nonatomic) NSString<Optional> *status;
@property (strong, nonatomic) NSArray<ProductPhotoModel> *photos;
@property (strong, nonatomic) NSArray<ShippingDataModel> *shippingData;

@end
