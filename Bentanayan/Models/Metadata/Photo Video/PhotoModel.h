//
//  PhotoModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 10/05/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class PhotoTypeModel;

@interface PhotoModel : JSONModel

@property (strong, nonatomic) PhotoTypeModel<Optional> *original;
@property (strong, nonatomic) PhotoTypeModel<Optional> *cropped;

@end
