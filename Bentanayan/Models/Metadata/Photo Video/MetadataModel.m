//
//  MetadataModel.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 20/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "MetadataModel.h"

@implementation MetadataModel

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    if([propertyName isEqualToString:@"photos"]) {
        return YES;
    } else if([propertyName isEqualToString:@"shippingData"]) {
        return YES;
    } else {
        return NO;
    }
}

@end
