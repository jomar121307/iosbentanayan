//
//  ShippingDataModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface ShippingDataModel : JSONModel

@property (strong, nonatomic) NSString *location;
@property (assign, nonatomic) float singleCost;

@end
