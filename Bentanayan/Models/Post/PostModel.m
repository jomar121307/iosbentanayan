//
//  PostModel.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "PostModel.h"

@implementation PostModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id" : @"postId"
                                                       }];
}

+(BOOL)propertyIsOptional:(NSString *)propertyName {
    if([propertyName isEqualToString:@"isLiked"]) {
        return YES;
    } else if([propertyName isEqualToString:@"comments"]) {
        return YES;
    } else {
        return NO;
    }
}

@end
