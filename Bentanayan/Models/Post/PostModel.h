//
//  PostModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class MetadataModel;
@class UserModel;
@class PageModel;
@class CommentModel;

@protocol CommentModel
@end

@interface PostModel : JSONModel

@property (strong, nonatomic) UserModel *userId;
@property (strong, nonatomic) PageModel<Optional> *pageId;
@property (strong, nonatomic) NSString *content;
@property (strong, nonatomic) MetadataModel<Optional> *metadata;
@property (assign, nonatomic) int commentCount;
@property (assign, nonatomic) int likeCount;
@property (strong, nonatomic) NSString *createdAt;
@property (strong, nonatomic) NSString *postId;
@property (assign, nonatomic) BOOL isLiked;
@property (strong, nonatomic) NSArray<CommentModel> *comments;

@end
