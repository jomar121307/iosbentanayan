//
//  CommentModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class UserModel;

@interface CommentModel : JSONModel

@property (strong, nonatomic) UserModel *userId;
@property (strong, nonatomic) NSString *postId;
@property (strong, nonatomic) NSString *content;
@property (assign, nonatomic) int likeCount;
@property (assign, nonatomic) int score;
@property (assign, nonatomic) int replyCount;
@property (assign, nonatomic) BOOL isReply;
@property (strong, nonatomic) NSString *createdAt;
@property (strong, nonatomic) NSString *commentId;

@property (assign, nonatomic) BOOL isSuccess;

@end
