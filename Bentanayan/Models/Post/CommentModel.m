//
//  CommentModel.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "CommentModel.h"

@implementation CommentModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id" : @"commentId"
                                                       }];
}

+(BOOL)propertyIsOptional:(NSString *)propertyName {
    if([propertyName isEqualToString:@"score"]) {
        return YES;
    } else if([propertyName isEqualToString:@"isSuccess"]) {
        return YES;
    } else {
        return NO;
    }
}

@end
