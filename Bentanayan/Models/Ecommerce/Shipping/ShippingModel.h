//
//  ShippingModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class UserModel;
@class ShippingAddressModel;

@interface ShippingModel : JSONModel

@property (strong, nonatomic) ShippingAddressModel *address;
@property (strong, nonatomic) NSString *fullName;
@property (strong, nonatomic) NSString *shippingId;

@end
