//
//  SubcategoryModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface GroupHeaderModel : JSONModel

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *slug;

@end

@protocol GrouplingModel
@end

@interface SubcategoryModel : JSONModel

@property (strong, nonatomic) GroupHeaderModel *groupheader;
@property (strong, nonatomic) NSArray<GrouplingModel> *groupling;

@end

