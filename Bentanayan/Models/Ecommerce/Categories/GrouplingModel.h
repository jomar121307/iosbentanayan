//
//  GrouplingModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 26/05/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface GrouplingModel : JSONModel

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *slug;

@end
