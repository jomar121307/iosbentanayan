//
//  CategoryModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class SubcategoryModel;

@protocol SubcategoryModel
@end

@interface CategoryModel : JSONModel

@property (strong, nonatomic) NSString<Optional> *thumbnail;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *slug;
@property (strong, nonatomic) NSArray<SubcategoryModel> *subcategories;

@end
