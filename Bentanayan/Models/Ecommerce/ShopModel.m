//
//  ShopModel.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ShopModel.h"

@implementation ShopMetadataModel

+(BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

@end

@implementation ShopModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id" : @"shopId"
                                                       }];
}


@end