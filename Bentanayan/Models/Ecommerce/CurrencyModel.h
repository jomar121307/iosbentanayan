//
//  CurrencyModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface CurrencyModel : JSONModel

@property (strong, nonatomic) NSString *code;
@property (strong, nonatomic) NSString *currency;
@property (strong, nonatomic) NSString *symbol;

@end
