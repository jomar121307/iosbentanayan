//
//  ProductModel.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ProductModel.h"

@implementation ProductModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"description" : @"productDescription",
                                                       @"id" : @"productId"
                                                       }];
}

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    if([propertyName isEqualToString:@"reviews"]) {
        return YES;
    } else {
        return NO;
    }
}

@end
