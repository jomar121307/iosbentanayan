//
//  ProductModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class UserModel;
@class ShopModel;
@class CurrencyModel;
@class MetadataModel;
@class ProductReviewModel;

@protocol ProductReviewModel
@end

@interface ProductModel : JSONModel

@property (strong, nonatomic) UserModel<Optional> *userId;
@property (strong, nonatomic) ShopModel<Optional> *shopId;
@property (strong, nonatomic) CurrencyModel<Optional> *currencyId;
@property (strong, nonatomic) NSString *brand;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *productDescription;
@property (strong, nonatomic) NSString *category;
@property (assign, nonatomic) float quantity;
@property (assign, nonatomic) float price;
@property (strong, nonatomic) MetadataModel *metadata;
@property (assign, nonatomic) float reviewScore;
@property (assign, nonatomic) int reviewCount;
@property (strong, nonatomic) NSString *createdAt;
@property (strong, nonatomic) NSString *productId;
@property (strong, nonatomic) NSArray<ProductReviewModel> *reviews;

@end