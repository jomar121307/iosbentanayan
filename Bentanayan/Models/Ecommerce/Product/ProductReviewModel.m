//
//  ProductReviewModel.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ProductReviewModel.h"

@implementation ProductReviewModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id" : @"reviewId"
                                                       }];
}

- (void)setMetadata:(NSDictionary *)metadata {
    self.likeCount = [[metadata valueForKey:@"likeCount"] intValue];
}

+(BOOL)propertyIsIgnored:(NSString *)propertyName {
    if([propertyName isEqualToString:@"likeCount"]) {
        return YES;
    } else {
        return NO;
    }
}

@end
