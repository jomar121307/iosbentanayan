//
//  ProductReviewModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class UserModel;

@interface ProductReviewModel : JSONModel

@property (strong, nonatomic) NSString *productId;
@property (strong, nonatomic) NSString *content;
@property (assign, nonatomic) float rating;
@property (strong, nonatomic) UserModel *userId;
@property (assign, nonatomic) BOOL isReply;
@property (strong, nonatomic) NSString *createdAt;
@property (strong, nonatomic) NSString *reviewId;
@property (assign, nonatomic) int likeCount;

@end