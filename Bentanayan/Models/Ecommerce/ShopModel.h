//
//  ShopModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface ShopMetadataModel : JSONModel

@property (assign, nonatomic) int productCount;
@property (assign, nonatomic) int feedbackScore;

@end

@interface ShopModel : JSONModel

@property (strong, nonatomic) UserModel<Optional> *userId;
@property (strong, nonatomic) ShopMetadataModel<Optional> *metadata;
@property (strong, nonatomic) NSString *createdAt;
@property (strong, nonatomic) NSString *shopId;

@end
