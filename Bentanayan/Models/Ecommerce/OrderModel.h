//
//  OrderModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class UserModel;
@class ShippingModel;
@class ShopModel;
@class CartItemModel;

@interface OrderModel : JSONModel

@property (strong, nonatomic) UserModel *userId;
@property (strong, nonatomic) ShopModel *shopId;
@property (strong, nonatomic) NSArray<CartItemModel> *items;
@property (strong, nonatomic) ShippingModel *shippingId;
@property (strong, nonatomic) NSString *payKey;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *createdAt;
@property (strong, nonatomic) NSString *orderId;

@end
