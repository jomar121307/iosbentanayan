//
//  CartShopModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class UserModel;
@class CartItemModel;
@class ShopModel;

@interface CartShopModel : JSONModel

@property (strong, nonatomic) UserModel *ownerId;
@property (strong, nonatomic) NSArray<CartItemModel> *cartItems;
@property (strong, nonatomic) NSString *shopId;
@property (assign, nonatomic) float totalPrice;
@property (assign, nonatomic) float shippingCost;

@end