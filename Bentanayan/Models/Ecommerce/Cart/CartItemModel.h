//
//  CartItemModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class UserModel;
@class ShopModel;
@class ProductModel;

@protocol CartItemModel
@end

@interface CartItemModel : JSONModel

@property (strong, nonatomic) UserModel *userId;
@property (strong, nonatomic) ShopModel *shopId;
@property (strong, nonatomic) ProductModel *productId;
@property (assign, nonatomic) float quantity;
@property (strong, nonatomic) NSString *createdAt;
@property (strong, nonatomic) NSString *cartItemId;

@property (strong, nonatomic) NSString<Optional> *status;
@property (assign, nonatomic) float shippingCost;

@end