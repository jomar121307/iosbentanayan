//
//  CartItemModel.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "CartItemModel.h"

@implementation CartItemModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id" : @"cartItemId"
                                                       }];
}

+(BOOL)propertyIsOptional:(NSString *)propertyName {
    if([propertyName isEqualToString:@"shippingCost"]) {
        return YES;
    } else {
        return NO;
    }
}

@end
