//
//  CartShopModel.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 13/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "CartShopModel.h"

@implementation CartShopModel

+(BOOL)propertyIsIgnored:(NSString *)propertyName {
    if([propertyName isEqualToString:@"totalPrice"]) {
        return YES;
    } else {
        return NO;
    }
}

+(BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

@end
