//
//  PagesModel.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 11/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "PageModel.h"

@implementation PageModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id" : @"pageId",
                                                       @"description" : @"pageDescription"
                                                       }];
}

+(BOOL)propertyIsOptional:(NSString *)propertyName {
    if([propertyName isEqualToString:@"isFollowed"]) {
        return YES;
    } else if([propertyName isEqualToString:@"posts"]){
        return YES;
    } else if([propertyName isEqualToString:@"followers"]) {
        return YES;
    } else {
        return NO;
    }
}

@end
