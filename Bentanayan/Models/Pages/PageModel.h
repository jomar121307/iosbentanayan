//
//  PagesModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 11/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class UserModel;
@class PostModel;
@class MetadataModel;

@protocol PostModel
@end

@protocol UserModel
@end

@interface PageModel : JSONModel

@property (strong, nonatomic) UserModel<Optional> *userId;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *pageDescription;
@property (assign, nonatomic) int postCount;
@property (assign, nonatomic) int followerCount;
@property (strong, nonatomic) NSString *createdAt;
@property (strong, nonatomic) NSString *pageId;
@property (assign, nonatomic) BOOL isFollowed;
@property (strong, nonatomic) NSArray<PostModel> *posts;
@property (strong, nonatomic) MetadataModel<Optional> *metadata;
@property (strong, nonatomic) NSArray<UserModel> *followers;
@property (strong, nonatomic) NSString *nameUrl;

@end
