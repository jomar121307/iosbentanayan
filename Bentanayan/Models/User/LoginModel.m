//
//  LoginModel.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 11/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "LoginModel.h"

@implementation SessionModel

@end

@implementation LoginModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"shop" : @"shopId"
                                                       }];
}

@end
