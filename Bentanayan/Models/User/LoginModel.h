//
//  LoginModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 11/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class UserModel;

@interface SessionModel : JSONModel

@property (strong, nonatomic) NSString *accessToken;
@property (strong, nonatomic) UserModel *userId;

@end

@interface LoginModel : JSONModel

@property (strong, nonatomic) SessionModel *session;
@property (strong, nonatomic) NSString *shop;

@end
