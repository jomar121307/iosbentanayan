//
//  UserModel.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 11/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class ProfilePhotoModel;

@interface UserModel : JSONModel

@property (strong, nonatomic) NSString *createdAt;
@property (strong, nonatomic) NSString<Optional> *email;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString<Optional> *middleName;
@property (strong, nonatomic) NSString<Optional> *lastName;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString<Optional> *gender;
@property (strong, nonatomic) ProfilePhotoModel<Optional> *profilePhoto;
@property (strong, nonatomic) NSArray<Optional> *paymentPlatformsAvailable;

@property (assign, nonatomic) BOOL isSelf;

@end
