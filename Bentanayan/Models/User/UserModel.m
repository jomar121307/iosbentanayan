//
//  UserModel.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 11/04/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id" : @"userId",
                                                       @"__self" : @"isSelf"
                                                       }];
}

+(BOOL)propertyIsOptional:(NSString *)propertyName {
    if([propertyName isEqualToString:@"isSelf"]) {
        return YES;
    } else {
        return NO;
    }
}

@end
