//
//  WebViewController.m
//  uBook
//
//  Created by Juston Paul Alcantara on 09/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (instancetype)initWithURL:(NSString *)url {
    self = [super init];
    if(self) {
        self.paypalURL = url;
        NSLog(@"Paypal URL: %@", self.paypalURL);
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Navigation
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor bentaGreenColor];
    self.navigationController.navigationBar.translucent = NO;
    
    self.navigationItem.title = self.paypalURL;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Exit
    UIImage *homeImage = [[UIImage imageNamed:@"close"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    CGRect frameimg = CGRectMake(0, 0, homeImage.size.width, homeImage.size.height);
    UIButton *homeButton = [[UIButton alloc] initWithFrame:frameimg];
    [homeButton setBackgroundImage:homeImage forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(exitLogin) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:homeButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    // Webview
    WKWebViewConfiguration *webConfiguration = [[WKWebViewConfiguration alloc] init];
    _webView = [[WKWebView alloc] initWithFrame:self.webContainerView.bounds
                                  configuration:webConfiguration];
    _webView.navigationDelegate = self;
    _webView.translatesAutoresizingMaskIntoConstraints = NO;
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.paypalURL]]];
    
    [self.webContainerView addSubview:_webView];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_webView);
    
    [self.webContainerView addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_webView]|"
                                             options:0
                                             metrics:nil
                                               views:views]];
    [self.webContainerView addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_webView]|"
                                             options:0
                                             metrics:nil
                                               views:views]];
    
    self.backButtonItem.enabled = NO;
    self.forwardBarItem.enabled = NO;
    
    self.progressView.progressViewStyle = UIProgressViewStyleDefault;
    self.progressView.progressTintColor = [UIColor flatOrangeColor];
    self.progressView.trackTintColor = [UIColor flatGrayColorDark];
    self.progressView.progress = 0;
    
    [_webView addObserver:self
               forKeyPath:@"loading"
                  options:NSKeyValueObservingOptionNew
                  context:nil];
    
    [_webView addObserver:self
               forKeyPath:@"estimatedProgress"
                  options:NSKeyValueObservingOptionOld
                  context:nil];
    
    [_webView addObserver:self
               forKeyPath:@"title"
                  options:NSKeyValueObservingOptionNew
                  context:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    
    [_webView removeObserver:self
                  forKeyPath:@"loading"];
    [_webView removeObserver:self
                  forKeyPath:@"estimatedProgress"];
    [_webView removeObserver:self
                  forKeyPath:@"title"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context
{
    if([keyPath isEqualToString:@"loading"]) {
        self.backButtonItem.enabled = _webView.canGoBack;
        self.forwardBarItem.enabled = _webView.canGoForward;
    }
    if([keyPath isEqualToString:@"estimatedProgress"]) {
        self.progressView.hidden = _webView.estimatedProgress == 1;
        [self.progressView setProgress:_webView.estimatedProgress animated:YES];
    }
    if([keyPath isEqualToString:@"title"]) {
        self.titleLabel.text = _webView.title;
        self.navigationItem.titleView = self.titleLabel;
    }
}

#pragma mark - Actions / Selectors

- (void)doBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)back:(id)sender {
    [_webView goBack];
}

- (IBAction)forward:(id)sender {
    [_webView goForward];
}

- (IBAction)reload:(id)sender {
    NSLog(@"Reload Page: %@", [_webView.URL relativePath]);
    [_webView reload];
}

- (void)checkWebNavigationButton {
    self.backButtonItem.enabled = _webView.canGoBack;
    self.forwardBarItem.enabled = _webView.canGoForward;
}

- (void)exitLogin {
    [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES
                                                                   completion:nil];
}

#pragma mark - WebView Delegates

- (void)webView:(WKWebView *)webView
didFailProvisionalNavigation:(WKNavigation *)navigation
      withError:(NSError *)error
{
    _alertController = [UIAlertController alertControllerWithTitle:@"Error"
                                                           message:error.localizedDescription
                                                    preferredStyle:UIAlertControllerStyleAlert];
    [_alertController addAction:
     [UIAlertAction actionWithTitle:@"OK"
                              style:UIAlertActionStyleDefault
                            handler:nil]];
    
    [self presentViewController:_alertController
                       animated:YES
                     completion:nil];
}

- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    NSLog(@"%@", webView.URL);
    self.navigationItem.title = [webView.URL relativeString];
    
    [self checkWebNavigationButton];
    
    NSString *urlString = [NSString stringWithFormat:@"%@", [webView.URL relativeString]];
    if([urlString isEqualToString:@"http://www.shop.zoogtech.com/#/myshop/purchases"]) {
        [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
        [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES
                                                                           completion:^
             {
                 SCLAlertView *alert = [[BESharedManager sharedInstance] showAlert];
                 alert.customViewColor = [UIColor bentaGreenColor];
                 
                 [alert alertIsDismissed:^{
                     [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
                 }];
                 
                 [alert showSuccess:self
                              title:@"Checkout Successful"
                           subTitle:@"Thank you for purchasing."
                   closeButtonTitle:@"OK"
                           duration:0.0f];
                 
             }];
    } else if([urlString containsString:@"http://www.shop.zoogtech.com"]) {
        NSLog(@"Checkout Warning");
    }
}

- (void)webView:(WKWebView *)webView
didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    NSLog(@"Web Navigation Failed: %@", error.localizedDescription);
    [self checkWebNavigationButton];
}

- (void)webView:(WKWebView *)webView
didFinishNavigation:(WKNavigation *)navigation
{
    [self checkWebNavigationButton];
    [self.progressView setProgress:0.0f animated:NO];
}


@end
