//
//  WebViewController.h
//  uBook
//
//  Created by Juston Paul Alcantara on 09/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <WebKit/WebKit.h>

@interface WebViewController : UIViewController <WKNavigationDelegate> {
    WKWebView *_webView;
    UIAlertController *_alertController;
}

@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIView *webContainerView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *forwardBarItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *reloadBarItem;

@property (nonatomic, strong) UILabel *titleLabel;

@property (strong, nonatomic) NSString *paypalURL;

- (IBAction)back:(id)sender;
- (IBAction)forward:(id)sender;
- (IBAction)reload:(id)sender;

- (instancetype)initWithURL:(NSString *)url;

@end
