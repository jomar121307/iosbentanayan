//
//  ProfileImageSelectorCell.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 17/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <XLForm/XLForm.h>

@interface ProfileImageSelectorCell : XLFormBaseCell

@property (nonatomic, readonly) UIImageView *imageView;

@end
