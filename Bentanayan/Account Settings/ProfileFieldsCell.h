//
//  ProfileFieldsCell.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 17/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <XLForm/XLForm.h>

@interface ProfileFieldsCell : XLFormBaseCell <XLFormReturnKeyProtocol>

@property (nonatomic, strong) NSString *rowType;

@property (nonatomic, readonly) UITextField * textField;

@property (nonatomic) NSNumber *textFieldLengthPercentage;

@end
