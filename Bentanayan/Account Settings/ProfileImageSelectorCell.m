//
//  ProfileImageSelectorCell.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 17/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "ProfileImageSelectorCell.h"

NSString *const kFormImageSelectorCellDefaultImage = @"defaultImage";
NSString *const kFormImageSelectorCellImageRequest = @"imageRequest";

@interface ProfileImageSelectorCell() <UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic) UIImage * defaultImage;
@property (nonatomic) NSURLRequest * imageRequest;

@end

@implementation ProfileImageSelectorCell

@synthesize imageView = _imageView;

#pragma mark - XLFormDescriptorCell

- (void)configure
{
    [super configure];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.defaultImage = [UIImage imageNamed:@"avatar-placeholder"];
    self.imageView.image = [UIImage imageNamed:@"avatar-placeholder"];
    self.backgroundColor = [UIColor lightGrayColor];
    self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.separatorInset = UIEdgeInsetsZero;
    [self.contentView addSubview:self.imageView];
    [self addLayoutConstraints];
}

- (void)update
{
    if([self.rowDescriptor.value isKindOfClass:[NSString class]]) {
        __weak __typeof(self) weakSelf = self;
        NSString *urlString = (NSString *)self.rowDescriptor.value;
        NSLog(@"%@", urlString);
        self.imageView.alpha = 0.0f;
        [self.imageView pin_setImageFromURL:[NSURL URLWithString:urlString]
                           placeholderImage:self.defaultImage
                                 completion:^(PINRemoteImageManagerResult * _Nonnull result)
         {
             if (result.requestDuration > 0.25) {
                 [UIView animateWithDuration:0.3 animations:^{
                     weakSelf.imageView.alpha = 1.0f;
                 }];
             } else {
                 weakSelf.imageView.alpha = 1.0f;
             }
             [self setBackgroundColor:[UIColor whiteColor]];
         }];
    }
}

+ (CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor
{
    return 180.0f;
}

- (void)formDescriptorCellDidSelectedWithFormController:(XLFormViewController *)controller
{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:self.rowDescriptor.selectorTitle
                                                                              message:nil
                                                                       preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                                        style:UIAlertActionStyleCancel
                                                      handler:nil]];
    __weak __typeof(self)weakSelf = self;
    [alertController addAction:[UIAlertAction actionWithTitle:@"Choose Existing Photo"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          UIImagePickerController * imagePickerController = [[UIImagePickerController alloc] init];
                                                          imagePickerController.delegate = weakSelf;
                                                          imagePickerController.allowsEditing = YES;
                                                          imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                          imagePickerController.mediaTypes = @[(NSString *)kUTTypeImage];
                                                          [weakSelf.formViewController presentViewController:imagePickerController animated:YES completion:nil];
                                                      }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Take a Picture"                                                            style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          UIImagePickerController * imagePickerController = [[UIImagePickerController alloc] init];
                                                          imagePickerController.delegate = weakSelf;
                                                          imagePickerController.allowsEditing = YES;
                                                          imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                          imagePickerController.mediaTypes = @[(NSString *)kUTTypeImage];
                                                          [weakSelf.formViewController presentViewController:imagePickerController animated:YES completion:nil];
                                                      }]];
    
    [self.formViewController presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - LayoutConstraints

-(void)addLayoutConstraints
{
    NSDictionary *uiComponents = @{ @"image" : self.imageView };
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView
                                                                 attribute:NSLayoutAttributeTop
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeTop
                                                                multiplier:1.0f
                                                                  constant:0.0f]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView
                                                                 attribute:NSLayoutAttributeBottom
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeBottom
                                                                multiplier:1.0f
                                                                  constant:0.0f]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[image]|" options:0 metrics:nil views:uiComponents]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView
                                                                 attribute:NSLayoutAttributeCenterX
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterX
                                                                multiplier:1.0f
                                                                  constant:0.0f]];
}

- (void)setImageValue:(UIImage *)image
{
    self.rowDescriptor.value = image;
    self.imageView.image = image;
}

- (void)updateConstraints
{
    [super updateConstraints];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    UIImage *originalImage, *editedImage, *imageToUse;
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0) == kCFCompareEqualTo) {
        // ensure the user has taken a picture
        editedImage = (UIImage *) [info objectForKey:UIImagePickerControllerEditedImage];
        originalImage = (UIImage *) [info objectForKey:UIImagePickerControllerOriginalImage];
        if (editedImage) {
            imageToUse = editedImage;
        }
        else {
            imageToUse = originalImage;
        }
        [self setImageValue:imageToUse];
    }
    [self.formViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Properties

- (UIImageView *)imageView
{
    if (_imageView) return _imageView;
    _imageView = [UIImageView autolayoutView];
    _imageView.layer.masksToBounds = YES;
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    return _imageView;
}

- (void)setDefaultImage:(UIImage *)defaultImage
{
    _defaultImage = defaultImage;
}


- (void)setImageRequest:(NSURLRequest *)imageRequest
{
    _imageRequest = imageRequest;
}

@end
