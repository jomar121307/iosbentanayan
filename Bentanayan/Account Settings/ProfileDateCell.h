//
//  ProfileDateCell.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 17/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <XLForm/XLForm.h>

@interface ProfileDateCell : XLFormBaseCell

@property (nonatomic, strong) NSString *rowType;

@property (nonatomic) XLFormDateDatePickerMode formDatePickerMode;
@property (nonatomic) NSDate *minimumDate;
@property (nonatomic) NSDate *maximumDate;
@property (nonatomic) NSInteger minuteInterval;

@end
