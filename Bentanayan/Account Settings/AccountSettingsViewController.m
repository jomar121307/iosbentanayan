//
//  AccountSettingsViewController.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 17/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "AccountSettingsViewController.h"

#import "ProfileImageSelectorCell.h"
#import "ProfileFieldsCell.h"
#import "ProfileDateCell.h"

@interface AccountSettingsViewController ()

@end

NSString *const kFirstName = @"firstName";
NSString *const kMiddleName = @"middleName";
NSString *const kLastName = @"lastName";
NSString *const kContact = @"contact";
NSString *const kEmail = @"email";
NSString *const kAddress = @"address";
NSString *const kBirthday = @"birthday";
NSString *const kAvatar = @"profilePhoto";

@implementation AccountSettingsViewController

- (instancetype)init {
    self = [super init];
    if(self) {
        [self initializeForm];
    }
    return self;
}

- (void)initializeForm {
    self.user = [[BESharedManager sharedInstance] login].session.userId;
    
    XLFormDescriptor *formDescriptor = [XLFormDescriptor formDescriptorWithTitle:@"Account Settings"];
    XLFormSectionDescriptor *section;
    XLFormRowDescriptor *row;
    
    formDescriptor.assignFirstResponderOnShow = YES;
    
    // Basic Information - Section
    section = [XLFormSectionDescriptor formSectionWithTitle:@"Profile Settings"];
    
    // Profile Image
    [[XLFormViewController cellClassesForRowDescriptorTypes] setObject:[ProfileImageSelectorCell class] forKey:@"profileImageCell"];
    row = [XLFormRowDescriptor formRowDescriptorWithTag:kAvatar rowType:@"profileImageCell"];
    if(self.user.profilePhoto) {
        ProfilePhotoModel *photo = self.user.profilePhoto;
        if(photo.cropped) {
            row.value = photo.cropped.secure_url;
        } else if(photo.original) {
            row.value = photo.original.secure_url;
        } else {
            row.value = photo.secure_url;
        }
    }
    //row.value = self.user.avatar;
    [section addFormRow:row];
    
    [formDescriptor addFormSection:section];
    
    section = [XLFormSectionDescriptor formSection];
    
    // First Name
    [[XLFormViewController cellClassesForRowDescriptorTypes] setObject:[ProfileFieldsCell class] forKey:@"profileFieldCell"];
    row = [XLFormRowDescriptor formRowDescriptorWithTag:kFirstName rowType:@"profileFieldCell"];
    [row.cellConfigAtConfigure setObject:XLFormRowDescriptorTypeName forKey:@"rowType"];
    [row.cellConfigAtConfigure setObject:@"First Name" forKey:@"textField.placeholder"];
    [row.cellConfigAtConfigure setObject:[UIImage imageNamed:@"user"] forKey:@"imageView.image"];
    row.required = YES;
    row.value = self.user.firstName;
    [section addFormRow:row];
    
    //    // Middle Name
    //    [[XLFormViewController cellClassesForRowDescriptorTypes] setObject:[ProfileFieldsCell class] forKey:@"profileFieldCell"];
    //    row = [XLFormRowDescriptor formRowDescriptorWithTag:kMiddleName rowType:@"profileFieldCell"];
    //    [row.cellConfigAtConfigure setObject:XLFormRowDescriptorTypeName forKey:@"rowType"];
    //    [row.cellConfigAtConfigure setObject:@"Middle Name" forKey:@"textField.placeholder"];
    //    [row.cellConfigAtConfigure setObject:[UIImage new] forKey:@"imageView.image"];
    //    row.value = self.user.middleName;
    //    [section addFormRow:row];
    
    // Last Name
    [[XLFormViewController cellClassesForRowDescriptorTypes] setObject:[ProfileFieldsCell class] forKey:@"profileFieldCell"];
    row = [XLFormRowDescriptor formRowDescriptorWithTag:kLastName rowType:@"profileFieldCell"];
    [row.cellConfigAtConfigure setObject:XLFormRowDescriptorTypeName forKey:@"rowType"];
    [row.cellConfigAtConfigure setObject:@"Last Name" forKey:@"textField.placeholder"];
    [row.cellConfigAtConfigure setObject:[UIImage new] forKey:@"imageView.image"];
    row.value = self.user.lastName;
    row.required = YES;
    [section addFormRow:row];
    
    // Email
    [[XLFormViewController cellClassesForRowDescriptorTypes] setObject:[ProfileFieldsCell class] forKey:@"profileFieldCell"];
    row = [XLFormRowDescriptor formRowDescriptorWithTag:kEmail rowType:@"profileFieldCell"];
    [row.cellConfigAtConfigure setObject:XLFormRowDescriptorTypeEmail forKey:@"rowType"];
    [row.cellConfigAtConfigure setObject:@"Email" forKey:@"textField.placeholder"];
    [row.cellConfigAtConfigure setObject:[UIImage imageNamed:@"email"] forKey:@"imageView.image"];
    [row addValidator:[XLFormValidator emailValidator]];
    row.required = YES;
    row.value = self.user.email;
    [section addFormRow:row];
    
    // Address
    [[XLFormViewController cellClassesForRowDescriptorTypes] setObject:[ProfileFieldsCell class] forKey:@"profileFieldCell"];
    row = [XLFormRowDescriptor formRowDescriptorWithTag:kAddress rowType:@"profileFieldCell"];
    [row.cellConfigAtConfigure setObject:XLFormRowDescriptorTypeEmail forKey:@"rowType"];
    [row.cellConfigAtConfigure setObject:@"Coming Soon" forKey:@"textField.placeholder"];
    [row.cellConfigAtConfigure setObject:[UIImage imageNamed:@"marker"] forKey:@"imageView.image"];
    [section addFormRow:row];
    
    // Contact
    [[XLFormViewController cellClassesForRowDescriptorTypes] setObject:[ProfileFieldsCell class] forKey:@"profileFieldCell"];
    row = [XLFormRowDescriptor formRowDescriptorWithTag:kContact rowType:@"profileFieldCell"];
    [row.cellConfigAtConfigure setObject:XLFormRowDescriptorTypePhone forKey:@"rowType"];
    [row.cellConfigAtConfigure setObject:@"Coming Soon" forKey:@"textField.placeholder"];
    [row.cellConfigAtConfigure setObject:[UIImage imageNamed:@"contact"] forKey:@"imageView.image"];
    [section addFormRow:row];
    
    //    // Birthday
    //    [[XLFormViewController cellClassesForRowDescriptorTypes] setObject:[ProfileDateCell class] forKey:@"profileDateCell"];
    //    row = [XLFormRowDescriptor formRowDescriptorWithTag:kBirthday rowType:@"profileDateCell"];
    //    [row.cellConfigAtConfigure setObject:XLFormRowDescriptorTypeDate forKey:@"rowType"];
    //    [row.cellConfigAtConfigure setObject:[UIImage imageNamed:@"birthday"] forKey:@"imageView.image"];
    //    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    //    [dateFormatter setDateFormat:BENTA_DATE_BIRTHDAY_FORMAT];
    //    NSLog(@"Birthday: %@ - %@", self.user.birthday, [dateFormatter dateFromString:self.user.birthday]);
    //    row.value = [dateFormatter dateFromString:self.user.birthday];
    //    [section addFormRow:row];
    
    [formDescriptor addFormSection:section];
    
    self.form = formDescriptor;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    // Back
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(goBack)];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    // Save
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                                           target:self
                                                                                           action:@selector(updateProfile:)];
    
    self.form.assignFirstResponderOnShow = NO;
}

#pragma mark - Method / Selectors

- (void)updateProfile:(UIBarButtonItem *)button {
    NSMutableDictionary *uploadParams = [[NSMutableDictionary alloc] initWithDictionary:self.formValues];
    if([self.formValues[kAvatar] isKindOfClass:[UIImage class]]) {
        NSData *imageData = UIImageJPEGRepresentation(self.formValues[kAvatar], 1.0);
        [self uploadPhotoToCloudinary:imageData type:BENTA_CLOUDINARY_PROFILE_TYPE uploadParams:uploadParams];
    } else {
        [uploadParams removeObjectsForKeys:@[kAvatar]];
        [self doUpdateProfile:uploadParams];
    }
}

- (void)doUpdateProfile:(NSMutableDictionary *)uploadParams {
    NSLog(@"Update Profile: %@", uploadParams);
    [[BESharedManager sharedInstance] be_editProfileWithParams:[uploadParams copy]
                                                withCompletion:^(BOOL finished, NSString *error)
     {
         if(finished) {
             [TSMessage showNotificationInViewController:self
                                                   title:@"Profile Updated"
                                                subtitle:nil
                                                    type:TSMessageNotificationTypeSuccess
                                                duration:2.5];
         } else {
             [TSMessage showNotificationInViewController:self
                                                   title:@"Profile Update Failed"
                                                subtitle:error
                                                    type:TSMessageNotificationTypeError
                                                duration:2.5];
         }
         dispatch_async(dispatch_get_main_queue(), ^{
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         });
     }];
}

- (void)goBack {
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

#pragma mark - Cloudinary Methods

- (void)uploadPhotoToCloudinary:(NSData *)imageData
                           type:(NSString *)imageType
                   uploadParams:(NSMutableDictionary *)uploadParams
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Uploading...";
    
    CLCloudinary *cloudinary = [[CLCloudinary alloc] initWithUrl:BENTA_CLOUDINARY_URL];
    
    CLUploader *uploader = [[CLUploader alloc] init:cloudinary delegate:nil];
    
    NSString *imageTags = [NSString stringWithFormat:@"%@-%@", self.user.userId, imageType];
    
    CLTransformation *imageTransformation = [CLTransformation transformation];
    [imageTransformation setWidthWithInt:800.0];
    [imageTransformation setCrop:@"fill"];
    
    [uploader upload:imageData
             options:@{@"tags" : imageTags,
                       @"resource_type" : @"image",
                       @"format" : @"png",
                       @"transformation" : imageTransformation
                       }
      withCompletion:^(NSDictionary *successResult, NSString *errorResult, NSInteger code, id context) {
          if(successResult) {
              hud.mode = MBProgressHUDModeIndeterminate;
              hud.labelText = @"Updating...";
              NSLog(@"Block upload success. %@", successResult);
              [uploadParams setValue:successResult
                              forKey:kAvatar];
              [self doUpdateProfile:uploadParams];
          } else {
              NSLog(@"Block upload error: %@, %lu", errorResult, code);
              dispatch_async(dispatch_get_main_queue(), ^{
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
              });
          }
      } andProgress:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite, id context) {
          NSLog(@"Block upload progress: %lu/%lu (+%lu)", totalBytesWritten, totalBytesExpectedToWrite, bytesWritten);
          CGFloat progress = (float)totalBytesWritten / (float)totalBytesExpectedToWrite;
          hud.progress = progress;
      }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
