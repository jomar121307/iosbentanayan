//
//  BentaColor.h
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (BentaColor)

+ (UIColor *)bentaGreenColor;

@end
