//
//  BentaColor.m
//  Bentanayan
//
//  Created by Juston Paul Alcantara on 15/03/2016.
//  Copyright © 2016 Juston Paul Alcantara. All rights reserved.
//

#import "BentaColor.h"

@implementation UIColor (BentaColor)

+ (UIColor *)bentaGreenColor {
    return [[UIColor colorWithHexString:@"0a4920" withAlpha:1.0] flatten];
}

@end
